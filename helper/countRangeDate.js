const countDays = (startDate, endDate) => {
  const difference =
    startDate.getTime() - endDate.getTime();
  const totalDays = Math.ceil(
    difference / (1000 * 3600 * 24)
  );

  return totalDays;
};

export { countDays };
