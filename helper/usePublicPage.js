import { parseJwt } from "./parseJwt";

const usePublicPage = () => {
  if (typeof window !== "undefined") {
    if (localStorage.getItem("token")) {
      const token = localStorage.getItem("token");
      const user = parseJwt(token);

      if (user.role == "admin") {
        window.location.href = "/admin";
      } else if (user.role == "customer") {
        window.location.href = "/package";
      } else if (user.role == "vendor") {
        window.location.href = "/vendor";
      }
    }
  }
};

export default usePublicPage;
