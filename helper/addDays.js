export const addDays = (dateIn, days) => {
  const date = new Date(dateIn);

  date.setDate(date.getDate() + days);

  return date;
};
