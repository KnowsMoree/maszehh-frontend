import Head from "next/head";

const Title = (props) => {
  return (
    <Head>
      <title>{props.title}</title>
    </Head>
  );
};

const Header = ({ children, title }) => {
  if (typeof window === "undefined") {
    return <div>{children}</div>;
  }

  return (
    <div>
      <Title title={title} />
      {children}
    </div>
  );
};

export default Header;
