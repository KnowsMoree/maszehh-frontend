import {
  ArrowLeftOutlined,
  EditOutlined,
  LeftOutlined,
  LoadingOutlined,
  MinusOutlined,
  PlusOutlined,
  StarFilled,
} from "@ant-design/icons";
import { Rate, Spin } from "antd";
import { useEffect, useLayoutEffect, useState } from "react";
import { Pagination } from "antd";
import { Image } from "antd";
import { useRouter } from "next/router";
import { packageRepository } from "../../repository/package";
import { mutate } from "swr";
import moment from "moment";
import "moment/locale/id";
import Link from "next/link";

const DetailPack = (props) => {
  const id = props.param;

  const [pack, setPack] = useState({});
  const [review, setReview] = useState([]);

  const [loading, setLoading] = useState(true);
  const [qty, setQty] = useState(1);
  const [minQty, setMinQty] = useState(1);
  const [priceTotal, setPriceTotal] = useState();
  const [note, setNote] = useState("");
  const router = useRouter();

  const [total, setTotal] = useState(0);
  const [limit, setLimit] = useState(4);
  const [current, setCurrent] = useState(1);

  const { data: dataDetailPack } = packageRepository.hooks.usePackageDetail(id);
  const { data: dataReview } = packageRepository.hooks.usePackageReview({
    id,
    current,
    limit,
  });

  useLayoutEffect(() => {
    if (dataDetailPack !== null && dataDetailPack !== undefined) {
      setPack(dataDetailPack?.data);
      setPriceTotal(
        dataDetailPack.data.price * dataDetailPack?.data.minHandymanQty
      );
      setLoading(false);
      setMinQty(dataDetailPack?.data.minHandymanQty);
      setQty(dataDetailPack?.data.minHandymanQty);
    }

    if (dataReview !== null && dataReview !== undefined) {
      setReview(dataReview?.items);
      setTotal(dataReview?.meta?.totalItems);
    }
  }, [dataDetailPack, dataReview]);

  const handleClick = () => {
    router.push(
      `/package/checkout?packId=${pack.id}&&qty=${qty}&&note=${note}`
    );
  };

  console.log(pack);

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  const formatRupiah = (money) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
    }).format(money);
  };

  const onCurrentPage = (page) => {
    setCurrent(page);
  };

  console.log(pack);

  return (
    <>
      <div className="py-9 px-8 flex flex-col gap-7 rounded-lg w-3/4">
        {loading ? (
          <div className="w-full h-[calc(100vh-115px)] flex items-center justify-center">
            <Spin indicator={loadingIcon} />
          </div>
        ) : (
          <>
            <div>
              <Link href={"/package"}>
                <div className="flex group cursor-pointer bg-transparent transition-all items-center flex-row justify-start gap-x-4 ">
                  <ArrowLeftOutlined className="text-text group-hover:text-yellow-500 duration-300 text-base flex items-center" />
                  <h2 className="capitalize m-0 text-text group-hover:text-yellow-500 duration-300 text-base font-semibold">
                    kembali
                  </h2>
                </div>
              </Link>
            </div>
            <div className="w-full gap-11 flex flex-row">
              <div className="w-fit flex flex-col gap-6">
                <div className="w-[350px] max-h-fit">
                  <img
                    alt={pack.photo}
                    src={pack.photo}
                    height={350}
                    width={350}
                    className="rounded-lg"
                  />
                </div>
                <div className="w-full flex gap-6 items-center flex-row">
                  <div className="flex items-center">
                    <img
                      alt=""
                      width={48}
                      height={48}
                      src={pack?.vendor?.user.photo}
                      className="rounded-full"
                    />
                  </div>
                  <div className="w-full flex flex-col">
                    <h2 className="font-bold p-0 text-sm m-0">
                      {pack.vendor.name}
                    </h2>
                    <h4 className="font-medium text-text text-sm m-0">
                      {pack.vendor.city.name}
                    </h4>
                    <div className="flex flex-row gap-1">
                      <StarFilled
                        className="flex items-center text-filled text-lg"
                        disabled
                      />
                      <p className="font-medium text-text flex items-center text-sm m-0">
                        {pack?.vendor?.rating}
                      </p>
                      <p className="font-medium text-text flex items-center text-sm m-0">
                        Rata rata ulasan
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="w-full flex gap-2 flex-col">
                <div className="w-full flex">
                  <h1 className="m-0 text-xl font-bold capitalize break-words">
                    {pack.name}
                  </h1>
                </div>
                <div className="w-full flex flex-row gap-2">
                  <Rate
                    count={1}
                    defaultValue={1}
                    className="flex items-center"
                    disabled
                  />
                  <h5 className="font-semibold flex items-center m-0 text-base">
                    {pack?.rating}
                  </h5>
                  <h5 className="font-medium flex items-center m-0 text-base text-gray-400">
                    ({pack?.totalReview} Ulasan)
                  </h5>
                </div>
                <div>
                  <h1 className="font-semibold text-xl m-0">
                    {formatRupiah(pack.price)}{" "}
                    <span className="text-text font-medium">/Orang</span>
                  </h1>
                </div>
                <div className="flex flex-col">
                  <p className="font-medium text-xs m-0">
                    Kategori: {pack.category.categoryName}
                  </p>
                  <p className="font-medium text-xs m-0">
                    Minimal jumlah tukang: {pack?.minHandymanQty}
                  </p>
                </div>
                <div className="flex flex-col gap-2">
                  <h2 className="m-0 font-semibold text-base">Deskripsi</h2>
                  <hr className="h-2 w-full" />
                  <p className="break-all whitespace-pre-line text-sm m-0 text-ellipsis">
                    {pack.desc}
                  </p>
                </div>
              </div>
              <div className="w-fit">
                <div className="px-2 py-3 w-72 sticky top-[115px] bg-white rounded-lg gap-3 shadow-md flex flex-col">
                  <h1 className="m-0 select-none font-bold text-base">
                    Atur jumlah tukang
                  </h1>
                  <div className="flex max-h-fit flex-row gap-2">
                    <div className="flex w-full bg-bg rounded-lg shadow-md justify-between flex-row p-2">
                      <button
                        onClick={() => {
                          setQty(qty - 1);
                          setPriceTotal(dataDetailPack.data.price * (qty - 1));
                        }}
                        disabled={qty === minQty ? true : false}
                      >
                        <MinusOutlined
                          className={`text-sm transition-colors cursor-pointer hover:text-main flex items-center`}
                        />
                      </button>

                      <p className="text-text select-none font-bold flex m-0 items-center text-sm">
                        {qty}
                      </p>
                      <button
                        onClick={() => {
                          setQty(qty + 1);
                          setPriceTotal(dataDetailPack.data.price * (qty + 1));
                        }}
                        disabled={
                          qty === pack.vendor.handymanAvailable ? true : false
                        }
                      >
                        <PlusOutlined className="text-sm transition-colors flex items-center cursor-pointer hover:text-main" />
                      </button>
                    </div>
                    <div className="w-full flex items-center">
                      <h3 className="m-0 select-none font-normal text-sm text-text">
                        tukang tersedia{" "}
                        <span className="font-bold text-black">
                          {pack.vendor.handymanAvailable}
                        </span>
                      </h3>
                    </div>
                  </div>
                  <h1 className="font-normal select-none capitalize text-xs m-0">
                    Min. {minQty} orang
                  </h1>
                  <div className="w-full">
                    <textarea
                      name=""
                      id=""
                      onChange={(e) => setNote(e.target.value)}
                      placeholder="catatan"
                      className="w-full h-20 placeholder:select-none active:outline-none active:ring-main focus:outline-none focus:ring-1 focus:ring-main active:ring-1 resize-none bg-bg rounded-lg shadow-md p-2"
                      rows="10"
                    />
                  </div>
                  <div className="flex flex-col select-none gap-1">
                    <h1 className="text-text m-0 capitalize font-normal text-base">
                      Total
                    </h1>
                    <h1 className="m-0 capitalize font-semibold text-lg">
                      {formatRupiah(priceTotal)}
                    </h1>
                  </div>
                  <div
                    onClick={handleClick}
                    className="w-full cursor-pointer hover:bg-secondary transition-colors bg-main py-3 rounded-lg shadow-md flex justify-center items-center"
                  >
                    <p className="m-0 text-white text-sm font-semibold">
                      Pesan
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex flex-col gap-4">
              <h1 className="m-0 font-bold text-lg">
                Ulasan{" "}
                <span className="font-light">({pack?.totalReview} Ulasan)</span>
              </h1>
              <div className="flex flex-row gap-10">
                {review?.map((v) => {
                  return (
                    <>
                      <div
                        key={v.id}
                        className="py-5 px-6 rounded-lg bg-white shadow-md flex flex-col w-1/4"
                      >
                        <div className="w-full flex flex-row gap-5">
                          <div className="w-[45px] h-[45px]">
                            <img
                              alt="profile"
                              src={v?.user?.photo}
                              width={45}
                              height={45}
                              className="rounded-full"
                            />
                          </div>
                          <div className="flex flex-col justify-center">
                            <h2 className="m-0 text-xs font-semibold">
                              {v?.user?.fullname}
                            </h2>
                            <h2 className="m-0 text-xs font-light">
                              {moment(v.createdAt).format("YYYY-MM-DD")}
                            </h2>
                          </div>
                        </div>
                        <div className="flex flex-col gap-2">
                          <Rate count={5} value={v.rate} disabled />
                          <p className="m-0 text-sm break-words text-ellipsis">
                            {v.comment}
                          </p>
                          <div className="flex flex-row gap-2">
                            {v.photoReview.map((photo) => {
                              return (
                                <>
                                  <img
                                    alt={photo?.id}
                                    src={photo?.photo}
                                    className="rounded-lg w-[100px] h-[100px] border-none"
                                    width={100}
                                    height={100}
                                  />
                                </>
                              );
                            })}
                          </div>
                        </div>
                      </div>
                    </>
                  );
                })}
              </div>
              <div className="w-full flex justify-center">
                <Pagination
                  current={current}
                  onChange={onCurrentPage}
                  total={total}
                  pageSize={limit}
                />
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default DetailPack;
