import Image from "next/image";
import { useEffect, useState } from "react";
import Img from "/public/hero/package/heropack.png";
import { Empty, Form, Pagination, Radio, Row, Space } from "antd";
import PackageCard from "./PackageCard";
import { packageRepository } from "../../repository/package";
import { categoryRepository } from "../../repository/category";
import { regionRepository } from "../../repository/region";
import {
  ClearOutlined,
  HourglassFilled,
  HourglassTwoTone,
} from "@ant-design/icons";
import Router from "next/router";
import Typewriter from "typewriter-effect";
import { useForm } from "antd/lib/form/Form";

const PackHero = () => {
  const [search, setSearch] = useState("");
  const [preSearch, setPreSearch] = useState("");

  const [totalPackage, setTotalPackage] = useState(0);
  const [limit, setLimit] = useState(12);
  const [current, setCurrent] = useState(1);
  const [selectProvince, setSelectProvince] = useState("");
  const [selectCategory, setSelectCategory] = useState("");

  const handleSearch = (e) => {
    e.preventDefault();
    setSelectCategory("");
    setSelectProvince("");

    setTimeout(() => {
      setSearch(preSearch);
      setLoading(true);
    }, 1000);
  };

  function handleProvince(selectValue) {
    setTimeout(() => {
      setSelectProvince(selectValue.target.value);
    }, 100);
  }

  function handleCategory(selectValue) {
    setTimeout(() => {
      setSelectCategory(selectValue.target.value);
    }, 100);
  }

  const onCurrentPage = (page) => {
    console.log(page);
    setCurrent(page);
  };

  //MARI CONNECT API
  const [dataPackages, setDatapackages] = useState([]);
  const [provinces, setProvinces] = useState([]);
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(true);

  const [form] = useForm();

  const { data: pack } = packageRepository.hooks.usePackage({
    current,
    limit,
    selectCategory,
    selectProvince,
    search,
  });

  const { data: dataCategory } = categoryRepository.hooks.useCategory();
  const { data: dataProvince } = regionRepository.hooks.useProvince();

  useEffect(() => {
    if (
      (pack && dataCategory && dataProvince) !== null &&
      (pack && dataCategory && dataProvince) !== undefined
    ) {
      setTotalPackage(pack.meta.totalItems);
      setDatapackages(pack.items);
      setCategories(dataCategory.data);
      setProvinces(dataProvince.data);
      setLoading(false);
    }

    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }, [pack, dataCategory, dataProvince]);

  const handleClickClear = (e) => {
    e.preventDefault();
    setSelectCategory("");
    setSelectProvince("");

    form.setFieldsValue({
      province: "",
      category: "",
    });
  };

  console.table(pack);

  return (
    <>
      <div className="flex flex-col">
        <div className="font-bold mt-7 h-[428px] w-full flex-col flex justify-center items-center text-white text-5xl">
          <h1 className="text-white m-0 text-center break-all">
            Bersama{" "}
            <span>
              <Typewriter
                options={{
                  autoStart: true,
                  loop: true,
                }}
                onInit={(typewriter) => {
                  typewriter
                    .typeString(`<i>Tukang Bangunan</i>`)
                    .pauseFor(1000)
                    .deleteAll()
                    .start();
                }}
              />
            </span>{" "}
            Kita bangun negeri
          </h1>
          <div className="w-full">
            <div className="w-full absolute z-49 mt-20 flex flex-row justify-center">
              <form onSubmit={handleSearch}>
                <div className="flex w-full flex-row bg-white rounded-lg drop-shadow-lg">
                  <label className="relative block">
                    <span className="sr-only">Search</span>
                    <input
                      className="placeholder:italic placeholder:text-slate-400 text-base text-black block font-normal bg-white w-full rounded-lg py-[10px] pl-4 pr-3 focus:outline-none sm:text-lg"
                      placeholder="Coba 'paket perbaikan'"
                      type="text"
                      name="search"
                      onChange={(e) => {
                        setPreSearch(e.target.value);
                      }}
                    />
                  </label>
                  <button
                    className="text-base w-fit py-13 px-9 rounded-r-lg bg-main hover:bg-secondary transition-colors"
                    type="submit"
                  >
                    Cari
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div className="w-full absolute -z-10  h-2/6">
          <Image src={Img} alt="bang" layout={"responsive"} />
        </div>
      </div>

      <div className="px-[84px] pt-[100px] pb-[50px] flex gap-10 flex-row">
        {loading ? (
          ""
        ) : (
          <div className="flex-col w-1/5 sticky top-[120px] mb-[68px] h-fit">
            <div className="w-full flex flex-row justify-between">
              <h2 className="text-main m-0 text-[18px] font-bold">Filter</h2>
              <ClearOutlined
                onClick={handleClickClear}
                className="text-text hover:text-secondary duration-300 flex items-center text-xl cursor-pointer select-none"
              />
            </div>
            <hr className="h-[1px] bg-text text-text my-4" />
            <h2 className="text-base text-[16px] font-semibold my-4">
              KATEGORI
            </h2>
            <Form form={form}>
              <Form.Item name="category">
                <Radio.Group name="category">
                  <Space direction="vertical">
                    {categories.map((category) => {
                      return (
                        <Radio
                          value={category.id}
                          key={category.id}
                          onChange={handleCategory}
                          className="text-text text-[14px] font-normal uppercase"
                        >
                          {category.categoryName}
                        </Radio>
                      );
                    })}
                  </Space>
                </Radio.Group>
              </Form.Item>
              {/* <Row> */}
              <hr className="h-[1px] bg-text text-text my-4" />
              <h2 className="text-base text-[16px] font-semibold my-4">
                LOKASI
              </h2>
              {/* </Row> */}
              <Form.Item name="province">
                <Radio.Group name="province">
                  <Space direction="vertical">
                    {provinces.map((province) => {
                      return (
                        <Radio
                          value={province.id}
                          key={province.id}
                          onChange={handleProvince}
                          className="text-text text-[14px] font-normal"
                        >
                          {province.name}
                        </Radio>
                      );
                    })}
                  </Space>
                </Radio.Group>
              </Form.Item>
            </Form>
          </div>
        )}

        {loading ? (
          <div className="w-full h-auto flex items-center justify-center">
            <HourglassTwoTone
              twoToneColor="#95A1AF"
              spin
              className="text-5xl flex items-center"
            />
            <h2 className="text-xl m-0 text-text font-semibold">
              mohon tunggu...
            </h2>
          </div>
        ) : dataPackages?.length == 0 ? (
          <div className="w-full h-auto flex items-center justify-center">
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
          </div>
        ) : (
          <>
            <div className="flex flex-col w-full gap-y-9">
              <div className="flex max-w-fit flex-col sm:flex-row sm:flex- gap-[35px] sm:flex-wrap flex-nowrap">
                {dataPackages?.map((pack) => {
                  return (
                    <PackageCard
                      key={pack?.id}
                      id={pack?.id}
                      bg={"bg"}
                      name={pack?.name}
                      photo={pack?.photo}
                      price={pack?.price}
                      city={pack?.vendor?.city?.name}
                      rating={pack?.rating}
                      totalReview={pack?.totalReview}
                      vendorName={pack?.vendor?.name}
                    />
                  );
                })}
              </div>
              <div className="flex flex-row justify-center">
                <Pagination
                  current={current}
                  onChange={onCurrentPage}
                  total={totalPackage}
                  pageSize={limit}
                />
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default PackHero;
