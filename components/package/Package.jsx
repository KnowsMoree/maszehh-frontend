import React from "react";
import Image from "next/image";
import { StarFilled } from "@ant-design/icons";
import Link from "next/link";

const formatRupiah = (money) => {
  return new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    minimumFractionDigits: 0,
  }).format(money);
};

const Package = (props) => {
  return (
    <Link href={`package/${props.id}`}>
      <div
        className="bg-white drop-shadow-md flex flex-col  rounded-xl w-[325px] h-[363px]"
        key={props.id}
      >
        <div className="w-full rounded-t-xl">
          <Image src={"/pack.png"} alt={props.photo} width={325} height={175} />
        </div>
        <div className="flex flex-col ml-4 mt-4">
          <h2 className="text-base font-normal">{props.name}</h2>
          <h1 className="text-main font-bold text-lg">
            {formatRupiah(props.price)}
            <span className="text-text">/Orang</span>
          </h1>
          <p className="text-text text-sm">{props.city}</p>
          <div className="flex flex-row">
            <StarFilled className="text-filled text-[18px] mr-2 flex items-center" />
            <h3 className="text-base text-black after:content-['|'] after:ml-1 after:mr-1 after:text-black m-0">
              {4.5}
            </h3>
            <h3 className="text-base text-black m-0">{`${12} ulasan`}</h3>
          </div>
        </div>
      </div>
    </Link>
  );
};

export default Package;
