import { useEffect, useState } from "react";
import { formatRupiah } from "../../helper/currencyFormat";
import { paymentUserRepository } from "../../repository/payment-user";
import moment, { now } from "moment";
import Link from "next/link";
import "moment/locale/id";

import { LeftOutlined } from "@ant-design/icons";

const ReceiptPackageContent = (props) => {
  const orderId = props.id;
  const [dataPayment, setDataPayment] = useState();

  const { data: orderedPayment } =
    paymentUserRepository.hooks.usePaymentUserOrdered(orderId);

  useEffect(() => {
    if (orderedPayment !== null && orderedPayment !== undefined) {
      setDataPayment(orderedPayment?.data);
      console.log(orderedPayment?.data);
    }
  }, [orderedPayment]);

  const nowDate = new Date(Date.now());

  const countDays = (startDate, endDate) => {
    const start = moment(startDate, "YYYY-MM-DD");
    const end = moment(endDate, "YYYY-MM-DD");
    const date = moment.duration(end.diff(start)).asDays();
    return date + 1;
  };

  console.log(dataPayment, "data");
  console.log(nowDate.toISOString());

  return (
    <>
      <div className="w-full h-full flex items-center justify-center">
        <div className="w-auto drop-shadow-lg flex divide-y-2 flex-col bg-white rounded-2xl">
          <div className="flex flex-col justify-center gap-y-3 w-full px-14 py-7">
            <div className="flex justify-center">
              <h1 className="text-2xl font-bold m-0">
                Selesaikan pembayaran minimal dalam
              </h1>
            </div>
            <div className="flex justify-center">
              <h5 className="text-base text-text font-medium m-0">
                Batas akhir pembayaran minimal
              </h5>
            </div>
            <div className="flex justify-center">
              <h1 className="text-xl font-bold m-0">
                {moment(dataPayment?.dueDate).format("YYYY-MM-DD HH:mm")}
              </h1>
            </div>
          </div>
          <div className="px-14 pb-9">
            <div className="w-full divide-y-2 divide-black divide-dashed flex flex-col">
              <div className="py-5 flex items-center">
                <h3 className="uppercase font-semibold text-lg m-0">
                  {dataPayment?.bank?.bankName}
                </h3>
              </div>
              <div className="py-4 flex flex-col gap-y-4">
                <div>
                  <h2 className="font-semibold m-0 text-text text-sm">
                    Nomor akun rekening
                  </h2>
                  <h1 className="font-semibold m-0 text-base">
                    {dataPayment?.bank?.accountNumber}
                  </h1>
                </div>
                <div>
                  <h2 className="font-semibold text-text m-0 text-sm">
                    total pembayaran minimal
                  </h2>
                  <h1 className="font-bold m-0 text-base">
                    {formatRupiah(dataPayment?.paymentAmount)}
                  </h1>
                </div>
              </div>
            </div>
            <div className="w-full flex justify-end">
              <Link href={`/account/user/payment/${dataPayment?.user?.id}`}>
                <button className="px-4 py-2 flex justify-between bg-main hover:bg-secondary duration-300 rounded-lg">
                  <h2 className="text-white m-0 text-base font-semibold">
                    Bayar
                  </h2>
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ReceiptPackageContent;
