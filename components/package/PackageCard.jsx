import React from "react";
import { ShopFilled, StarFilled } from "@ant-design/icons";
import Link from "next/link";

const formatRupiah = (money) => {
  return new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    minimumFractionDigits: 0,
  }).format(money);
};

const PackageCard = (props) => {
  return (
    <Link href={`package/${props.id}`}>
      <div
        className={`${
          props?.bg == "white" ? "bg-white" : props?.bg == "bg" ? "bg-bg" : ""
        } transition-all shadow-md hover:drop-shadow-lg flex flex-col  rounded-xl w-[325px] h-auto cursor-pointer`}
        key={props.key}
      >
        <div className="w-full rounded-t-xl">
          <img
            src={props.photo}
            alt={props.photo}
            className="rounded-t-lg"
            width={325}
          />
        </div>
        <div className="flex flex-col mx-4 my-4">
          <h2 className="text-base font-normal">{props.name}</h2>
          <h1 className="text-main font-bold text-lg">
            {formatRupiah(props.price)}
            <span className="text-text">/Orang</span>
          </h1>
          <div className="flex flex-row mb-2 gap-x-2">
            <ShopFilled className="text-sm text-main m-0 flex items-center" />
            <h2 className="text-text text-sm m-0">{props.vendorName}</h2>
          </div>
          <p className="text-text text-sm">{props.city}</p>
          <div className="flex flex-row">
            <StarFilled className="text-filled text-[18px] mr-2 flex items-center" />
            <h3 className="text-base text-black after:content-['|'] after:ml-1 after:mr-1 after:text-black m-0">
              {props.rating}
            </h3>
            <h3 className="text-base text-black m-0">{`${props.totalReview} Ulasan`}</h3>
          </div>
        </div>
      </div>
    </Link>
  );
};

export default PackageCard;
