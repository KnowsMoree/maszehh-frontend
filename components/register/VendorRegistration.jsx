import {
  ArrowLeftOutlined,
  CheckOutlined,
  CloseOutlined,
  LeftCircleOutlined,
  LeftOutlined,
  LoadingOutlined,
  PlusOutlined,
  RightOutlined,
} from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Input,
  InputNumber,
  message,
  Row,
  Select,
  Upload,
} from "antd";
import { useForm } from "antd/lib/form/Form";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { appConfig } from "../../config/app";
import { authRepository } from "../../repository/auth";
import { regionRepository } from "../../repository/region";
import { userRepository } from "../../repository/user";
import { vendorRepository } from "../../repository/vendor";

const VendorRegistration = () => {
  const [province, setProvince] = useState();
  const [provinceId, setProvinceId] = useState();
  const [city, setCity] = useState();
  const [form] = useForm();
  const router = useRouter();
  const { Option } = Select;

  const [fileListLogo, setFileListLogo] =
    useState([
      {
        url:
          appConfig.apiUrl +
          "/users/profile/default.png",
        name: "profile",
      },
    ]);
  const [fileListSiujk, setFileListSiujk] =
    useState([]);
  const [fileListKtp, setFileListKtp] = useState(
    []
  );

  const { data: dataProvince } =
    regionRepository.hooks.useProvince();

  const { data: dataCity } =
    regionRepository.hooks.useCity(provinceId);

  useEffect(() => {
    if (
      dataProvince !== null &&
      dataProvince !== undefined
    ) {
      setProvince(dataProvince.data);
      setCity(dataCity);
    }
  }, [dataProvince, dataCity]);

  const uploadButton = (
    <div>
      <PlusOutlined />

      <div
        style={{
          marginTop: 8,
        }}>
        Upload
      </div>
    </div>
  );

  const handleUploadLogo = async (args) => {
    const file = args.file;
    try {
      const processUpload =
        await userRepository.manipulateData.uploadUserPhoto(
          file
        );
      setFileListLogo([
        {
          url:
            appConfig.apiUrl +
            "/users/profile/" +
            processUpload.body.profile,
          name: "profile",
        },
      ]);

      message.success("upload logo berhasil");
    } catch (e) {
      console.log(e);
      message.error("upload gagal");
    }
  };

  const handleUploadSiujk = async (args) => {
    const file = args.file;
    try {
      const processUpload =
        await vendorRepository.manipulateData.uploadSiujk(
          file
        );
      setFileListSiujk([
        {
          url:
            appConfig.apiUrl +
            "/vendor/siujk/" +
            processUpload.body.siujk,
          name: "siujk",
        },
      ]);

      message.success("upload siujk berhasil");
    } catch (e) {
      console.log(e);
      message.error("upload siujk gagal");
    }
  };

  const handleUploadKtp = async (args) => {
    const file = args.file;
    try {
      const processUpload =
        await vendorRepository.manipulateData.uploadKtp(
          file
        );
      setFileListKtp([
        {
          url:
            appConfig.apiUrl +
            "/vendor/ktp/" +
            processUpload.body.ktp,
          name: "ktp",
        },
      ]);

      message.success("upload ktp berhasil");
    } catch (e) {
      console.log(e);
      message.error("upload ktp gagal");
    }
  };

  const handleFinish = async (values) => {
    const data = {
      ...form.getFieldsValue(),
      profile: fileListLogo[0].url,
      siujk: fileListSiujk[0].url,
      ktp: fileListKtp[0].url,
    };

    try {
      await authRepository.manipulateData.createVendor(
        data
      );
      message.success("Berhasil Register");

      setTimeout(() => {
        router.push("/login");
      }, 1000);
    } catch (error) {
      console.log(error.response);
      message.error("Gagal Register");
    }
  };

  return (
    <>
      <div className="w-full">
        <Form
          onFinish={handleFinish}
          layout="vertical"
          className="w-full"
          form={form}>
          <Row
            gutter={24}
            className="m-0"
            style={{
              margin: "0px",
              display: "flex",
              padding: "0px",
              justifyContent: "space-between",
              flexDirection: "row",
            }}>
            <Form.Item hidden>
              <Input
                type="text"
                value={
                  "44e17545-9379-4d7a-84dc-ac1105a290fa"
                }
              />
            </Form.Item>
            <Col
              span={8}
              style={{
                margin: "0px",
                display: "flex",
                padding: "0px",
                flexDirection: "column",
                gap: "41px 0px",
                backgroundColor: "transparent",
              }}>
              <Row>
                <Col span={24}>
                  <div className="w-full rounded-lg flex h-full justify-center self-center items-center bg-white">
                    <Form.Item
                      // name={"vendorPhoto"}
                      label={
                        <label className="m-0 text-center text-base font-semibold capitalize">
                          logo vendor
                        </label>
                      }
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                      }}>
                      <Upload
                        name="photo"
                        listType="picture-card"
                        className="avatar-uploader m-0 justify-center items-center flex"
                        onChange={(args) =>
                          handleUploadLogo(args)
                        }
                        fileList={fileListLogo}
                        beforeUpload={() => false}
                        onRemove={() =>
                          setFileListLogo([])
                        }
                        maxCount={1}>
                        {uploadButton}
                      </Upload>
                    </Form.Item>
                  </div>
                </Col>
              </Row>
              <Row>
                <div className="w-full rounded-lg flex h-full justify-center self-center items-center bg-white">
                  <Form.Item
                    name={"ktpPhoto"}
                    label={
                      <label className="m-0 text-center text-base font-semibold capitalize">
                        foto ktp sesuai SIUJK
                      </label>
                    }
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    rules={[
                      {
                        required: true,
                        message:
                          "foto ktp wajib dilampirkan",
                      },
                    ]}>
                    <Upload
                      name="photo"
                      listType="picture-card"
                      className="avatar-uploader m-0 justify-center items-center flex"
                      onChange={(args) =>
                        handleUploadKtp(args)
                      }
                      fileList={fileListKtp}
                      beforeUpload={() => false}
                      onRemove={() =>
                        setFileListKtp([])
                      }
                      maxCount={1}>
                      {uploadButton}
                    </Upload>
                  </Form.Item>
                </div>
              </Row>
              <Row>
                <div className="w-full rounded-lg flex h-full justify-center self-center items-center bg-white">
                  <Form.Item
                    name={"siujkPhoto"}
                    label={
                      <label className="m-0 text-center text-base font-semibold capitalize">
                        foto SIUJK
                      </label>
                    }
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    rules={[
                      {
                        required: true,
                        message:
                          "foto surat izin wajib dilampirkan",
                      },
                    ]}>
                    <Upload
                      name="photoSiujk"
                      listType="picture-card"
                      className="avatar-uploader m-0 justify-center items-center flex"
                      onChange={(args) =>
                        handleUploadSiujk(args)
                      }
                      fileList={fileListSiujk}
                      beforeUpload={() => false}
                      onRemove={() =>
                        setFileListSiujk([])
                      }
                      maxCount={1}>
                      {uploadButton}
                    </Upload>
                  </Form.Item>
                </div>
              </Row>
            </Col>
            <Col
              span={15}
              style={{
                margin: "0px",
                display: "flex",
                flexDirection: "column",
                gap: "21px 0px",
                backgroundColor: "transparent",
              }}>
              <Row gutter={24}>
                <Col
                  span={24}
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "5px 0px",
                  }}>
                  <Row gutter={24}>
                    <Col span={12}>
                      <Form.Item
                        name={"fullname"}
                        label={
                          <label className="m-0 text-base font-semibold capitalize">
                            nama pemilik
                          </label>
                        }
                        style={{
                          margin: "0px",
                        }}
                        rules={[
                          {
                            required: true,
                            message:
                              "masukan nama sesuai KTP",
                          },
                          {
                            max: 100,
                            message:
                              "nama maksimal 100 karakter ",
                          },
                        ]}>
                        <Input
                          type="text"
                          placeholder="Nama sesuai KTP"
                          className="w-full text-base hover:drop-shadow-md outline-none border-none rounded-lg bg-white py-3 px-2"
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name={"nik"}
                        label={
                          <label className="m-0 text-base font-semibold capitalize">
                            NIK
                          </label>
                        }
                        style={{
                          margin: "0px",
                        }}
                        rules={[
                          {
                            required: true,
                            message:
                              "masukan NIK sesuai KTP",
                          },
                          {
                            max: 16,
                            message:
                              "NIK maksimal 16 digit",
                          },
                        ]}>
                        <Input
                          type="text"
                          placeholder="NIK Sesuai KTP"
                          className="w-full text-base hover:drop-shadow-md outline-none border-none rounded-lg bg-white py-3 px-2"
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col span={12}>
                      <Form.Item
                        name={"vendorName"}
                        label={
                          <label className="m-0 after:content-['* NIK sesuai KTP'] after:text-base text-base font-semibold capitalize">
                            Nama vendor
                          </label>
                        }
                        style={{
                          margin: "0px",
                        }}
                        rules={[
                          {
                            required: true,
                            message:
                              "masukan nama vendor",
                          },
                        ]}>
                        <Input
                          type="text"
                          placeholder="nama vendor sesuai SIUJK"
                          className="w-full text-base hover:drop-shadow-md outline-none border-none rounded-lg bg-white py-3 px-2"
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name={"email"}
                        label={
                          <label className="m-0 text-base font-semibold capitalize">
                            Email Vendor
                          </label>
                        }
                        style={{
                          margin: "0px",
                        }}
                        rules={[
                          {
                            required: true,
                            message:
                              "masukan email",
                          },
                          {
                            type: "email",
                            message:
                              "email harus benar",
                          },
                        ]}>
                        <Input
                          type="email"
                          placeholder="email vendor"
                          className="w-full text-base hover:drop-shadow-md outline-none border-none rounded-lg bg-white py-3 px-2"
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col span={12}>
                      <Form.Item
                        name={"handymanTotal"}
                        label={
                          <label className="m-0 text-base font-semibold capitalize">
                            Jumlah tukang
                          </label>
                        }
                        style={{
                          margin: "0px",
                        }}
                        rules={[
                          {
                            type: "number",
                            min: 5,
                            message:
                              "tidak boleh kurang dari 5",
                          },
                          {
                            required: true,
                            message:
                              "masukan jumlah tukang",
                          },
                        ]}>
                        <InputNumber
                          type="text"
                          min={5}
                          placeholder="jumlah tukang"
                          className="w-full text-base hover:drop-shadow-md outline-none border-none rounded-lg bg-white py-3 px-2"
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name={"phoneNumber"}
                        label={
                          <label className="m-0 text-base font-semibold capitalize">
                            nomor telepon vendor
                          </label>
                        }
                        style={{
                          margin: "0px",
                        }}
                        rules={[
                          {
                            required: true,
                            message:
                              "masukan nomor telepon",
                          },
                          {
                            max: 13,
                            message:
                              "nomor telepon maksimal 13 digit",
                          },
                        ]}>
                        <Input
                          type="text"
                          placeholder="nomor telepon"
                          className="w-full text-base hover:drop-shadow-md outline-none border-none rounded-lg bg-white py-3 px-2"
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col span={12}>
                      <Form.Item
                        name={"username"}
                        label={
                          <label className="m-0 text-base font-semibold capitalize">
                            username vendor
                          </label>
                        }
                        style={{
                          margin: "0px",
                        }}
                        rules={[
                          {
                            required: true,
                            message:
                              "masukan username",
                          },
                          {
                            min: 10,
                            message:
                              "username memiliki panjang 8 karakter",
                          },
                          {
                            max: 32,
                            message:
                              "username memiliki maksimal 32 karakter",
                          },
                        ]}>
                        <Input
                          type="text"
                          placeholder="Username vendor"
                          className="w-full text-base hover:drop-shadow-md outline-none border-none rounded-lg bg-white py-3 px-2"
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name={"password"}
                        label={
                          <label className="m-0 text-base font-semibold capitalize">
                            password
                          </label>
                        }
                        style={{
                          margin: "0px",
                        }}
                        rules={[
                          {
                            required: true,
                            pattern: new RegExp(
                              /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/g
                            ),
                            message:
                              "password harus memiliki 1 huruf kapital dan angka",
                          },
                          {
                            min: 8,
                            message:
                              "password minimal 8 karakter",
                          },
                        ]}>
                        <Input.Password
                          type="password"
                          placeholder="masukan password"
                          className="w-full text-base hover:drop-shadow-md outline-none border-none rounded-lg bg-white py-3 px-2"
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col span={12}>
                      <Form.Item
                        name={"confpassword"}
                        dependencies={[
                          "password",
                        ]}
                        label={
                          <label className="m-0 text-base font-semibold capitalize">
                            konfirmasi password
                          </label>
                        }
                        style={{
                          margin: "0px",
                        }}
                        rules={[
                          {
                            required: true,
                            message:
                              "masukan konfirmasi password anda",
                          },
                          ({
                            getFieldValue,
                          }) => ({
                            validator(_, value) {
                              if (
                                !value ||
                                getFieldValue(
                                  "password"
                                ) === value
                              ) {
                                return Promise.resolve();
                              }

                              return Promise.reject(
                                new Error(
                                  "pasword tidak sama"
                                )
                              );
                            },
                          }),
                        ]}>
                        <Input.Password
                          type="password"
                          placeholder="masukan konfirmasi password"
                          className="w-full text-base hover:drop-shadow-md outline-none border-none rounded-lg bg-white py-3 px-2"
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col span={12}>
                      <Form.Item
                        name={"province"}
                        style={{
                          margin: "0px",
                        }}
                        label={
                          <label className="m-0 text-base font-semibold capitalize">
                            provinsi
                          </label>
                        }
                        rules={[
                          {
                            required: true,
                            message:
                              "pilih provinsi",
                          },
                        ]}>
                        <Select
                          id="provinsi"
                          placeholder="provinsi"
                          onChange={(e) =>
                            setProvinceId(e)
                          }
                          className="w-full outline-none text-base rounded-md shadow-sm bg-bg font-medium">
                          {province?.map((v) => {
                            return (
                              <>
                                <Option
                                  key={v.id}
                                  className="border-0 text-sm font-medium"
                                  value={v.id}>
                                  {v.name}
                                </Option>
                              </>
                            );
                          })}
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name={"cityId"}
                        label={
                          <label className="m-0 text-base font-semibold capitalize">
                            kota
                          </label>
                        }
                        style={{
                          margin: "0px",
                        }}
                        rules={[
                          {
                            required: true,
                            message: "pilih kota",
                          },
                        ]}>
                        <Select
                          id="kota"
                          placeholder="kota"
                          className="w-full text-base rounded-md shadow-sm bg-bg font-medium">
                          {city?.data?.map(
                            (v) => {
                              return (
                                <>
                                  <Option
                                    key={v.id}
                                    className="border-0 text-sm font-medium"
                                    value={v.id}>
                                    {v.name}
                                  </Option>
                                </>
                              );
                            }
                          )}
                        </Select>
                      </Form.Item>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col span={12}>
                  <Form.Item
                    name={"address"}
                    style={{ margin: "0" }}
                    label={
                      <label className="text-base font-semibold">
                        Alamat
                      </label>
                    }
                    rules={[
                      {
                        required: true,
                        message: "masukan alamat",
                      },
                    ]}>
                    <Input.TextArea
                      autoSize={{
                        minRows: 4,
                        maxRows: 5,
                      }}
                      type="text"
                      className="w-full text-base shadow-sm border-none rounded-lg bg-white font-semibold"
                    />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    name={"desc"}
                    style={{ margin: "0" }}
                    label={
                      <label className="text-base font-semibold capitalize">
                        deskripsi
                      </label>
                    }
                    rules={[
                      {
                        required: true,
                        message:
                          "masukan deskripsi",
                      },
                    ]}>
                    <Input.TextArea
                      autoSize={{
                        minRows: 4,
                        maxRows: 5,
                      }}
                      type="text"
                      className="w-full text-base shadow-sm border-none rounded-lg bg-white font-semibold"
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col
              span={24}
              style={{
                display: "flex",
                flexDirection: "row",
                gap: "14px",
                justifyContent: "end",
                margin: "24px 0 0 0",
              }}>
              <Link href={"/login"}>
                <Button
                  type="primary"
                  icon={<ArrowLeftOutlined />}
                  size="large"
                  className="text-sm font-semibold w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-secodary py-[11px] px-[21px] rounded-lg border-none flex items-center">
                  Kembali
                </Button>
              </Link>
              <Button
                type="primary"
                htmlType="submit"
                icon={<CheckOutlined />}
                size="large"
                className="text-sm font-semibold w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-blue-500 py-[11px] px-[21px] rounded-lg border-none flex items-center">
                Daftar
              </Button>
            </Col>
          </Row>
        </Form>
      </div>
    </>
  );
};

export default VendorRegistration;
