import React, {
  useEffect,
  useState,
} from "react";
import IsInput from "../input/Input";
import Button from "/components/input/Button";
import { Input, message } from "antd";
import Image from "next/image";
import { authRepository } from "../../repository/auth";
import { useRouter } from "next/router";
import { data } from "autoprefixer";
import { set } from "mobx";
import Link from "next/link";
import {
  EyeInvisibleOutlined,
  EyeOutlined,
} from "@ant-design/icons";

const { TextArea } = Input;

const RegForm = () => {
  const router = useRouter();
  const [name, setName] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] =
    useState("");
  const [isValid, setIsValid] = useState(true);
  const [passwordShown, setPasswordShown] =
    useState(false);
  const [
    confirmPasswordShown,
    setConfirmPasswordShown,
  ] = useState(false);

  const tooglePassword = (e) => {
    e.preventDefault();

    setPasswordShown(!passwordShown);
  };

  const toogleConfirmPassword = (e) => {
    e.preventDefault();

    setConfirmPasswordShown(
      !confirmPasswordShown
    );
  };

  //GET ROLE
  const { data: dataRoles } =
    authRepository.hooks.useRoles();

  const handleSubmit = async (e) => {
    e.preventDefault();
    //VALIDATE DATA
    const patternUsername = /^(?=.*\d).{8,32}$/g;
    if (!patternUsername.exec(username))
      return message.error(
        "username minimal 10 karakter dan memiliki angka"
      );

    const passwordPattern =
      /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,32}$/g;
    if (!passwordPattern.exec(password))
      return message.error(
        "password harus memiliki huruf, angka, dan minimal 8 karakter"
      );

    if (password !== passwordConfirm)
      return message.error(
        "password tidak sesuai"
      );

    let roleCustomer = "";
    dataRoles?.data.map((role) => {
      if (role?.roleName == "customer") {
        roleCustomer = role.id;
      }
    });

    //SEND DATA
    try {
      const data = {
        fullname: name,
        username,
        email,
        address,
        phoneNumber: phone,
        password,
        roleId: roleCustomer,
      };

      await authRepository.manipulateData.createUser(
        data
      );
      message.success("Akun Berhasil Dibuat");

      setTimeout(() => {
        router.push("/login");
      }, 1000);
    } catch (e) {
      console.log(e.response);
      message.error(e.response.body.message);
    }
  };

  const textAreaStyle =
    "outline-none border-none resize-none max-h-[100px] rounded-lg drop-shadow-lg placeholder:text-base p-[10px] text-base focus:ring-secondary";

  return (
    <div className="w-full flex flex-col justify-center gap-[50px] mt-14 mb-5">
      <div className="flex justify-center">
        <h1 className="m-0 font-bold text-2xl text-black capitalize">
          daftar maszehh
        </h1>
      </div>
      <div className="w-full">
        <form
          action=""
          method="post"
          onSubmit={handleSubmit}>
          <div className="w-full max-h-auto">
            <div className="w-full flex flex-row gap-7">
              <div className="w-full">
                <IsInput
                  name="name"
                  labelname="Nama"
                  onChange={(e) => {
                    setName(e.target.value);
                  }}
                  type="text"
                  placeholder="Masukan nama"
                  required={true}
                />
              </div>
              <div className="w-full">
                <IsInput
                  name="username"
                  labelname="Username"
                  onChange={(e) =>
                    setUsername(e.target.value)
                  }
                  maxLength={32}
                  type="text"
                  placeholder="Masukan username"
                  // pattern="^(?=.*\d).{8,32}$"
                  required={true}
                />
              </div>
            </div>
            <div className="w-full flex flex-row gap-7">
              <div className="w-full">
                <IsInput
                  name="email"
                  labelname="E-mail"
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                  type="email"
                  placeholder="Masukan email"
                  required={true}
                />
              </div>
              <div className="w-full">
                <IsInput
                  name="phone"
                  labelname="Nomor telepon"
                  type="tel"
                  placeholder="08xxxxxxxxxx"
                  pattern="[0-9]{4}[0-9]{4}[0-9]{3,4}"
                  onChange={(e) => {
                    setPhone(e.target.value);
                  }}
                  required={true}
                />
              </div>
            </div>
            <div className="w-full mb-[22px]">
              <label
                htmlFor="address"
                className="text-base font-semibold">
                Alamat
              </label>
              <TextArea
                rows={4}
                name="address"
                onChange={(e) => {
                  setAddress(e.target.value);
                }}
                className={textAreaStyle}
                required={true}
              />
            </div>
            <div className="w-full flex flex-row gap-[30px]">
              <div className="w-full">
                <IsInput
                  name="password"
                  labelname="Password"
                  type={
                    passwordShown
                      ? "text"
                      : "password"
                  }
                  maxLength={32}
                  placeholder="Masukan password"
                  onChange={(e) => {
                    setPassword(e.target.value);
                  }}
                  required={true}>
                  <button
                    onClick={tooglePassword}
                    className="absolute right-4 top-10 text-gray-400 text-sm">
                    {passwordShown ? (
                      <EyeInvisibleOutlined className="text-xl text-text flex items-center" />
                    ) : (
                      <EyeOutlined className="text-xl text-text flex items-center" />
                    )}
                  </button>
                </IsInput>
              </div>
              <div className="w-full">
                <IsInput
                  name="confirmPassword"
                  labelname="Konfirmasi Password"
                  type={
                    confirmPasswordShown
                      ? "text"
                      : "password"
                  }
                  placeholder="Masukan kembali password"
                  maxLength={32}
                  onChange={(e) => {
                    setPasswordConfirm(
                      e.target.value
                    );
                  }}
                  required={true}>
                  <button
                    onClick={
                      toogleConfirmPassword
                    }
                    className="absolute right-4 top-10 text-gray-400 text-sm">
                    {confirmPasswordShown ? (
                      <EyeInvisibleOutlined className="text-xl text-text flex items-center" />
                    ) : (
                      <EyeOutlined className="text-xl text-text flex items-center" />
                    )}
                  </button>
                </IsInput>
              </div>
            </div>
          </div>
          <h3 className="text-base font-medium">
            Sudah punya akun?{" "}
            <Link href={"/login"}>
              <span className="text-main font-medium cursor-pointer hover:text-secondary transition-colors">
                Login
              </span>
            </Link>
          </h3>
          <div className="flex flex-col gap-3">
            <Button
              name="Daftar"
              width="w-1/3 justify-center bg-blue-500"
            />
          </div>
        </form>
      </div>
    </div>
  );
};

export default RegForm;
