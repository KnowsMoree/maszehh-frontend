import { FormatPainterOutlined } from "@ant-design/icons";
import {
  Empty,
  Modal,
  Pagination,
  Rate,
} from "antd";
import moment from "moment";
import "moment/locale/id";

import Image from "next/image";
import { useEffect, useState } from "react";
import { mutate } from "swr";
import { reviewRepository } from "../../repository/review";
import Img from "/public/image/pack.png";

const payment = [
  {
    category: "perbaikan",
    orderDate: "27 Oktober 2021",
    dueDate: "27 Oktober 2021",
    bankName: "BCA",
    accountNumber: "00099992390293843",
    paymentType: "full payment",
    paymentAmount: "Rp 400.000",
    orderCode: "MSZO829389832",
  },
  {
    category: "pembangunan",
    orderDate: "27 Oktober 2021",
    dueDate: "27 Oktober 2021",
    bankName: "BCA",
    accountNumber: "00099992390293843",
    paymentType: "full payment",
    paymentAmount: "Rp 400.000",
    orderCode: "MSZ909090909090",
  },
];

const MyReview = (props) => {
  const [visible, setVisible] = useState(false);
  const [review, setReview] = useState([]);

  const [reviewPhoto, setReviewPhoto] = useState(
    []
  );
  const [reviewDetail, setReviewDetail] =
    useState({});

  const [id, setId] = useState("");

  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(3);
  const [total, setTotal] = useState(0);

  const { data: reviewUser } =
    reviewRepository.hooks.reviewUser({
      page,
      limit,
    });
  const { data: detailReview } =
    reviewRepository.hooks.reviewDetail(id);

  useEffect(() => {
    if (
      reviewUser !== undefined &&
      reviewUser !== null
    ) {
      setReview(reviewUser?.items);
      console.log(reviewUser?.items);

      setTotal(reviewUser?.meta?.totalItems);
    }

    if (
      detailReview !== undefined &&
      detailReview !== null
    ) {
      setReviewPhoto(detailReview?.photos);
    }
  }, [reviewUser, detailReview]);

  const handleModal = (value) => {
    setId(value.id);
    console.log(value, "data");
    setReviewDetail(value);
    setVisible(true);
  };

  const onChange = (page) => {
    setPage(page);
  };

  return (
    <>
      <div className="flex pt-3 gap-y-5 flex-col">
        {review?.length == 0 ? (
          <div className="w-full h-full flex items-center justify-center">
            <Empty
              image={Empty.PRESENTED_IMAGE_SIMPLE}
              description={"Tidak ada ulasan"}
            />
          </div>
        ) : (
          <>
            {review?.map((rev) => {
              return (
                <>
                  <div className="p-4 flex flex-col gap-y-2 rounded-lg bg-white drop-shadow-lg">
                    <div className="flex flex-row justify-between">
                      <div className="flex flex-row gap-x-1">
                        <h3 className="text-text font-normal text-sm flex items-center m-0">
                          {
                            rev?.orderDetail
                              ?.order?.orderCode
                          }
                        </h3>
                      </div>
                      <div className="flex flex-row gap-x-1">
                        <h2 className="text-text m-0 font-normal text-sm">
                          Tanggal Ulasan:{" "}
                          <span className="m-0 font-normal text-black">
                            {moment(
                              rev?.createdAt
                            ).format(
                              "YYYY-MM-DD HH:mm"
                            )}
                          </span>
                        </h2>
                      </div>
                    </div>
                    <div className="flex flex-row gap-x-3 justify-between">
                      <div className="flex flex-col gap-y-2">
                        <div className="w-full">
                          <h2 className="m-0 text-base font-semibold">
                            {rev?.vendor?.name}
                          </h2>
                        </div>
                        <div className="flex flex-row gap-x-4">
                          <div className="w-[83px] h-[62px]">
                            <img
                              alt=""
                              src={
                                rev?.pack?.photo
                              }
                              width={83}
                              height={62}
                              className="rounded-md"
                            />
                          </div>
                          <div className="flex flex-col w-fit justify-center">
                            <h1 className="m-0 text-sm break-words font-medium">
                              {rev?.pack?.name}
                            </h1>
                            <Rate
                              value={rev?.rate}
                              disabled
                            />
                          </div>
                        </div>
                      </div>
                      <div className="flex flex-col w-1/5 items-center justify-center">
                        <h4
                          className="m-0 hover:text-secondary active:text-sm select-none hover:text-base transition-all cursor-pointer text-main text-sm capitalize font-semibold"
                          onClick={() =>
                            handleModal(rev)
                          }>
                          detail ulasan
                        </h4>
                      </div>
                    </div>
                  </div>
                  <Modal
                    visible={visible}
                    onCancel={() =>
                      setVisible(!visible)
                    }
                    centered
                    bodyStyle={{
                      padding: "50px",
                    }}
                    width={"45%"}
                    footer={null}
                    okButtonProps={{
                      hidden: true,
                    }}>
                    <div className="w-full flex flex-col gap-y-2">
                      <div className="w-full flex-row justify-between flex">
                        <h3 className="font-bold text-sm m-0">
                          {
                            reviewDetail?.review
                              ?.vendor?.name
                          }
                        </h3>
                        <h2 className="font-normal text-text m-0">
                          Tanggal Ulasan:{" "}
                          <span className="text-black">
                            {moment(
                              reviewDetail?.createdAt
                            ).format(
                              "YYYY-MM-DD HH:mm"
                            )}
                          </span>
                        </h2>
                      </div>
                      <div className="flex flex-row gap-x-4">
                        <div className="w-[100px] h-[100px]">
                          <img
                            alt="foto"
                            src={
                              reviewDetail?.pack
                                ?.photo
                            }
                            width={100}
                            height={100}
                            className="rounded-md"
                          />
                        </div>
                        <div className="flex flex-col gap-y-2 w-full">
                          <h3 className="text-sm font-normal m-0">
                            {
                              reviewDetail?.pack
                                ?.name
                            }
                          </h3>
                          <Rate
                            className="flex items-center"
                            disabled
                            value={
                              reviewDetail?.rate
                            }
                          />
                          <p className="text-text text-sm m-0">
                            {
                              reviewDetail?.comment
                            }
                          </p>
                          <div className="flex-row flex gap-2">
                            {reviewPhoto?.map(
                              (p) => {
                                return (
                                  <img
                                    alt="foto"
                                    src={p.photo}
                                    width={100}
                                    height={100}
                                    className="rounded-md"
                                  />
                                );
                              }
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </Modal>
                </>
              );
            })}
            <div className="flex fixed bottom-36">
              <Pagination
                current={page}
                pageSize={limit}
                onChange={onChange}
                total={total}
              />
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default MyReview;
