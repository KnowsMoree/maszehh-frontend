import {
  DollarCircleOutlined,
  DollarOutlined,
  FormatPainterFilled,
  LoadingOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import {
  Empty,
  Form,
  Input,
  message,
  Modal,
  Pagination,
  Upload,
} from "antd";
import ImgCrop from "antd-img-crop";
import { useForm } from "antd/lib/form/Form";
import moment from "moment";
import "moment/locale/id";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { mutate } from "swr";
import { appConfig } from "../../config/app";
import { formatRupiah } from "../../helper/currencyFormat";
import { paymentUserRepository } from "../../repository/payment-user";

const UserPayment = (props) => {
  const id = props?.param;
  const router = useRouter();
  const [status1, setStataus1] =
    useState("unpaid");
  const [status2, setStataus2] = useState("hold");
  const [form] = useForm();

  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] =
    useState(false);
  const [fileList, setFileList] = useState();
  const [paymentUsers, setPaymentUsers] =
    useState([]);
  const [idUser, setId] = useState();

  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(3);
  const [total, setTotal] = useState(0);

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div
        style={{
          marginTop: 8,
        }}>
        Upload
      </div>
    </div>
  );

  const { data: dataPaymentUser } =
    paymentUserRepository.hooks.usePaymentByUser({
      status1,
      status2,
      page,
      limit,
    });

  useEffect(() => {
    if (
      dataPaymentUser !== undefined &&
      dataPaymentUser !== null
    ) {
      setStataus1("unpaid");
      setStataus2("hold");
      setId(id);
      setPaymentUsers(
        dataPaymentUser?.data?.items
      );
      setTotal(
        dataPaymentUser?.data?.meta?.totalItems
      );
    }
  }, [dataPaymentUser]);

  const handleShowModal = (value) => {
    form.setFieldsValue({
      id: value.id,
    });
    setVisible(true);
  };

  const handleUpload = async () => {
    try {
      const idPayment = form.getFieldValue("id");
      const data = {
        photoProof: fileList[0].url,
      };
      await paymentUserRepository.manipulateData.uploadProofPayment(
        idPayment,
        data
      );
      setConfirmLoading(true);
      await mutate(
        paymentUserRepository.url.paymentByUser({
          status1,
          status2,
          page,
          limit,
        })
      );
      setTimeout(() => {
        setVisible(false);
        setConfirmLoading(false);
        router.push(
          `/account/user/payment/${idUser}`
        );
        message.success(
          "Berhasil Mengupload Bukti Pembayaran"
        );
      }, 2000);
    } catch (error) {
      console.log(error.response);
      if (
        error?.response?.body?.error ==
        "Order had done"
      ) {
        message.error(
          "Pesanan sudah selesai. Tidak dapat melakukan pembayaran "
        );
      } else {
        message.error("terjadi kesalahan");
      }
      setVisible(false);
    }
  };

  const onPreview = async (file) => {
    let src = file.url;

    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);

        reader.onload = () =>
          resolve(reader.result);
      });
    }

    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };

  const beforeUpload = (file) => {
    const isJpgOrPng =
      file.type === "image/jpeg" ||
      file.type === "image/png";

    if (!isJpgOrPng) {
      message.error(
        "You can only upload JPG/PNG file!"
      );
    }

    const isLt2M = file.size / 1024 / 1024 < 2;

    if (!isLt2M) {
      message.error(
        "Image must smaller than 2MB!"
      );
    }

    return isJpgOrPng && isLt2M;
  };

  const handleImage = async (args) => {
    const file = args.file;
    try {
      const processUpload =
        await paymentUserRepository.manipulateData.uploadPhotoProof(
          file
        );
      setFileList([
        {
          url:
            appConfig.apiUrl +
            "/payment-user/proof-payment/" +
            processUpload.body.proof,
          name: "proof",
        },
      ]);

      message.success("upload berhasil");
    } catch (e) {
      console.log(e);
      message.error("upload gagal");
    }
  };

  const onChange = (page) => {
    setPage(page);
  };

  return (
    <>
      <div className="flex gap-y-5 flex-col">
        {paymentUsers?.length == 0 ? (
          <div className="w-full h-full flex items-center justify-center">
            <Empty
              image={Empty.PRESENTED_IMAGE_SIMPLE}
              description={"Belum ada pembayaran"}
            />
          </div>
        ) : (
          <>
            {paymentUsers?.map((v, i) => {
              return (
                <>
                  <div
                    className="w-full rounded-xl transition-all drop-shadow-sm hover:drop-shadow-lg bg-white py-[17px] px-[15px] flex flex-col gap-y-6"
                    key={v.id}>
                    <div className="flex flex-row items-center justify-between">
                      <div className="flex w-full flex-row gap-1 items-center justify-between">
                        <div className="flex flex-col justify-center">
                          <DollarCircleOutlined className="flex justify-center items-center m-0 text-main text-base" />
                        </div>
                        <div className=" flex flex-row justify-between w-full">
                          <div className="flex flex-row justify-center">
                            <h5 className="m-0 font-semibold text-sm">
                              {v.paymentCode}
                            </h5>
                          </div>
                          <div className="text-text text-sm font-normal m-0">
                            {v.status ==
                            "unpaid" ? (
                              <span className="text-secondary font-semibold">
                                Belum Dibayar
                              </span>
                            ) : v.status ==
                              "hold" ? (
                              <span className="text-main font-semibold">
                                Pembayaran
                                Terlambat
                              </span>
                            ) : (
                              <span className="text-gray-500 font-semibold">
                                Pembayaran Ditolak
                              </span>
                            )}
                          </div>
                          <div className="flex justify-end">
                            <h4 className="m-0 font-semibold text-sm">
                              Bayar Sebelum :{" "}
                            </h4>
                            <h5 className="text-main text-sm font-normal m-0">
                              {moment(
                                v.dueDate
                              ).format(
                                "YYYY-MM-DD HH:mm"
                              )}
                            </h5>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="flex flex-row justify-between">
                      <div className="flex flex-row divide-x-2 divide-text ">
                        {/* <div className="flex px-3 flex-col gap-1">
        {paymentUsers?.map((v, i) => {
          return (
            <>
              <div
                className="w-full rounded-xl transition-all drop-shadow-sm hover:drop-shadow-lg bg-white py-[17px] px-[15px] flex flex-col gap-y-6"
                key={v.id}
              >
                
                <div className="flex flex-row justify-between">
                  <div className="flex flex-row divide-x-2 divide-text ">
                    {/* <div className="flex px-3 flex-col gap-1">
                      <h2 className="m-0 text-sm text-text capitalize">
                        Kode Pembayaran :
                      </h2>
                      <h2 className="m-0 text-sm capitalize font-semibold">
                        {v.paymentCode}
                      </h2>
                    </div> */}
                        <div className="flex px-3 flex-col gap-1">
                          <h2 className="m-0 text-sm text-text capitalize">
                            Melalui :
                          </h2>
                          <h2 className="m-0 text-sm capitalize font-semibold">
                            {v.bank.bankName}
                          </h2>
                        </div>
                        <div className="flex px-3 flex-col gap-1">
                          <h2 className="m-0 text-sm text-text capitalize">
                            Nomor rekening:
                          </h2>
                          <h2 className="m-0 text-sm capitalize font-semibold">
                            {v.bank.accountNumber}
                          </h2>
                        </div>
                        <div className="flex px-3 flex-col gap-1">
                          <h2 className="m-0 text-sm text-text capitalize">
                            tipe :
                          </h2>
                          <h2 className="m-0 text-sm capitalize font-semibold">
                            {v.paymentType ==
                            "fullpayment"
                              ? "Pembayaran Penuh"
                              : v.paymentType ==
                                "payment1"
                              ? "Pembayaran 1"
                              : v.paymentType ==
                                "payment2"
                              ? "pembayaran 2"
                              : v.paymentType ==
                                "lastpayment"
                              ? "Pembayaran Terakhir"
                              : "DP"}
                          </h2>
                        </div>
                      </div>
                      <div className="flex flex-col gap-y-1 justify-center">
                        <h2 className="m-0 text-sm text-main font-medium capitalize">
                          total tagihan:
                        </h2>
                        <h2 className="m-0 text-sm font-semibold capitalize">
                          {formatRupiah(
                            v.paymentAmount
                          )}
                        </h2>
                      </div>
                    </div>
                    <div className="flex justify-end">
                      <button
                        onClick={() =>
                          handleShowModal(v)
                        }
                        className="hover:bg-secondary select-none px-3 py-2 bg-main text-white rounded-lg font-semibold text-sm hover:drop-shadow-md transition-colors flex items-center justify-center">
                        Bayar
                      </button>
                    </div>
                  </div>
                  <Modal
                    visible={visible}
                    onOk={handleUpload}
                    confirmLoading={
                      confirmLoading
                    }
                    onCancel={() =>
                      setVisible(!visible)
                    }
                    centered
                    title={`Upload Bukti Pembayaran`}
                    okText="Upload"
                    cancelText="Batal">
                    <div className="w-full flex justify-center items-center flex-col">
                      <Form name="form">
                        <Form.Item
                          name={"id"}
                          style={{
                            display: "none",
                          }}>
                          <Input
                            type="text"
                            className="w-full text-base shadow-sm bg-bg font-medium"
                          />
                        </Form.Item>
                        <Form.Item
                          name={"photoProof"}>
                          <ImgCrop aspect={1 / 2}>
                            <Upload
                              name="photo"
                              listType="picture-card"
                              className="avatar-uploader"
                              fileList={fileList}
                              beforeUpload={
                                beforeUpload
                              }
                              customRequest={(
                                args
                              ) =>
                                handleImage(args)
                              }
                              onPreview={
                                onPreview
                              }
                              onRemove={() =>
                                setFileList([])
                              }
                              maxCount={1}>
                              {uploadButton}
                            </Upload>
                          </ImgCrop>
                        </Form.Item>
                      </Form>
                    </div>
                  </Modal>
                </>
              );
            })}
            <div className="flex fixed bottom-36 justify-center">
              <Pagination
                current={page}
                pageSize={limit}
                onChange={onChange}
                total={total}
              />
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default UserPayment;
