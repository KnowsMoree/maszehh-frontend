import { DollarCircleOutlined } from "@ant-design/icons";
import { Empty, Pagination } from "antd";
import moment from "moment";
import { useEffect, useState } from "react";
import { formatRupiah } from "../../helper/currencyFormat";
import { paymentUserRepository } from "../../repository/payment-user";

const Transaction = () => {
  const [status1, setStataus1] = useState("paid");
  const [status2, setStataus2] = useState("done");
  const [paymentUsers, setPaymentUsers] =
    useState([]);

  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(3);
  const [total, setTotal] = useState(0);

  const onChange = (page) => {
    setPage(page);
  };

  const { data: dataPaymentUser } =
    paymentUserRepository.hooks.usePaymentByUser({
      status1,
      status2,
      page,
      limit,
    });

  useEffect(() => {
    setPaymentUsers(dataPaymentUser?.data?.items);
    setTotal(
      dataPaymentUser?.data?.meta?.totalItems
    );
  }, [dataPaymentUser]);

  return (
    <>
      {paymentUsers?.length == 0 ? (
        <div className="w-full h-full flex items-center justify-center">
          <Empty
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description={"Tidak ada transaksi"}
          />
        </div>
      ) : (
        <>
          <div className="flex gap-y-5 flex-col">
            {paymentUsers?.map((v, i) => {
              return (
                <>
                  <div
                    className="w-full rounded-xl transition-all drop-shadow-sm hover:drop-shadow-lg bg-white py-[17px] px-[15px] flex flex-col gap-y-6"
                    key={v.id}>
                    <div className="flex flex-row items-center justify-between">
                      <div className=" flex flex-row gap-3">
                        <div className="flex flex-col justify-center">
                          <DollarCircleOutlined className="flex justify-center items-center m-0 text-main text-base" />
                        </div>
                        <div className="flex flex-col justify-center">
                          <div className="flex">
                            <h5 className="m-0 font-semibold text-sm">
                              {v.paymentCode}
                            </h5>
                          </div>

                          <div className="flex gap-2">
                            <h4 className="m-0 font-semibold text-sm">
                              Dibayar Pada :
                            </h4>
                            <h5 className="text-text text-sm font-normal m-0">
                              {moment(
                                v.paymentDate
                              ).format(
                                "YYYY-MM-DD HH:mm"
                              )}
                            </h5>
                          </div>
                        </div>
                      </div>
                      <div className="text-sm font-semibold m-0">
                        {v.status == "paid" ||
                        v.status == "done" ? (
                          <span className="text-green-500">
                            Sudah Dibayar
                          </span>
                        ) : (
                          <span className="text-yellow-500">
                            Menunggu Konfirmasi
                            Admin
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="flex flex-row justify-between">
                      <div className="flex flex-row divide-x-2 divide-text ">
                        <div className="flex px-3 flex-col gap-1">
                          <h2 className="m-0 text-sm text-text capitalize">
                            Pembayaran Melalui:
                          </h2>
                          <h2 className="m-0 text-sm capitalize font-semibold">
                            {v.bank.bankName}
                          </h2>
                        </div>
                        <div className="flex px-3 flex-col gap-1">
                          <h2 className="m-0 text-sm text-text capitalize">
                            Nomor rekening:
                          </h2>
                          <h2 className="m-0 text-sm capitalize font-semibold">
                            {v.bank.accountNumber}
                          </h2>
                        </div>
                        <div className="flex px-3 flex-col gap-1">
                          <h2 className="m-0 text-sm text-text capitalize">
                            tipe Pembayaran:
                          </h2>
                          <h2 className="m-0 text-sm capitalize font-semibold">
                            {v.paymentType ==
                            "fullpayment"
                              ? "Pembayaran Penuh"
                              : v.paymentType ==
                                "payment1"
                              ? "Pembayaran 1"
                              : v.paymentType ==
                                "payment2"
                              ? "pembayaran 2"
                              : v.paymentType ==
                                "lastpayment"
                              ? "Pembayaran Terakhir"
                              : "DP"}
                          </h2>
                        </div>
                      </div>
                      <div className="flex flex-col gap-y-1 justify-center">
                        <h2 className="m-0 text-sm text-main font-medium capitalize">
                          total tagihan:
                        </h2>
                        <h2 className="m-0 text-sm font-semibold capitalize">
                          {formatRupiah(
                            v.paymentAmount
                          )}
                        </h2>
                      </div>
                    </div>
                  </div>
                </>
              );
            })}
          </div>
          <div className="flex fixed bottom-36 justify-center">
            <Pagination
              current={page}
              pageSize={limit}
              total={total}
              onChange={onChange}
            />
          </div>
        </>
      )}
    </>
  );
};

export default Transaction;
