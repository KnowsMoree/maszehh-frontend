import Image from "next/image";
import Img from "/public/avatar/avatar2.png";
import { userRepository } from "../../repository/user";
import { useEffect, useState } from "react";
import Link from "next/link";

const UserProfile = (props) => {
  const id = props?.param;
  const [user, setUser] = useState(null);
  const { data: userProfile } = userRepository.hooks.useUserDetail(id);

  useEffect(() => {
    if (userProfile !== null && userProfile !== undefined) {
      setUser(userProfile?.data);
    }
  }, [userProfile]);

  console.log(user);

  return (
    <>
      <div className="w-full rounded-lg shadow-sm">
        <div className="relative h-full p-11 rounded-t-lg bg-main"></div>
        <div className="realtive rounded-b-lg bg-white h-full px-7 pb-7">
          <div className="flex flex-row gap-3 max-h-fit w-full">
            <div className="flex -translate-y-10 items-center rounded-full bg-white border-white border-[10px] justify-center">
              {user?.photo ? (
                <img
                  alt=""
                  src={user?.photo}
                  width={100}
                  height={100}
                  className="rounded-full"
                />
              ) : (
                <h1 className="m-0 text-2xl text-main font-bold">
                  {user?.username.charAt(0).toUpperCase()}
                </h1>
              )}
            </div>
            <div className="flex w-full flex-row justify-between pt-4">
              <h1 className="font-semibold text-xl m-0">{user?.fullname}</h1>
              <Link href={`/account/user/edit/${user?.id}`}>
                <button className="bg-main h-fit rounded-lg hover:drop-shadow-lg hover:bgse transition-all py-2 px-6">
                  <h1 className="text-white  m-0 font-semibold text-base">
                    Ubah
                  </h1>
                </button>
              </Link>
            </div>
          </div>
          <div className="w-full flex rounded-lg bg-bg p-5 gap-y-4 flex-col">
            <div className="w-full">
              <h1 className="m-0 text-text uppercase font-semibold text-base">
                username
              </h1>
              <h2 className="m-0 font-bold text-base">{user?.username}</h2>
            </div>
            <div className="w-full">
              <h1 className="m-0 text-text uppercase font-semibold text-base">
                email
              </h1>
              <h2 className="m-0 font-bold text-base">{user?.email}</h2>
            </div>
            <div className="w-full">
              <h1 className="m-0 text-text uppercase font-semibold text-base">
                nomor telepon
              </h1>
              <h2 className="m-0 font-bold text-base">{user?.phoneNumber}</h2>
            </div>
            <div className="w-full">
              <h1 className="m-0 text-text uppercase font-semibold text-base">
                alamat
              </h1>
              <h2 className="m-0 font-bold text-base">{user?.address}</h2>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default UserProfile;
