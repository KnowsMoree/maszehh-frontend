import {
  FormatPainterOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import {
  Col,
  Empty,
  Form,
  Input,
  message,
  Modal,
  Pagination,
  Rate,
  Row,
  Upload,
} from "antd";
import ImgCrop from "antd-img-crop";
import { useForm } from "antd/lib/form/Form";
import moment from "moment";
import "moment/locale/id";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { mutate } from "swr";
import { appConfig } from "../../config/app";
import { orderRepository } from "../../repository/order";
import { reviewRepository } from "../../repository/review";
import Img from "/public/image/pack.png";

const WaitingReview = (props) => {
  const router = useRouter();

  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] =
    useState(false);

  const [fileList, setFileList] = useState([]);
  const [form] = useForm();

  const [ordersDone, setOrdersDone] = useState(
    []
  );
  const [orderSelected, setOrderSelected] =
    useState({});

  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(3);
  const [status, setStatus] = useState("done");
  const [total, setTotal] = useState(0);

  const { data: orderDone } =
    orderRepository.hooks.useOrderbyUser({
      page,
      limit,
      status,
    });

  useEffect(() => {
    if (
      orderDone !== undefined &&
      orderDone !== null
    ) {
      setOrdersDone(orderDone?.items);
      setTotal(orderDone?.meta?.totalItems);
    }
  }, [orderDone]);

  const uploadButton = (
    <div>
      <PlusOutlined />

      <div
        style={{
          marginTop: 8,
        }}>
        Upload
      </div>
    </div>
  );

  const handleImage = async (args) => {
    const file = args.file;

    try {
      const processUpload =
        await reviewRepository.manipulateData.uploadPhotoReview(
          file
        );

      setFileList([
        ...fileList,
        {
          url:
            appConfig.apiUrl +
            "/review/photo/" +
            processUpload.body.photo,
          name: "profile",
        },
      ]);

      message.success("upload berhasil");
    } catch (e) {
      console.log(e);
      message.error("upload gagal");
    }
  };

  const handleFinish = async () => {
    const data = {
      ...form.getFieldsValue(),
      photos: fileList,
    };
    try {
      setConfirmLoading(true);
      await reviewRepository.manipulateData.createReview(
        data
      );
      await mutate(
        orderRepository.url.orderByUser({
          page,
          limit,
          status,
        })
      );
      setTimeout(async () => {
        setVisible(false);
        setConfirmLoading(false);
        router.reload();
        message.success(
          "Berhasil Membuat Review"
        );
      }, 1000);
    } catch (error) {
      console.log(error);
      setVisible(false);
      setConfirmLoading(false);
      message.error("gagal membuat review");
    }
  };

  const onChange = (page) => {
    setPage(page);
  };

  const handleModal = (value) => {
    setOrderSelected(value);
    form.setFieldsValue({
      orderDetailId: value?.id,
    });
    setVisible(true);
  };

  const onPreview = async (file) => {
    let src = file.url;

    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);

        reader.onload = () =>
          resolve(reader.result);
      });
    }

    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };

  const beforeUpload = (file) => {
    const isJpgOrPng =
      file.type === "image/jpeg" ||
      file.type === "image/png";

    if (!isJpgOrPng) {
      message.error(
        "You can only upload JPG/PNG file!"
      );
    }

    const isLt2M = file.size / 1024 / 1024 < 2;

    if (!isLt2M) {
      message.error(
        "Image must smaller than 2MB!"
      );
    }

    return isJpgOrPng && isLt2M;
  };

  console.log(ordersDone);

  return (
    <>
      {ordersDone?.length == 0 ? (
        <div className="w-full h-full flex items-center justify-center">
          <Empty
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description={
              "Tidak ada ulasan yang ditunggu"
            }
          />
        </div>
      ) : (
        <>
          <div className="flex pt-3 gap-y-5 flex-col">
            {ordersDone.map((order) => {
              return (
                <>
                  <div className="p-4 flex flex-col gap-y-2 rounded-lg bg-white drop-shadow-lg">
                    <div className="flex flex-row justify-between">
                      <div className="flex flex-row gap-x-1">
                        <h3 className="text-text font-normal text-sm flex items-center m-0">
                          {
                            order?.order
                              ?.orderCode
                          }
                        </h3>
                      </div>
                      <div className="flex flex-row gap-x-1">
                        <h2 className="text-text m-0 font-normal text-sm">
                          Tanggal selesai:{" "}
                          <span className="m-0 font-normal text-black">
                            {moment(
                              order?.currentEndDate
                            ).format(
                              "YYYY-MM-DD HH:mm"
                            )}
                          </span>
                        </h2>
                      </div>
                    </div>
                    <div className="flex flex-row gap-x-3 justify-between">
                      <div className="flex flex-col gap-y-2">
                        <div className="w-full">
                          <h2 className="m-0 text-base font-semibold">
                            {
                              order?.pack?.vendor
                                ?.name
                            }
                          </h2>
                        </div>
                        <div className="flex flex-row gap-x-4">
                          <div className="w-[83px] h-[62px]">
                            <img
                              alt={
                                order?.pack?.photo
                              }
                              src={
                                order?.pack?.photo
                              }
                              width={83}
                              height={62}
                              className="rounded-md"
                            />
                          </div>
                          <div className="flex flex-col w-fit justify-center">
                            <h1 className="m-0 text-sm break-words font-medium">
                              {order?.pack?.name}
                            </h1>
                          </div>
                        </div>
                      </div>
                      <div className="flex flex-col w-1/5 items-center justify-center">
                        <h4
                          className="m-0 hover:text-secondary active:text-sm select-none hover:text-base transition-all cursor-pointer text-main text-sm capitalize font-semibold"
                          onClick={() =>
                            handleModal(order)
                          }>
                          beri ulasan
                        </h4>
                      </div>
                    </div>
                  </div>
                  <Modal
                    visible={visible}
                    onCancel={() =>
                      setVisible(!visible)
                    }
                    centered
                    confirmLoading={
                      confirmLoading
                    }
                    onOk={handleFinish}
                    width={"45%"}
                    bodyStyle={{
                      padding: "50px 22px 22px",
                    }}
                    okText="Simpan"
                    cancelText="Batal">
                    <div className="w-full flex gap-y-2 flex-col">
                      <div className="w-full flex-row flex justify-between">
                        <h1 className="text-base font-semibold m-0">
                          {
                            orderSelected?.pack
                              ?.vendor?.name
                          }
                        </h1>
                        <h2 className="text-text text-sm m-0 font-medium">
                          Tanggal selesai:{" "}
                          <span className="text-black">
                            {" "}
                            {moment(
                              orderSelected?.currentEndDate
                            ).format(
                              "YYYY-MM-DD HH:mm"
                            )}
                          </span>
                        </h2>
                      </div>
                      <div className="flex w-full flex-row gap-x-3">
                        <div className="w-1/4">
                          <img
                            alt={
                              orderSelected?.pack
                                ?.photo
                            }
                            src={
                              orderSelected?.pack
                                ?.photo
                            }
                            width={83}
                            height={62}
                            className="rounded-md"
                          />
                        </div>
                        <div className="w-full flex flex-col">
                          <h2 className="text-base font-bold m-0">
                            {
                              orderSelected?.pack
                                ?.name
                            }
                          </h2>
                          <h2 className="text-sm text-text font-medium m-0">
                            Bagaimana kinerja
                            vendor ini?
                          </h2>
                          <Form
                            layout="vertical"
                            form={form}>
                            <Form.Item
                              name={
                                "orderDetailId"
                              }
                              style={{
                                display: "none",
                              }}>
                              <Input
                                type="text"
                                className="w-full text-base shadow-sm bg-bg font-medium"
                              />
                            </Form.Item>
                            <Row gutter={24}>
                              <Col span={24}>
                                <Form.Item
                                  name={"rate"}>
                                  <Rate
                                    count={5}
                                    defaultValue={
                                      0
                                    }
                                  />
                                </Form.Item>
                              </Col>
                            </Row>
                            <Row gutter={24}>
                              <Col span={12}>
                                <Form.Item
                                  name={"comment"}
                                  label={
                                    <label className="m-0 text-sm font-semibold text-text">
                                      Berikan
                                      ulasan untuk
                                      kinerja
                                      vendor ini
                                    </label>
                                  }>
                                  <Input.TextArea
                                    autoSize={{
                                      minRows: 4,
                                      maxRows: 5,
                                    }}
                                    type="text"
                                    placeholder="Tulis ulasan anda"
                                    className="w-full placeholder:text-sm placeholder:text-text placeholder:font-medium text-base shadow-sm border-none rounded-lg bg-bg font-semibold"
                                  />
                                </Form.Item>
                              </Col>
                              <Col span={12}>
                                <Form.Item
                                  name={"photo"}
                                  label={
                                    <label className="m-0 text-center text-base font-semibold text-text">
                                      Bagikan foto
                                      foto dari
                                      pengerjaan
                                      vendor
                                    </label>
                                  }
                                  style={{
                                    display:
                                      "flex",
                                    alignItems:
                                      "center",
                                    justifyContent:
                                      "center",
                                  }}>
                                  {/* multiple upload yaaa */}
                                  <ImgCrop
                                    aspect={
                                      1 / 1
                                    }>
                                    <Upload
                                      name="photo"
                                      listType="picture-card"
                                      className="avatar-uploader m-0 justify-center items-center flex"
                                      beforeUpload={
                                        beforeUpload
                                      }
                                      customRequest={(
                                        args
                                      ) =>
                                        handleImage(
                                          args
                                        )
                                      }
                                      onPreview={
                                        onPreview
                                      }
                                      fileList={
                                        fileList
                                      }
                                      onRemove={() =>
                                        setFileList(
                                          []
                                        )
                                      }
                                      maxCount={
                                        3
                                      }>
                                      {
                                        uploadButton
                                      }
                                    </Upload>
                                  </ImgCrop>
                                </Form.Item>
                              </Col>
                            </Row>
                          </Form>
                        </div>
                      </div>
                    </div>
                  </Modal>
                </>
              );
            })}
          </div>
          <div className="flex fixed bottom-36">
            <Pagination
              current={page}
              pageSize={limit}
              onChange={onChange}
              total={total}
            />
          </div>
        </>
      )}
    </>
  );
};

export default WaitingReview;
