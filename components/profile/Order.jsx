import {
  CheckOutlined,
  FormatPainterOutlined,
} from "@ant-design/icons";
import Img from "/public/image/pack.png";
import Image from "next/image";
import {
  Empty,
  message,
  Modal,
  Pagination,
  Tag,
} from "antd";
import { useEffect, useState } from "react";
import { orderRepository } from "../../repository/order";
import moment from "moment";
import "moment/locale/id";
import { formatRupiah } from "../../helper/currencyFormat";
import { mutate } from "swr";
import { useRouter } from "next/router";
import { paymentUserRepository } from "../../repository/payment-user";

const OrderCard = () => {
  const router = useRouter();
  const { id } = router.query;
  const [visible, setVisible] = useState(false);

  const [orders, setOrders] = useState([]);
  const [orderSelected, setOrderSelected] =
    useState({});
  const [orderId, setOrderId] = useState("");
  const [userPayment, setUserPayment] = useState(
    []
  );

  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(3);
  const [status, setStatus] = useState("");
  const [total, setTotal] = useState(0);

  const { data: orderUser } =
    orderRepository.hooks.useOrderbyUser({
      page,
      limit,
      status,
    });

  const { data: orderDetail } =
    orderRepository.hooks.useOrderDetail(orderId);

  const countDays = (startDate, endDate) => {
    const start = moment(startDate, "YYYY-MM-DD");
    const end = moment(endDate, "YYYY-MM-DD");
    const date = moment
      .duration(end.diff(start))
      .asDays();
    return date + 1;
  };

  useEffect(() => {
    if (
      orderUser !== undefined &&
      orderUser !== null
    ) {
      setOrders(orderUser?.items);
      setTotal(orderUser?.meta?.totalItems);
    }

    if (
      orderDetail !== undefined &&
      orderDetail !== null
    ) {
      setUserPayment(orderDetail?.userPayment);
      console.log(orderDetail?.userPayment);
    }
  }, [orderUser, orderDetail]);

  const handleModal = (value) => {
    setOrderSelected(value);
    setOrderId(value?.order?.id);
    setVisible(true);
  };

  const handleApprove = async () => {
    try {
      const status = { status: "done" };
      const id = orderSelected?.order?.id;

      await orderRepository.manipulateData.approveUserOrder(
        id,
        status
      );
      await mutate(
        orderRepository.url.orderByUser({
          page,
          limit,
        })
      );

      setTimeout(() => {
        message.success("pesanan telah selesai");
        router.push(`/account/user/review/${id}`);
      }, 1000);
    } catch (error) {
      console.log(error);
    }
  };

  const onChange = (page) => {
    setPage(page);
  };

  return (
    <>
      <div className="flex pt-3 gap-y-5 flex-col">
        {orders?.length == 0 ? (
          <div className="w-full h-full flex items-center justify-center">
            <Empty
              image={Empty.PRESENTED_IMAGE_SIMPLE}
              description={"Belum ada pesanan"}
            />
          </div>
        ) : (
          <>
            {orders.map((v) => {
              return (
                <>
                  <div
                    className="p-4 flex flex-col gap-y-1 rounded-lg bg-white transition-all hover:drop-shadow-lg"
                    key={v.id}>
                    <div className="flex flex-row justify-between">
                      <div className="flex flex-row gap-x-1">
                        <FormatPainterOutlined className="flex items-center text-main text-sm" />
                        <h1 className="m-0 flex items-center font-semibold text-sm">
                          Pesanan Dibuat Pada :
                        </h1>
                        <h3 className="text-text font-normal flex items-center m-0">
                          {moment(
                            v.order.createdAt
                          ).format(
                            "YYYY-MM-DD hh:mm"
                          )}
                        </h3>
                      </div>
                      <div className="flex flex-row gap-x-1">
                        <h2 className="text-text m-0 font-normal text-sm">
                          {v.order.orderCode}
                        </h2>
                        {v.order.statusToUser ==
                        "done" ? (
                          <div className="py-[2px] px-1 rounded-md bg-[#dad7d7] flex items-center justify-center">
                            <h2 className="m-0 text-text font-semibold tex-sm">
                              {"selesai"}
                            </h2>
                          </div>
                        ) : v.order
                            .statusToUser ==
                          "hold" ? (
                          <div className="py-[2px] px-1 rounded-md bg-red-500 flex items-center justify-center">
                            <h2 className="m-0 text-white font-semibold tex-sm">
                              {"ditahan"}
                            </h2>
                          </div>
                        ) : v.order
                            .statusToUser ==
                          "pending" ? (
                          <div className="py-[2px] px-1 rounded-md bg-yellow-500 flex items-center justify-center">
                            <h2 className="m-0 text-white font-semibold tex-sm">
                              {
                                "menunggu konfirmasi"
                              }
                            </h2>
                          </div>
                        ) : v.order
                            .statusToUser ==
                          "in progress" ? (
                          <div className="py-[2px] px-1 rounded-md bg-green-500 flex items-center justify-center">
                            <h2 className="m-0 text-white font-semibold tex-sm">
                              {"dalam proses"}
                            </h2>
                          </div>
                        ) : v.order
                            .statusToUser ==
                          "first paid done" ? (
                          <div className="py-[2px] px-1 rounded-md bg-blue-500 flex items-center justify-center">
                            <h2 className="m-0 text-white font-semibold tex-sm">
                              {"awal pembayaran"}
                            </h2>
                          </div>
                        ) : v.order
                            .statusToUser ==
                          "done by vendor" ? (
                          <div className="py-[2px] px-1 rounded-md bg-green-500 flex items-center justify-center">
                            <h2 className="m-0 text-white font-semibold tex-sm">
                              {
                                "diselesaikan vendor"
                              }
                            </h2>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                    <div className="flex flex-row gap-x-3 justify-between">
                      <div className="flex flex-col gap-y-1">
                        <div className="w-full">
                          <h2 className="m-0 text-base font-semibold">
                            {v.pack.vendor.name}
                          </h2>
                        </div>
                        <div className="flex flex-row gap-x-4">
                          <div className="w-[85px] h-[65px]">
                            <img
                              alt={v.pack.photo}
                              src={v.pack.photo}
                              width={85}
                              height={65}
                              className="rounded-md"
                            />
                          </div>
                          <div className="flex flex-col w-fit justify-center">
                            <h1 className="m-0 text-sm break-words font-medium">
                              {v.pack.name}
                            </h1>
                            <h2 className="m-0 text-sm font-semibold text-text">
                              {v.handymanQty}{" "}
                              Orang
                            </h2>
                          </div>
                        </div>
                      </div>
                      <div className="flex flex-col w-1/5 items-center justify-center">
                        <h4 className="m-0 text-main text-base font-semibold">
                          total harga
                        </h4>
                        <h5 className="m-0 text-base font-bold">
                          {formatRupiah(
                            v.totalPrice
                          )}
                        </h5>
                      </div>
                    </div>
                    <div className="flex justify-end">
                      <h2
                        onClick={() =>
                          handleModal(v)
                        }
                        className="text-main text-sm hover:text-secondary select-none cursor-pointer transition-colors m-0 ">
                        Lihat Detail pesanan
                      </h2>
                    </div>
                  </div>
                  <Modal
                    visible={visible}
                    onCancel={() =>
                      setVisible(!visible)
                    }
                    centered
                    footer={false}
                    bodyStyle={{
                      padding: "0px",
                    }}
                    width={"45%"}
                    okButtonProps={{
                      hidden: true,
                    }}
                    cancelButtonProps={{
                      hidden: true,
                    }}>
                    <div className="w-full divide-y-2 flex flex-col">
                      <div className="w-full px-8 py-9 gap-y-3 flex flex-col">
                        <div className="w-full flex justify-center">
                          <h1 className="m-0 text-xl font-bold ">
                            Detail pesanan
                          </h1>
                        </div>
                        <div className="w-full flex flex-col gap-y-5">
                          <div className="flex w-full flex-row gap-x-4 justify-between">
                            <div className="w-full">
                              <h2 className="m-0 text-sm text-text font-semibold capitalize">
                                kode pesanan:
                              </h2>
                              <h2 className="m-0 text-base text-redText font-semibold capitalize">
                                {
                                  orderSelected
                                    ?.order
                                    ?.orderCode
                                }
                              </h2>
                            </div>
                            <div className="w-full">
                              <h2 className="m-0 text-sm text-text font-semibold capitalize">
                                nama vendor:
                              </h2>
                              <h2 className="m-0 text-sm font-semibold capitalize">
                                {
                                  orderSelected
                                    ?.pack?.vendor
                                    ?.name
                                }
                              </h2>
                            </div>
                            <div className="w-full">
                              <h2 className="m-0 text-sm text-text font-semibold capitalize">
                                kategori:
                              </h2>
                              <h2 className="m-0 text-sm font-semibold capitalize">
                                {
                                  orderSelected
                                    ?.pack
                                    ?.category
                                    ?.categoryName
                                }
                              </h2>
                            </div>
                          </div>
                          <div className="flex flex-row gap-x-3 justify-between">
                            <div className="w-full">
                              <h2 className="m-0 text-sm text-text font-semibold capitalize">
                                tanggal pesanan
                                dibuat:
                              </h2>
                              <h2 className="m-0 text-sm font-semibold capitalize">
                                {moment(
                                  orderSelected
                                    ?.order
                                    ?.createdAt
                                ).format(
                                  "YYYY-MM-DD HH:mm"
                                )}
                              </h2>
                            </div>
                            <div className="w-full">
                              <h2 className="m-0 text-sm text-text font-semibold capitalize">
                                tanggal mulai
                                layanan:
                              </h2>
                              <h2 className="m-0 text-sm font-semibold capitalize">
                                {moment(
                                  orderSelected?.startDate
                                ).format(
                                  "YYYY-MM-DD HH:mm"
                                )}
                              </h2>
                            </div>
                            <div className="w-full">
                              <h2 className="m-0 text-sm text-text font-semibold capitalize">
                                tanggal selesai
                                layanan:
                              </h2>
                              <h2 className="m-0 text-sm font-semibold capitalize">
                                {moment(
                                  orderSelected?.endDate
                                ).format(
                                  "YYYY-MM-DD HH:mm"
                                )}
                              </h2>
                            </div>
                          </div>
                          <div className="flex gap-x-3 flex-row justify-between">
                            <div className="w-full">
                              <h2 className="m-0 text-sm text-text font-semibold capitalize">
                                alamat tujuan
                                layanan:
                              </h2>
                              <h2 className="m-0 text-sm break-words font-semibold capitalize">
                                {
                                  orderSelected
                                    ?.order
                                    ?.locationService
                                }
                              </h2>
                            </div>
                            <div className="w-full">
                              <h2 className="m-0 text-sm text-text font-semibold capitalize">
                                luas:
                              </h2>
                              <h2 className="m-0 text-sm font-semibold capitalize">
                                {
                                  orderSelected
                                    ?.order
                                    ?.serviceArea
                                }{" "}
                                M<sup>2</sup>
                              </h2>
                            </div>
                            <div className="w-full">
                              <h2 className="m-0 text-sm text-text font-semibold capitalize">
                                status pesanan:
                              </h2>
                              <h2 className="m-0 text-sm font-sm capitalize">
                                {orderSelected
                                  ?.order
                                  ?.statusToUser ==
                                "pending" ? (
                                  <span>
                                    <Tag
                                      color="yellow"
                                      className="border-none capitalize rounded-md">
                                      menunggu
                                      konfirmasi
                                    </Tag>
                                  </span>
                                ) : orderSelected
                                    ?.order
                                    ?.statusToUser ==
                                  "hold" ? (
                                  <span>
                                    <Tag
                                      color="red"
                                      className="border-none capitalize rounded-md">
                                      ditahan
                                    </Tag>
                                  </span>
                                ) : orderSelected
                                    ?.order
                                    ?.statusToUser ==
                                  "done" ? (
                                  <span>
                                    <Tag
                                      color="gray"
                                      className="border-none capitalize rounded-md">
                                      selesai
                                    </Tag>
                                  </span>
                                ) : orderSelected
                                    ?.order
                                    ?.statusToUser ==
                                  "in progress" ? (
                                  <span>
                                    <Tag
                                      color="green"
                                      className="border-none capitalize rounded-md">
                                      dalam proses
                                    </Tag>
                                  </span>
                                ) : orderSelected
                                    ?.order
                                    ?.statusToUser ==
                                  "first paid done" ? (
                                  <span>
                                    <Tag
                                      color="blue"
                                      className="border-none capitalize rounded-md">
                                      awal
                                      pembayaran
                                    </Tag>
                                  </span>
                                ) : orderSelected
                                    ?.order
                                    ?.statusToUser ==
                                  "done by vendor" ? (
                                  <span>
                                    <Tag
                                      color="green"
                                      className="border-none capitalize rounded-md">
                                      diselesaikan
                                      vendor
                                    </Tag>
                                  </span>
                                ) : (
                                  ""
                                )}
                              </h2>
                            </div>
                          </div>
                          <div className="w-full">
                            <h2 className="m-0 text-sm text-text font-semibold capitalize">
                              catatan pesanan:
                            </h2>
                            <h2 className="m-0 text-sm font-semibold capitalize">
                              {orderSelected?.notes ==
                              ""
                                ? "tidak ada"
                                : orderSelected?.notes}
                            </h2>
                          </div>
                        </div>
                      </div>
                      <div className="w-full flex flex-col gap-y-1 px-8 pb-10 pt-6">
                        <h3 className="font-medium text-lg m-0 capitalize">
                          detail pembayaran
                        </h3>
                        <div className="flex flex-col divide-y-2 divide-dashed divide-text">
                          <div className="flex flex-col py-2">
                            {userPayment?.map(
                              (uPay) => {
                                return (
                                  <div className="flex flex-row justify-between">
                                    <div className="flex gap-x-4 justify-between">
                                      <h2 className="font-semibold text-text text-base m-0">
                                        {uPay.paymentType ==
                                        "fullpayment"
                                          ? "pembayaran penuh"
                                          : uPay.paymentType ==
                                            "payment1"
                                          ? "pembayaran 1"
                                          : uPay.paymentType ==
                                            "payment2"
                                          ? "pembayaran 2"
                                          : uPay.paymentType ==
                                            "lastpayment"
                                          ? "pembayaran Terakhir"
                                          : "DP"}{" "}
                                      </h2>
                                      <h2 className=" font-semibold break-normal whitespace-nowrap text-base">
                                        {uPay?.status ==
                                        "unpaid" ? (
                                          <span>
                                            <Tag
                                              color="yellow"
                                              className="border-none rounded-md">
                                              belum
                                              dibayar
                                            </Tag>
                                          </span>
                                        ) : uPay?.status ==
                                          "hold" ? (
                                          <span>
                                            <Tag
                                              color="red"
                                              className="border-none rounded-md">
                                              pembayaran
                                              terlambat
                                            </Tag>
                                          </span>
                                        ) : uPay?.status ==
                                          "rejected" ? (
                                          <span>
                                            <Tag
                                              color="red"
                                              className="border-none rounded-md">
                                              ditolak
                                            </Tag>
                                          </span>
                                        ) : uPay?.status ==
                                            "paid" ||
                                          uPay?.status ==
                                            "done" ? (
                                          <span>
                                            <Tag
                                              color="green"
                                              className="border-none rounded-md">
                                              sudah
                                              dibayar
                                            </Tag>
                                          </span>
                                        ) : (
                                          <span>
                                            <Tag
                                              color="geekblue"
                                              className="border-none rounded-md">
                                              menunggu
                                              konfirmasi
                                              admin
                                            </Tag>
                                          </span>
                                        )}
                                      </h2>
                                    </div>

                                    <h3 className="font-semibold text-black text-base m-0">
                                      {formatRupiah(
                                        uPay.paymentAmount
                                      )}
                                    </h3>
                                  </div>
                                );
                              }
                            )}
                          </div>

                          <div className="flex flex-col py-2">
                            <h3 className="font-medium text-lg m-0 capitalize">
                              rincian pembayaran
                            </h3>
                            <div className="flex flex-row justify-between">
                              <h2 className="font-semibold text-text text-base m-0">
                                harga layanan:
                              </h2>
                              <h3 className="m-0 font-normal text-base">
                                {formatRupiah(
                                  orderSelected?.price
                                )}
                              </h3>
                            </div>
                            <div className="flex flex-row justify-between">
                              <h2 className="font-semibold text-text text-base m-0">
                                total tukang:
                              </h2>
                              <h3 className="m-0 font-normal text-base">
                                {
                                  orderSelected?.handymanQty
                                }{" "}
                                orang
                              </h3>
                            </div>
                            <div className="flex flex-row justify-between">
                              <h2 className="font-semibold text-text text-base m-0">
                                Jumlah hari:
                              </h2>
                              <h3 className="m-0 font-normal text-base">
                                {countDays(
                                  orderSelected?.startDate,
                                  orderSelected?.endDate
                                )}{" "}
                                Hari
                              </h3>
                            </div>
                          </div>
                          <div className="flex pt-2 flex-row justify-between">
                            <h2 className="font-bold text-lg m-0">
                              Total:
                            </h2>
                            <h3 className="m-0 font-bold text-lg">
                              {formatRupiah(
                                orderSelected?.totalPrice
                              )}
                            </h3>
                          </div>
                        </div>
                        {orderSelected?.order
                          ?.statusToUser ==
                        "done by vendor" ? (
                          <div className="mt-5 flex justify-end w-full">
                            <button
                              onClick={
                                handleApprove
                              }
                              className="flex bg-green-600 rounded-lg hover:drop-shadow-lg transition-all items-center flex-row justify-center gap-x-4 py-2 px-4">
                              <CheckOutlined className="text-white text-base flex items-center" />
                              <h2 className="capitalize m-0 text-white text-base font-semibold">
                                selesai
                              </h2>
                            </button>
                          </div>
                        ) : (
                          " "
                        )}
                      </div>
                    </div>
                  </Modal>
                </>
              );
            })}
            <div className="flex fixed bottom-36">
              <Pagination
                current={page}
                pageSize={limit}
                onChange={onChange}
                total={total}
              />
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default OrderCard;
