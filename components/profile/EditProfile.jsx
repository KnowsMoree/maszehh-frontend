import Image from "next/image";
import Img from "/public/avatar/avatar2.png";
import { userRepository } from "../../repository/user";
import { useEffect, useState } from "react";
import Link from "next/link";
import {
  Button,
  Col,
  Form,
  Input,
  message,
  Row,
  Select,
  Upload,
} from "antd";
import {
  ArrowLeftOutlined,
  CheckOutlined,
  LoadingOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import { useForm } from "antd/lib/form/Form";
import { appConfig } from "../../config/app";
import { useRouter } from "next/router";
import { mutate } from "swr";
import ImgCrop from "antd-img-crop";

const UserProfile = (props) => {
  const id = props?.param;
  const router = useRouter();
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);
  const { data: userProfile } =
    userRepository.hooks.useUserDetail(id);

  const [form] = useForm();

  const { Option } = Select;

  useEffect(() => {
    if (
      userProfile !== null &&
      userProfile !== undefined
    ) {
      setUser(userProfile?.data);
      setForm();
      setLoading(false);
      setFileList([
        {
          url: userProfile?.data?.photo,
          name: "profile",
        },
      ]);
    }
  }, [userProfile]);

  async function setForm() {
    form.setFieldsValue({
      fullname: await userProfile?.data?.fullname,
      username: await userProfile?.data?.username,
      email: await userProfile?.data?.email,
      level: await userProfile?.data?.role?.id,
      phoneNumber: await userProfile?.data
        ?.phoneNumber,
      status: await userProfile?.data?.status,
      address: await userProfile?.data?.address,
    });
  }

  const [fileList, setFileList] = useState();
  const handleImage = async (args) => {
    const file = args.file;
    console.log(file);
    try {
      const processUpload =
        await userRepository.manipulateData.uploadUserPhoto(
          file
        );
      console.log(processUpload.body.profile);
      setFileList([
        {
          url:
            appConfig.apiUrl +
            "/users/profile/" +
            processUpload.body.profile,
          name: "profile",
        },
      ]);

      message.success("upload berhasil");
    } catch (e) {
      console.log(e);
      message.error("upload gagal");
    }
  };

  const onPreview = async (file) => {
    let src = file.url;

    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);

        reader.onload = () =>
          resolve(reader.result);
      });
    }

    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };

  const beforeUpload = (file) => {
    const isJpgOrPng =
      file.type === "image/jpeg" ||
      file.type === "image/png";

    if (!isJpgOrPng) {
      message.error(
        "You can only upload JPG/PNG file!"
      );
    }

    const isLt2M = file.size / 1024 / 1024 < 2;

    if (!isLt2M) {
      message.error(
        "Image must smaller than 2MB!"
      );
    }

    return isJpgOrPng && isLt2M;
  };

  const uploadButton = (
    <div>
      {loading ? (
        <LoadingOutlined />
      ) : (
        <PlusOutlined />
      )}
      <div
        style={{
          marginTop: 8,
        }}>
        Upload
      </div>
    </div>
  );

  const onFinish = async (values) => {
    try {
      const data = {
        ...values,
        photo: fileList[0].url,
      };

      await userRepository.manipulateData.updateUser(
        id,
        data
      );
      await mutate(
        userRepository.url.userDetail(id)
      );

      router.push(`/account/user/${id}`);
      message.success("Data Berhasil Diubah");
    } catch (error) {
      console.log(error);
      message.error("data gagal diubah");
    }
    // console.log("Success:", { ...values, photo: fileList[0].url });
  };

  return (
    <>
      <div className="p-5 flex flex-col gap-7 rounded-lg w-full bg-white drop-shadow-md">
        <Form
          form={form}
          layout="vertical"
          onFinish={onFinish}
          size="large">
          <Row
            gutter={32}
            style={{ margin: "20px" }}>
            <Col className="gutter-row" span={12}>
              <Form.Item
                style={{ margin: "0" }}
                name={"fullname"}
                label={
                  <label className="text-base font-semibold text-text">
                    Nama Lengkap
                  </label>
                }
                rules={[
                  {
                    required: true,
                    message:
                      "masukan nama lengkap",
                  },
                  {
                    min: 5,
                    message:
                      "nama lengkap minimal 5 karakter",
                  },
                ]}>
                <Input
                  type="text"
                  className="w-full text-base shadow-sm bg-white rounded-md font-semibold"
                />
              </Form.Item>
              <Form.Item
                style={{ margin: "0" }}
                rules={[
                  {
                    required: true,
                    message:
                      "masukan nomor telepon",
                  },
                  {
                    max: 13,
                    message:
                      "nomor telepon maksimal 13 karakter",
                  },
                  {
                    pattern: new RegExp(
                      /[0-9]{4}[0-9]{4}[0-9]{3,4}/g
                    ),
                    message:
                      "masukan hanya angka",
                  },
                ]}
                name={"phoneNumber"}
                label={
                  <label className="text-base font-semibold text-text">
                    Nomor Telepon
                  </label>
                }>
                <Input
                  type="text"
                  className="w-full text-base shadow-sm bg-white rounded-md font-semibold"
                />
              </Form.Item>
              <Form.Item
                name={"email"}
                rules={[
                  {
                    required: true,
                    message: "masukan email",
                  },
                  {
                    type: "email",
                    message:
                      "masukan email dengan benar",
                  },
                ]}
                style={{ margin: "0" }}
                label={
                  <label className="text-base font-semibold text-text">
                    Email
                  </label>
                }>
                <Input
                  type="text"
                  className="w-full text-base shadow-sm bg-white rounded-md font-semibold"
                />
              </Form.Item>
              <Form.Item
                style={{ margin: "0" }}
                name={"address"}
                rules={[
                  {
                    required: true,
                    message: "masukan alamat",
                  },
                ]}
                label={
                  <label className="text-base font-semibold text-text">
                    Alamat
                  </label>
                }>
                <Input.TextArea
                  autoSize={{
                    minRows: 4,
                    maxRows: 5,
                  }}
                  type="text"
                  className="w-full text-base shadow-sm bg-white rounded-md font-semibold"
                />
              </Form.Item>
            </Col>

            <Form.Item
              name={"status"}
              style={{ margin: "0" }}>
              <Select className="w-full hidden text-base shadow-sm bg-bg font-semibold">
                <Option value="active">
                  active
                </Option>
                <Option value="not active">
                  not active
                </Option>
              </Select>
            </Form.Item>

            <Col className="gutter-row" span={12}>
              <div className="w-full rounded-lg flex h-full justify-center items-center bg-bg">
                <Form.Item
                  name={"photo"}
                  style={{ margin: "0" }}>
                  <ImgCrop>
                    <Upload
                      name="photo"
                      listType="picture-card"
                      className="avatar-uploader"
                      beforeUpload={beforeUpload}
                      customRequest={(args) =>
                        handleImage(args)
                      }
                      onPreview={onPreview}
                      fileList={fileList}
                      onRemove={() =>
                        setFileList([])
                      }
                      maxCount={1}>
                      {uploadButton}
                    </Upload>
                  </ImgCrop>
                </Form.Item>
              </div>
            </Col>
          </Row>
          <Row className="flex flex-row gap-6 justify-end">
            <Link
              href={`/account/user/${user?.id}`}>
              <Button
                type="primary"
                icon={<ArrowLeftOutlined />}
                size="large"
                className="text-sm font-semibold w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-main py-[11px] px-[21px] rounded-lg border-none flex items-center">
                Kembali
              </Button>
            </Link>
            <Button
              type="primary"
              htmlType="submit"
              icon={<CheckOutlined />}
              size="large"
              className="text-sm font-semibold w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-blue-500 py-[11px] px-[21px] rounded-lg border-none flex items-center">
              Simpan
            </Button>
          </Row>
        </Form>
      </div>
    </>
  );
};

export default UserProfile;
