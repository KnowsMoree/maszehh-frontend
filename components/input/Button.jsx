const Button = (props) => {
  return (
    <>
      <button
        type="submit"
        onSubmit={props.onSubmit}
        value={props.value}
        className={`
          ${
            props.width
              ? props.width
              : "w-full bg-main justify-center mt-[15px] "
          } active:ring-main active:ring-1 focus:outline-none focus:ring-2 focus:ring-main hover:shadow-lg flex  items-center text-white rounded-lg py-[14px] font-bold text-[16px] cursor-pointer hover:bg-secondary transition ease-linear`}>
        {props.name}
      </button>
    </>
  );
};

export default Button;
