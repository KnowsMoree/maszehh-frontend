import { SearchOutlined } from "@ant-design/icons";

const SearchTable = (props) => {
  return (
    <>
      <div className="rounded-xl w-full transition-all peer-focus:ring-main peer-main:ring-1 cursor-text bg-bg hover:shadow-md p-3 flex flex-row focus:shadow-md">
        <div className="flex items-center">
          <SearchOutlined className="flex peer items-center text-text text-lg" />
        </div>
        <input
          type="text"
          onChange={props.onChange}
          placeholder={props.placeholder}
          className="h-full placeholder:font-medium text-justBlack font-medium bg-transparent peer text-lg border-none outline-none w-full px-2"
        />
      </div>
    </>
  );
};

export default SearchTable;
