import { EyeInvisibleOutlined, EyeOutlined } from "@ant-design/icons";
import { useState } from "react";

const Input = ({ children, ...props }) => {
  return (
    <>
      <div className="relative w-full mb-[22px]">
        <label
          htmlFor={props.name}
          className={
            props.labelclass ? props.labelclass : "font-semibold text-base"
          }
        >
          {props.labelname}
        </label>
        <input
          type={props.type}
          name={props.name}
          id={props.name}
          value={props.value}
          onChange={props.onChange}
          placeholder={props.placeholder}
          minLength={props.minLength}
          maxLength={props.maxLength}
          disabled={props.disabled}
          pattern={props.pattern}
          className={
            props.className
              ? props.className
              : "invalid:ring-red-600 placeholder-transparent text-[16px] w-full p-[14px] shadow-md bg-white rounded-lg placeholder:font-medium placeholder:text-text placeholder:text-[16px] focus:outline-none focus:ring-2 focus:ring-gray-500"
          }
          required={props.required}
        />
        {children}
      </div>
    </>
  );
};

export default Input;
