const Line = () => {
  return (
    <>
      <div className=" text-text w-full h-[1px] my-[22px]">
        <hr className="h-[2px] bg-text text-text" />
      </div>
    </>
  );
};

export default Line;
