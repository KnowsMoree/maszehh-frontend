import Link from "next/link";

const NavLink = ({ children, ...props }) => {
  return (
    <>
      <div className="w-full cursor-pointer">
        <Link href={props.link}>
          <div
            className={`flex rounded-xl text-base font-medium flex-row gap-3 py-[22px] transition-colors px-[17px]  ${
              props.active === props.name
                ? "bg-white drop-shadow-md"
                : "bg-transparent text-text group hover:bg-white hover:drop-shadow-md"
            }`}>
            {children}
          </div>
        </Link>
      </div>
    </>
  );
};

export default NavLink;
