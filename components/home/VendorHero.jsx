import Image from "next/image";
import Link from "next/link";
import Img from "/public/image/vend1.png";
import React, { Component } from "react";

const Hero = () => {
  return (
    <div className="w-full flex flex-col-reverse lg:flex-row">
      <div className="w-full flex items-center justify-center">
        <div className="w-3/5 lg:w-1/2 flex flex-col gap-[19px] justify-center">
          <h1 className="font-bold text-[28px] leading-9 ">
            Ingin menjadi Vendor di{" "}
            <span className="text-main">
              Maszehh
            </span>
            ?
          </h1>
          <div className="w-3/4">
            <p className="font-medium text-sm text-text">
              Daftarkan vendor anda di maszehh
              dengan sekali klik tombol di bawah
              ini
            </p>
          </div>

          <Link href={"/register/vendor"}>
            <p className="cursor-pointer text-base text-white bg-main py-3 px-6 max-w-fit rounded-xl hover:bg-secondary transition-colors">
              Daftar vendor
            </p>
          </Link>
        </div>
      </div>
      <div className="w-full">
        <div className="max-w-fit m-0 flex justify-start items-center">
          <Image src={Img} alt="img" />
        </div>
      </div>
    </div>
  );
};

class VendorHero extends Component {
  render() {
    return (
      <div className="w-full h-full flex flex-col md:flex-row">
        <Hero />
      </div>
    );
  }
}

export default VendorHero;
