import { StarFilled } from "@ant-design/icons";
import Image from "next/image";
import Link from "next/link";
import PackageCard from "../package/PackageCard";
import { useState, useEffect } from "react";
import axios from "axios";

const Pack = () => {
  const [dataPackages, setDataPackges] = useState([]);
  const endpoint = "http://localhost:3222/package/public?limit=4";

  const getPackages = async () => {
    await axios
      .get(endpoint)
      .then((res) => res.data)
      .then((data) => {
        setDataPackges(data);
      });
  };

  useEffect(() => {
    getPackages();
  }, []);

  console.log(dataPackages);

  return (
    <>
      <div className="flex flex-row gap-[46px]">
        {dataPackages.map((pack) => {
          return (
            <PackageCard
              bg="bg"
              id={pack.id}
              name={pack.name}
              vendorName={pack?.vendor?.name}
              photo={pack.photo}
              price={pack.price}
              city={pack.vendor.city.name}
              rating={pack?.rating}
              totalReview={pack?.totalReview}
            />
          );
        })}
        <div className="bg-main p-[15px] drop-shadow-md flex flex-col  rounded-xl w-[325px] min-h-full">
          <div className="h-full w-full mt-[15px]">
            <h1 className="font-semibold text-[26px] text-white">
              Ingin tau lebih banyak paket di Maszehh?
            </h1>
          </div>
          <Link href={"/login"}>
            <p className="bg-white py-[8px] m-0 rounded-lg text-base text-redText flex justify-center items-center cursor-pointer hover:bg-secondary transition-colors hover:text-white">
              Cari paket
            </p>
          </Link>
        </div>
      </div>
    </>
  );
};

export default Pack;
