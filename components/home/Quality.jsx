import { CheckCircleOutlined } from "@ant-design/icons";

const Quality = () => {
  const qlty = [
    {
      name: "Aman",
      desc: "Aman, karena Maszehh menerapkan Rekening bersama sebagai metode pembayaran",
    },
    {
      name: "Terpercaya",
      desc: "Terpercaya, di Maszehh semua vendor sudah memiliki izin dari pemerintah",
    },
    {
      name: "Mudah",
      desc: "Di Maszehh pengguna dengan mudah memesan paket layanan dimana saja dan kapan saja",
    },
  ];

  return (
    <>
      <div className="w-full flex items-center flex-col gap-[42px]">
        <h2 className="font-bold text-[28px]">
          Kenapa memilih
          <span className="text-main ">
            {" "}
            Maszehh
          </span>
          ?
        </h2>
        <div className="flex flex-row flex-wrap gap-[38px]">
          {qlty.map((v, i) => {
            return (
              <div
                className="flex flex-col max-w-[200px] text-center gap-[10px]"
                key={i}>
                <CheckCircleOutlined className="text-main text-[70px]" />
                <h2 className="font-semibold text-[25px]">
                  {v.name}
                </h2>
                <p className="font-normal text-[14px] text-text">
                  {v.desc}
                </p>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Quality;
