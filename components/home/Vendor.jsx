import React, {
  Component,
  useState,
  useEffect,
} from "react";
import { useRouter } from "next/router";
import Img from "/public/image/vendor.png";
import Image from "next/image";
import { Rate } from "antd";
import Link from "next/link";
import axios from "axios";

const Vendor = () => {
  const [dataVendor, setDataVendor] = useState(
    []
  );

  const endpoint =
    "http://localhost:3222/vendor/public?limit=3";
  const getVendor = async () => {
    await axios
      .get(endpoint)
      .then((res) => res.data)
      .then((data) => {
        setDataVendor(data);
      });
  };

  useEffect(() => {
    getVendor();
  }, []);

  return (
    <div className="w-full shadow-md rounded-3xl flex flex-col gap-3 bg-white py-[42px] px-[94px]">
      <div className="w-full flex">
        <div className="w-full">
          <h1 className="font-bold text-[28px]">
            Vendor Ternama di{" "}
            <span className="text-main">
              Maszehh
            </span>
          </h1>
        </div>

        <div className="w-full items-center flex justify-end float-right">
          <Link href={"/login"}>
            <h5 className="hover:text-secondary cursor-pointer transition-colors lg:text-md text-sm text-main font-semibold">
              Lainnya
            </h5>
          </Link>
        </div>
      </div>
      <div className="w-full flex flex-col md:flex-row gap-[50px]">
        {dataVendor.map((value) => {
          return (
            <>
              <div
                className="h-fit w-fit"
                key={value.id}>
                <div className="w-full">
                  <Image
                    src={Img}
                    width={1500}
                    height={1000}
                    alt="yaya"
                  />
                </div>
                <div className="w-full flex flex-col gap-4">
                  <h2 className="font-semibold text-[22px] m-0">
                    {value.name}
                  </h2>
                  <p className="text-sm text-text m-0">
                    {value.desc}
                  </p>
                  <div className="flex flex-row items-center gap-2">
                    <p className="text-text tedxt-base text-center m-0">
                      {value?.rating}
                    </p>
                    <Rate
                      value={value?.rating}
                      allowHalf
                      disabled
                    />
                  </div>
                </div>
              </div>
            </>
          );
        })}
      </div>
    </div>
  );
};

export default Vendor;
