import Image from "next/image";
import Link from "next/link";
import HeroImg from "/public/hero/home/hero.png";
import { ArrowRightOutlined } from "@ant-design/icons";
import Typewriter from "typewriter-effect";
import { useEffect, useState } from "react";

const Hero = () => {
  const [heroHeight, setHeroHeight] = useState(
    "min-h-[calc(100vh-104px)]"
  );

  const listenScrollEvent = () => {
    window.scrollY > 575
      ? setHeroHeight("min-h-[595px]")
      : setHeroHeight(
          "min-h-[calc(100vh-104px)]"
        );
  };

  useEffect(() => {
    window.addEventListener(
      "scroll",
      listenScrollEvent
    );
    return () => {
      window.removeEventListener(
        "scroll",
        listenScrollEvent
      );
    };
  }, []);

  return (
    <>
      <div
        className={`flex md:flex-row ${heroHeight} duration-1000 flex-col-reverse`}>
        <div className="w-full flex justify-center items-center">
          <div className="flex flex-col max-w-[427px] ">
            <h1 className="text-[45px] font-bold leading-tight mb-[28px]">
              Cari{" "}
              <span className="before:block before:absolute before:-inset-1 before:skew-y-2  before:-z-10 before:bg-main relative inline-block">
                <span className="text-white font-bold">
                  tukang
                </span>
              </span>
              {` dengan `}
              <span className="text-main font-bold">
                <Typewriter
                  options={{
                    autoStart: true,
                    loop: true,
                  }}
                  onInit={(typewriter) => {
                    typewriter
                      .typeString("Aman")
                      .pauseFor(1500)
                      .deleteAll()
                      .typeString("Mudah")
                      .pauseFor(1500)
                      .deleteAll()
                      .start();
                  }}
                />
              </span>
            </h1>
            <p className="font-medium text-[14px] text-text mb-[28px]">
              Maszehh adalah web untuk mencari
              tukang yang diinginkan. dimana
              tukang disini berada di sebuah
              vendor. vendor disini sudah memiliki
              surat izin dari pemerintah.
            </p>
            <Link href={"/login"}>
              <div className="hover:bg-secondary transition-all duration-150 py-[12px] px-[18px] w-auto rounded-xl cursor-pointer max-w-[155px] bg-main flex gap-[5px] items-center justify-center">
                <span className="text-center font-medium text-[15px] text-white">
                  Cari Tukang
                </span>{" "}
                <ArrowRightOutlined className="flex items-center text-base text-white" />
              </div>
            </Link>
          </div>
        </div>
        <div className="w-full flex justify-center items-center relative z-0 ">
          <Image
            src={HeroImg}
            alt="hero"
            layout="fixed"
          />
        </div>
      </div>
    </>
  );
};

export default Hero;
