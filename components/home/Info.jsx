import {
  SearchOutlined,
  AuditOutlined,
  DollarOutlined,
  ToolOutlined,
} from "@ant-design/icons";
import Link from "next/link";

const Info = () => {
  const flow = [
    {
      name: "Cari Paket",
      icon: (
        <SearchOutlined className="text-white text-[20px]" />
      ),
      desc: "Pertama, Cari paket yang sesuai dengan kebutuhan dan lokasi pelayanan anda",
    },
    {
      name: "Pemesanan",
      icon: (
        <AuditOutlined className="text-white text-[20px]" />
      ),
      desc: "Kedua, Pesan paket layanan yang diinginkan",
    },
    {
      name: "Pembayaran",
      icon: (
        <DollarOutlined className="text-white text-[20px]" />
      ),
      desc: "Penuhi pembayaran yang harus dibayar sebelum tenggat pembayaran habis",
    },
    {
      name: "Pengerjaan",
      icon: (
        <ToolOutlined className="text-white text-[20px]" />
      ),
      desc: "Terakhir, jika status pesanan sudah menjadi pembayaran pertama, vendor harus mengerjakan dan mengirimkan pekerja nya kepada customer hingga selesai",
    },
  ];

  return (
    <>
      <div className="flex flex-row w-full flex-wrap md:flex-nowrap items-center justify-center">
        <div className="w-full flex items-end justify-end">
          <div className="flex flex-wrap w-3/4 flex-row gap-[30px] ">
            {flow.map((v, i) => {
              return (
                <div
                  className="bg-bg rounded-xl shadow-md flex flex-col gap-[10px] max-w-[265px] px-[25px] py-[35px]"
                  key={i}>
                  <div className="rounded-full w-[50px] h-[50px] bg-main flex-col flex justify-center items-center">
                    {v.icon}
                  </div>
                  <h3 className="text-redText font-semibold text-[24px]">
                    {v.name}
                  </h3>
                  <p className="text-text font-medium text-sm">
                    {v.desc}
                  </p>
                </div>
              );
            })}
          </div>
        </div>
        <div className="flex w-full justify-center">
          <div className="flex flex-col gap-[24px] max-w-sm">
            <h1 className="font-bold text-[32px]">
              Cari tukang? pakai
              <span className=" text-main">
                {" "}
                Maszehh
              </span>{" "}
              aja
            </h1>
            <p className="text-sm font-medium text-text">
              Dengan Maszehh, mencari tukang
              dengan mudah tanpa harus mencari ke
              orang orang terdekat
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Info;
