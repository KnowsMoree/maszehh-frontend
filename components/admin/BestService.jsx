import { HourglassTwoTone } from "@ant-design/icons";
import { Empty } from "antd";

const chart = [
  {
    no: 1,
    orderName: "Penggalian Saluran Air Rumah",
    orderCount: 1000,
  },
  {
    no: 2,
    orderName: "Penggalian Saluran Air Rumah",
    orderCount: 343,
  },
  {
    no: 3,
    orderName: "Penggalian Saluran Air Rumah",
    orderCount: 334,
  },
  {
    no: 4,
    orderName: "Penggalian Saluran Air Rumah",
    orderCount: 123,
  },
];

const BestService = (props) => {
  const bestService = props?.data;

  console.log(bestService);
  return (
    <>
      <div className="bg-white w-[400px] rounded-2xl py-[30px] px-8 flex flex-col gap-[10px]">
        <h1 className="font-bold text-xl capitalize">{props?.title}</h1>
        <div className="w-full flex flex-col">
          {bestService?.length == 0 ? (
            <div className="w-full h-auto flex items-center justify-center">
              <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
            </div>
          ) : (
            bestService?.map((v, i) => {
              return (
                <>
                  <div
                    key={v.pack_id}
                    className="w-full px-[15px] even:bg-bg flex flex-row gap-4 py-2 rounded-lg justify-start"
                  >
                    <div>
                      <h5 className="font-bold text-sm m-0">{i + 1}</h5>
                    </div>
                    <div className="flex w-full">
                      <p className="break-words text-ellipsis  m-0 font-medium text-sm">
                        {v.pack_name}
                      </p>
                    </div>
                    <div className="flex justify-end w-1/2">
                      <h4 className="font-medium m-0">{v.totalOrder}</h4>
                    </div>
                  </div>
                </>
              );
            })
          )}
        </div>
      </div>
    </>
  );
};

export default BestService;
