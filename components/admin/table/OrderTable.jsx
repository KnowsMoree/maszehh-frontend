import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import { Radio, Space, Spin, Table, Tag } from "antd";
import Image from "next/image";
import { DeleteOutlined, LoadingOutlined } from "@ant-design/icons";
import SearchTable from "/components/input/SearchTable";
import Link from "next/link";
import { userRepository } from "/repository/user";
import { orderRepository } from "../../../repository/order";
import moment from "moment";

const onChange = (pagination, filters, sorter, extra) => {
  console.log("params", pagination, filters, sorter, extra);
};

const OrderTable = () => {
  const [listdata, setListData] = useState(null);
  const [search, setSearch] = useState("");
  const [status, setStatus] = useState("");

  const { data: dataOrder } = orderRepository.hooks.useOrderAll({
    search,
    status,
  });

  useEffect(() => {
    if (dataOrder !== null && dataOrder !== undefined) {
      setListData(dataOrder?.items);
      console.log(dataOrder?.items);
    }
  }, [dataOrder]);

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  const columns = [
    {
      title: "Kode pesanan",
      dataIndex: "orderCode",
      key: "orderCode",
      width: "20%",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <Link href={`/admin/order/${record.id}`}>
          <span className="text-blue-500 hover:text-secondary duration-300 cursor-pointer select-none font-medium">
            {key}
          </span>
        </Link>
      ),
    },
    {
      title: "Nama pelanggan",
      dataIndex: "order",
      key: "orders",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <span className="font-medium text-black capitalize">
          {record.user.fullname}
        </span>
      ),
    },
    {
      title: "Nama vendor",
      dataIndex: "vendor_id",
      key: "vendor_id",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <span className="font-medium text-black capitalize">
          {record.vendor.name}
        </span>
      ),
    },
    {
      title: "Tanggal pesanan dibuat",
      dataIndex: "order",
      key: "order",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <span className="font-medium text-black">
          {moment(record.createdAt).format("YYYY-MM-DD")}
        </span>
      ),
    },
    {
      title: "Status pesanan",
      key: "statusToUser",
      dataIndex: "statusToUser",
      filterSearch: false,
      className: "text-base text-text font-semibold",
      width: "15%",
      filters: [
        {
          text: <span>menunggu konfirmasi</span>,
          value: "pending",
        },
        {
          text: <span>dalam proses</span>,
          value: "in progress",
        },
        {
          text: <span>awal pembayaran</span>,
          value: "first paid done",
        },
        {
          text: <span>diselesaikan vendor</span>,
          value: "done by vendor",
        },
        {
          text: <span>ditahan</span>,
          value: "hold",
        },
        {
          text: <span>selesai</span>,
          value: "done",
        },
      ],
      onFilter: (value, record) => record.statusToUser.startsWith(value),
      filterSearch: false,
      render: (statusToUser) => {
        let color =
          statusToUser == "hold"
            ? "red"
            : statusToUser == "done"
            ? "gray"
            : statusToUser == "pending"
            ? "yellow"
            : statusToUser == "first paid done"
            ? "blue"
            : statusToUser == "in progress"
            ? "green"
            : statusToUser == "done by vendor"
            ? "green"
            : "";

        let status =
          statusToUser == "hold"
            ? "ditahan"
            : statusToUser == "done"
            ? "selesai"
            : statusToUser == "pending"
            ? "menunggu konfirmasi"
            : statusToUser == "first paid done"
            ? "awal pembayaran"
            : statusToUser == "in progress"
            ? "dalam proses"
            : statusToUser == "done by vendor"
            ? "diselesaikan vendor"
            : "";

        return (
          <span>
            <Tag
              color={color}
              className="border-none capitalize rounded-md"
              key={statusToUser}
            >
              {status}
            </Tag>
          </span>
        );
      },
    },
  ];

  return (
    <div className="bg-white shadow-lg rounded-2xl p-7 flex gap-4 flex-col">
      <div className="flex flex-row justify-evenly">
        <div className="w-full flex items-center m-0">
          <h1 className="font-bold m-0 text-2xl capitalize">list Pesanan</h1>
        </div>
        <div className="w-1/3 m-0">
          <SearchTable
            placeholder="cari pesanan"
            onChange={(e) => setSearch(e.target.value)}
          />
        </div>
      </div>
      <Table
        columns={columns}
        onChange={onChange}
        dataSource={listdata}
        pagination={{
          position: ["bottomCenter"],
          pageSize: 5,
          style: {
            display: "flex",
            alignItems: "center",
          },
          className: "border-none rounded-lg",
        }}
      />
    </div>
  );
};

export default OrderTable;
