import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import { Radio, Space, Spin, Table, Tag } from "antd";
import Image from "next/image";
import {
  DeleteOutlined,
  LoadingOutlined,
  StarOutlined,
} from "@ant-design/icons";
import Link from "next/link";
import SearchTable from "../../input/SearchTable";
import { paymentUserRepository } from "../../../repository/payment-user";
import { formatRupiah } from "../../../helper/currencyFormat";
import moment from "moment";
import "moment/locale/id";

const onChange = (pagination, filters, sorter, extra) => {
  console.log("params", pagination, filters, sorter, extra);
};

const OrderTable = () => {
  const [listdata, setListData] = useState(null);
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(true);

  const { data: dataPaymentUser } =
    paymentUserRepository.hooks.useAllPaymentUser(search);

  useEffect(() => {
    if (dataPaymentUser !== null && dataPaymentUser !== undefined) {
      console.log(dataPaymentUser.items);
      setListData(dataPaymentUser.items);
      setLoading(false);
    }
  }, [dataPaymentUser]);

  const columns = [
    {
      title: "Kode Pembayaran",
      dataIndex: "paymentCode",
      key: "paymentCode",
      width: "10%",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <Link href={`/admin/payment/order/${record.id}`}>
          <span className="text-blue-500 hover:text-secondary duration-300 cursor-pointer select-none font-medium">
            {key}
          </span>
        </Link>
      ),
    },
    {
      title: "Nama Pelanggan",
      dataIndex: "customer",
      key: "customer",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <span className="text-black font-medium capitalize">
          {record.user.fullname}
        </span>
      ),
    },
    {
      title: "Nomor Rekening Admin",
      dataIndex: "vendor_id",
      key: "vendor_id",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <span className="text-black font-medium">
          {record.bank.accountNumber}
        </span>
      ),
    },
    {
      title: "Tenggat Pembayaran",
      dataIndex: "dueDate",
      key: "dueDate",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <span className="text-black font-medium">
          {moment(key).format("YYYY-MM-DD HH:mm")}
        </span>
      ),
    },
    {
      title: "Total Pembayaran",
      className: "text-base text-text font-semibold",
      key: "paymentAmount",
      dataIndex: "paymentAmount",
      render: (key) => (
        <span className="text-black font-medium">{formatRupiah(key)}</span>
      ),
    },
    {
      title: "Tipe Pembayaran",
      className: "text-base text-text font-semibold",
      key: "paymentType",
      dataIndex: "paymentType",
      render: (key) => {
        let typePayment;
        key == "fullpayment"
          ? (typePayment = "Pembayaran Penuh")
          : key == "dp"
          ? (typePayment = "DP")
          : key == "payment1"
          ? (typePayment = "Pembayaran 1")
          : key == "payment2"
          ? (typePayment = "Pembayaran 2")
          : key == "lastpayment"
          ? (typePayment = "Pembayaran Terakhir")
          : "";

        return <span className="text-black font-medium">{typePayment}</span>;
      },
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      filterSearch: false,
      className: "text-base text-text font-semibold",
      width: "15%",
      filters: [
        {
          text: <span>belum dibayar</span>,
          value: "unpaid",
        },
        {
          text: <span>pembayaran terlambat</span>,
          value: "hold",
        },
        {
          text: <span>menunggu konfirmasi admin</span>,
          value: "waiting admin confirm",
        },
        {
          text: <span>ditolak</span>,
          value: "rejected",
        },
        {
          text: <span>sudah dibayar</span>,
          value: "paid",
        },
        {
          text: <span>sudah dibayar ke vendor</span>,
          value: "done",
        },
      ],
      onFilter: (value, record) => record.status.startsWith(value),
      filterSearch: false,
      render: (key) => {
        let color;
        let status;
        key == "done"
          ? ((color = "grey"), (status = "sudah dibayar ke vendor"))
          : key == "hold"
          ? ((color = "red"), (status = "pembayaran terlambat"))
          : key == "paid"
          ? ((color = "green"), (status = "sudah dibayar"))
          : key == "unpaid"
          ? ((color = "yellow"), (status = "belum dibayar"))
          : key == "rejected"
          ? ((color = "red"), (status = "ditolak"))
          : ((color = "geekblue"), (status = "menunggu konfirmasi admin"));

        return (
          <span>
            <Tag
              color={color}
              className="border-none capitalize rounded-md"
              key={status}
            >
              {status}
            </Tag>
          </span>
        );
      },
    },
  ];

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  return (
    <div className="bg-white shadow-lg rounded-2xl p-7 flex gap-4 flex-col">
      {loading ? (
        <div className="w-full h-full flex items-center justify-center">
          <Spin indicator={loadingIcon} />
        </div>
      ) : (
        <>
          <div className="flex flex-row justify-evenly">
            <div className="w-full flex items-center m-0">
              <h1 className="font-bold m-0 text-2xl capitalize">
                list pembayaran pesanan
              </h1>
            </div>
            <div className="w-1/3 m-0">
              <SearchTable
                placeholder="cari kode pesanan"
                onChange={(e) => setSearch(e.target.value)}
              />
            </div>
          </div>
          <Table
            columns={columns}
            onChange={onChange}
            dataSource={listdata}
            pagination={{
              position: ["bottomCenter"],
              pageSize: 5,
              style: {
                display: "flex",
                alignItems: "center",
              },
              className: "border-none rounded-lg",
            }}
          />
        </>
      )}
    </div>
  );
};

export default OrderTable;
