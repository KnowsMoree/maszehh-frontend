import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import { Radio, Space, Spin, Table, Tag } from "antd";
import Image from "next/image";
import {
  DeleteOutlined,
  LoadingOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import Link from "next/link";
import { userRepository } from "/repository/user";
import SearchTable from "/components/input/SearchTable";
import { paymentVendorRepository } from "../../../repository/payment-vendor";
import moment from "moment";
import "moment/locale/id";
import { formatRupiah } from "../../../helper/currencyFormat";

const onChange = (pagination, filters, sorter, extra) => {
  console.log("params", pagination, filters, sorter, extra);
};

const OrderTable = () => {
  const [listdata, setListData] = useState([]);
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(true);

  const { data: dataVendorPayment } =
    paymentVendorRepository.hooks.useAllVendorPaymet(search);

  useEffect(() => {
    if (dataVendorPayment !== null && dataVendorPayment !== undefined) {
      console.log(dataVendorPayment?.data);
      setListData(dataVendorPayment?.data);
      setTimeout(() => {
        setLoading(false);
      }, 1000);
    }
  }, [dataVendorPayment]);

  const columns = [
    {
      title: "Kode pembayaran",
      dataIndex: "codeVendorPayment",
      key: "codeVendorPayment",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <Link href={`/admin/payment/vendor/${record.id}`}>
          <span className="text-blue-500 hover:text-secondary duration-300 cursor-pointer select-none font-medium">
            {key}
          </span>
        </Link>
      ),
    },
    {
      title: "Tanggal pembayaran",
      dataIndex: "createdAt",
      key: "createdAt",
      className: "text-base text-text font-semibold",
      render: (key) => (
        <span className="text-justBlack font-medium">
          {moment(key).format("YYYY-MM-DD hh:mm")}
        </span>
      ),
    },
    {
      title: "Nama vendor",
      dataIndex: "vendor",
      key: "vendor",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <span className="text-justBlack font-medium">
          {record?.vendor?.name}
        </span>
      ),
    },
    {
      title: "Rekening vendor",
      dataIndex: "bankVendor",
      key: "bankVendor",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <span className="text-justBlack font-medium">
          {record.bankVendor.accountNumber}
        </span>
      ),
    },
    {
      title: "Total Pembayaran",
      dataIndex: "total",
      key: "total",
      className: "text-base text-text font-semibold",
      render: (key) => (
        <span className="text-justBlack font-medium">{formatRupiah(key)}</span>
      ),
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      className: "text-base text-text font-semibold",
      render: (key) => {
        let color;
        let status;

        if (key == "done") {
          (color = "grey"), (status = "selesai");
        } else if (key == "rejected") {
          (color = "red"), (status = "ditolak");
        } else if (key == "waiting vendor confirm") {
          (color = "yellow"), (status = "menunggu konfirmasi vendor");
        }

        return (
          <Tag
            color={color}
            className="border-none capitalize rounded-md"
            key={status}
          >
            {status}
          </Tag>
        );
      },
      filters: [
        {
          text: <span>menunggu konfirmasi vendor</span>,
          value: "waiting vendor confirm",
        },
        {
          text: <span>ditolak</span>,
          value: "rejected",
        },
        {
          text: <span>selesai</span>,
          value: "done",
        },
      ],
      onFilter: (value, record) => record.status.startsWith(value),
      filterSearch: false,
      width: "15%",
    },
  ];

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  return (
    <div className="bg-white shadow-lg gap-4  rounded-2xl p-7 flex flex-col">
      {loading ? (
        <div className="w-full h-full flex items-center justify-center">
          <Spin indicator={loadingIcon} />
        </div>
      ) : (
        <>
          <div className="flex flex-row justify-between">
            <div className="flex items-center m-0">
              <h1 className="font-bold m-0 text-2xl capitalize">
                List pembayaran vendor
              </h1>
            </div>
            <div className="flex flex-row gap-x-4">
              <div className="w-full flex m-0">
                <SearchTable
                  placeholder="Cari kode pembayaran..."
                  onChange={(e) => setSearch(e.target.value)}
                />
              </div>
              <Link href={"/admin/payment/vendor/create"}>
                <button className="bg-main w-1/2 rounded-lg hover:drop-shadow-lg hover:bg-secondary transition-all px-3 py-1 flex flex-row items-center gap-x-2 justify-center">
                  <PlusOutlined className="text-white text-base flex items-center" />
                  <h2 className="m-0 text-base text-white font-semibold flex items-center">
                    Pembayaran
                  </h2>
                </button>
              </Link>
            </div>
          </div>
          <Table
            columns={columns}
            onChange={onChange}
            dataSource={listdata}
            pagination={{
              position: ["bottomCenter"],
              pageSize: 5,
              style: {
                display: "flex",
                alignItems: "center",
              },
              className: "border-none rounded-lg",
            }}
          />
        </>
      )}
    </div>
  );
};

export default OrderTable;
