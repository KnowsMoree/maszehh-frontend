import { Table } from "antd";
import moment from "moment";
import Link from "next/link";
import { formatRupiah } from "../../../helper/currencyFormat";

const data = [
  {
    key: "1",
    fullname: "John Brown",
    startDate: 32,
    orderCode: "New York No. 1 Lake Park",
  },
  {
    key: "2",
    fullname: "John Brown",
    startDate: 32,
    orderCode: "New York No. 1 Lake Park",
  },
  {
    key: "3",
    fullname: "John Brown",
    startDate: 32,
    orderCode: "New York No. 1 Lake Park",
  },
  {
    key: "4",
    fullname: "John Brown",
    startDate: 32,
    orderCode: "New York No. 1 Lake Park",
  },
  {
    key: "5",
    fullname: "John Brown",
    startDate: 32,
    orderCode: "New York No. 1 Lake Park",
  },
  {
    key: "6",
    fullname: "John Brown",
    startDate: 32,
    orderCode: "New York No. 1 Lake Park",
  },
];

const onChange = (pagination, filters) => {
  console.log("params", pagination, filters);
};

const TableCom = (props) => {
  const vendorColumns = [
    {
      title: "kode pesanan",
      dataIndex: "orderCode",
      className: "text-text font-semibold text-base capitalize",
      width: "33%",
      render: (key, record) => (
        <Link href={`/vendor/order/${record.id}`}>
          <span className="text-blue-500 hover:text-secondary duration-300 text-base font-normal cursor-pointer">
            {key}
          </span>
        </Link>
      ),
    },
    {
      title: "Nama pengguna",
      dataIndex: "fullname",
      className: "text-text font-semibold text-base capitalize",
      render: (key, record) => (
        <span className="text-base text-black font-normal capitalize">
          {record?.user?.fullname}
        </span>
      ),
      width: "33%",
    },
    {
      title: "tanggal pesan",
      dataIndex: "createdAt",
      className: "text-text font-semibold text-base capitalize",
      render: (key) => (
        <span className="text-base text-black font-normal capitalize">
          {moment(key).format("YYYY-MM-DD")}
        </span>
      ),
      width: "33%",
    },
  ];

  const adminColumns = [
    {
      title: "kode pembayaran",
      dataIndex: "paymentCode",
      className: "text-text font-semibold text-base capitalize",
      width: "25%",
      render: (key, record) => (
        <Link href={`/admin/payment/order/${record.id}`}>
          <span className="text-blue-500 font-medium capitalize hover:text-secondary duration-300 cursor-pointer select-none">
            {key}
          </span>
        </Link>
      ),
    },
    {
      title: "Nama pengguna",
      dataIndex: "fullname",
      className: "text-text font-semibold text-base capitalize",
      width: "25%",
      render: (key, record) => (
        <span className="text-base font-medium text-black m-0 text-center capitalize">
          {record?.user?.fullname}
        </span>
      ),
    },
    {
      title: "Tenggat pembayaran",
      className: "text-text font-semibold text-base capitalize",
      dataIndex: "dueDate",
      width: "25%",
      render: (key) => (
        <span className="text-base text-black font-normal capitalize">
          {moment(key).format("YYYY-MM-DD HH:mm")}
        </span>
      ),
    },
    {
      title: "Total Pembayaran",
      className: "text-text font-semibold text-base capitalize",
      dataIndex: "paymentAmount",
      width: "25%",
      render: (key) => (
        <span className="text-base font-medium text-black m-0 text-center">
          {formatRupiah(key)}
        </span>
      ),
    },
  ];

  if (props?.userLogin == "admin") {
    return (
      <>
        <div className="bg-white p-7 rounded-2xl ">
          <div>
            <h2 className="font-bold text-xl mb-7">{props.title}</h2>
          </div>
          <Table
            columns={adminColumns}
            dataSource={props?.dataAdmin}
            onChange={onChange}
            pagination={{
              position: ["bottomCenter"],
              pageSize: 4,
            }}
          />
        </div>
      </>
    );
  } else if (props.userLogin == "vendor") {
    return (
      <>
        <div className="bg-white p-7 rounded-2xl ">
          <div>
            <h2 className="font-bold text-xl mb-7">{props.title}</h2>
          </div>
          <Table
            columns={vendorColumns}
            dataSource={props?.data}
            onChange={onChange}
            pagination={{
              position: ["bottomCenter"],
              pageSize: 4,
            }}
          />
        </div>
      </>
    );
  }
};

export default TableCom;
