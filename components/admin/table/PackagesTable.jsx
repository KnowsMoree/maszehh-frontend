import React, {
  useEffect,
  useState,
} from "react";
import "antd/dist/antd.css";
import {
  Radio,
  Rate,
  Space,
  Spin,
  Table,
  Tag,
} from "antd";
import Image from "next/image";
import {
  DeleteOutlined,
  LoadingOutlined,
  StarFilled,
} from "@ant-design/icons";
import Link from "next/link";
import { userRepository } from "/repository/user";
import SearchTable from "/components/input/SearchTable";
import { packageRepository } from "../../../repository/package";
import { formatRupiah } from "../../../helper/currencyFormat";

const columns = [
  {
    title: "Nama Paket Layanan",
    dataIndex: "name",
    key: "name",
    className:
      "text-base text-text font-semibold",
    width: "20%",
    render: (key, record) => (
      <Link href={`/admin/packages/${record.id}`}>
        <span className="text-blue-500 font-medium capitalize hover:text-secondary duration-300 cursor-pointer select-none">
          {key}
        </span>
      </Link>
    ),
  },
  {
    title: "Nama vendor",
    dataIndex: "vendor_id",
    key: "vendor_id",
    className:
      "text-base text-text font-semibold",
    render: (_, record) => (
      <span className="text-justBlack font-medium">
        {record?.vendor?.name}
      </span>
    ),
  },
  {
    title: "Harga",
    dataIndex: "price",
    key: "price",
    className:
      "text-base text-text font-semibold",
    render: (key) => (
      <span className="text-justBlack font-medium">
        {formatRupiah(key)}
      </span>
    ),
  },
  {
    title: "Kategori",
    key: "category_id",
    dataIndex: "category_id",
    filterSearch: false,
    className:
      "text-base text-text font-semibold",
    width: "15%",
    filters: [
      {
        text: <span>perbaikan</span>,
        value: "perbaikan",
      },
      {
        text: <span>renovasi</span>,
        value: "renovasi",
      },
      {
        text: <span>membangun</span>,
        value: "membangun",
      },
      {
        text: <span>pemasangan</span>,
        value: "pemasangan",
      },
    ],
    onFilter: (value, record) =>
      record.category.categoryName.startsWith(
        value
      ),
    filterSearch: false,
    render: (_, record) => (
      <span className="text-justBlack font-medium capitalize">
        {record.category.categoryName}
      </span>
    ),
  },
  {
    title: "Rating",
    dataIndex: "rating",
    key: "rating",
    className:
      "text-base text-text font-semibold",
    width: "10%",
    render: (key) => (
      <div className="flex flex-row">
        <StarFilled className="text-filled text-[18px] mr-2 flex items-center" />
        <span className="text-justBlack font-medium capitalize">
          {key}
        </span>
      </div>
    ),
  },
];

const onChange = (
  pagination,
  filters,
  sorter,
  extra
) => {
  console.log(
    "params",
    pagination,
    filters,
    sorter,
    extra
  );
};

const PackagesTable = () => {
  const [listdata, setListData] = useState(null);
  const [loading, setLoading] = useState(true);

  const [search, setSearch] = useState("");
  const [limit, setLimit] = useState(100);

  const { data: dataPackVen } =
    packageRepository.hooks.usePackageVendor({
      search,
      limit,
    });

  useEffect(() => {
    if (
      dataPackVen !== null &&
      dataPackVen !== undefined
    ) {
      setListData(dataPackVen?.items);
      setLoading(false);
    }
  }, [dataPackVen]);

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  return (
    <div className="bg-white shadow-lg rounded-2xl p-7 flex gap-4 flex-col">
      {loading ? (
        <div className="w-full h-full flex items-center justify-center">
          <Spin indicator={loadingIcon} />
        </div>
      ) : (
        <>
          <div className="flex flex-row justify-evenly">
            <div className="w-full flex items-center m-0">
              <h1 className="font-bold m-0 text-2xl capitalize">
                List Paket Layanan
              </h1>
            </div>
            <div className="w-1/3 m-0">
              <SearchTable
                placeholder="cari paket..."
                onChange={(e) =>
                  setSearch(e.target.value)
                }
              />
            </div>
          </div>
          <Table
            columns={columns}
            onChange={onChange}
            dataSource={listdata}
            pagination={{
              position: ["bottomCenter"],
              pageSize: 5,
              style: {
                display: "flex",
                alignItems: "center",
              },
              className: "border-none rounded-lg",
            }}
          />
        </>
      )}
    </div>
  );
};

export default PackagesTable;
