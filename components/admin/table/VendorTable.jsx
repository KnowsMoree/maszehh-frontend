import React, {
  useEffect,
  useState,
} from "react";
import "antd/dist/antd.css";
import {
  Radio,
  Space,
  Spin,
  Table,
  Tag,
  Rate,
  Popconfirm,
  message,
} from "antd";
import Image from "next/image";
import {
  DeleteOutlined,
  LoadingOutlined,
  StarFilled,
  StarOutlined,
  StarTwoTone,
} from "@ant-design/icons";
import Link from "next/link";
import SearchTable from "/components/input/SearchTable";
import { userRepository } from "/repository/user";
import { vendorRepository } from "../../../repository/vendor";
import { regionRepository } from "../../../repository/region";
import { mutate } from "swr";

const VendorTable = () => {
  const [listData, setListData] = useState(null);
  const [search, setSearch] = useState("");
  const [city, setCity] = useState([]);
  const [loading, setLoading] = useState(true);

  const { data: dataVendor } =
    vendorRepository.hooks.useVendor(search);
  const { data: dataCity } =
    regionRepository.hooks.useCity();

  useEffect(() => {
    if (
      dataVendor !== null &&
      dataVendor !== undefined
    ) {
      setLoading(false);
      setCity(dataCity?.data);
      setListData(dataVendor?.items);
    }
  }, [dataVendor, dataCity]);

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  console.log(listData);

  const deleteVendor = async (id) => {
    await vendorRepository.manipulateData.deleteVendor(
      id
    );
    await mutate(
      vendorRepository.url.vendor(search)
    );
    message.success("Berhasil menghapus vendor");
  };

  const columns = [
    {
      title: "Profile",
      dataIndex: "photo",
      key: "photo",
      align: "center",
      width: "10%",
      className:
        "text-base text-text font-semibold",
      render: (_, record) => (
        <div className="flex justify-center">
          <img
            src={record.user.photo}
            width={40}
            height={40}
            alt={record.user.photo}
            className="rounded-full"
          />
        </div>
      ),
    },
    {
      title: "Nama vendor",
      dataIndex: "name",
      key: "name",
      className:
        "text-base text-text font-semibold",
      render: (key, record) => (
        <Link href={`/admin/vendor/${record.id}`}>
          <span className="text-blue-500 hover:text-secondary duration-300 cursor-pointer select-none font-medium">
            {key}
          </span>
        </Link>
      ),
    },
    {
      title: "Nama pemilik",
      dataIndex: "user",
      key: "user",
      className:
        "text-base text-text font-semibold",
      render: (_, record) => (
        <div className="font-medium text-black">
          {record.user.fullname}
        </div>
      ),
    },
    {
      title: "Lokasi kota/kabupaten",
      dataIndex: "city",
      key: "city",
      className:
        "text-base text-text font-semibold",
      render: (_, record) => (
        <div className="font-medium text-black">
          {record.city.name}
        </div>
      ),
      onFilter: (value, record) =>
        record.city.name.startsWith(value),
      filterSearch: true,
      width: "15%",
    },
    {
      title: "Rating",
      dataIndex: "rating",
      key: "rating",
      className:
        "text-base text-text font-semibold",
      render: (key, record) => (
        <div className="flex flex-row gap-2">
          <StarFilled className="text-filled text-lg flex items-center" />
          <span className="font-medium text-black">
            {key}
          </span>
        </div>
      ),
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      filterSearch: false,
      className:
        "text-base text-text font-semibold",
      render: (_, record) => (
        <div>{record.user.status}</div>
      ),
      width: "15%",
      filters: [
        {
          text: <span>menunggu konfirmasi</span>,
          value: "pending",
        },
        {
          text: <span>aktif</span>,
          value: "active",
        },
        {
          text: <span>tidak aktif</span>,
          value: "not active",
        },
      ],
      onFilter: (value, record) =>
        record.user.status.startsWith(value),
      filterSearch: false,
      render: (_, record) => {
        let color =
          record.user.status.length <= 6
            ? "green"
            : "yellow";
        let status =
          record.user.status.length <= 6
            ? "Aktif"
            : "pending";

        if (record.user.status === "not active") {
          color = "volcano";
          status = "tidak aktif";
        }

        return (
          <span>
            <Tag
              color={color}
              className="border-none capitalize rounded-md"
              key={status}>
              {status}
            </Tag>
          </span>
        );
      },
    },
    {
      title: "Aksi",
      className:
        "text-base text-text font-semibold",
      key: "action",
      width: "10%",
      render: (_, record) => (
        <Space size="middle">
          <Popconfirm
            title="Anda yakin akan menghapus vendor ini?"
            onConfirm={async () =>
              deleteVendor(record.id)
            }
            okText="Ya"
            cancelText="Batal">
            <DeleteOutlined className="text-xl text-main flex items-center" />
          </Popconfirm>
        </Space>
      ),
    },
  ];

  const onChange = (
    pagination,
    filters,
    sorter,
    extra
  ) => {
    console.log(
      "params",
      pagination,
      filters,
      sorter,
      extra
    );
  };

  return (
    <div className="bg-white shadow-lg rounded-2xl p-7 flex gap-4 flex-col">
      {loading ? (
        <div className="w-full h-full flex items-center justify-center">
          <Spin indicator={loadingIcon} />
        </div>
      ) : (
        <>
          <div className="flex flex-row justify-evenly">
            <div className="w-full flex items-center m-0">
              <h1 className="font-bold m-0 text-2xl capitalize">
                list vendor
              </h1>
            </div>
            <div className="w-1/3 m-0">
              <SearchTable
                placeholder="cari vendor"
                onChange={(e) =>
                  setSearch(e.target.value)
                }
              />
            </div>
          </div>
          <Table
            columns={columns}
            onChange={onChange}
            dataSource={listData}
            pagination={{
              position: ["bottomCenter"],
              pageSize: 5,
              style: {
                display: "flex",
                justifyContent: "center",
              },
              className: "border-none rounded-lg",
            }}
          />
        </>
      )}
    </div>
  );
};

export default VendorTable;
