import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import { Radio, Space, Spin, Table, Tag, Popconfirm } from "antd";
import Image from "next/image";
import { DeleteOutlined, LoadingOutlined } from "@ant-design/icons";
import Link from "next/link";
import { userRepository } from "/repository/user";
import SearchTable from "/components/input/SearchTable";
import { mutate } from "swr";

const UserTable = () => {
  const [listdata, setListData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState("");

  const { data: dataUser } = userRepository.hooks.useUser(search);

  useEffect(() => {
    if (dataUser !== null && dataUser !== undefined) {
      setListData(dataUser.items);
      dataUser.items;

      setLoading(false);
    }
  }, [dataUser]);

  const deleteUser = async (id) => {
    await userRepository.manipulateData.deleteUser(id);
    await mutate(userRepository.url.user());
    message.success("Berhasil menghapus akun");
  };

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  const columns = [
    {
      title: "Profile",
      dataIndex: "photo",
      key: "photo",
      align: "center",
      width: "10%",
      className: "text-base text-text font-semibold",
      render: (img) => (
        <div className="flex justify-center">
          <img
            src={img}
            width={40}
            height={40}
            alt="profile"
            className="rounded-full"
          />
        </div>
      ),
    },
    {
      title: "Nama lengkap",
      dataIndex: "fullname",
      key: "fullname",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <Link href={`/admin/user/${record.id}`}>
          <span className="text-blue-500 hover:text-secondary duration-300 cursor-pointer select-none font-medium">
            {key}
          </span>
        </Link>
      ),
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      className: "text-base text-text font-semibold",
      render: (key) => <p className="text-black font-medium">{key}</p>,
    },
    {
      title: "Level pengguna",
      dataIndex: "role",
      key: "role",
      className: "text-base text-text font-semibold",
      render: (_, record) => (
        <div className="text-black font-medium">{record.role.roleName}</div>
      ),
      filters: [
        {
          text: <span>Admin</span>,
          value: "admin",
        },
        {
          text: <span>Pengguna</span>,
          value: "customer",
        },
      ],
      onFilter: (value, record) => record.role.roleName.startsWith(value),
      filterSearch: false,
      width: "15%",
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      filterSearch: false,
      className: "text-base text-text font-semibold",
      width: "15%",
      filters: [
        {
          text: <span>aktif</span>,
          value: "active",
        },
        {
          text: <span>tidak aktif</span>,
          value: "not active",
        },
      ],
      onFilter: (value, record) => record.status.startsWith(value),
      filterSearch: false,
      render: (status) => {
        let color = status.length <= 6 ? "green" : "yellow";
        let stat = status.length <= 6 ? "Aktif" : "menunggu konfirmasi";

        if (status === "not active") {
          color = "volcano";
          stat = "tidak aktif";
        }

        return (
          <span>
            <Tag
              color={color}
              className="border-none capitalize rounded-md"
              key={status}
            >
              {stat}
            </Tag>
          </span>
        );
      },
    },
    {
      title: "Aksi",
      className: "text-base text-text font-semibold",
      key: "action",
      width: "10%",
      render: (_, record) => (
        <Space size="middle">
          <Popconfirm
            title="Anda yakin akan menghapus akun ini?"
            onConfirm={async () => deleteUser(record.id)}
            okText="Ya"
            cancelText="Batal"
          >
            <DeleteOutlined className="text-xl text-main flex items-center" />
          </Popconfirm>
        </Space>
      ),
    },
  ];

  const onChange = (pagination, filters, sorter, extra) => {
    console.log("params", pagination, filters, sorter, extra);
  };

  return (
    <div className="bg-white shadow-lg rounded-2xl p-7 flex gap-4 flex-col">
      {loading ? (
        <div className="w-full h-full flex items-center justify-center">
          <Spin indicator={loadingIcon} />
        </div>
      ) : (
        <>
          <div className="flex flex-row justify-evenly">
            <div className="w-full flex items-center m-0">
              <h1 className="font-bold m-0 text-2xl capitalize">
                list pengguna
              </h1>
            </div>
            <div className="w-1/3 m-0">
              <SearchTable
                placeholder="cari pengguna"
                onChange={(e) => setSearch(e.target.value)}
              />
            </div>
          </div>
          <Table
            columns={columns}
            onChange={onChange}
            dataSource={listdata}
            pagination={{
              position: ["bottomCenter"],
              pageSize: 5,
              style: {
                display: "flex",
                alignItems: "center",
              },
              className: "border-none rounded-lg",
            }}
          />
        </>
      )}
    </div>
  );
};

export default UserTable;
