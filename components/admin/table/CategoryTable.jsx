import { DeleteOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons";
import {
  Form,
  Input,
  message,
  Modal,
  Popconfirm,
  Select,
  Space,
  Switch,
  Table,
  Tag,
} from "antd";
import { useForm } from "antd/lib/form/Form";
import { data } from "autoprefixer";
import { useEffect, useState } from "react";
import { mutate } from "swr";
import { categoryRepository } from "../../../repository/category";
import Button from "/components/input/Button";
import InputCo from "/components/input/Input";

const CategoryTable = () => {
  const [category, setCategory] = useState("");
  const [categories, setCategories] = useState();
  const [editCategory, setEditCategory] = useState({});
  const { Option } = Select;
  const [form] = useForm();

  const { data: dataCategory } = categoryRepository.hooks.useCategory();

  useEffect(() => {
    if (dataCategory !== null && data !== undefined) {
      setCategories(dataCategory?.data);
    }
  }, [dataCategory]);

  const columns = [
    {
      title: "Kategori",
      dataIndex: "categoryName",
      key: "categoryName",
      className: "text-base text-text font-semibold",
      render: (key) => <p className="text-black m-0 font-medium">{key}</p>,
    },
    {
      title: "Status",
      className: "text-base text-text font-semibold",
      dataIndex: "isActive",
      key: "isActive",
      render: (key) => {
        if (key == true) {
          return <Switch checked={key} />;
        } else {
          return <Switch checked={key} />;
        }
      },
    },
    {
      title: "Aksi",
      className: "text-base text-text font-semibold",
      key: "action",
      width: "15%",
      render: (_, record) => (
        <Space size="middle">
          <Popconfirm
            title="Anda yakin akan menghapus Kategori ini?"
            onConfirm={async () => deleteCategory(record.id)}
            okText="Ya"
            cancelText="Batal"
          >
            <DeleteOutlined className="text-xl text-main flex items-center" />
          </Popconfirm>
          <EditOutlined
            className="text-xl text-filled flex items-center"
            onClick={() => showModal(record)}
          />
          <Modal
            visible={visible}
            onOk={handleOk}
            confirmLoading={confirmLoading}
            onCancel={handleCancel}
            okText="Simpan"
            cancelText="Batal"
          >
            <Form form={form} layout="vertical">
              <Form.Item name={"id"} style={{ display: "none" }}>
                <Input
                  type="text"
                  className="w-full text-base shadow-sm bg-bg font-medium"
                />
              </Form.Item>
              <Form.Item
                name={"categoryName"}
                label={
                  <label className="text-base font-semibold text-black">
                    Kategori
                  </label>
                }
              >
                <Input
                  type="text"
                  className="w-full text-base shadow-sm bg-bg font-medium"
                />
              </Form.Item>
              <Form.Item
                name={"isActive"}
                label={
                  <label className="text-base font-semibold text-black">
                    Status
                  </label>
                }
              >
                <Select className="w-full text-base shadow-sm bg-bg font-medium">
                  <Option value={true}>aktif</Option>
                  <Option value={false}>tidak aktif</Option>
                </Select>
              </Form.Item>
            </Form>
          </Modal>
        </Space>
      ),
    },
  ];

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      categoryName: category,
    };

    await categoryRepository.manipulateData.createCategory(data);
    await mutate(categoryRepository.url.category());
    message.success("Berhasil menambahkan kategori");
  };

  const deleteCategory = async (id) => {
    await categoryRepository.manipulateData.deleteCategory(id);
    await mutate(categoryRepository.url.category());
    message.success("Berhasil menghapus kategori");
  };

  const but = (
    <>
      <div className="flex flex-row ">
        <PlusOutlined className="flex items-center text-white text-2xl" />
        <p className="m-0 text-base ml-5">Tambah</p>
      </div>
    </>
  );

  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const showModal = (record) => {
    form.setFieldsValue({
      categoryName: record.categoryName,
      isActive: record.isActive,
      id: record.id,
    });
    setVisible(true);
  };

  const handleOk = async () => {
    const data = form.getFieldsValue();
    const id = data.id;
    setConfirmLoading(true);
    await categoryRepository.manipulateData.updateCategory(id, data);
    await mutate(categoryRepository.url.category());
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
      message.success("Kategori berhasil di edit");
    }, 2000);
  };

  const handleCancel = () => {
    console.log("Clicked cancel button");
    setVisible(false);
  };

  return (
    <>
      <div className="bg-white flex shadow-lg flex-col w-auto justify-center p-7 rounded-2xl ">
        <div>
          <h2 className="font-bold text-xl mb-7">List Kategori</h2>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="flex flex-row gap-3">
            <InputCo
              onChange={(e) => setCategory(e.target.value)}
              required={true}
              placeholder="masukan kategori"
              className="bg-bg w-full text-black text-base focus:ring-2 focus:outline-none shadow-md rounded-lg p-[14px] placeholder:text-text placeholder:font-medium "
            />
            <div className="w-1/2">
              <Button
                name={but}
                width="w-full px-[14px] mt-0 justify-evenly bg-blue-500 focus:none"
              />
            </div>
          </div>
        </form>
        <Table
          columns={columns}
          dataSource={categories}
          pagination={{
            pageSize: 5,
          }}
        />
      </div>
    </>
  );
};

export default CategoryTable;
