import {
  ArrowLeftOutlined,
  CloseOutlined,
  EditOutlined,
  LeftOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import Image from "next/image";
import Link from "next/link";
import Input from "/components/input/Input";
import Ava from "/public/avatar/avatar.png";
import Pack from "/public/image/pack.png";
import Coba from "/public/hero/login/login.png";
import { message, Spin, Table, Tag, Timeline } from "antd";
import Img from "/public/image/pack.png";
import { useEffect, useState } from "react";
import { orderRepository } from "../../../repository/order";
import moment from "moment";
import "moment/locale/id";
import { formatRupiah } from "../../../helper/currencyFormat";
import { useRouter } from "next/router";

const DetailOrder = (props) => {
  const id = props.param;
  const [orderItems, setOrderItems] = useState({});
  const [orderDetail, setOrderDetail] = useState([]);
  const [userPayment, setUserPayment] = useState([]);
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState({});
  const [vendor, setVendor] = useState({});

  const router = useRouter();

  const { data: dataOrder } = orderRepository.hooks.useOrderDetail(id);

  useEffect(() => {
    if (dataOrder !== null && dataOrder !== undefined) {
      setOrderItems(dataOrder?.order);

      setUser(dataOrder.order.user);
      setVendor(dataOrder.order.vendor);

      if (Array.isArray(dataOrder?.orderDetail)) {
        setOrderDetail(dataOrder?.orderDetail);
        setUserPayment(dataOrder?.userPayment);
        setLoading(false);
      }
    }
  }, [dataOrder, orderDetail, orderItems]);

  const countDays = (startDate, endDate) => {
    const start = moment(startDate, "YYYY-MM-DD");
    const end = moment(endDate, "YYYY-MM-DD");
    const date = moment.duration(end.diff(start)).asDays();
    return date + 1;
  };

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  const columns = [
    {
      title: "Nama paket layanan",
      dataIndex: "pack",
      key: "pack",
      className: "text-sm text-text font-normal",
      render: (key, record) => (
        <span className="text-black m-0">{record.pack.name}</span>
      ),
    },
    {
      title: "Kategori",
      dataIndex: "category",
      key: "category",
      className: "text-sm text-text font-normal",
      render: (key, record) => (
        <p className="text-black m-0">{record.pack.category.categoryName}</p>
      ),
    },
    {
      title: "tanggal mulai",
      dataIndex: "startDate",
      key: "startDate",
      className: "text-sm text-text font-normal",
      render: (key) => (
        <p className="text-black m-0">
          {moment(key).format("YYYY-MM-DD HH:mm")}
        </p>
      ),
    },
    {
      title: "tanggal selesai",
      dataIndex: "endDate",
      key: "endDate",
      className: "text-sm text-text font-normal",
      render: (key) => (
        <p className="text-black m-0">
          {moment(key).format("YYYY-MM-DD HH:mm")}
        </p>
      ),
    },
    {
      title: "jumlah pekerja",
      dataIndex: "handymanQty",
      key: "handymanQty",
      className: "text-sm text-text font-normal",
      render: (key) => <p className="text-black m-0">{key} orang</p>,
    },
    {
      title: "jumlah hari",
      dataIndex: "amountDay",
      key: "amountDay",
      className: "text-sm text-text font-normal",
      render: (key, record) => {
        let days;
        days = countDays(record.startDate, record.endDate);
        return <p className="text-black m-0">{days} hari</p>;
      },
    },
    {
      title: "harga layanan",
      dataIndex: "price",
      key: "price",
      className: "text-sm text-text font-normal",
      render: (key) => <p className="text-black m-0">{formatRupiah(key)}</p>,
    },
  ];

  const handleClickApprove = async () => {
    try {
      const id = orderItems.id;
      let status;
      if (orderItems.statusToUser == "hold") {
        status = { status: "done" };
      }

      await orderRepository.manipulateData.approveUserOrder(id, status);
      router.push("/admin/order");
      message.success("pesanan diselesaikan admin");
    } catch (e) {
      console.log(e);
      message.error("gagal mengkonfirmasi");
    }
  };

  return (
    <>
      {loading ? (
        <div className="w-full h-full flex items-center justify-center">
          <Spin indicator={loadingIcon} />
        </div>
      ) : (
        <div className="flex flex-row gap-9">
          <div className="py-9 px-8 flex flex-col gap-7 rounded-lg w-3/4 bg-white drop-shadow-md">
            <div className="w-full flex flex-row justify-between">
              <div>
                <Link href={`/admin/order`}>
                  <div className="flex group cursor-pointer bg-transparent rounded-lg  transition-all items-center flex-row justify-center gap-x-4 py-2 px-4">
                    <ArrowLeftOutlined className="text-text group-hover:text-yellow-500 duration-300 text-base flex items-center" />
                    <h2 className="capitalize m-0 text-text group-hover:text-yellow-500 duration-300 text-base font-semibold">
                      kembali
                    </h2>
                  </div>
                </Link>
              </div>

              <div className="flex flex-row gap-x-4 justify-between">
                {orderItems.statusToUser == "hold" ? (
                  <button
                    onClick={handleClickApprove}
                    className="flex bg-red-500 rounded-lg hover:drop-shadow-lg transition-all items-center flex-row justify-center gap-x-4 py-2 px-4"
                  >
                    <CloseOutlined className="text-white text-base flex items-center" />
                    <h2 className="capitalize m-0 text-white text-base font-semibold">
                      Selesaikan Pesanan
                    </h2>
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
            <div className="flex flex-row gap-5">
              <div className="w-full flex-col flex gap-5">
                <div className="w-full">
                  <h3 className="m-0 text-text text-sm">Kode pesanan</h3>
                  <h2 className="text-redText font-bold text-base">
                    {orderItems.orderCode}
                  </h2>
                </div>
                <div className="w-full">
                  <h3 className="m-0 text-text text-sm">
                    Tanggal Pesanan dibuat:
                  </h3>
                  <h2 className=" font-normal text-base">
                    {moment(orderItems.createdAt).format("YYYY-MM-DD HH:mm")}
                  </h2>
                </div>
                <div className="w-full">
                  <h3 className="m-0 text-text text-sm">
                    Alamat tujuan layanan:
                  </h3>
                  <h2 className=" font-normal text-base">
                    {orderItems.locationService}
                  </h2>
                </div>
              </div>
              <div className="w-full flex-col flex gap-5">
                <div className="w-full">
                  <h3 className="m-0 text-text text-sm">Nama Pelanggan:</h3>
                  <h2 className=" font-normal text-base">{user.fullname}</h2>
                </div>
                <div className="w-full">
                  <h3 className="m-0 text-text text-sm">Tipe Pembayaran :</h3>
                  <h2 className=" font-normal text-base">
                    {orderItems.paymentType == "fullpayment"
                      ? "pembayaran penuh"
                      : orderItems.paymentType == "dp_lastpayment"
                      ? "DP + pembayaran penuh"
                      : orderItems.paymentType == "dp_payment1_payment2"
                      ? "DP + 2 Pembayaran"
                      : ""}
                  </h2>
                </div>
                <div className="w-full">
                  <h3 className="m-0 text-text text-sm">Luas:</h3>
                  <h2 className=" font-normal text-base">
                    {orderItems.serviceArea} M<sup>2</sup>
                  </h2>
                </div>
              </div>
              <div className="w-full flex-col flex gap-5">
                <div className="w-full">
                  <h3 className="m-0 text-text text-sm">Status pesanan:</h3>
                  <h2 className=" font-normal text-base">
                    {orderItems.statusToUser == "pending" ? (
                      <span>
                        <Tag
                          color="yellow"
                          className="border-none capitalize rounded-md"
                        >
                          menunggu konfirmasi
                        </Tag>
                      </span>
                    ) : orderItems.statusToUser == "hold" ? (
                      <span>
                        <Tag
                          color="red"
                          className="border-none capitalize rounded-md"
                        >
                          ditahan
                        </Tag>
                      </span>
                    ) : orderItems.statusToUser == "done" ? (
                      <span>
                        <Tag
                          color="gray"
                          className="border-none capitalize rounded-md"
                        >
                          selesai
                        </Tag>
                      </span>
                    ) : orderItems.statusToUser == "in progress" ? (
                      <span>
                        <Tag
                          color="green"
                          className="border-none capitalize rounded-md"
                        >
                          dalam proses
                        </Tag>
                      </span>
                    ) : orderItems.statusToUser == "first paid done" ? (
                      <span>
                        <Tag
                          color="blue"
                          className="border-none capitalize rounded-md"
                        >
                          awal pembayaran
                        </Tag>
                      </span>
                    ) : orderItems.statusToUser == "done by vendor" ? (
                      <span>
                        <Tag
                          color="green"
                          className="border-none capitalize rounded-md"
                        >
                          diselesaikan vendor
                        </Tag>
                      </span>
                    ) : (
                      ""
                    )}
                  </h2>
                </div>
                <div className="w-full">
                  <h3 className="m-0 text-text text-sm">
                    Status pembayaran vendor:
                  </h3>

                  <h2 className=" font-normal text-base">
                    {orderItems.statusToVendor == "unpaid" ? (
                      <span>
                        <Tag
                          color="red"
                          className="border-none capitalize rounded-md"
                        >
                          belum dibayar
                        </Tag>
                      </span>
                    ) : orderItems.statusToVendor ==
                      "waiting vendor confirm" ? (
                      <span>
                        <Tag
                          color="yellow"
                          className="border-none capitalize rounded-md"
                        >
                          menunggu konfirmasi vendor
                        </Tag>
                      </span>
                    ) : orderItems.statusToVendor == "done" ? (
                      <span>
                        <Tag
                          color="gray"
                          className="border-none capitalize rounded-md"
                        >
                          selesai
                        </Tag>
                      </span>
                    ) : (
                      <span>
                        <Tag
                          color="green"
                          className="border-none capitalize rounded-md"
                        >
                          Sudah dibayar admin
                        </Tag>
                      </span>
                    )}
                  </h2>
                </div>
                <div className="w-full">
                  <h3 className="m-0 text-text text-sm">Nama vendor:</h3>
                  <h2 className=" font-normal text-base">{vendor.name}</h2>
                </div>
              </div>
            </div>
            {loading ? (
              "loading..."
            ) : (
              <Table
                columns={columns}
                dataSource={orderDetail}
                pagination={false}
              />
            )}
            <div className="flex justify-end">
              <h1 className="font-bold float-right text-base m-0">
                Total: <span>{formatRupiah(orderDetail[0]?.totalPrice)}</span>
              </h1>
            </div>
            <hr className="h-[2px] " />
            <div>
              <h3 className="m-0 font-semibold text-text">Note:</h3>
              <h4 className="m-0 font-normal text-base">
                {orderDetail[0]?.notes}
              </h4>
            </div>
          </div>
          <div className="flex flex-col gap-6">
            <div className="p-9 bg-white flex flex-col gap-4 rounded-lg shadow-md">
              <h1 className="font-semibold text-xl m-0">
                status pembayaran pesanan
              </h1>
              <Timeline>
                {userPayment.map((uPay) => {
                  return (
                    <Timeline.Item>
                      <div>
                        <p className="text-sm m-0 font-bold">
                          {uPay.paymentCode}
                        </p>
                        <p className="font-medium text-sm m-0">
                          {uPay.paymentType == "fullpayment"
                            ? "Pembayaran Penuh"
                            : uPay.paymentType == "payment1"
                            ? "Pembayaran 1"
                            : uPay.paymentType == "payment2"
                            ? "pembayaran 2"
                            : uPay.paymentType == "lastpayment"
                            ? "Pembayaran Terakhir"
                            : "DP"}{" "}
                          |{" "}
                          {uPay.paymentDate
                            ? moment(uPay.paymentDate).format("YYYY-MM-DD")
                            : ""}
                        </p>
                        <p className="font-light text-sm m-0">
                          Pembayaran sejumlah {formatRupiah(uPay.paymentAmount)}
                        </p>
                        <p className="font-light text-sm m-0">
                          {uPay.status == "done" ? (
                            <Tag
                              color="grey"
                              className="border-none capitalize rounded-md"
                            >
                              sudah dibayar ke vendor
                            </Tag>
                          ) : uPay.status == "hold" ? (
                            <Tag
                              color="red"
                              className="border-none capitalize rounded-md"
                            >
                              pembayaran terlambat
                            </Tag>
                          ) : uPay.status == "paid" ? (
                            <Tag
                              color="green"
                              className="border-none capitalize rounded-md"
                            >
                              sudah dibayar ke admin
                            </Tag>
                          ) : uPay.status == "unpaid" ? (
                            <Tag
                              color="yellow"
                              className="border-none capitalize rounded-md"
                            >
                              belum dibayar
                            </Tag>
                          ) : (
                            <Tag
                              color="geekblue"
                              className="border-none capitalize rounded-md"
                            >
                              menunggu konfirmasi admin
                            </Tag>
                          )}
                        </p>
                      </div>
                    </Timeline.Item>
                  );
                })}
              </Timeline>
            </div>
            <div className="bg-white rounded-lg p-6 shadow-md">
              <h1 className="font-bold text-base m-0">Desain arsitektur</h1>
              <div className="rounded-lg bg-bg p-10 flex justify-center items-center">
                <img
                  alt=""
                  width={200}
                  height={100}
                  src={orderDetail[0]?.design}
                  className="rounded-md"
                />
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default DetailOrder;
