import {
  CheckOutlined,
  CloseOutlined,
  EyeFilled,
  PlusOutlined,
} from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Input,
  message,
  Row,
  Select,
  Table,
  Upload,
} from "antd";
import { useForm } from "antd/lib/form/Form";
import moment from "moment";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { mutate } from "swr";
import { appConfig } from "../../../config/app";
import { formatRupiah } from "../../../helper/currencyFormat";
import { bankRepository } from "../../../repository/bank";
import { paymentVendorRepository } from "../../../repository/payment-vendor";
import Img from "/public/image/transaksi.png";

const CreatePaymentVendor = () => {
  const [form] = useForm();
  const [form2] = useForm();
  const { Option } = Select;
  const router = useRouter();

  const columns = [
    {
      title: "Kode Pembayaran",
      dataIndex: "paymentCode",
      key: "paymentCode",
      className: "text-base text-text font-semibold",
      width: "20%",
      render: (key) => <span className="text-black font-medium">{key}</span>,
    },
    {
      title: "Kode Pemesanan",
      dataIndex: "orderCode",
      key: "orderCode",
      width: "20%",
      className: "text-base text-text font-semibold",
      render: (_, record) => (
        <span className="text-justBlack font-medium">
          {record.order.orderCode}
        </span>
      ),
    },
    {
      title: "Tipe Pembayaran",
      dataIndex: "paymentType",
      key: "paymentType",
      width: "20%",
      className: "text-base text-text font-semibold",
      render: (key) => {
        let typePayment;
        key == "fullpayment"
          ? (typePayment = "Pembayaran Penuh")
          : key == "dp"
          ? (typePayment = "DP")
          : key == "payment1"
          ? (typePayment = "Pembayaran 1")
          : key == "payment2"
          ? (typePayment = "Pembayaran 2")
          : key == "lastpayment"
          ? (typePayment = "Pembayaran Terakhir")
          : "";

        return <span className="text-black font-medium">{typePayment}</span>;
      },
    },
    {
      title: "Tanggal Pembayaran",
      key: "paymentDate",
      dataIndex: "paymentDate",
      filterSearch: false,
      className: "text-base text-text font-semibold",
      width: "20%",
      render: (key) => (
        <span className="text-black font-medium">
          {moment(key).format("YYYY-MM-DD hh:mm")}
        </span>
      ),
    },
    {
      title: "Total Pembayaran",
      dataIndex: "paymentAmount",
      align: "right",
      key: "paymentAmount",
      className: "text-base text-text font-semibold",
      width: "20%",
      render: (key) => (
        <span className="text-black font-medium">{formatRupiah(key)}</span>
      ),
    },
  ];

  const [disabled, setDisabled] = useState(false);

  const [roleVendor, setRoleVendor] = useState("vendor");
  const [roleAdmin, setRoleAdmin] = useState("admin");

  const [bankVendor, setBankVendor] = useState([]);
  const [bankAdmin, setBankAdmin] = useState([]);

  const [bankVendorId, setBankVendorId] = useState("");
  const [bankAdminId, setBankAdminId] = useState("");

  const [paymentUser, setpaymentUser] = useState([]);
  const [result, setResult] = useState(null);

  const [fileList, setFileList] = useState([]);

  const { data: dataBankVendor } = bankRepository.hooks.useAllBank(roleVendor);
  const { data: dataBankAdmin } = bankRepository.hooks.useAllBank(roleAdmin);

  const { data: detailBankVendor } =
    bankRepository.hooks.useDetailBank(bankVendorId);

  const { data: detailBankAdmin } =
    bankRepository.hooks.useDetailBank(bankAdminId);

  useEffect(() => {
    if (dataBankVendor !== undefined && dataBankVendor !== null) {
      setBankVendor(dataBankVendor?.data);
    }

    if (dataBankAdmin !== undefined && dataBankAdmin !== null) {
      setBankAdmin(dataBankAdmin?.data);
    }

    if (detailBankVendor?.data || detailBankAdmin?.data) {
      setForm();
    }
  }, [dataBankVendor, dataBankAdmin, detailBankVendor, detailBankAdmin]);

  const uploadButton = (
    <div>
      <PlusOutlined />

      <div
        style={{
          marginTop: 8,
        }}
      >
        Upload
      </div>
    </div>
  );

  const onChangeBankVendor = (value) => {
    setBankVendorId(value);
  };

  const onChangeBankAdmin = (value) => {
    setBankAdminId(value);
  };

  const handleImage = async (args) => {
    const file = args.file;
    console.log(file);
    try {
      const processUpload =
        await paymentVendorRepository.manipulateData.uploadVendorPaymentProof(
          file
        );
      setFileList([
        {
          url:
            appConfig.apiUrl +
            "/vendor-payment/proof/" +
            processUpload.body.proof,
          name: "proof",
        },
      ]);
      message.success("berhasil upload");
    } catch (e) {
      console.log(e);
      message.error("gagal upload");
    }
  };

  const setForm = async () => {
    form.setFieldsValue({
      vendorBankAccountName: await detailBankVendor?.data?.bank?.accountName,
      vendorBankAccount: await detailBankVendor?.data?.bank?.accountNumber,
      vendorBankName: await detailBankVendor?.data?.bank?.bankName,
      vendorName: await detailBankVendor?.data?.vendor?.name,
      adminBankName: await detailBankAdmin?.data?.bankName,
      adminBankAccount: await detailBankAdmin?.data?.accountNumber,
      adminBankAccountName: await detailBankAdmin?.data?.accountName,
      adminName: await detailBankAdmin?.data?.user?.fullname,
    });
  };

  const getData = async () => {
    const data = form.getFieldsValue();

    try {
      const getData =
        await paymentVendorRepository.manipulateData.getDataCreatePaymentVendor(
          data
        );
      await mutate(bankRepository.url.detailBank(bankVendorId));
      await mutate(bankRepository.url.detailBank(bankAdminId));

      setDisabled(true);
      form2.setFieldsValue({
        paymentVendor: formatRupiah(getData?.body?.paymentVendor),
        adminCharge: formatRupiah(getData?.body?.adminCharge),
        total: formatRupiah(getData.body?.total),
      });

      const paymentUser = getData?.body?.userPayment;
      if (paymentUser.length == 0) {
        message.warning("data tidak ada");
      } else {
        console.log(paymentUser);
        setResult(getData?.body);
        setpaymentUser(getData.body?.userPayment);
        message.success("Berhasil mendapatkan data");
      }
    } catch (e) {
      console.log(e);
    }
  };

  const onFinish = async () => {
    try {
      const data = {
        ...result,
        proof: fileList[0]?.url,
      };

      await paymentVendorRepository.manipulateData.createVendorPayment(data);
      router.push("/admin/payment/vendor");
    } catch (error) {
      message.error(error?.response?.body?.message[0]);
    }
  };

  return (
    <>
      <div className="w-full flex flex-col rounded-xl bg-white gap-y-4 drop-shadow-lg p-8">
        <div className="flex flex-col gap-y-2">
          <Form
            form={form}
            size="large"
            layout="vertical"
            className="w-full"
            onFinish={getData}
            style={{
              display: "flex",
              flexDirection: "column",
              gap: "29px 0",
            }}
          >
            <Row
              gutter={24}
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Col span={6}>
                <Form.Item name={"vendorBankName"} style={{ display: "none" }}>
                  <Input
                    type="text"
                    className="w-full text-base shadow-sm bg-bg font-medium"
                  />
                </Form.Item>
                <Form.Item
                  name={"vendorBankId"}
                  label={
                    <label className="m-0 text-base font-semibold capitalize">
                      bank vendor
                    </label>
                  }
                  style={{
                    margin: "0px",
                  }}
                >
                  <Select
                    showSearch
                    placeholder="pilih bank vendor"
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      option.children
                        .toLowerCase()
                        .includes(input.toLowerCase())
                    }
                    onChange={onChangeBankVendor}
                    disabled={disabled}
                  >
                    {bankVendor?.map((bank) => {
                      return (
                        <Option value={bank.bank_id}>{bank.bankVendor}</Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item
                  name={"vendorBankAccount"}
                  label={
                    <label className="m-0 text-base font-semibold capitalize">
                      nomor rekening vendor:
                    </label>
                  }
                  style={{
                    margin: "0px",
                  }}
                >
                  <Input
                    type="text"
                    readOnly
                    placeholder="nomor rekening"
                    className="w-full text-base drop-shadow-md outline-none border-none"
                  />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item
                  name={"vendorBankAccountName"}
                  label={
                    <label className="m-0 text-base font-semibold capitalize">
                      nama rekening vendor:
                    </label>
                  }
                  style={{
                    margin: "0px",
                  }}
                >
                  <Input
                    type="text"
                    readOnly
                    placeholder="nama rekening bank"
                    className="w-full text-base drop-shadow-md outline-none border-none"
                  />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item
                  name={"vendorName"}
                  label={
                    <label className="m-0 text-base font-semibold capitalize">
                      nama vendor:
                    </label>
                  }
                  style={{
                    margin: "0px",
                  }}
                >
                  <Input
                    type="text"
                    placeholder="nama vendor"
                    readOnly
                    className="w-full text-base drop-shadow-md outline-none border-none"
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row
              gutter={24}
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Col span={6}>
                <Form.Item name={"adminBankName"} style={{ display: "none" }}>
                  <Input
                    type="text"
                    className="w-full text-base shadow-sm font-medium"
                  />
                </Form.Item>
                <Form.Item
                  name={"adminBankId"}
                  label={
                    <label className="m-0 text-base font-semibold capitalize">
                      bank admin
                    </label>
                  }
                  style={{
                    margin: "0px",
                  }}
                >
                  <Select
                    showSearch
                    placeholder="pilih bank admin"
                    optionFilterProp="children"
                    size="large"
                    filterOption={(input, option) =>
                      option.children
                        .toLowerCase()
                        .includes(input.toLowerCase())
                    }
                    onChange={onChangeBankAdmin}
                    disabled={disabled}
                  >
                    {bankAdmin.map((bank) => {
                      return (
                        <Option key={bank.bank_id} value={bank.bank_id}>
                          {bank.bank_bank_name}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item
                  name={"adminBankAccount"}
                  label={
                    <label className="m-0 text-base font-semibold capitalize">
                      nomor rekening admin:
                    </label>
                  }
                  style={{
                    margin: "0px",
                  }}
                >
                  <Input
                    type="text"
                    readOnly
                    placeholder="nomor rekening"
                    className="w-full text-base drop-shadow-md outline-none border-none"
                  />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item
                  name={"adminBankAccountName"}
                  label={
                    <label className="m-0 text-base font-semibold capitalize">
                      nama rekening admin:
                    </label>
                  }
                  style={{
                    margin: "0px",
                  }}
                >
                  <Input
                    type="text"
                    readOnly
                    placeholder="nama rekening bank"
                    className="w-full text-base drop-shadow-md outline-none border-none"
                  />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item
                  name={"adminName"}
                  label={
                    <label className="m-0 text-base font-semibold capitalize">
                      nama admin
                    </label>
                  }
                  style={{
                    margin: "0px",
                  }}
                >
                  <Input
                    type="text"
                    placeholder="nama vendor"
                    readOnly
                    className="w-full text-base drop-shadow-md outline-none border-none"
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col
                span={24}
                style={{
                  display: "flex",
                  justifyContent: "end",
                }}
              >
                <Button
                  type="primary"
                  htmlType="submit"
                  icon={<EyeFilled />}
                  size="large"
                  disabled={disabled}
                  className="text-sm font-semibold w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-main py-[11px] px-[21px] rounded-lg border-none flex items-center"
                >
                  Tampilkan Data
                </Button>
              </Col>
            </Row>
          </Form>
        </div>
        <div className="w-full flex flex-row gap-x-8">
          <div className="flex-col w-1/3 flex gap-y-2">
            <h1 className="m-0 text-base font-semibold">Bukti Pembayaran:</h1>
            <div className="w-full rounded-lg flex h-full justify-center items-center bg-bg">
              <Form.Item name={"photo"}>
                <Upload
                  name="photo"
                  listType="picture-card"
                  className="avatar-uploader"
                  onChange={(args) => handleImage(args)}
                  fileList={fileList}
                  beforeUpload={() => false}
                  onRemove={() => setFileList([])}
                  maxCount={1}
                >
                  {uploadButton}
                </Upload>
              </Form.Item>
            </div>
          </div>
          <div className="w-full">
            <Table
              columns={columns}
              dataSource={paymentUser}
              pagination={{
                position: ["bottomCenter"],
                pageSize: 5,
                style: {
                  display: "flex",
                  alignItems: "center",
                },
                className: "border-none rounded-lg",
              }}
            />
          </div>
        </div>
        <div className="flex flex-row justify-end">
          <Form
            form={form2}
            layout="horizontal"
            style={{
              display: "flex",
              flexDirection: "column",
              gap: "15px 0",
              width: "30%",
            }}
            onFinish={onFinish}
          >
            <Row gutter={24}>
              <Col span={24}>
                <Form.Item
                  name={"paymentVendor"}
                  label={
                    <label className="m-0 text-base font-semibold capitalize">
                      total pendapatan vendor
                    </label>
                  }
                  style={{
                    margin: "0px",
                  }}
                >
                  <Input
                    type="text"
                    readOnly
                    placeholder="total pendapatan vendor"
                    className="w-full text-base placeholder:text-text placeholder:text-base placeholder:font-bold p-0 bg-transparent outline-none border-none rounded-lg font-bold"
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col span={24}>
                <Form.Item
                  name={"adminCharge"}
                  label={
                    <label className="m-0 text-base font-semibold capitalize">
                      total biaya admin
                    </label>
                  }
                  style={{
                    margin: "0px",
                  }}
                >
                  <Input
                    type="text"
                    readOnly
                    placeholder="biaya admin"
                    className="w-full text-base placeholder:text-text placeholder:text-base placeholder:font-bold p-0 bg-transparent outline-none border-none rounded-lg font-bold"
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col span={24}>
                <Form.Item
                  name={"total"}
                  label={
                    <label className="m-0 text-base font-semibold capitalize">
                      total seluruh
                    </label>
                  }
                  style={{
                    margin: "0px",
                  }}
                >
                  <Input
                    type="text"
                    readOnly
                    placeholder="total"
                    className="w-full font-bold text-base placeholder:text-text placeholder:text-base placeholder:font-bold p-0 bg-transparent outline-none border-none rounded-lg"
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row
              gutter={24}
              style={{
                display: "flex",
                flexDirection: "row",
                marginTop: "15px",
              }}
            >
              <Col span={12}>
                <Link href={"/admin/payment/vendor"}>
                  <Button
                    icon={<CloseOutlined />}
                    type="primary"
                    size="large"
                    className="w-full text-sm justify-center font-semibold text-white cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-main py-[11px] px-[21px] rounded-lg border-none flex items-center"
                  >
                    Batal
                  </Button>
                </Link>
              </Col>
              <Col span={12}>
                <Button
                  type="primary"
                  htmlType="submit"
                  icon={<CheckOutlined />}
                  size="large"
                  className="text-sm w-full justify-center font-semibold cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-blue-500 py-[11px] px-[21px] rounded-lg border-none flex items-center"
                >
                  Simpan
                </Button>
              </Col>
            </Row>
          </Form>
        </div>
      </div>
    </>
  );
};

export default CreatePaymentVendor;
