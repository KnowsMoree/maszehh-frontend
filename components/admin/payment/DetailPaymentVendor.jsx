import {
  ArrowLeftOutlined,
  EditOutlined,
  LeftOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import Image from "next/image";
import Link from "next/link";
import Input from "/components/input/Input";
import Ava from "/public/avatar/avatar.png";
import Pack from "/public/image/pack.png";
import Coba from "/public/hero/login/login.png";
import { Spin, Table, Tag, Timeline } from "antd";
import Img from "/public/image/pack.png";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { paymentVendorRepository } from "../../../repository/payment-vendor";
import { formatRupiah } from "../../../helper/currencyFormat";
import moment from "moment";
import "moment/locale/id";

const DetailPaymentOrder = (props) => {
  const id = props.param;

  const [detailPayment, setDetailPayment] =
    useState({});
  const [loading, setLoading] = useState(true);

  const { data: detailPaymentVendor } =
    paymentVendorRepository.hooks.useDetailVendorPayment(
      id
    );

  useEffect(() => {
    if (
      detailPaymentVendor !== undefined &&
      detailPaymentVendor !== null
    ) {
      setDetailPayment(detailPaymentVendor?.data);
      console.log(detailPaymentVendor?.data);

      setTimeout(() => {
        setLoading(false);
      }, 500);
    }
  }, [detailPaymentVendor]);

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  return (
    <>
      <div className="flex justify-center flex-row gap-9">
        <div className="bg-white w-3/5 gap-4 rounded-lg p-8 flex-col flex shadow-md">
          {loading ? (
            <div className="w-full h-full flex items-center justify-center">
              <Spin indicator={loadingIcon} />
            </div>
          ) : (
            <>
              <div className="w-full flex flex-row justify-between">
                <div>
                  <Link
                    href={`/admin/payment/vendor`}>
                    <div className="flex group cursor-pointer bg-transparent rounded-lg  transition-all items-center flex-row justify-center gap-x-4 py-2 px-4">
                      <ArrowLeftOutlined className="text-text group-hover:text-yellow-500 duration-300 text-base flex items-center" />
                      <h2 className="capitalize m-0 text-text group-hover:text-yellow-500 duration-300 text-base font-semibold">
                        kembali
                      </h2>
                    </div>
                  </Link>
                </div>
              </div>
              <div className="flex w-full flex-row gap-10">
                <div className="w-full flex-col items-center flex gap-4">
                  <div className="w-full flex justify-center">
                    <img
                      alt=""
                      src={
                        detailPayment?.photoProof
                      }
                      width={300}
                      height={300}
                    />
                  </div>
                </div>
                <div className="w-full flex flex-row gap-x-7">
                  <div className="flex w-full flex-col gap-y-4">
                    <div>
                      <h3 className="text-text capitalize font-semibold">
                        kode pembayaran vendor:
                      </h3>
                      <h2 className="text-redText font-bold capitalize text-base">
                        {
                          detailPayment?.codeVendorPayment
                        }
                      </h2>
                    </div>
                    <div>
                      <h3 className="text-text capitalize font-semibold">
                        tanggal pembayaran dibuat:
                      </h3>
                      <h2 className="uppercase text-base">
                        {moment(
                          detailPayment?.createdAt
                        ).format(
                          "YYYY-MM-DD hh:mm"
                        )}
                      </h2>
                    </div>
                    <div>
                      <h3 className="text-text capitalize font-semibold">
                        tanggal pembayaran
                        dikonfirmasi:
                      </h3>
                      <h2 className=" capitalize text-base">
                        {detailPayment?.paymentDateConfirm !==
                        null
                          ? moment(
                              detailPayment?.paymentDateConfirm
                            ).format(
                              "YYYY-MM-DD hh:mm"
                            )
                          : "-"}
                      </h2>
                    </div>
                    <div>
                      <h3 className="text-text whitespace-nowrap break-normal capitalize font-semibold">
                        Nama vendor:
                      </h3>
                      <h2 className=" capitalize text-base">
                        {
                          detailPayment?.vendor
                            ?.name
                        }
                      </h2>
                    </div>
                    <div>
                      <h3 className="text-text whitespace-nowrap break-normal capitalize font-semibold">
                        bank vendor:
                      </h3>
                      <h2 className=" capitalize text-base">
                        {
                          detailPayment
                            ?.bankVendor?.bankName
                        }
                      </h2>
                    </div>
                    <div>
                      <h3 className="text-text break-normal whitespace-nowrap capitalize font-semibold">
                        nomor rekening vendor:
                      </h3>
                      <h2 className=" capitalize text-base">
                        {
                          detailPayment
                            ?.bankVendor
                            ?.accountNumber
                        }
                      </h2>
                    </div>
                    <div>
                      <h3 className="text-text capitalize font-semibold">
                        nama rekening vendor:
                      </h3>
                      <h2 className="text-base">
                        {
                          detailPayment
                            ?.bankVendor
                            ?.accountName
                        }
                      </h2>
                    </div>
                  </div>
                  <div className="flex w-full flex-col gap-y-4">
                    <div>
                      <h3 className="text-text capitalize font-semibold">
                        status pembayaran:
                      </h3>
                      <h2 className="font-bold capitalize text-base">
                        {detailPayment?.status ==
                        "waiting vendor confirm" ? (
                          <span>
                            <Tag
                              color="yellow"
                              className="border-none rounded-md">
                              menunggu konfirmasi
                              vendor
                            </Tag>
                          </span>
                        ) : detailPayment?.status ==
                          "done" ? (
                          <span>
                            <Tag
                              color="gray"
                              className="border-none rounded-md">
                              selesai
                            </Tag>
                          </span>
                        ) : (
                          <span>
                            <Tag
                              color="red"
                              className="border-none rounded-md">
                              ditolak
                            </Tag>
                          </span>
                        )}
                      </h2>
                    </div>
                    <div>
                      <h3 className="text-text capitalize font-semibold">
                        pembayaran vendor:
                      </h3>
                      <h2 className="font-bold capitalize text-base">
                        {formatRupiah(
                          detailPayment?.paymentVendor
                        )}
                      </h2>
                    </div>
                    <div>
                      <h3 className="text-text capitalize font-semibold">
                        biaya admin:
                      </h3>
                      <h2 className="font-bold capitalize text-base">
                        {formatRupiah(
                          detailPayment?.adminCharge
                        )}
                      </h2>
                    </div>
                    <div>
                      <h3 className="text-text capitalize font-semibold">
                        total pembayaran:
                      </h3>
                      <h2 className="font-bold capitalize text-base">
                        {formatRupiah(
                          detailPayment?.total
                        )}
                      </h2>
                    </div>
                    <div>
                      <h3 className="text-text capitalize font-semibold">
                        bank admin:
                      </h3>
                      <h2 className=" font-medium break-normal whitespace-nowrap capitalize text-base">
                        {
                          detailPayment?.bankAdmin
                            ?.bankName
                        }
                      </h2>
                    </div>
                    <div>
                      <h3 className="text-text capitalize font-semibold">
                        nomor rekening admin:
                      </h3>
                      <h2 className="break-normal whitespace-nowrap capitalize text-base">
                        {
                          detailPayment?.bankAdmin
                            ?.accountNumber
                        }
                      </h2>
                    </div>
                    <div>
                      <h3 className="text-text capitalize font-semibold">
                        nama rekening admin:
                      </h3>
                      <h2 className="text-base">
                        {
                          detailPayment?.bankAdmin
                            ?.accountName
                        }
                      </h2>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default DetailPaymentOrder;
