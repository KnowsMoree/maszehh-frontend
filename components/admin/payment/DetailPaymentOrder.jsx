import {
  ArrowLeftOutlined,
  CheckOutlined,
  CloseOutlined,
  CreditCardOutlined,
  LeftOutlined,
} from "@ant-design/icons";
import { message, Tag } from "antd";
import moment from "moment";
import "moment/locale/id";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { mutate } from "swr";
import { appConfig } from "../../../config/app";
import { formatRupiah } from "../../../helper/currencyFormat";
import { paymentUserRepository } from "../../../repository/payment-user";
import Img from "/public/image/transaksi.png";

const DetailPaymentOrder = (props) => {
  let id = props.id;
  const [detailPayment, setDetailPayment] =
    useState({});
  const router = useRouter();

  const { data: detailPaymentUser } =
    paymentUserRepository.hooks.useDetailPaymentUser(
      id
    );

  useEffect(() => {
    if (
      detailPaymentUser !== null &&
      detailPaymentUser !== undefined
    ) {
      setDetailPayment(detailPaymentUser?.data);
    }
  }, [detailPaymentUser]);

  const handleClickConfirm = async (value) => {
    let status = {};
    if (value == "rejected") {
      status = { status: "rejected" };
    } else if (value == "paid") {
      status = { status: "paid" };
    }

    try {
      await paymentUserRepository.manipulateData.confirmProofPayment(
        id,
        status
      );
      await mutate(
        paymentUserRepository.url.allPaymentUser()
      );
      router.push("/admin/payment/order");
      message.success(
        "Pembayaran telah dikonfirmasi"
      );
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <div className="bg-white gap-4 rounded-lg p-8 flex-col flex shadow-md">
        <div className="w-full flex flex-row justify-between">
          <div>
            <Link href={`/admin/payment/order`}>
              <div className="flex group cursor-pointer bg-transparent rounded-lg  transition-all items-center flex-row justify-center gap-x-4 py-2 px-4">
                <ArrowLeftOutlined className="text-text group-hover:text-yellow-500 duration-300 text-base flex items-center" />
                <h2 className="capitalize m-0 text-text group-hover:text-yellow-500 duration-300 text-base font-semibold">
                  kembali
                </h2>
              </div>
            </Link>
          </div>
          {detailPayment?.status ==
          "waiting admin confirm" ? (
            <div className="flex flex-row gap-x-3">
              <button
                onClick={() => {
                  handleClickConfirm("rejected");
                }}
                className="flex bg-red-500 rounded-lg hover:drop-shadow-lg transition-all items-center flex-row justify-center gap-x-4 py-2 px-4">
                <CloseOutlined className="text-white text-base flex items-center" />
                <h2 className="capitalize m-0 text-white text-base font-semibold">
                  Tolak
                </h2>
              </button>
              <button
                onClick={() => {
                  handleClickConfirm("paid");
                }}
                className="flex bg-blue-500 rounded-lg hover:drop-shadow-lg transition-all items-center flex-row justify-center gap-x-4 py-2 px-4">
                <CheckOutlined className="text-white text-base flex items-center" />
                <h2 className="capitalize m-0 text-white text-base font-semibold">
                  Terima
                </h2>
              </button>
            </div>
          ) : (
            ""
          )}
        </div>
        <div className="flex w-full flex-row gap-6">
          <div className="w-full flex-col min-h-fit flex gap-4">
            <div className="min-w-fit min-h-fit">
              <img
                alt=""
                src={
                  detailPayment?.photoProof ===
                  null
                    ? appConfig.apiUrl +
                      `/payment-user/proof-payment/no_data.png`
                    : detailPayment?.photoProof
                }
                width={200}
                height={200}
                className="bg-slate-200  rounded-md"
              />
            </div>
          </div>
          <div className="w-full flex flex-row gap-x-7">
            <div className="flex w-full flex-col gap-y-4">
              <div>
                <h3 className="text-text capitalize font-semibold">
                  kode pembayaran:
                </h3>
                <h2 className="text-redText font-bold text-base uppercase">
                  {detailPayment.paymentCode}
                </h2>
              </div>
              <div>
                <h3 className="text-text capitalize font-semibold">
                  tipe pembayaran:
                </h3>
                <h2 className="text-redText font-bold capitalize text-base">
                  {detailPayment.paymentType ==
                  "fullpayment"
                    ? "Pembayaran Penuh"
                    : detailPayment.paymentType ==
                      "payment1"
                    ? "Pembayaran 1"
                    : detailPayment.paymentType ==
                      "payment2"
                    ? "pembayaran 2"
                    : detailPayment.paymentType ==
                      "lastpayment"
                    ? "Pembayaran Terakhir"
                    : "DP"}
                </h2>
              </div>
              <div>
                <h3 className="text-text capitalize font-semibold">
                  total pembayaran:
                </h3>
                <h2 className="font-bold capitalize text-base">
                  {formatRupiah(
                    detailPayment.paymentAmount
                  )}
                </h2>
              </div>
              <div>
                <h3 className="text-text capitalize font-semibold">
                  tanggal pembayaran:
                </h3>
                <h2 className=" capitalize text-base">
                  {detailPayment.paymentDate ==
                  null
                    ? "-"
                    : moment(
                        detailPayment.paymentDate
                      ).format(
                        "YYYY-MM-DD HH:mm"
                      )}
                </h2>
              </div>
              <div>
                <h3 className="text-text whitespace-nowrap break-normal capitalize font-semibold">
                  nama pelanggan:
                </h3>
                <h2 className=" capitalize text-base">
                  {detailPayment?.user?.fullname}
                </h2>
              </div>
            </div>
            <div className="flex w-full flex-col gap-y-4">
              <div>
                <h3 className="text-text capitalize font-semibold">
                  kode pesanan:
                </h3>
                <h2 className="text-redText font-bold capitalize text-base">
                  {
                    detailPayment?.order
                      ?.orderCode
                  }
                </h2>
              </div>
              <div>
                <h3 className="text-text capitalize font-semibold">
                  status pembayaran:
                </h3>
                <h2 className=" font-semibold break-normal whitespace-nowrap capitalize text-base">
                  {detailPayment?.status ==
                  "unpaid" ? (
                    <span>
                      <Tag
                        color="yellow"
                        className="border-none rounded-md">
                        belum dibayar
                      </Tag>
                    </span>
                  ) : detailPayment?.status ==
                    "done" ? (
                    <span>
                      <Tag
                        color="gray"
                        className="border-none rounded-md">
                        sudah dibayar ke vendor
                      </Tag>
                    </span>
                  ) : detailPayment?.status ==
                    "hold" ? (
                    <span>
                      <Tag
                        color="red"
                        className="border-none rounded-md">
                        pembayaran terlambat
                      </Tag>
                    </span>
                  ) : detailPayment?.status ==
                    "rejected" ? (
                    <span>
                      <Tag
                        color="red"
                        className="border-none rounded-md">
                        ditolak
                      </Tag>
                    </span>
                  ) : detailPayment?.status ==
                    "paid" ? (
                    <span>
                      <Tag
                        color="green"
                        className="border-none rounded-md">
                        sudah dibayar
                      </Tag>
                    </span>
                  ) : (
                    <span>
                      <Tag
                        color="geekblue"
                        className="border-none rounded-md">
                        menunggu konfirmasi admin
                      </Tag>
                    </span>
                  )}
                </h2>
              </div>
              <div>
                <h3 className="text-text capitalize font-semibold">
                  tenggat pembayaran:
                </h3>
                <h2 className="uppercase text-base">
                  {moment(
                    detailPayment.dueDate
                  ).format("YYYY-MM-DD HH:mm")}
                </h2>
              </div>
              <div>
                <h3 className="text-text capitalize font-semibold">
                  nama rekening admin:
                </h3>
                <h2 className="break-normal whitespace-nowrap capitalize text-base">
                  {
                    detailPayment?.bank
                      ?.accountName
                  }
                </h2>
              </div>
              <div>
                <h3 className="text-text capitalize font-semibold">
                  nomor rekening admin:
                </h3>
                <h2 className="break-normal whitespace-nowrap capitalize text-base">
                  {
                    detailPayment?.bank
                      ?.accountNumber
                  }
                </h2>
              </div>
              <div>
                <h3 className="text-text capitalize font-semibold">
                  nama bank admin:
                </h3>

                <h2 className="uppercase text-base">
                  {detailPayment?.bank?.bankName}
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DetailPaymentOrder;
