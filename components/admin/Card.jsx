import { RightOutlined } from "@ant-design/icons";
import Link from "next/link";
import React from "react";

const Card = (props) => {
  return (
    <>
      <Link href={props.href}>
        <div className="bg-white group hover:bg-main cursor-pointer w-[250px] h-[230px] hover:drop-shadow-lg transition-colors py-[30px] px-5 rounded-3xl">
          <RightOutlined className="float-right text-xl text-main group-hover:text-white " />
          {props.icon}
          <h2 className="m-0 font-semibold text-lg text-text group-hover:text-white">
            {props.title}
          </h2>
          <h1 className="m-0 font-bold text-4xl text-main group-hover:text-white">
            {props.count}
          </h1>
        </div>
      </Link>
    </>
  );
};

export default Card;
