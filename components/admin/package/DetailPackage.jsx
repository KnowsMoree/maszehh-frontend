import {
  ArrowLeftOutlined,
  EditOutlined,
  LeftOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import { Empty, Rate, Spin } from "antd";
import Image from "next/image";
import { useEffect, useState } from "react";
import { userRepository } from "/repository/user";
import Img from "/public/avatar/avatar.png";
import Input from "/components/input/Input";
import Link from "next/link";
import Avatar from "/public/avatar/avatar.png";
import Line from "/components/micro/Line";
import DetailImage from "/public/image/paket.png";
import { Pagination } from "antd";
import { packageRepository } from "../../../repository/package";
import { formatRupiah } from "../../../helper/currencyFormat";
import moment from "moment";
import "moment/locale/id";

const DetailUser = (props) => {
  const id = props?.param;
  const [loading, setLoading] = useState(true);

  const [pack, setPack] = useState({});
  const [review, setReview] = useState([]);

  const [total, setTotal] = useState(0);
  const [limit, setLimit] = useState(4);
  const [current, setCurrent] = useState(1);

  const { data: detailPackage } = packageRepository.hooks.usePackageDetail(id);
  const { data: dataReview } = packageRepository.hooks.usePackageReview({
    id,
    current,
    limit,
  });

  useEffect(() => {
    if (detailPackage !== undefined && detailPackage !== null) {
      console.log(detailPackage?.data);
      setPack(detailPackage?.data);
      setLoading(false);
    }

    if (dataReview !== null && dataReview !== undefined) {
      setReview(dataReview?.items);
      setTotal(dataReview?.meta?.totalItems);
    }
  }, [detailPackage, dataReview]);

  const onCurrentPage = (page) => {
    setCurrent(page);
  };

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  return (
    <>
      <div className="py-9 px-8 flex flex-col gap-7 rounded-lg w-3/4 bg-white drop-shadow-md">
        {loading ? (
          <div className="w-full h-full flex items-center justify-center">
            <Spin indicator={loadingIcon} />
          </div>
        ) : (
          <>
            <div className="w-full flex flex-row justify-between">
              <div>
                <Link href={`/admin/packages`}>
                  <div className="flex group cursor-pointer bg-transparent rounded-lg  transition-all items-center flex-row justify-center gap-x-4 py-2 px-4">
                    <ArrowLeftOutlined className="text-text group-hover:text-yellow-500 duration-300 text-base flex items-center" />
                    <h2 className="capitalize m-0 text-text group-hover:text-yellow-500 duration-300 text-base font-semibold">
                      kembali
                    </h2>
                  </div>
                </Link>
              </div>
            </div>

            <div className="w-full gap-11 flex flex-row">
              <div className="w-fit flex flex-col gap-6">
                <div className="w-[350px] max-h-fit">
                  <img
                    alt="package photo"
                    src={pack?.photo}
                    height={350}
                    width={350}
                    className="rounded-md"
                  />
                </div>
                <div className="w-full flex gap-6 items-center flex-row">
                  <div className="flex items-center">
                    <img
                      alt="provil vendor"
                      width={48}
                      height={48}
                      src={pack?.vendor?.user?.photo}
                      className="rounded-full"
                    />
                  </div>
                  <div className="w-full flex flex-col">
                    <h2 className="font-bold p-0 text-sm m-0">
                      {pack?.vendor?.name}
                    </h2>
                    <h4 className="font-medium text-text text-sm m-0">
                      {pack?.vendor?.city?.name}
                    </h4>
                  </div>
                </div>
              </div>
              <div className="w-full flex gap-2 flex-col">
                <div className="w-full flex">
                  <h1 className="m-0 text-xl font-bold capitalize break-words">
                    {pack?.name}
                  </h1>
                </div>
                <div className="w-full flex flex-row gap-2">
                  <Rate
                    count={1}
                    defaultValue={1}
                    className="flex items-center"
                    disabled
                  />
                  <h5 className="font-semibold flex items-center m-0 text-base">
                    {pack?.rating}
                  </h5>
                  <h5 className="text-base flex items-center m-0 font-light">
                    ({pack?.totalReview + " Ulasan"})
                  </h5>
                </div>
                <div>
                  <h1 className="font-semibold text-xl m-0">
                    {formatRupiah(pack?.price)}{" "}
                    <span className="text-text font-medium">/Orang</span>
                  </h1>
                </div>
                <div className="flex flex-col">
                  <p className="font-medium text-xs m-0">
                    Kategori: {pack?.category?.categoryName}
                  </p>
                  <p className="font-medium text-xs m-0">
                    Minimal jumlah tukang: {pack?.minHandymanQty}
                  </p>
                </div>
                <div className="flex flex-col gap-2">
                  <h2 className="m-0 font-semibold text-base">Deskripsi</h2>
                  <hr className="h-2 w-full" />
                  <p className="break-all whitespace-pre-line text-sm m-0 text-ellipsis">
                    {pack?.desc}
                  </p>
                </div>
              </div>
            </div>
            <div className="flex flex-col gap-4 mt-20">
              <h1 className="m-0 font-bold text-lg">
                Ulasan{" "}
                <span className="font-light">
                  ({pack?.totalReview + " Ulasan"})
                </span>
              </h1>
              <div className="flex flex-row gap-10">
                {review?.map((v, i) => {
                  return (
                    <>
                      <div
                        key={v.id}
                        className="py-5 px-6 rounded-lg bg-bg shadow-md flex flex-col"
                      >
                        <div className="w-full flex flex-row gap-5">
                          <div className="w-[60px] h-[60px]">
                            <img
                              alt="profile"
                              src={v?.user?.photo}
                              width={45}
                              height={45}
                              className="rounded-full"
                            />
                          </div>
                          <div className="flex flex-col justify-center">
                            <h2 className="m-0 text-xs font-semibold capitalize">
                              {v?.user?.fullname}
                            </h2>
                            <h2 className="m-0 text-xs font-light">
                              {moment(v.createdAt).format("YYYY-MM-DD")}
                            </h2>
                          </div>
                        </div>
                        <div className="flex flex-col gap-2">
                          <Rate count={5} value={v.rate} disabled />
                          <p className="m-0 text-sm break-words text-ellipsis">
                            {v.comment}
                          </p>
                          <div className="flex flex-row gap-2">
                            {v?.photoReview.map((photo) => {
                              return (
                                <>
                                  <img
                                    alt={photo?.id}
                                    src={photo?.photo}
                                    className="rounded-lg w-[100px] h-[100px] border-none"
                                    width={100}
                                    height={100}
                                  />
                                </>
                              );
                            })}
                          </div>
                        </div>
                      </div>
                    </>
                  );
                })}
              </div>
              {review.length == 0 ? (
                <div className="w-full h-auto flex items-center justify-center">
                  <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                </div>
              ) : (
                <div className="w-full flex justify-center">
                  <Pagination
                    current={current}
                    onChange={onCurrentPage}
                    total={total}
                    pageSize={limit}
                  />
                </div>
              )}
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default DetailUser;
