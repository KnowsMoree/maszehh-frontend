import {
  ArrowLeftOutlined,
  CheckCircleOutlined,
  CheckOutlined,
  CloseOutlined,
  EditOutlined,
  LeftOutlined,
} from "@ant-design/icons";
import { Image } from "antd";
import Link from "next/link";
import Input from "/components/input/Input";
import Img from "/public/avatar/avatar.png";
import Pack from "/public/image/pack.png";
import Coba from "/public/hero/login/login.png";
import { vendorRepository } from "../../../repository/vendor";
import { useEffect, useState } from "react";
import { userRepository } from "../../../repository/user";
import { useRouter } from "next/router";
import { message } from "antd";
import { mutate } from "swr";

const DetailVendor = (props) => {
  const id = props.param;
  const [vendor, setVendor] = useState();
  const { data: vendorDetail } =
    vendorRepository.hooks.useVendorDetail(id);
  const router = useRouter();

  useEffect(() => {
    if (
      vendorDetail !== null &&
      vendorDetail !== undefined
    ) {
      setVendor(vendorDetail?.data);
    }
  }, [vendorDetail]);

  const changeStatus = async () => {
    let data;
    const status = vendor?.user.status;
    if (status == "pending") {
      data = { status: "active" };
    } else if (status == "active") {
      data = { status: "not active" };
    } else if (status == "not active") {
      data = { status: "active" };
    }

    try {
      await userRepository.manipulateData.updateUser(
        vendor?.user.id,
        data
      );
      message.success(
        "Vendor Berhasil Dikonfirmasi"
      );
      await mutate(
        vendorRepository.url.vendorAll()
      );
      router.push("/admin/vendor");
    } catch (error) {
      console.log(error.response);
      message.error("terjadi kesalahan");
    }
  };

  console.log(vendor);

  return (
    <>
      <div className="py-9 px-8 flex flex-col gap-7 rounded-lg w-3/4 bg-white drop-shadow-md">
        <div className="flex flex-row gap-x-4 justify-between">
          <div>
            <div
              onClick={() => router.back()}
              className="flex group cursor-pointer bg-transparent rounded-lg  transition-all items-center flex-row justify-center gap-x-4 py-2 px-4">
              <ArrowLeftOutlined className="text-text group-hover:text-yellow-500 duration-300 text-base flex items-center" />
              <h2 className="capitalize m-0 text-text group-hover:text-yellow-500 duration-300 text-base font-semibold">
                kembali
              </h2>
            </div>
          </div>
          <div className="flex flex-row gap-6 justify-end">
            {vendor?.user.status == "pending" ? (
              <div
                onClick={() => changeStatus()}
                className="select-none w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-blue-500 rounded-lg py-[11px] px-[21px] flex flex-row gap-x-2">
                <CheckOutlined className="text-white text-[15px] flex items-center" />
                <p className="text-sm font-semibold m-0 text-white">
                  Konfirmasi Vendor
                </p>
              </div>
            ) : vendor?.user?.status ==
              "active" ? (
              <div
                onClick={() => changeStatus()}
                className="select-none w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-red-500 rounded-lg py-[11px] px-[21px] flex flex-row gap-x-2">
                <CloseOutlined className="text-white text-[15px] flex items-center" />
                <p className="text-sm font-semibold m-0 text-white">
                  Non-Aktifkan Vendor
                </p>
              </div>
            ) : vendor?.user?.status ==
              "not active" ? (
              <div
                onClick={() => changeStatus()}
                className="select-none w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-blue-500 rounded-lg py-[11px] px-[21px] flex flex-row gap-x-2">
                <CheckOutlined className="text-white text-[15px] flex items-center" />
                <p className="text-sm font-semibold m-0 text-white">
                  Aktifkan Vendor
                </p>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>

        <div className="w-full flex flex-row gap-[56px]">
          <div className="w-1/2 flex flex-col gap-y-5">
            <div className="flex flex-col">
              <h5 className="text-text m-0 text-base font-semibold capitalize">
                foto profil vendor:
              </h5>
              <div className="w-full rounded-xl flex py-[14px] h-auto justify-center items-center bg-bg">
                <div className="w-[120px] h-[120px] rounded-full">
                  <img
                    src={vendor?.user.photo}
                    alt="avatar"
                    height={120}
                    width={120}
                  />
                </div>
              </div>
            </div>
            <div className="flex flex-col">
              <h5 className="text-text m-0 text-base font-semibold capitalize">
                foto KTP Pemilik:
              </h5>
              <div className="w-full rounded-xl flex py-[14px] h-auto justify-center items-center bg-bg">
                <div className="w-[210px] h-full rounded-lg">
                  <img
                    src={vendor?.ktp}
                    alt="KTP"
                    height={100}
                    width={210}
                    className="h-[150px]"
                  />
                </div>
              </div>
            </div>
            <div className="flex flex-col h-full">
              <h5 className="text-text m-0 text-base font-semibold capitalize">
                foto siujk:
              </h5>
              <div className="w-full rounded-xl flex py-[14px] h-full justify-center items-center bg-bg">
                <div className="w-[210px] h-full rounded-lg">
                  <img
                    src={vendor?.siujk}
                    alt="siujk"
                    height={310}
                    width={210}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="w-full flex flex-col ">
            <div className="flex flex-row gap-x-[20px]">
              <div className="w-full">
                <Input
                  name="fullname"
                  labelname="Nama pemilik:"
                  labelclass="text-base font-semibold text-text"
                  type="text"
                  value={vendor?.user.fullname}
                  disabled={true}
                  className="w-full rounded-lg text-base p-[14px] shadow-sm bg-bg font-semibold"
                />
                <Input
                  name="fullname"
                  labelname="Nama vendor:"
                  labelclass="text-base font-semibold text-text"
                  type="text"
                  value={vendor?.name}
                  disabled={true}
                  className="w-full rounded-lg text-base p-[14px] shadow-sm bg-bg font-semibold"
                />
                <Input
                  name="rating"
                  labelname="rating:"
                  labelclass="text-base font-semibold text-text"
                  type="text"
                  value={vendor?.rating}
                  disabled={true}
                  className={`${
                    vendor?.rating != null &&
                    vendor?.rating != undefined
                      ? ""
                      : "hidden"
                  } w-full rounded-lg text-base p-[14px] shadow-sm bg-bg font-semibold`}
                />
                <Input
                  name="handymanTotal"
                  labelname="jumlah tukang:"
                  labelclass="text-base font-semibold text-text"
                  type="text"
                  value={vendor?.handymanTotal}
                  disabled={true}
                  className="w-full rounded-lg text-base p-[14px] shadow-sm bg-bg font-semibold"
                />
                <Input
                  name="city"
                  labelname="Kota:"
                  labelclass="text-base font-semibold text-text"
                  type="text"
                  value={vendor?.city.name}
                  disabled={true}
                  className="w-full rounded-lg text-base p-[14px] shadow-sm bg-bg font-semibold"
                />
              </div>
              <div className="w-full">
                <Input
                  name="nik"
                  labelname="NIK:"
                  labelclass="text-base font-semibold text-text"
                  type="text"
                  value={vendor?.nik}
                  disabled={true}
                  className="w-full rounded-lg text-base p-[14px] shadow-sm bg-bg font-semibold"
                />
                <Input
                  name="status"
                  labelname="status vendor:"
                  labelclass="text-base font-semibold text-text"
                  type="text"
                  value={
                    vendor?.user.status ==
                    "not active"
                      ? "Tidak Aktif"
                      : vendor?.user.status ==
                        "aktif"
                      ? "aktif"
                      : vendor?.user?.status
                  }
                  disabled={true}
                  className="w-full rounded-lg text-base p-[14px] shadow-sm bg-bg font-semibold"
                />
                <Input
                  name="email"
                  labelname="email:"
                  labelclass="text-base font-semibold text-text"
                  type="text"
                  value={vendor?.user.email}
                  disabled={true}
                  className="w-full rounded-lg text-base p-[14px] shadow-sm bg-bg font-semibold"
                />
                <Input
                  name="phoneNumber"
                  labelname="nomor telepon:"
                  labelclass="text-base font-semibold text-text"
                  type="text"
                  value={vendor?.user.phoneNumber}
                  disabled={true}
                  className="w-full rounded-lg text-base p-[14px] shadow-sm bg-bg font-semibold"
                />
                <Input
                  name="province"
                  labelname="provinsi:"
                  labelclass="text-base font-semibold text-text"
                  type="text"
                  value={
                    vendor?.city.province.name
                  }
                  disabled={true}
                  className="w-full rounded-lg text-base p-[14px] shadow-sm bg-bg font-semibold"
                />
              </div>
            </div>
            <div className="flex flex-col">
              <Input
                name="address"
                labelname="Alamat vendor:"
                labelclass="text-base font-semibold text-text"
                type="text"
                value={vendor?.user?.address}
                disabled={true}
                className="w-full rounded-lg text-base p-[14px] shadow-sm bg-bg font-semibold"
              />
              <Input
                name="desc"
                labelname="Deskripsi vendor:"
                labelclass="text-base font-semibold text-text"
                type="text"
                value={vendor?.desc}
                disabled={true}
                className="w-full rounded-lg text-base p-[14px] shadow-sm bg-bg font-semibold"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DetailVendor;
