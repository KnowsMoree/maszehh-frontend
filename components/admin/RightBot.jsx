const RightBot = ({ children, ...props }) => {
  return (
    <>
      <div className="w-[400px] p-[30px] flex flex-col gap-6 bg-white rounded-2xl">
        <h1 className="font-bold text-[20px] m-0">{props.title}</h1>
        <div className="w-full flex flex-col gap-3">{children}</div>
      </div>
    </>
  );
};

export default RightBot;
