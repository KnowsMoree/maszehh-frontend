import {
  LoadingOutlined,
  CheckOutlined,
  ArrowLeftOutlined,
  UploadOutlined,
  PlusOutlined,
  CloseOutlined,
} from "@ant-design/icons";
import {
  Col,
  Form,
  Row,
  Spin,
  Input,
  Button,
  Select,
  Upload,
  message,
} from "antd";
import Image from "next/image";
import Img from "/public/avatar/avatar.png";
import Link from "next/link";
// import Input from "/components/input/Input";
import { userRepository } from "../../../repository/user";
import { useEffect, useState } from "react";
import { useForm } from "antd/lib/form/Form";
import { useRouter } from "next/router";
import { appConfig } from "../../../config/app";

const EditUser = (props) => {
  const id = props.param;
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);
  const { Option } = Select;
  const [form] = useForm();
  const router = useRouter();

  const { data: dataUser } = userRepository.hooks.useUserDetail(id);

  useEffect(() => {
    if (dataUser !== null && dataUser !== undefined) {
      setUser(dataUser?.data);
      setLoading(false);
      setForm();
      setFileList([
        {
          url: dataUser?.data?.photo,
          name: "profile",
        },
      ]);
    }
  }, [dataUser]);

  async function setForm() {
    form.setFieldsValue({
      fullname: await dataUser?.data?.fullname,
      username: await dataUser?.data?.username,
      email: await dataUser?.data?.email,
      level: "customer",
      phoneNumber: await dataUser?.data?.phoneNumber,
      status: await dataUser?.data?.status,
      address: await dataUser?.data?.address,
    });
  }

  const onFinish = async (values) => {
    try {
      const data = {
        ...values,
        level: await dataUser?.data?.role?.id,
        photo: fileList[0].url,
      };
      console.log(data);

      await userRepository.manipulateData.updateUser(id, data);
      message.success("Data Berhasil Diubah");

      router.push(`/admin/user`);
    } catch (error) {
      message.error(error.response.body.message[0]);
    }
  };

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  const [fileList, setFileList] = useState();
  const handleImage = async (args) => {
    const file = args.file;
    try {
      const processUpload = await userRepository.manipulateData.uploadUserPhoto(
        file
      );
      setFileList([
        {
          url:
            appConfig.apiUrl + "/users/profile/" + processUpload.body.profile,
          name: "profile",
        },
      ]);

      message.success("upload berhasil");
    } catch (e) {
      console.log(e);
      message.error("upload gagal");
    }
  };

  console.log(fileList, "fileList");
  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div
        style={{
          marginTop: 8,
        }}
      >
        Upload
      </div>
    </div>
  );

  return (
    <>
      <div className="py-9 px-8 flex flex-col gap-7 rounded-lg w-3/4 bg-white drop-shadow-md">
        {loading ? (
          <div className="w-full h-full flex items-center justify-center">
            <Spin indicator={loadingIcon} />
          </div>
        ) : (
          <>
            <Form
              form={form}
              layout="vertical"
              onFinish={onFinish}
              size="large"
            >
              <Row gutter={32}>
                <Col className="gutter-row" span={10}>
                  <div className="w-full rounded-lg flex h-full justify-center items-center bg-bg">
                    <Form.Item name={"photo"}>
                      <Upload
                        name="photo"
                        listType="picture-card"
                        className="avatar-uploader"
                        onChange={(args) => handleImage(args)}
                        fileList={fileList}
                        beforeUpload={() => false}
                        onRemove={() => setFileList([])}
                        maxCount={1}
                      >
                        {uploadButton}
                      </Upload>
                    </Form.Item>
                  </div>
                </Col>
                <Col className="gutter-row" span={7}>
                  <Form.Item
                    name={"fullname"}
                    label={
                      <label className="text-base font-semibold text-text">
                        Nama Lengkap
                      </label>
                    }
                  >
                    <Input
                      type="text"
                      className="w-full text-base shadow-sm bg-bg font-semibold"
                      disabled
                    />
                  </Form.Item>
                  <Form.Item
                    name={"username"}
                    label={
                      <label className="text-base font-semibold text-text">
                        Username
                      </label>
                    }
                  >
                    <Input
                      type="text"
                      className="w-full text-base shadow-sm bg-bg font-semibold"
                      disabled
                    />
                  </Form.Item>
                  <Form.Item
                    name={"email"}
                    label={
                      <label className="text-base font-semibold text-text">
                        Email
                      </label>
                    }
                  >
                    <Input
                      type="text"
                      className="w-full text-base shadow-sm bg-bg font-semibold"
                      disabled
                    />
                  </Form.Item>
                  <Form.Item
                    name={"level"}
                    label={
                      <label className="text-base font-semibold text-text">
                        Level Pengguna
                      </label>
                    }
                  >
                    <Input
                      type="text"
                      className="w-full text-base shadow-sm bg-bg font-semibold"
                      disabled
                    />
                  </Form.Item>
                </Col>
                <Col className="gutter-row" span={7}>
                  <Form.Item
                    name={"phoneNumber"}
                    label={
                      <label className="text-base font-semibold text-text">
                        Nomor Telepon
                      </label>
                    }
                  >
                    <Input
                      type="text"
                      className="w-full text-base shadow-sm bg-bg font-semibold"
                    />
                  </Form.Item>
                  <Form.Item
                    name={"status"}
                    label={
                      <label className="text-base font-semibold text-text">
                        Status Pengguna
                      </label>
                    }
                  >
                    <Select className="w-full text-base shadow-sm bg-bg font-semibold">
                      <Option value="active">aktif</Option>
                      <Option value="not active">tidak aktif</Option>
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name={"address"}
                    label={
                      <label className="text-base font-semibold text-text">
                        Alamat
                      </label>
                    }
                  >
                    <Input.TextArea
                      autoSize={{
                        minRows: 4,
                        maxRows: 5,
                      }}
                      type="text"
                      className="w-full text-base shadow-sm bg-bg font-semibold"
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row className="flex flex-row gap-6 justify-end">
                <Link href={`/admin/user/${user.id}`}>
                  <Button
                    type="primary"
                    icon={<CloseOutlined />}
                    size="large"
                    className="text-sm font-semibold w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-main py-[11px] px-[21px] rounded-lg border-none flex items-center"
                  >
                    Batal
                  </Button>
                </Link>
                <Button
                  type="primary"
                  htmlType="submit"
                  icon={<CheckOutlined />}
                  size="large"
                  className="text-sm font-semibold w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-blue-500 py-[11px] px-[21px] rounded-lg border-none flex items-center"
                >
                  Simpan
                </Button>
              </Row>
            </Form>
          </>
        )}
      </div>
    </>
  );
};

export default EditUser;
