import {
  ArrowLeftOutlined,
  EditOutlined,
  LeftOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import { Spin } from "antd";
import Image from "next/image";
import { useEffect, useState } from "react";
import { userRepository } from "/repository/user";
import Img from "/public/avatar/avatar.png";
import Input from "/components/input/Input";
import Link from "next/link";

const DetailUser = (props) => {
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState(null);
  const id = props?.param;

  const { data: dataDetailUser } = userRepository.hooks.useUserDetail(id);

  useEffect(() => {
    setInterval(() => {
      if (dataDetailUser !== null && dataDetailUser !== undefined) {
        setUser(dataDetailUser?.data);
        setLoading(false);
      }
    }, 1000);
  }, [dataDetailUser]);

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  return (
    <>
      <div className="py-9 px-8 flex flex-col gap-7 rounded-lg w-3/4 bg-white drop-shadow-md">
        {loading ? (
          <div className="w-full h-full flex items-center justify-center">
            <Spin indicator={loadingIcon} />
          </div>
        ) : (
          <>
            <div className="flex flex-row gap-x-4">
              <Link href={`/admin/user`}>
                <div className="flex group cursor-pointer bg-transparent rounded-lg  transition-all items-center flex-row justify-center gap-x-4 py-2 px-4">
                  <ArrowLeftOutlined className="text-text group-hover:text-yellow-500 duration-300 text-base flex items-center" />
                  <h2 className="capitalize m-0 text-text group-hover:text-yellow-500 duration-300 text-base font-semibold">
                    kembali
                  </h2>
                </div>
              </Link>
            </div>
            <div className="w-full flex flex-row gap-x-[60px]">
              <div className="w-1/2 items-center flex h-full">
                <div className="w-full rounded-xl flex py-[71px] h-full justify-center items-center bg-bg">
                  <div className="w-[120px] h-[120px] rounded-full">
                    <img
                      src={user.photo}
                      alt={user.photo}
                      height={120}
                      width={120}
                      className="rounded-full"
                    />
                  </div>
                </div>
              </div>
              <div className="w-full flex flex-row gap-5">
                <div className="w-full flex flex-col">
                  <Input
                    name="fullname"
                    labelname="Nama lengkap:"
                    labelclass="text-base font-semibold text-text"
                    type="text"
                    value={user.fullname}
                    disabled={true}
                    className="w-full text-base p-[14px] shadow-sm bg-bg font-semibold"
                  />
                  <Input
                    name="email"
                    labelname="Email:"
                    labelclass="text-base font-semibold text-text"
                    type="text"
                    value={user.email}
                    disabled={true}
                    className="w-full text-base p-[14px] shadow-sm bg-bg font-semibold"
                  />
                  <Input
                    name="level"
                    labelname="Level pengguna:"
                    labelclass="text-base font-semibold text-text"
                    type="text"
                    value={user.role.roleName}
                    disabled={true}
                    className="w-full text-base p-[14px] shadow-sm bg-bg font-semibold"
                  />
                </div>
                <div className="w-full flex flex-col">
                  <Input
                    name="nomor"
                    labelname="Nomor telepon:"
                    labelclass="text-base font-semibold text-text"
                    type="text"
                    value={user.phoneNumber}
                    disabled={true}
                    className="w-full text-base p-[14px] shadow-sm bg-bg font-semibold"
                  />
                  <Input
                    name="status"
                    labelname="Status pengguna:"
                    labelclass="text-base font-semibold text-text"
                    type="text"
                    value={user.status}
                    disabled={true}
                    className={`w-full text-base p-[14px] shadow-sm bg-bg font-semibold`}
                  />
                  <Input
                    name="alamat"
                    labelname="alamat:"
                    labelclass="text-base font-semibold text-text"
                    type="text"
                    value={user.address}
                    disabled={true}
                    className={`w-full truncate h-auto text-base p-[14px] shadow-sm bg-bg font-semibold`}
                  />
                </div>
              </div>
            </div>
            <div className="flex flex-row gap-6 justify-end">
              <Link href={`/admin/user/edit/${user.id}`}>
                <div className="w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-blue-500 rounded-lg py-[11px] px-[21px] flex flex-row gap-x-2">
                  <EditOutlined className="text-white text-[15px] flex items-center" />
                  <p className="text-sm font-semibold m-0 text-white">Ubah</p>
                </div>
              </Link>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default DetailUser;
