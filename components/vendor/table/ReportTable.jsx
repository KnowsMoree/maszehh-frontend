import React, {
  useEffect,
  useState,
} from "react";
import "antd/dist/antd.css";
import {
  Radio,
  Space,
  Spin,
  Table,
  Tag,
  DatePicker,
} from "antd";
import Image from "next/image";
import {
  DeleteOutlined,
  DownloadOutlined,
  EyeOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import Link from "next/link";
import { userRepository } from "/repository/user";
import { orderRepository } from "../../../repository/order";
import moment from "moment";
import { parseJwt } from "../../../helper/parseJwt";

const { RangePicker } = DatePicker;

const columns = [
  {
    title: "kode pesanan",
    dataIndex: "order_order_code",
    key: "order_order_code",
    align: "center",
    className:
      "text-base text-text font-semibold",
    render: (key, record) => (
      <Link
        href={`/vendor/order/${record.order_id}`}>
        <span className="text-blue-500 hover:text-secondary duration-300 cursor-pointer select-none">
          {key}
        </span>
      </Link>
    ),
  },
  {
    title: "nama pelanggan",
    dataIndex: "user_fullname",
    key: "user_fullname",
    className:
      "text-base text-text font-semibold",
    render: (key) => (
      <span className="text-base font-medium text-black m-0 text-center">
        {key}
      </span>
    ),
  },
  {
    title: "Nama Vendor",
    dataIndex: "vendor_name",
    key: "vendor_name",
    className:
      "text-base text-text font-semibold",
    render: (key) => (
      <span className="text-base font-medium text-black m-0 text-center">
        {key}
      </span>
    ),
  },
  {
    title: "tanggal pesanan dibuat",
    dataIndex: "order_created_at",
    key: "order_created_at",
    className:
      "text-base text-text font-semibold",
    render: (key) => (
      <span className="text-base font-medium text-black m-0 text-center">
        {moment(key).format("YYYY-MM-DD")}
      </span>
    ),
  },
  {
    title: "tanggal pembayaran",
    dataIndex: "uPay_payment_date",
    key: "uPay_payment_date",
    className:
      "text-base text-text font-semibold",
    render: (key) => (
      <span className="text-base font-medium text-black m-0 text-center">
        {moment(key).format("YYYY-MM-DD")}
      </span>
    ),
  },
  {
    title: "Total Biaya",
    key: "totalPayment",
    dataIndex: "totalPayment",
    className:
      "text-base text-text font-semibold",
    render: (key) => (
      <span className="text-base font-medium text-black m-0 text-center">
        {formatRupiah(key)}
      </span>
    ),
  },
  {
    title: "Tanggal order selesai",
    key: "currentEndDate",
    dataIndex: "currentEndDate",
    className:
      "text-base text-text font-semibold",
    render: (key) => (
      <span className="text-base font-medium text-black m-0 text-center">
        {moment(key).format("YYYY-MM-DD")}
      </span>
    ),
  },
];

const formatRupiah = (money) => {
  return new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    minimumFractionDigits: 0,
  }).format(money);
};

const onChange = (
  pagination,
  filters,
  sorter,
  extra
) => {
  console.log(
    "params",
    pagination,
    filters,
    sorter,
    extra
  );
};

const OrderTable = () => {
  const [listdata, setListData] = useState(null);
  const [loading, setLoading] = useState(true);

  const { data: dataReport } =
    orderRepository.hooks.useReportVendor();

  // const token = localStorage.getItem("token");
  const [user, setUser] = useState({});
  const [idUser, setIdUser] = useState("");

  useEffect(() => {
    if (
      dataReport !== null &&
      dataReport !== undefined
    ) {
      setListData(dataReport?.data);
      setLoading(false);
    }

    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
      setUser(parseJwt(token));
    }
  }, [dataReport]);

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  return (
    <div className="bg-white shadow-lg rounded-2xl p-7 gap-y-3 flex flex-col">
      {loading ? (
        <div className="w-full h-full flex items-center justify-center">
          <Spin indicator={loadingIcon} />
        </div>
      ) : (
        <>
          <div className="w-full flex flex-row justify-end gap-x-2">
            {/* <div className="w-full">
          <RangePicker className="w-full h-full placeholder:text-base rounded-lg text-base font-semibold" />
        </div>
        <div className="w-1/5">
          <button className="w-full py-3 px-4 rounded-lg bg-main flex flex-row items-center justify-center gap-x-3 hover:drop-shadow-lg hover:bg-secondary transition-all">
            <EyeOutlined className="text-white text-lg flex items-center m-0" />
            <h2 className="text-white capitalize font-semibold m-0 text-base">
              tampilkan data
            </h2>
          </button>
        </div> */}
            <div className="w-1/6">
              <Link
                href={
                  process.env
                    .NEXT_PUBLIC_BASE_URL +
                  `/order/vendor/report-download/${user?.id}`
                }>
                <button className="w-full py-3 px-4 rounded-lg bg-main flex flex-row items-center justify-center gap-x-3 hover:drop-shadow-lg hover:bg-secondary transition-all">
                  <DownloadOutlined className="text-white text-lg flex items-center m-0" />
                  <h2 className="text-white capitalize font-semibold m-0 text-base">
                    unduh laporan
                  </h2>
                </button>
              </Link>
            </div>
          </div>
          <Table
            columns={columns}
            onChange={onChange}
            dataSource={listdata}
            pagination={{
              position: ["bottomCenter"],
              pageSize: 5,
              style: {
                display: "flex",
                alignItems: "center",
              },
              className: "border-none rounded-lg",
            }}
          />
        </>
      )}
    </div>
  );
};

export default OrderTable;
