import React, {
  useEffect,
  useState,
} from "react";
import "antd/dist/antd.css";
import {
  message,
  Popconfirm,
  Radio,
  Rate,
  Space,
  Spin,
  Table,
  Tag,
} from "antd";
import {
  DeleteOutlined,
  EditOutlined,
  LoadingOutlined,
  PlusOutlined,
  StarFilled,
} from "@ant-design/icons";
import Link from "next/link";
import SearchTable from "/components/input/SearchTable";
import { packageRepository } from "../../../repository/package";
import { categoryRepository } from "../../../repository/category";
import { formatRupiah } from "../../../helper/currencyFormat";
import { mutate } from "swr";

const onChange = (
  pagination,
  filters,
  sorter,
  extra
) => {
  console.log(
    "params",
    pagination,
    filters,
    sorter,
    extra
  );
};

const PackagesTable = () => {
  const [listdata, setListData] = useState(null);
  const [loading, setLoading] = useState(true);

  const [search, setSearch] = useState("");
  const [limit, setLimit] = useState(100);

  const { data: dataPackVen } =
    packageRepository.hooks.usePackageVendor({
      search,
      limit,
    });

  useEffect(() => {
    if (
      dataPackVen !== null &&
      dataPackVen !== undefined
    ) {
      setListData(dataPackVen?.items);
      console.log(dataPackVen?.items);

      setLoading(false);
    }
  }, [dataPackVen]);

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  const deletePackage = async (id) => {
    console.log(id);
    await packageRepository.manipulateData.deletePackage(
      id
    );
    await mutate(
      packageRepository.url.getPackVendor({
        search,
        limit,
      })
    );
    message.success(
      "Berhasil menghapus paket layanan"
    );
  };

  const columns = [
    {
      title: "Nama Paket Layanan",
      dataIndex: "name",
      key: "name",
      className:
        "text-base text-text font-semibold capitalize",
      width: "20%",
      render: (key, record) => (
        <Link
          href={`/vendor/packages/${record.id}`}>
          <span className="text-blue-500 font-medium capitalize hover:text-secondary duration-300 cursor-pointer select-none">
            {key}
          </span>
        </Link>
      ),
    },
    {
      title: "harga",
      dataIndex: "price",
      key: "price",
      className:
        "text-base text-text font-semibold capitalize",
      width: "10%",
      render: (key) => (
        <span className="text-justBlack font-medium">
          {formatRupiah(key)}
        </span>
      ),
    },
    {
      title: "kategori",
      dataIndex: "category",
      key: "category",
      className:
        "text-base text-text font-semibold capitalize",
      width: "15%",
      filters: [
        {
          text: <span>perbaikan</span>,
          value: "perbaikan",
        },
        {
          text: <span>renovasi</span>,
          value: "renovasi",
        },
        {
          text: <span>membangun</span>,
          value: "membangun",
        },
        {
          text: <span>pemasangan</span>,
          value: "pemasangan",
        },
      ],
      onFilter: (value, record) =>
        record.category.categoryName.startsWith(
          value
        ),
      filterSearch: false,
      render: (_, record) => (
        <span className="text-justBlack font-medium capitalize">
          {record.category.categoryName}
        </span>
      ),
    },
    {
      title: "Rating",
      key: "rating",
      dataIndex: "rating",
      filterSearch: false,
      className:
        "text-base text-text font-semibold capitalize",
      width: "10%",
      render: (key) => (
        <div className="flex flex-row">
          <StarFilled className="text-filled text-[18px] mr-2 flex items-center" />
          <span className="text-justBlack font-medium capitalize">
            {key}
          </span>
        </div>
      ),
    },

    {
      title: "Aksi",
      className:
        "text-base text-text font-semibold capitalize",
      key: "action",
      width: "10%",
      render: (_, record) => (
        <Space size="middle">
          <Popconfirm
            title="Anda yakin akan menghapus akun ini?"
            onConfirm={async () =>
              deletePackage(record.id)
            }
            okText="Ya"
            cancelText="Batal">
            <DeleteOutlined className="text-xl text-main flex items-center" />
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <div className="bg-white shadow-lg rounded-2xl p-7 flex gap-4 flex-col">
      {loading ? (
        <div className="w-full h-full flex items-center justify-center">
          <Spin indicator={loadingIcon} />
        </div>
      ) : (
        <>
          <div className="flex flex-row justify-between">
            <div className="flex items-center m-0">
              <h1 className="font-bold m-0 text-2xl capitalize">
                List Paket Layanan
              </h1>
            </div>
            <div className="flex flex-row gap-x-4">
              <div className="w-full flex m-0">
                <SearchTable
                  placeholder="cari paket..."
                  onChange={(e) =>
                    setSearch(e.target.value)
                  }
                />
              </div>
              <Link
                href={"/vendor/packages/create"}>
                <button className="bg-main w-1/2 rounded-lg hover:drop-shadow-lg hover:bg-secondary transition-all px-3 py-1 flex flex-row items-center gap-x-2 justify-center">
                  <PlusOutlined className="text-white text-base flex items-center" />
                  <h2 className="m-0 text-base text-white font-semibold flex items-center">
                    Paket
                  </h2>
                </button>
              </Link>
            </div>
          </div>
          <Table
            columns={columns}
            onChange={onChange}
            dataSource={listdata}
            pagination={{
              position: ["bottomCenter"],
              pageSize: 5,
              style: {
                display: "flex",
                alignItems: "center",
              },
              className: "border-none rounded-lg",
            }}
          />
        </>
      )}
    </div>
  );
};

export default PackagesTable;
