import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import { Radio, Space, Spin, Table, Tag } from "antd";
import Image from "next/image";
import { DeleteOutlined, LoadingOutlined } from "@ant-design/icons";
import Link from "next/link";
import { userRepository } from "/repository/user";
import SearchTable from "../../input/SearchTable";
import { paymentVendorRepository } from "../../../repository/payment-vendor";
import moment from "moment";
import { formatRupiah } from "../../../helper/currencyFormat";

const OrderTable = () => {
  const [listdata, setListData] = useState(null);
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(true);

  const { data: dataVendorPayment } =
    paymentVendorRepository.hooks.useAllVendorPaymet(search);

  useEffect(() => {
    if (dataVendorPayment !== null && dataVendorPayment !== undefined) {
      console.log(dataVendorPayment?.data);
      setListData(dataVendorPayment?.data);
      setTimeout(() => {
        setLoading(false);
      }, 1000);
    }
  }, [dataVendorPayment]);

  const columns = [
    {
      title: "Kode Pembayaran",
      dataIndex: "codeVendorPayment",
      key: "codeVendorPayment",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <Link href={`/vendor/payment/${record.id}`}>
          <span className="text-blue-500 hover:text-secondary duration-300 cursor-pointer select-none font-medium">
            {key}
          </span>
        </Link>
      ),
    },
    {
      title: "Tanggal Pembayaran",
      dataIndex: "createdAt",
      key: "createdAt",
      className: "text-base text-text font-semibold",
      render: (key) => (
        <span className="text-justBlack font-medium">
          {moment(key).format("YYYY-MM-DD hh:mm")}
        </span>
      ),
    },
    {
      title: "Rekening Vendor",
      dataIndex: "bankVendor",
      key: "bankVendor",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <span className="text-justBlack font-medium">
          {record.bankVendor.accountNumber}
        </span>
      ),
    },
    {
      title: "Total Pembayaran",
      dataIndex: "total",
      key: "total",
      className: "text-base text-text font-semibold",
      render: (key) => (
        <span className="text-justBlack font-medium">{formatRupiah(key)}</span>
      ),
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      className: "text-base text-text font-semibold",
      render: (key) => {
        let color;
        let status;

        if (key == "done") {
          (color = "grey"), (status = "selesai");
        } else if (key == "rejected") {
          (color = "red"), (status = "ditolak");
        } else if (key == "waiting vendor confirm") {
          (color = "yellow"), (status = "menunggu konfirmasi vendor");
        }

        return (
          <Tag
            color={color}
            className="border-none capitalize rounded-md"
            key={status}
          >
            {status}
          </Tag>
        );
      },
      filters: [
        {
          text: <span>menunggu konfirmasi vendor</span>,
          value: "waiting vendor confirm",
        },
        {
          text: <span>ditolak</span>,
          value: "rejected",
        },
        {
          text: <span>selesai</span>,
          value: "done",
        },
      ],
      onFilter: (value, record) => record.status.startsWith(value),
      filterSearch: false,
      width: "15%",
    },
  ];

  const onChange = (pagination, filters, sorter, extra) => {
    console.log("params", pagination, filters, sorter, extra);
  };

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  return (
    <div className="bg-white shadow-lg rounded-2xl p-7 flex gap-4 flex-col">
      {loading ? (
        <div className="w-full h-full flex items-center justify-center">
          <Spin indicator={loadingIcon} />
        </div>
      ) : (
        <>
          <div className="flex flex-row justify-evenly">
            <div className="w-full flex items-center m-0">
              <h1 className="font-bold m-0 text-2xl capitalize">
                list pembayaran vendor
              </h1>
            </div>
            <div className="w-1/3 m-0">
              <SearchTable
                placeholder="cari kode pembayaran"
                onChange={(e) => setSearch(e.target.value)}
              />
            </div>
          </div>
          <Table
            columns={columns}
            onChange={onChange}
            dataSource={listdata}
            pagination={{
              position: ["bottomCenter"],
              pageSize: 5,
              style: {
                display: "flex",
                alignItems: "center",
              },
              className: "border-none rounded-lg",
            }}
          />
        </>
      )}
    </div>
  );
};

export default OrderTable;
