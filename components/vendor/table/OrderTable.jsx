import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import { Radio, Space, Spin, Table, Tag } from "antd";
import Image from "next/image";
import { DeleteOutlined, LoadingOutlined } from "@ant-design/icons";
import SearchTable from "/components/input/SearchTable";
import Link from "next/link";
import { orderRepository } from "../../../repository/order";
import moment from "moment";
import "moment/locale/id";

const onChange = (pagination, filters, sorter, extra) => {
  console.log("params", pagination, filters, sorter, extra);
};

const OrderTable = () => {
  const [listdata, setListData] = useState(null);
  const [search, setSearch] = useState("");
  const [status, setStatus] = useState("");

  const [loading, setLoading] = useState(true);

  const { data: dataOrder } = orderRepository.hooks.useOrderAll({
    search,
    status,
  });

  const columns = [
    {
      title: "Kode Pesanan",
      dataIndex: "orderCode",
      key: "orderCode",
      width: "20%",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <Link href={`/vendor/order/${record.id}`}>
          <span className="text-blue-500 font-medium capitalize hover:text-secondary duration-300 cursor-pointer select-none">
            {key}
          </span>
        </Link>
      ),
    },
    {
      title: "Nama Pelanggan",
      dataIndex: "user",
      key: "user",
      className: "text-base text-text flex font-semibold",
      render: (key) => (
        <span className="font-medium text-black">{key.fullname}</span>
      ),
    },
    {
      title: "Tanggal Pesanan Dibuat",
      dataIndex: "order",
      key: "order",
      className: "text-base text-text font-semibold",
      render: (key, record) => (
        <span className="font-medium text-black">
          {moment(record.createdAt).format("YYYY-MM-DD HH:mm")}
        </span>
      ),
    },
    {
      title: "Status",
      key: "statusToUser",
      dataIndex: "statusToUser",
      filterSearch: false,
      className: "text-base text-text font-semibold",
      width: "15%",
      filters: [
        {
          text: <span>menunggu konfirmasi</span>,
          value: "pending",
        },
        {
          text: <span>aktif</span>,
          value: "active",
        },
        {
          text: <span>tidak aktif</span>,
          value: "not active",
        },
      ],
      onFilter: (value, record) => record.status.startsWith(value),
      filterSearch: false,
      render: (statusToUser) => {
        let color =
          statusToUser == "hold"
            ? "red"
            : statusToUser == "done"
            ? "gray"
            : statusToUser == "pending"
            ? "yellow"
            : statusToUser == "first paid done"
            ? "blue"
            : statusToUser == "in progress"
            ? "green"
            : statusToUser == "done by vendor"
            ? "green"
            : "";

        let status =
          statusToUser == "hold"
            ? "ditahan"
            : statusToUser == "done"
            ? "selesai"
            : statusToUser == "pending"
            ? "menunggu konfirmasi"
            : statusToUser == "first paid done"
            ? "awal pembayaran"
            : statusToUser == "in progress"
            ? "dalam proses"
            : statusToUser == "done by vendor"
            ? "diselesaikan vendor"
            : "";

        return (
          <span>
            <Tag
              color={color}
              className="border-none capitalize rounded-md"
              key={statusToUser}
            >
              {status}
            </Tag>
          </span>
        );
      },
    },
  ];

  useEffect(() => {
    if (dataOrder !== null && dataOrder !== undefined) {
      setListData(dataOrder?.items);
      setLoading(false);
    }
  }, [dataOrder]);

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  return (
    <div className="bg-white shadow-lg rounded-2xl p-7 flex gap-4 flex-col">
      {loading ? (
        <div className="w-full h-full flex items-center justify-center">
          <Spin indicator={loadingIcon} />
        </div>
      ) : (
        <>
          <div className="flex flex-row justify-evenly">
            <div className="w-full flex items-center m-0">
              <h1 className="font-bold m-0 text-2xl capitalize">
                list Pesanan
              </h1>
            </div>
            <div className="w-1/3 m-0">
              <SearchTable
                placeholder="cari pesanan"
                onChange={(e) => setSearch(e.target.value)}
              />
            </div>
          </div>
          <Table
            columns={columns}
            onChange={onChange}
            dataSource={listdata}
            pagination={{
              position: ["bottomCenter"],
              pageSize: 5,
              style: {
                display: "flex",
                alignItems: "center",
              },
              className: "border-none rounded-lg",
            }}
          />
        </>
      )}
    </div>
  );
};

export default OrderTable;
