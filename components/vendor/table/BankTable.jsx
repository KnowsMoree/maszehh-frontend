import {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import {
  Form,
  message,
  Modal,
  Popconfirm,
  Space,
  Table,
  Input,
  InputNumber,
} from "antd";
import { useEffect, useState } from "react";
import Button from "/components/input/Button";
import IsInput from "/components/input/Input";
import { Select } from "antd";
import { bankRepository } from "../../../repository/bank";
import { mutate } from "swr";
import { useForm } from "antd/lib/form/Form";

const { Option } = Select;

const Bank = () => {
  const [bank, setBank] = useState("");
  const [owner, setOwner] = useState("");
  const [accountNumber, setAccountNumber] =
    useState("");

  const [banks, setBanks] = useState([]);
  const [form] = useForm();

  const columns = [
    {
      title: "Nama Bank",
      dataIndex: "bankName",
      key: "bankName",
      className:
        "text-base text-text font-semibold",
      render: (key) => (
        <p className="text-black m-0 font-medium">
          {key}
        </p>
      ),
    },
    {
      title: "Nama Pemilik Rekening",
      dataIndex: "accountName",
      key: "accountName",
      className:
        "text-base text-text font-semibold",
      render: (key) => (
        <p className="text-black m-0 font-medium">
          {key}
        </p>
      ),
    },
    {
      title: "Nomor Rekening",
      dataIndex: "accountNumber",
      key: "accountNumber",
      className:
        "text-base text-text font-semibold",
      render: (key) => (
        <p className="text-black m-0 font-medium">
          {key}
        </p>
      ),
    },
    {
      title: "Aksi",
      className:
        "text-base text-text font-semibold",
      key: "action",
      width: "10%",
      render: (_, record) => (
        <Space size="middle">
          <Popconfirm
            title="Anda yakin akan menghapus data bank ini?"
            onConfirm={async () =>
              deleteBank(record.id)
            }
            okText="Ya"
            cancelText="Batal">
            <DeleteOutlined className="text-xl text-main flex items-center" />
          </Popconfirm>
          <EditOutlined
            className="text-xl text-filled flex items-center"
            onClick={() => showModal(record)}
          />
          <Modal
            visible={visible}
            onOk={handleOk}
            confirmLoading={confirmLoading}
            onCancel={handleCancel}
            okText="Simpan"
            cancelText="Batal">
            <Form form={form} layout="vertical">
              <Form.Item
                name={"id"}
                style={{ display: "none" }}>
                <Input
                  type="text"
                  className="w-full px- text-base shadow-sm bg-bg font-medium"
                />
              </Form.Item>
              <Form.Item
                name={"bankName"}
                label={
                  <label className="text-base font-semibold text-black">
                    Nama Bank
                  </label>
                }>
                <Input
                  type="text"
                  disabled={true}
                  className="w-full px- text-base shadow-sm bg-bg font-medium"
                />
              </Form.Item>
              <Form.Item
                name={"accountName"}
                label={
                  <label className="text-base font-semibold text-black">
                    Nama Pemilik Rekening
                  </label>
                }>
                <Input
                  type="text"
                  className="w-full px- text-base shadow-sm bg-bg font-medium"
                />
              </Form.Item>
              <Form.Item
                name={"accountNumber"}
                label={
                  <label className="text-base font-semibold text-black">
                    Nomor Rekening Bank
                  </label>
                }
                rules={[
                  {
                    required: true,
                    pattern: new RegExp(
                      /[0-9]{15,20}\d/g
                    ),
                    message:
                      "masukan minimal 15 digit angka",
                  },
                ]}>
                <Input
                  type="text"
                  className="w-full px- text-base shadow-sm bg-bg font-medium"
                />
              </Form.Item>
            </Form>
          </Modal>
        </Space>
      ),
    },
  ];

  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] =
    useState(false);

  const showModal = (record) => {
    form.setFieldsValue({
      id: record.id,
      bankName: record.bankName,
      accountName: record.accountName,
      accountNumber: record.accountNumber,
    });
    setVisible(true);
  };

  const handleOk = async () => {
    try {
      setConfirmLoading(true);
      const data = form.getFieldsValue();
      const id = data.id;
      await bankRepository.manipulateData.updateBank(
        id,
        data
      );
      await mutate(bankRepository.url.bank());
      setTimeout(() => {
        setVisible(false);
        setConfirmLoading(false);
        message.success(
          "Data bank berhasil di edit"
        );
      }, 2000);
    } catch (error) {
      setConfirmLoading(false);
      message.error("gagal mengubah data");
      console.log(error.response);
    }
  };

  const handleCancel = () => {
    console.log("Clicked cancel button");
    setVisible(false);
  };

  const { data: dataBank } =
    bankRepository.hooks.useBank();

  const deleteBank = async (id) => {
    await bankRepository.manipulateData.deleteBank(
      id
    );
    await mutate(bankRepository.url.bank());
    message.success("Data bank berhasil dihapus");
  };

  useEffect(() => {
    if (
      dataBank !== null &&
      dataBank !== undefined
    ) {
      setBanks(dataBank?.data);
    }
  }, [dataBank]);

  const data = [
    {
      key: "002",
      bank_name: "BANK RAKYAT INDONESIA",
    },
    {
      key: "008",
      bank_name: "BANK MANDIRI",
    },
    {
      key: "009",
      bank_name: "BANK NEGARA INDONESIA",
    },
    {
      key: "200",
      bank_name: "BANK TABUNGAN NEGARA",
    },
    {
      key: "014",
      bank_name: "BANK CENTRAL ASIA",
    },
  ];

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      bankName: bank,
      accountName: owner,
      accountNumber: accountNumber,
    };

    try {
      await bankRepository.manipulateData.createBank(
        data
      );
      await mutate(bankRepository.url.bank());
      message.success(
        "Data bank berhasil dibuat"
      );
    } catch (error) {
      message.error(
        error.response.body.message[0]
      );
    }
  };

  const but = (
    <>
      <div className="flex flex-row ">
        <PlusOutlined className="flex items-center text-white text-2xl " />
        <p className="m-0 text-base ml-5">
          Tambah
        </p>
      </div>
    </>
  );

  return (
    <>
      <div className="bg-white gap-5 flex shadow-lg flex-col w-3/4 justify-center p-7 rounded-2xl ">
        <div>
          <h2 className="font-bold text-xl m-0">
            List Data Bank
          </h2>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="flex flex-row items-center gap-3">
            <div className="w-full flex gap-1 flex-col">
              <label
                className="font-semibold text-base"
                htmlFor="bank">
                Nama Bank:
              </label>
              <select
                required={true}
                onChange={(e) =>
                  setBank(e.target.value)
                }
                id="bank"
                className="bg-bg appearance-none relative w-full mb-[22px] text-base focus:ring-2 focus:outline-none shadow-md rounded-lg p-[14px] placeholder:text-text placeholder:font-medium ">
                <option disabled selected>
                  pilih Bank
                </option>
                {data.map((v) => {
                  return (
                    <>
                      <option
                        key={v.account_number}
                        value={v.bank_name}>
                        {v.bank_name}
                      </option>
                    </>
                  );
                })}
              </select>
            </div>
            <div className="w-full flex gap-1 flex-col">
              <label
                className="font-semibold text-base"
                htmlFor="pemilik">
                Nama Pemilik:
              </label>
              <IsInput
                id="pemilik"
                onChange={(e) =>
                  setOwner(e.target.value)
                }
                placeholder="masukan nama pemilik"
                required={true}
                className="bg-bg w-full text-base focus:ring-2 focus:outline-none shadow-md rounded-lg p-[14px] placeholder:text-text placeholder:font-medium "
              />
            </div>
            <div className="w-full flex gap-1 flex-col">
              <label
                className="font-semibold text-base"
                htmlFor="nomor">
                Nomor Rekening:
              </label>
              <IsInput
                id="nomor"
                type={"text"}
                pattern={"[0-9]{15,20}"}
                onChange={(e) =>
                  setAccountNumber(e.target.value)
                }
                placeholder="masukan angka"
                required={true}
                className="bg-bg w-full text-base focus:ring-2 focus:outline-none shadow-md rounded-lg p-[14px] placeholder:text-text placeholder:font-medium "
              />
            </div>
            <Button
              name={but}
              width="w-1/4 px-[14px] mt-0 active:ring-0 active:outline-none focus:ring-0 justify-evenly bg-blue-500 h-full"
            />
          </div>
        </form>
        <Table
          columns={columns}
          dataSource={banks}
          pagination={{
            pageSize: 5,
            position: ["bottomCenter"],
          }}
        />
      </div>
    </>
  );
};

export default Bank;
