import {
  EditOutlined,
  HomeOutlined,
  HomeTwoTone,
  HourglassTwoTone,
  StarFilled,
} from "@ant-design/icons";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import { vendorRepository } from "../../repository/vendor";
import Img from "/public/avatar/avatar.png";

const ProfileChild = (props) => {
  const [data, setdata] = useState();
  const [loading, setLoading] = useState(true);

  const { data: dataVendor } = vendorRepository.hooks.useVendorProfile();

  useEffect(() => {
    if (dataVendor !== null && dataVendor !== undefined) {
      setLoading(false);
      setdata(dataVendor?.data);
    }
  });

  console.log(data);
  return (
    <>
      {loading ? (
        <div className="w-full h-full flex-col gap-y-2 flex items-center justify-center px-[84px] pt-[100px] pb-[50px]">
          <HourglassTwoTone
            twoToneColor="#95A1AF"
            spin
            className="text-5xl flex items-center"
          />
          <h2 className="text-xl m-0 text-text font-semibold">
            mohon tunggu...
          </h2>
        </div>
      ) : (
        <>
          <div className="flex flex-col gap-11 w-full">
            <div className="bg-white rounded-lg divide-x flex flex-row shadow-md py-4 px-5">
              <div className="flex flex-row w-full gap-14">
                <div className="w-[90px] h-[90px]">
                  <img
                    alt=""
                    src={data?.user?.photo}
                    className="rounded-full"
                  />
                </div>
                <div className="flex flex-col justify-center justify-items-start">
                  <h2 className="m-0 capitalize font-bold text-lg">
                    {data?.name}
                  </h2>
                  <div className="flex flex-row gap-4 items-center">
                    <h3 className="m-0 font-medium text-base flex items-center">
                      {data?.city?.province?.name} | {data?.city?.name}
                    </h3>
                  </div>
                </div>
              </div>
              <div className="w-full flex flex-row pl-6 justify-between">
                <div className="flex flex-col justify-items-start justify-center">
                  <h2 className="text-base capitalize">Rating vendor:</h2>
                  <div className="flex flex-row gap-4 items-center">
                    <StarFilled className="text-xl flex items-center text-filled" />
                    <h1 className="m-0 font-bold text-lg flex items-center">
                      {data?.rating}
                    </h1>
                  </div>
                </div>
                <div className="flex items-center justify-center">
                  <Link href={`/account/vendor/edit/${data?.id}`}>
                    <div className="w-full hover:bg-secondary hover:shadow-md transition-colors cursor-pointer rounded-lg flex bg-blue-500 py-2 px-4 flex-row gap-6">
                      <EditOutlined className="text-white text-base flex items-center" />
                      <p className="m-0 capitalize text-white flex items-center font-normal">
                        ubah profil
                      </p>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
            <div className="flex flex-row py-5 divide-x px-6 rounded-lg shadow-md bg-white">
              <div className="w-full flex flex-col gap-3 pr-4">
                <div className="flex flex-row w-full gap-3">
                  <div className="w-3/5">
                    <h2 className="m-0 text-text capitalize text-sm">
                      pemilik vendor:
                    </h2>
                    <h2 className="m-0 capitalize text-base">
                      {data?.user?.fullname}
                    </h2>
                  </div>
                  <div>
                    <h2 className="m-0 text-text capitalize text-sm">
                      username:
                    </h2>
                    <h2 className="m-0 text-base">{data?.user?.username}</h2>
                  </div>
                </div>
                <div className="flex flex-row w-full gap-3 justify-start">
                  <div className="w-3/5">
                    <h2 className="m-0 text-text capitalize text-sm">email:</h2>
                    <h2 className="m-0 text-base">{data?.user?.email}</h2>
                  </div>
                  <div>
                    <h2 className="m-0 text-text capitalize text-sm">
                      nomor telepon:
                    </h2>
                    <h2 className="m-0 capitalize text-base">
                      {data?.user?.phoneNumber}
                    </h2>
                  </div>
                </div>
                <div className="flex flex-row w-full gap-3 justify-start">
                  <div>
                    <h2 className="m-0 text-text capitalize text-sm">
                      alamat:
                    </h2>
                    <h2 className="m-0 capitalize text-base">
                      {data?.user?.address}
                    </h2>
                  </div>
                </div>
              </div>
              <div className="w-full flex items-center pl-4">
                <div>
                  <h2 className="m-0 text-text capitalize text-base">
                    deskripsi vendor:
                  </h2>
                  <p className="m-0 text-sm">{data?.desc}</p>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default ProfileChild;
