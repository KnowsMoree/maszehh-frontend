import {
  ArrowLeftOutlined,
  CheckOutlined,
  CloseOutlined,
  EditOutlined,
  LeftOutlined,
  LoadingOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import Image from "next/image";
import Img from "/public/avatar/avatar.png";
import Link from "next/link";
// import Input from "/components/input/Input";
import { Button, Col, Form, Input, message, Row, Select, Upload } from "antd";
import { useForm } from "antd/lib/form/Form";
import { useEffect, useState } from "react";
import TextArea from "antd/lib/input/TextArea";
import { vendorRepository } from "../../repository/vendor";
import { regionRepository } from "../../repository/region";
import { mutate } from "swr";
import { useRouter } from "next/router";
import { userRepository } from "../../repository/user";
import { appConfig } from "../../config/app";

const EditUser = () => {
  const [form] = useForm();
  const [loading, setLoading] = useState(true);
  const router = useRouter();

  const [data, setdata] = useState();
  const [provinceId, setProvinceId] = useState();
  const [province, setProvince] = useState();
  const [city, setCity] = useState();
  const [logo, setLogo] = useState([]);

  const { Option } = Select;
  const { data: dataVendor } = vendorRepository.hooks.useVendorProfile();

  const { data: dataProvince } = regionRepository.hooks.useProvince();
  const { data: dataCity } = regionRepository.hooks.useCity(provinceId);

  useEffect(() => {
    if (dataVendor !== null && dataVendor !== undefined) {
      setLoading(false);
      setdata(dataVendor?.data);
      setForm();
      setLogo([{ url: dataVendor?.data?.user?.photo, name: "profile" }]);
      setProvinceId(dataVendor?.data?.city?.province?.id);
    }
  }, [dataVendor]);

  useEffect(() => {
    if (dataProvince !== null && dataProvince !== undefined) {
      setProvince(dataProvince.data);
      setCity(dataCity);
    }
  }, [dataProvince, dataCity]);

  const setForm = async () => {
    form.setFieldsValue({
      id: await dataVendor?.data?.id,
      name: await dataVendor?.data?.name,
      username: await dataVendor?.data?.user?.username,
      email: await dataVendor?.data?.user?.email,
      phoneNumber: await dataVendor?.data?.user?.phoneNumber,
      province: await dataVendor?.data?.city?.province?.name,
      cityId: await dataVendor?.data?.city?.id,
      desc: await dataVendor?.data?.desc,
      address: await dataVendor?.data?.user?.address,
      logo: await dataVendor?.data?.user?.photo,
    });
  };

  const handleImage = async (args) => {
    const file = args.file;
    try {
      const processUpload = await userRepository.manipulateData.uploadUserPhoto(
        file
      );
      setLogo([
        {
          url:
            appConfig.apiUrl + "/users/profile/" + processUpload.body.profile,
          name: "profile",
        },
      ]);

      message.success("upload berhasil");
    } catch (e) {
      console.log(e);
      message.error("upload gagal");
    }
  };

  const onFinish = async () => {
    const data = { ...form.getFieldsValue(), photo: logo[0].url };
    const id = data.id;

    try {
      await vendorRepository.manipulateData.editVendor(id, data);
      await mutate(vendorRepository.url.vendorProfile());

      window.location.replace("/account/vendor");
      message.success("Berhasil mengubah profil");
    } catch (error) {
      console.log(error);
    }
  };

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div
        style={{
          marginTop: 8,
        }}
      >
        Upload
      </div>
    </div>
  );

  return (
    <>
      <div className="py-9 px-8 flex flex-col gap-7 rounded-lg w-3/4 bg-white drop-shadow-md">
        <Form layout="vertical" form={form} onFinish={onFinish}>
          <Row gutter={32}>
            <Col className="gutter-row" span={12}>
              <Row gutter={24} span={24}>
                <Col span={24} style={{ height: "100%" }}>
                  <div className="w-full rounded-lg flex h-56 justify-center items-center self-center bg-bg">
                    <Form.Item
                      name={"logo"}
                      label={
                        <label className="m-0 text-center text-base font-semibold capitalize text-text">
                          logo vendor
                        </label>
                      }
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      rules={[
                        {
                          required: true,
                          message: "logo vendor wajib dilampirkan",
                        },
                      ]}
                    >
                      <Upload
                        name="photo"
                        listType="picture-card"
                        className="avatar-uploader m-0 justify-center items-center flex"
                        style={{
                          height: "100% !important",
                          width: "100% !important",
                          overflow: "hidden",
                        }}
                        onChange={(args) => handleImage(args)}
                        fileList={logo}
                        beforeUpload={() => false}
                        onRemove={() => setLogo([])}
                        maxCount={1}
                      >
                        {uploadButton}
                      </Upload>
                    </Form.Item>
                  </div>
                </Col>
              </Row>
              <Row gutter={24} span={24}>
                <Col span={24}>
                  <Form.Item
                    name={"id"}
                    style={{
                      display: "none",
                    }}
                  >
                    <Input
                      type="text"
                      className="w-full text-base shadow-sm bg-white rounded-md font-medium"
                    />
                  </Form.Item>
                  <Form.Item
                    name={"desc"}
                    rules={[
                      {
                        required: true,
                        message: "alamat harus diisi",
                      },
                    ]}
                    label={
                      <label className="text-base font-semibold text-text">
                        Deskripsi
                      </label>
                    }
                  >
                    <TextArea
                      autoSize={{
                        minRows: 6.5,
                        maxRows: 8,
                      }}
                      type="text"
                      className="w-full text-base shadow-sm bg-white rounded-md font-medium"
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col className="gutter-row" span={12}>
              <Row gutter={32}>
                <Col span={12}>
                  <Form.Item
                    style={{
                      padding: "0",
                    }}
                    name={"username"}
                    label={
                      <label className="text-base font-semibold text-text capitalize">
                        username
                      </label>
                    }
                    rules={[
                      {
                        required: true,
                        message: "isi nama lengkap",
                      },
                      {
                        min: 5,
                        message: "nama lengkap minimal 5 karakter",
                      },
                    ]}
                  >
                    <Input
                      disabled={true}
                      type="text"
                      className="w-full m-0 text-base p-2 shadow-sm bg-white rounded-md font-medium"
                    />
                  </Form.Item>
                  <Form.Item
                    rules={[
                      { required: true, message: "masukan email" },
                      {
                        type: "email",
                        message: "masukan email dengan benar",
                      },
                    ]}
                    name={"email"}
                    label={
                      <label className="text-base font-semibold text-text capitalize">
                        email
                      </label>
                    }
                  >
                    <Input
                      type="text"
                      className="w-full text-base shadow-sm bg-white rounded-md font-medium"
                    />
                  </Form.Item>
                  <Form.Item
                    name={"province"}
                    rules={[
                      {
                        required: true,
                        message: "masukan provinsi",
                      },
                    ]}
                    label={
                      <label className="text-base font-semibold text-text capitalize">
                        provinsi
                      </label>
                    }
                  >
                    <Select
                      size="large"
                      id="province"
                      placeholder="provinsi"
                      onChange={(value) => setProvinceId(value)}
                      className="w-full outline-none text-base rounded-md shadow-sm bg-bg font-medium"
                    >
                      {province?.map((v) => {
                        return (
                          <>
                            <Option
                              key={v.id}
                              className="border-0 text-sm font-medium"
                              value={v.id}
                            >
                              {v.name}
                            </Option>
                          </>
                        );
                      })}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    style={{
                      padding: "0",
                    }}
                    name={"name"}
                    label={
                      <label className="text-base font-semibold text-text capitalize">
                        Nama vendor
                      </label>
                    }
                    rules={[
                      {
                        required: true,
                        message: "isi nama vendor",
                      },
                      {
                        min: 5,
                        message: "nama vendor minimal 5 karakter",
                      },
                    ]}
                  >
                    <Input
                      type="text"
                      className="w-full m-0 text-base p-2 shadow-sm bg-white rounded-md font-medium"
                    />
                  </Form.Item>

                  <Form.Item
                    rules={[
                      { required: true, message: "masukan nomor telepon" },
                      {
                        max: 13,
                        message: "nomor telepon maksimal 13 karakter",
                      },
                      {
                        pattern: new RegExp(/[0-9]{4}[0-9]{4}[0-9]{3,4}/g),
                        message: "masukan hanya angka",
                      },
                    ]}
                    name={"phoneNumber"}
                    label={
                      <label className="text-base font-semibold text-text">
                        Nomor Telepon
                      </label>
                    }
                  >
                    <Input
                      type="text"
                      className="w-full text-base shadow-sm bg-white rounded-md font-medium"
                    />
                  </Form.Item>
                  <Form.Item
                    name={"cityId"}
                    rules={[
                      {
                        required: true,
                        message: "masukan kota",
                      },
                    ]}
                    label={
                      <label className="text-base font-semibold text-text">
                        Kota
                      </label>
                    }
                  >
                    <Select
                      size="large"
                      id="kota"
                      placeholder="kota"
                      className="w-full text-base rounded-md shadow-sm bg-bg font-medium"
                    >
                      {city?.data?.map((v) => {
                        return (
                          <>
                            <Option
                              key={v.id}
                              className="border-0 text-sm font-medium"
                              value={v.id}
                            >
                              {v.name}
                            </Option>
                          </>
                        );
                      })}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <Form.Item
                name={"address"}
                rules={[
                  {
                    required: true,
                    message: "alamat harus diisi",
                  },
                ]}
                label={
                  <label className="text-base font-semibold text-text">
                    Alamat
                  </label>
                }
              >
                <TextArea
                  autoSize={{
                    minRows: 4,
                    maxRows: 6,
                  }}
                  type="text"
                  className="w-full text-base shadow-sm bg-white rounded-md font-medium"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row className="flex mt-4 flex-row gap-6 justify-end">
            <Link href={`/account/vendor/`}>
              <Button
                type="primary"
                icon={<CloseOutlined />}
                size="large"
                className="text-sm font-semibold w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-main py-[11px] px-[21px] rounded-lg border-none flex items-center"
              >
                Batal
              </Button>
            </Link>
            <Button
              type="primary"
              htmlType="submit"
              icon={<CheckOutlined />}
              size="large"
              className="text-sm font-semibold w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-blue-500 py-[11px] px-[21px] rounded-lg border-none flex items-center"
            >
              Simpan
            </Button>
          </Row>
        </Form>
      </div>
    </>
  );
};

export default EditUser;
