import {
  LoadingOutlined,
  CheckOutlined,
  ArrowLeftOutlined,
  UploadOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import {
  Col,
  Form,
  Row,
  Spin,
  Input,
  Button,
  Select,
  Upload,
  message,
  InputNumber,
} from "antd";

import Img from "/public/avatar/avatar.png";
import Link from "next/link";
// import Input from "/components/input/Input";
import { userRepository } from "../../../repository/user";
import { useEffect, useState } from "react";
import ImgCrop from "antd-img-crop";
import { useForm } from "antd/lib/form/Form";
import { useRouter } from "next/router";
import { appConfig } from "../../../config/app";
import { packageRepository } from "../../../repository/package";
import { categoryRepository } from "../../../repository/category";
import { formatRupiah } from "../../../helper/currencyFormat";

const CreatePackage = (props) => {
  const id = props.param;
  const [user, setUser] = useState({});
  const [category, setCategory] = useState();
  const { Option } = Select;
  const [form] = useForm();
  const router = useRouter();

  const [loading, setLoading] = useState(false);

  const { data: dataCategory } =
    categoryRepository.hooks.useCategory();

  useEffect(() => {
    if (
      dataCategory !== null &&
      dataCategory !== undefined
    ) {
      setCategory(dataCategory?.data);
    }
  }, [dataCategory]);

  const [fileList, setFileList] = useState();
  const [uang, setUang] = useState(0);

  const onFinish = async (values) => {
    console.log(form.getFieldsValue());
    try {
      const data = {
        ...values,
        photoPackage: fileList[0].url,
      };

      await packageRepository.manipulateData.createPackage(
        data
      );

      setTimeout(() => {
        router.push(`/vendor/packages`);
        message.success("Data Berhasil Ditambah");
      }, 1000);
    } catch (error) {
      console.log(error.response);
      message.error("Gagal menambah");
    }
  };

  const onPreview = async (file) => {
    let src = file.url;

    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);

        reader.onload = () =>
          resolve(reader.result);
      });
    }

    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };

  const beforeUpload = (file) => {
    const isJpgOrPng =
      file.type === "image/jpeg" ||
      file.type === "image/png";

    if (!isJpgOrPng) {
      message.error(
        "You can only upload JPG/PNG file!"
      );
    }

    const isLt2M = file.size / 1024 / 1024 < 2;

    if (!isLt2M) {
      message.error(
        "Image must smaller than 2MB!"
      );
    }

    return isJpgOrPng && isLt2M;
  };

  const handleImage = async (args) => {
    const file = args.file;
    try {
      const processUpload =
        await packageRepository.manipulateData.uploadPackagePhoto(
          file
        );
      console.log(
        processUpload.body.pack,
        "file"
      );
      setFileList([
        {
          url:
            appConfig.apiUrl +
            "/package/photo/" +
            processUpload.body.pack,
          name: "package",
        },
      ]);

      message.success("berhasil upload");
    } catch (e) {
      console.log(e);
      message.error("gagal upload");
    }
  };

  const uploadButton = (
    <div>
      {loading ? (
        <LoadingOutlined />
      ) : (
        <PlusOutlined />
      )}
      <div
        style={{
          marginTop: 8,
        }}>
        Upload
      </div>
    </div>
  );

  // console.log(fileList, "filelist");

  return (
    <>
      <div className="py-9 px-8 flex flex-col gap-7 rounded-lg w-3/4 bg-white drop-shadow-md">
        <div className="w-full flex flex-row justify-between">
          <Link href={`/vendor/packages`}>
            <div className="flex group cursor-pointer bg-transparent rounded-lg  transition-all items-center flex-row justify-center gap-x-4 py-2 px-4">
              <ArrowLeftOutlined className="text-text group-hover:text-yellow-500 duration-300 text-base flex items-center" />
              <h2 className="capitalize m-0 text-text group-hover:text-yellow-500 duration-300 text-base font-semibold">
                kembali
              </h2>
            </div>
          </Link>
        </div>
        <Form
          form={form}
          layout="vertical"
          onFinish={onFinish}
          size="large">
          <Row gutter={32}>
            <Col className="gutter-row" span={10}>
              <div className="w-full px-5 rounded-lg flex-col gap-y-2 flex h-full justify-center items-center bg-bg">
                <Form.Item
                  name={"photo"}
                  label={
                    <label className="m-0 text-center text-base font-semibold capitalize">
                      foto paket layanan
                    </label>
                  }>
                  <ImgCrop aspect={2 / 1}>
                    <Upload
                      name="photo"
                      listType="picture-card"
                      className="avatar-uploader flex justify-center"
                      fileList={fileList}
                      beforeUpload={beforeUpload}
                      customRequest={(args) =>
                        handleImage(args)
                      }
                      onPreview={onPreview}
                      onRemove={() =>
                        setFileList([])
                      }
                      maxCount={1}>
                      {uploadButton}
                    </Upload>
                  </ImgCrop>
                </Form.Item>
              </div>
            </Col>
            <Col className="gutter-row" span={7}>
              <Form.Item
                name={"name"}
                label={
                  <label className="text-base font-semibold text-black capitalize">
                    Nama paket layanan
                  </label>
                }
                rules={[
                  {
                    required: true,
                    message:
                      "nama paket harus diisi",
                  },
                ]}>
                <Input
                  type="text"
                  placeholder="masukan nama paket"
                  className="w-full text-base shadow-sm bg-bg font-medium"
                />
              </Form.Item>
              <Form.Item
                name={"categoryId"}
                rules={[
                  {
                    required: true,
                    message:
                      "kategori harus diisi",
                  },
                ]}
                label={
                  <label className="text-base font-semibold text-black capitalize">
                    Kategori
                  </label>
                }>
                <Select
                  placeholder="pilih kategori paket"
                  className="w-full text-base shadow-sm bg-bg font-semibold">
                  {category?.map((v) => {
                    return (
                      <Option
                        key={v.id}
                        value={v.id}
                        className="text-text font-medium text-base">
                        {v.categoryName}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>
              <Form.Item
                name={"price"}
                extra="Harga untuk per-orang dan per-hari"
                rules={[
                  {
                    required: true,
                    message: "harga harus diisi",
                  },
                ]}
                label={
                  <label className="text-base font-semibold text-black capitalize">
                    harga : {formatRupiah(uang)}
                  </label>
                }>
                <InputNumber
                  // defaultValue={50000}
                  // formatter={(value) => `${formatRupiah(value)}`}
                  onChange={(value) =>
                    setUang(value)
                  }
                  type="text"
                  placeholder="masukan harga per tukang"
                  className="w-full text-base shadow-sm bg-bg font-medium"
                />
              </Form.Item>
              <Form.Item
                name={"minHandymanQty"}
                label={
                  <label className="text-base font-semibold text-black capitalize">
                    Jumlah minimal tukang
                  </label>
                }
                rules={[
                  {
                    required: true,
                    message:
                      "jumlah minimal tukang harus diisi",
                  },
                  {
                    type: "number",
                    min: 1,
                    message:
                      "jumlah minimal tukang 1",
                  },
                ]}>
                <InputNumber
                  type="text"
                  placeholder="masukan minimal tukang"
                  className="w-full text-base shadow-sm bg-bg font-medium"
                  addonAfter="Orang"
                />
              </Form.Item>
            </Col>
            <Col className="gutter-row" span={7}>
              <Form.Item
                name={"desc"}
                className="w-full m-0"
                rules={[
                  {
                    required: true,
                    message:
                      "deskripsi harus diisi",
                  },
                ]}
                label={
                  <label className="text-base font-semibold text-black capitalize">
                    deskripsi
                  </label>
                }>
                <Input.TextArea
                  autoSize={{
                    minRows: 15,
                    maxRows: 15,
                  }}
                  placeholder="deskripsi memuat informasi detail mengenai paket layanan "
                  type="text"
                  className="w-full text-base shadow-sm bg-bg font-medium"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row className="flex flex-row gap-6 justify-end">
            <Button
              type="primary"
              htmlType="submit"
              icon={<CheckOutlined />}
              size="large"
              className="text-sm font-semibold w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-blue-500 py-[11px] px-[21px] rounded-lg border-none flex items-center">
              Simpan
            </Button>
          </Row>
        </Form>
      </div>
    </>
  );
};

export default CreatePackage;
