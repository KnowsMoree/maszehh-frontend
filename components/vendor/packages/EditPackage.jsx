import {
  LoadingOutlined,
  CheckOutlined,
  ArrowLeftOutlined,
  UploadOutlined,
  PlusOutlined,
  CloseOutlined,
} from "@ant-design/icons";
import {
  Col,
  Form,
  Row,
  Spin,
  Input,
  Button,
  Select,
  Upload,
  message,
  InputNumber,
} from "antd";
import Image from "next/image";
import Img from "/public/avatar/avatar.png";
import Link from "next/link";
// import Input from "/components/input/Input";
import { userRepository } from "../../../repository/user";
import { useEffect, useState } from "react";
import { useForm } from "antd/lib/form/Form";
import { useRouter } from "next/router";
import { appConfig } from "../../../config/app";
import { packageRepository } from "../../../repository/package";
import { categoryRepository } from "../../../repository/category";
import { formatRupiah } from "../../../helper/currencyFormat";
import { mutate } from "swr";
import ImgCrop from "antd-img-crop";

const EditPackage = (props) => {
  const id = props?.param;

  const [category, setCategory] = useState();
  const [pack, setPack] = useState({});
  const [loading, setLoading] = useState(true);

  const { Option } = Select;
  const [form] = useForm();
  const router = useRouter();

  const { data: detailPackage } =
    packageRepository.hooks.usePackageDetail(id);
  const { data: dataCategory } =
    categoryRepository.hooks.useCategory();

  useEffect(() => {
    if (
      dataCategory !== null &&
      dataCategory !== undefined
    ) {
      setCategory(dataCategory?.data);
    }

    if (
      detailPackage !== undefined &&
      detailPackage !== null
    ) {
      setPack(detailPackage?.data);
      setForm();
      setFileList([
        {
          url: detailPackage?.data?.photo,
          name: "package",
        },
      ]);

      setTimeout(() => {
        setLoading(false);
      }, 1000);
    }
  }, [dataCategory, detailPackage]);

  const [fileList, setFileList] = useState();
  const [uang, setUang] = useState(0);

  const onFinish = async (values) => {
    console.log(form.getFieldsValue());
    try {
      const data = {
        ...values,
        photoPackage: fileList[0].url,
      };
      const id = data.id;
      console.log(data, "simpannn");

      await packageRepository.manipulateData.editPackage(
        id,
        data
      );

      setTimeout(() => {
        router.push(`/vendor/packages`);
        message.success("Data Berhasil Ditambah");
      }, 1000);
    } catch (error) {
      console.log(error.response);
      message.error("Gagal menambah");
    }
  };

  const handleImage = async (args) => {
    const file = args.file;
    try {
      const processUpload =
        await packageRepository.manipulateData.uploadPackagePhoto(
          file
        );
      console.log(
        processUpload.body.pack,
        "file"
      );
      setFileList([
        {
          url:
            appConfig.apiUrl +
            "/package/photo/" +
            processUpload.body.pack,
          name: "package",
        },
      ]);

      message.success("berhasil upload");
    } catch (e) {
      console.log(e);
      message.error("gagal upload");
    }
  };

  const setForm = async () => {
    form.setFieldsValue({
      id: await detailPackage?.data?.id,
      name: await detailPackage?.data?.name,
      categoryId: await detailPackage?.data
        ?.category?.id,
      price: await detailPackage?.data?.price,
      minHandymanQty: await detailPackage?.data
        ?.minHandymanQty,
      desc: await detailPackage?.data?.desc,
    });

    setUang(detailPackage?.data?.price);
  };

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  const uploadButton = (
    <div>
      <PlusOutlined />

      <div
        style={{
          marginTop: 8,
        }}>
        Upload
      </div>
    </div>
  );

  const onPreview = async (file) => {
    let src = file.url;

    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);

        reader.onload = () =>
          resolve(reader.result);
      });
    }

    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };

  const beforeUpload = (file) => {
    const isJpgOrPng =
      file.type === "image/jpeg" ||
      file.type === "image/png";

    if (!isJpgOrPng) {
      message.error(
        "You can only upload JPG/PNG file!"
      );
    }

    const isLt2M = file.size / 1024 / 1024 < 2;

    if (!isLt2M) {
      message.error(
        "Image must smaller than 2MB!"
      );
    }

    return isJpgOrPng && isLt2M;
  };

  return (
    <>
      <div className="py-9 px-8 flex flex-col gap-7 rounded-lg w-3/4 bg-white drop-shadow-md">
        {loading ? (
          <div className="w-full h-auto flex items-center justify-center">
            <Spin indicator={loadingIcon} />
          </div>
        ) : (
          <>
            <Form
              form={form}
              layout="vertical"
              onFinish={onFinish}
              size="large">
              <Row gutter={32}>
                <Col
                  className="gutter-row"
                  span={10}>
                  <div className="w-full px-5 rounded-lg flex-col gap-y-2 flex h-full justify-center items-center bg-bg">
                    <Form.Item name={"photo"}>
                      <ImgCrop aspect={2 / 1}>
                        <Upload
                          name="photo"
                          listType="picture-card"
                          className="avatar-uploader"
                          beforeUpload={
                            beforeUpload
                          }
                          customRequest={(args) =>
                            handleImage(args)
                          }
                          fileList={fileList}
                          onRemove={() =>
                            setFileList([])
                          }
                          onPreview={onPreview}
                          maxCount={1}>
                          {uploadButton}
                        </Upload>
                      </ImgCrop>
                    </Form.Item>
                  </div>
                </Col>
                <Col
                  className="gutter-row"
                  span={7}>
                  <Form.Item
                    name={"id"}
                    style={{ display: "none" }}>
                    <Input
                      type="text"
                      className="w-full text-base shadow-sm bg-bg font-semibold"
                    />
                  </Form.Item>
                  <Form.Item
                    name={"name"}
                    rules={[
                      {
                        required: true,
                        message:
                          "nama paket harus diisi",
                      },
                    ]}
                    label={
                      <label className="text-base font-semibold text-text">
                        Nama paket layanan
                      </label>
                    }>
                    <Input
                      type="text"
                      placeholder="masukan nama paket"
                      className="w-full text-base shadow-sm bg-bg font-semibold"
                    />
                  </Form.Item>
                  <Form.Item
                    name={"categoryId"}
                    label={
                      <label className="text-base font-semibold text-text">
                        Kategori
                      </label>
                    }
                    rules={[
                      {
                        required: true,
                        message:
                          "kategori harus diisi",
                      },
                    ]}>
                    <Select
                      placeholder="pilih kategori paket"
                      className="w-full text-base shadow-sm bg-bg font-semibold">
                      {category?.map((v) => {
                        return (
                          <Option
                            key={v.id}
                            value={v.id}
                            className="text-text font-medium text-base">
                            {v.categoryName}
                          </Option>
                        );
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name={"price"}
                    rules={[
                      {
                        required: true,
                        message:
                          "harga harus diisi",
                      },
                    ]}
                    label={
                      <label className="text-base font-semibold text-text">
                        harga :{" "}
                        {formatRupiah(uang)}
                      </label>
                    }>
                    <InputNumber
                      // defaultValue={100000}
                      // formatter={(value) => `${formatRupiah(value)}`}
                      onChange={(value) =>
                        setUang(value)
                      }
                      type="text"
                      placeholder="masukan harga per tukang"
                      className="w-full text-base shadow-sm bg-bg font-semibold"
                    />
                  </Form.Item>
                  <Form.Item
                    name={"minHandymanQty"}
                    label={
                      <label className="text-base font-semibold text-text">
                        Jumlah minimal tukang
                      </label>
                    }>
                    <InputNumber
                      type="text"
                      placeholder="masukan minimal tukang"
                      className="w-full text-base shadow-sm bg-bg font-semibold"
                      addonAfter="Orang"
                    />
                  </Form.Item>
                </Col>
                <Col
                  className="gutter-row"
                  span={7}>
                  <Form.Item
                    name={"desc"}
                    rules={[
                      {
                        required: true,
                        message:
                          "deskripsi harus diisi",
                      },
                    ]}
                    className="w-full m-0"
                    label={
                      <label className="text-base font-semibold text-text">
                        deskripsi
                      </label>
                    }>
                    <Input.TextArea
                      autoSize={{
                        minRows: 15,
                        maxRows: 15,
                      }}
                      placeholder="deskripsi memuat informasi detail mengenai paket layanan "
                      type="text"
                      className="w-full text-base shadow-sm bg-bg font-semibold"
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row className="flex flex-row gap-6 justify-end">
                <Link
                  href={`/vendor/packages/${pack?.id}`}>
                  <Button
                    type="primary"
                    icon={<CloseOutlined />}
                    size="large"
                    className="text-sm font-semibold w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-main py-[11px] px-[21px] rounded-lg border-none flex items-center">
                    Batal
                  </Button>
                </Link>
                <Button
                  type="primary"
                  htmlType="submit"
                  icon={<CheckOutlined />}
                  size="large"
                  className="text-sm font-semibold w-auto cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-blue-500 py-[11px] px-[21px] rounded-lg border-none flex items-center">
                  Simpan
                </Button>
              </Row>
            </Form>
          </>
        )}
      </div>
    </>
  );
};

export default EditPackage;
