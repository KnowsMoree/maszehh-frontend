import Navbar from "/components/templates/Navbar";
import ShortFooter from "/components/templates/ShortFooter";
import NavComponent from "/components/templates/NavComponent";
import { parseJwt } from "/helper/parseJwt";
import { useEffect, useState } from "react";
import { userRepository } from "../../repository/user";

const ProfileLayout = ({
  children,
  ...props
}) => {
  const [isLoggedIn, setIsLoggedIn] =
    useState(false);
  const [user, setUser] = useState({});
  const [id, setId] = useState("");

  const { data: dataDetailUser } =
    userRepository.hooks.useUserDetail(id);

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    const decodeJwt = parseJwt(token);
    if (decodeJwt) {
      setId(decodeJwt.id);
      setUser(dataDetailUser?.data);
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }, [dataDetailUser]);

  return (
    <>
      <Navbar
        user_login={isLoggedIn}
        active={props?.navbarActive}
        user={user}
      />
      <div className="w-full flex flex-col justify-center relative top-[108px]">
        <div
          className={`${
            props?.noNavigation ? "pt-5" : "pt-3"
          } flex flex-row h-[calc(100vh-132px-104px)] gap-x-9 ${
            props?.center ? "items-center" : ""
          } justify-center bg-bg`}>
          <div className="w-1/3">{children}</div>
        </div>
        <ShortFooter />
      </div>
    </>
  );
};

export default ProfileLayout;
