import Link from "next/link";

const NavComponent = (props) => {
  return (
    <>
      <div className="flex flex-col divide-y-2 sticky top-[108px]">
        <div className="w-full py-1 flex flex-col gap-y-2">
          <Link
            href={`/account/user/${props.user.id}`}>
            <h2
              className={`m-0 font-semibold cursor-pointer hover:text-main transition-colors text-base capitalize ${
                props.active === "profil"
                  ? "text-main"
                  : "text-text"
              }`}>
              profil
            </h2>
          </Link>
          <Link
            href={`/account/user/review/${props.user.id}`}>
            <h2
              className={`m-0 font-semibold cursor-pointer hover:text-main transition-colors text-base capitalize ${
                props.active === "ulasan"
                  ? "text-main"
                  : "text-text"
              }`}>
              ulasan
            </h2>
          </Link>
          <Link
            href={`/account/user/payment/${props.user.id}`}>
            <h2
              className={`m-0 font-semibold cursor-pointer hover:text-main transition-colors text-base capitalize ${
                props.active ===
                "menunggu pembayaran"
                  ? "text-main"
                  : "text-text"
              }`}>
              menunggu pembayaran
            </h2>
          </Link>
          <Link
            href={`/account/user/transaction/${props.user.id}`}>
            <h2
              className={`m-0 font-semibold cursor-pointer hover:text-main transition-colors text-base capitalize ${
                props.active ===
                "daftar transaksi"
                  ? "text-main"
                  : "text-text"
              }`}>
              daftar transaksi
            </h2>
          </Link>
        </div>
      </div>
    </>
  );
};

export default NavComponent;
