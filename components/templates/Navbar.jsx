import { useState, useEffect } from "react";
import Image from "next/image";
import LogoBlack from "/public/logo/logoTypoBlack.svg";
import Link from "next/link";
import { Fragment } from "react";
import {
  Menu,
  Popover,
  Transition,
} from "@headlessui/react";
import {
  BellFilled,
  BellOutlined,
  CheckCircleTwoTone,
  CloseCircleTwoTone,
  DownOutlined,
  EyeInvisibleOutlined,
  EyeOutlined,
  InfoCircleTwoTone,
  UpOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {
  Avatar,
  Badge,
  Empty,
  message,
} from "antd";
import { useRouter } from "next/router";
import { notifficationRepository } from "../../repository/notification";
import { mutate } from "swr";

const Navbar = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [navShadow, setNavShadow] = useState("");
  const [navColor, setnavColor] = useState(
    "transparent"
  );
  const router = useRouter();
  const [notif, setNotif] = useState([]);
  const [notifCount, setNotifCount] = useState(0);

  const { data: userNotif } =
    notifficationRepository.hooks.useNotificationUser(
      props?.user?.id
    );

  useEffect(() => {
    const token = localStorage.getItem(token);
    if (
      userNotif !== undefined &&
      userNotif !== null
    ) {
      console.log(userNotif);
      setNotif(userNotif?.data);
      setNotifCount(userNotif?.count);
    }
  }, [userNotif]);

  const listenScrollEvent = () => {
    window.scrollY > 90
      ? setnavColor("backdrop-blur bg-white/50")
      : setnavColor("transparent");
    // window.scrollY > 90 ? setNavShadow("drop-shadow-lg") : setNavShadow("");
  };
  useEffect(() => {
    window.addEventListener(
      "scroll",
      listenScrollEvent
    );
    return () => {
      window.removeEventListener(
        "scroll",
        listenScrollEvent
      );
    };
  }, []);

  const readNotif = async (id) => {
    console.log(id, "id notifikasi");
    try {
      await notifficationRepository.manipulateData.updateReadAll(
        id
      );
      await mutate(
        notifficationRepository.url.notificationUser(
          props?.id
        )
      );
    } catch (error) {
      console.log(error);
      message.error("terjadi kesalahan");
    }
  };

  const linkPrivate = [
    {
      href: "/package",
      name: "Paket",
    },
    {
      href: `/account/user/order/${props?.user?.id}`,
      name: "Pesanan",
    },
    {
      href: `/account/user/payment/${props?.user?.id}`,
      name: "Pembayaran",
    },
    {
      href: `/account/user/review/${props?.user?.id}`,
      name: "Ulasan",
    },
  ];

  const handleLogout = () => {
    localStorage.removeItem("token");

    router.push("/");
  };

  if (props.user_login === false) {
    return (
      <>
        <div
          className={`w-full py-[30px] px-[84px] ${navColor} transition-all z-50 flex justify-between flex-row ${navShadow} fixed`}>
          <div className="justify-start flex w-full">
            <Link href={"/"}>
              <Image
                className="cursor-pointer"
                src={LogoBlack}
                alt="logo"
                width={185}
                height={40}
              />
            </Link>
          </div>
          <div className="flex gap-8 justify-end items-center w-full flex-row">
            <Link href={"/register"}>
              <h2 className="py-[11px] m-0 px-[29px] text-[14px] font-semibold hover:text-secondary transition-colors text-main cursor-pointer">
                Daftar
              </h2>
            </Link>
            <Link href={"/login"}>
              <h2 className="hover:bg-secondary m-0 text-[14px] font-semibold transition-colors bg-main py-[11px] px-[29px] text-white rounded-lg cursor-pointer">
                Masuk
              </h2>
            </Link>
          </div>
        </div>
      </>
    );
  } else {
    return (
      <>
        <div
          className={`w-full py-[30px] px-[84px] bg-white z-50 flex justify-between flex-row drop-shadow-sm fixed ${scrollY}`}>
          <div className="w-full flex justify-start">
            <Link href={"/"}>
              <Image
                className="cursor-pointer"
                src={LogoBlack}
                alt="logo"
                width={185}
                height={40}
              />
            </Link>
          </div>

          <div className="flex w-full flex-row gap-11 justify-center items-center">
            {linkPrivate.map((v, i) => {
              return (
                <div className="" key={i}>
                  <Link href={v.href}>
                    <p
                      className={`${
                        props.active === v.name
                          ? "text-main"
                          : "text-justBlack"
                      } text-md lg:text-lg font-medium hover:text-main transition-[1s] cursor-pointer m-0`}>
                      {v.name}
                    </p>
                  </Link>
                </div>
              );
            })}
          </div>
          <div className="flex items-center justify-end cursor-pointer w-full ">
            <Popover className="relative">
              {({ open }) => (
                <>
                  <Popover.Button
                    className={`
                ${open ? "" : "text-opacity-90"}
                group inline-flex items-center rounded-full p-3 text-base font-medium ${
                  open
                    ? "hover:bg-main/40"
                    : "hover:bg-slate-600/10"
                } transition-all text-white hover:text-opacity-100 focus:outline-none`}>
                    <Badge
                      size="small"
                      count={notifCount}>
                      <BellFilled
                        className={`${
                          open
                            ? "text-main/70"
                            : "text-text"
                        } text-lg flex items-center transition-all ease-in-out`}
                      />
                    </Badge>
                  </Popover.Button>
                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-200"
                    enterFrom="opacity-0 translate-y-1"
                    enterTo="opacity-100 translate-y-0"
                    leave="transition ease-in duration-150"
                    leaveFrom="opacity-100 translate-y-0"
                    leaveTo="opacity-0 translate-y-1">
                    <Popover.Panel className="absolute left-0 z-10 mt-3 w-screen max-w-sm -translate-x-[40%] transform px-4 sm:px-0 lg:max-w-xs">
                      <div className="overflow-hidden divide-y-[1px] divide-dashed divide-slate-600/50 rounded-lg shadow-lg ring-1 ring-black ring-opacity-5">
                        <div className="bg-white py-4 px-5 cursor-default flex justify-between">
                          <div>
                            <span className="text-base font-semibold text-gray-900">
                              Notifikasi
                            </span>
                            <span className="block text-sm text-gray-500">
                              Kamu mendapatkan{" "}
                              {notifCount} pesan
                              yang belum dibaca
                            </span>
                          </div>
                        </div>
                        <div
                          className={`relative grid gap-8 max-h-[450px] bg-white p-7 lg:grid-cols-1 ${
                            notifCount > 4
                              ? "overflow-y-scroll overscroll-y-auto"
                              : "overflow-hidden"
                          }`}>
                          {notif?.length == 0 ? (
                            <div className="w-full h-auto flex items-center justify-center">
                              <Empty
                                image={
                                  Empty.PRESENTED_IMAGE_SIMPLE
                                }
                                description={
                                  "tidak ada notifikasi terbaru"
                                }
                              />
                            </div>
                          ) : (
                            notif?.map((item) => (
                              <a
                                key={item?.id}
                                href={
                                  item?.pathAction
                                }
                                onClick={() =>
                                  readNotif(
                                    item?.id
                                  )
                                }
                                className="-m-3 flex items-center rounded-lg p-2 transition duration-150 ease-in-out hover:bg-gray-50 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50">
                                {item?.title ==
                                  "Pembayaran Ditolak" ||
                                item?.title ==
                                  "Pesanan Ditahan" ? (
                                  <CloseCircleTwoTone
                                    twoToneColor="#FF3B3F"
                                    className="flex justify-center text-[24px]"
                                  />
                                ) : (
                                  <InfoCircleTwoTone className="flex justify-center text-[24px]" />
                                )}
                                <div className="ml-4">
                                  <p className="text-sm font-bold text-gray-900">
                                    {item?.title}
                                  </p>
                                  <p className="text-sm text-gray-500">
                                    {
                                      item?.message
                                    }
                                  </p>
                                </div>
                              </a>
                            ))
                          )}
                        </div>
                      </div>
                    </Popover.Panel>
                  </Transition>
                </>
              )}
            </Popover>
            <Menu
              className="relative py-[2px] m-0"
              as="div">
              <Menu.Button
                as="div"
                className="cursor-pointer px-2 flex flex-row gap-x-2"
                onClick={() =>
                  setIsOpen(!isOpen)
                }>
                {props.user?.photo !== null ? (
                  <img
                    src={props.user?.photo}
                    alt={props.user?.photo}
                    className="rounded-full h-11"
                    style={{
                      display: "flex",
                      alignItems: "center",
                    }}
                  />
                ) : (
                  <Avatar
                    size={{
                      xs: 24,
                      sm: 32,
                      md: 40,
                      lg: 35,
                      xl: 35,
                      xxl: 40,
                    }}
                    style={{
                      backgroundColor:
                        "transparent",
                      display: "flex",
                      alignItems: "center",
                      width: "100%",
                    }}
                    icon={
                      <UserOutlined className="text-black text-[20px]" />
                    }
                  />
                )}
                <UpOutlined
                  className={`${
                    isOpen ? "" : "rotate-180"
                  } text-lg text-text flex duration-300 items-center m-0`}
                />
              </Menu.Button>
              <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95">
                <Menu.Items
                  as="div"
                  className="bg-white w-full absolute right-0 mt-4 gap-y-1 flex flex-col p-2 outline-none drop-shadow-md rounded-md text-sm font-medium">
                  <Link
                    href={`/account/user/${props.user?.id}`}>
                    <Menu.Item
                      as={"div"}
                      className="hover:bg-main transition-colors px-2 py-1 group rounded-sm select-none">
                      <p className="text-text text-base font-medium group-hover:text-white m-0 transition-colors">
                        profil
                      </p>
                    </Menu.Item>
                  </Link>
                  <Menu.Item
                    onClick={handleLogout}
                    as={"div"}
                    className="w-full rounded-sm hover:bg-main transition-colors px-2 py-1 group select-none">
                    <p className="text-text text-base font-medium group-hover:text-white m-0 transition-colors">
                      keluar
                    </p>
                  </Menu.Item>
                </Menu.Items>
              </Transition>
            </Menu>
          </div>
        </div>
      </>
    );
  }
};

export default Navbar;
