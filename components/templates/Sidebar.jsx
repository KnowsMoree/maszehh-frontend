import {
  BankOutlined,
  ContainerOutlined,
  CreditCardOutlined,
  DashboardOutlined,
  FundOutlined,
  HddOutlined,
  InboxOutlined,
  ReconciliationOutlined,
  ShopOutlined,
  UserOutlined,
} from "@ant-design/icons";
import Image from "next/image";
import NavLink from "/components/micro/NavLink";
import Logo from "/public/logo/logo.png";
import { parseJwt } from "../../helper/parseJwt";
import { useEffect, useState } from "react";

const Sidebar = (props) => {
  const adminData = [
    {
      link: "/admin",
      icon: (
        <DashboardOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Dashboard",
    },
    {
      link: "/admin/user",
      icon: (
        <UserOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Pengguna",
    },
    {
      link: "/admin/vendor",
      icon: (
        <ShopOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Vendor",
    },
    {
      link: "/admin/category",
      icon: (
        <HddOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Kategori",
    },
    {
      link: "/admin/bank",
      icon: (
        <BankOutlined
          className={`text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Data Bank",
    },
    {
      link: "/admin/packages",
      icon: (
        <InboxOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Paket Layanan",
    },
    {
      link: "/admin/order",
      icon: (
        <ReconciliationOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Pesanan",
    },
    {
      link: "/admin/payment/order",
      icon: (
        <ContainerOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Pembayaran Pesanan",
    },
    {
      link: "/admin/payment/vendor",
      icon: (
        <CreditCardOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Pembayaran Vendor",
    },
    {
      link: "/admin/report",
      icon: (
        <FundOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Laporan pesanan",
    },
  ];

  const vendorData = [
    {
      link: "/vendor",
      icon: (
        <DashboardOutlined
          className={`text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Dashboard",
    },
    {
      link: "/vendor/packages",
      icon: (
        <InboxOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Paket Layanan",
    },
    {
      link: "/vendor/order",
      icon: (
        <ReconciliationOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Pesanan",
    },
    {
      link: "/vendor/bank",
      icon: (
        <BankOutlined
          className={`text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Data Bank",
    },
    {
      link: "/vendor/payment",
      icon: (
        <ContainerOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Pembayaran",
    },
    {
      link: "/vendor/report",
      icon: (
        <FundOutlined
          className={` text-xl flex items-center ${
            props.active === props.name
              ? "text-main"
              : "text-text"
          }`}
        />
      ),
      name: "Laporan pesanan",
    },
  ];

  const Link = (props) => {
    if (props.user === "admin") {
      return adminData.map((v, i) => {
        return (
          <>
            <div key={i}>
              <NavLink
                link={v.link}
                active={props.active}
                name={v.name}>
                {v.icon}
                <h2
                  className={`text-base text-[17px] mb-0 ${
                    props.active === v.name
                      ? "text-black"
                      : "text-text group-hover:text-black"
                  }`}>
                  {v.name}
                </h2>
              </NavLink>
            </div>
          </>
        );
      });
    } else if (
      props.user === "vendor" &&
      props.status == "active"
    ) {
      return vendorData.map((v, i) => {
        return (
          <>
            <div key={i}>
              <NavLink
                link={v.link}
                active={props.active}
                name={v.name}>
                {v.icon}
                <h2
                  className={`text-base font-medium text-[17px] mb-0 ${
                    props.active === v.name
                      ? "text-black"
                      : "text-text group-hover:text-black"
                  }`}>
                  {v.name}
                </h2>
              </NavLink>
            </div>
          </>
        );
      });
    } else if (
      props.user === "vendor" &&
      props.status == "pending"
    ) {
      return (
        <>
          <div>
            <NavLink
              link={vendorData[0].link}
              active={props.active}
              name={vendorData[0].name}>
              {vendorData[0].icon}
              <h2
                className={`text-base font-medium text-[17px] mb-0 ${
                  props.active ===
                  vendorData[0].name
                    ? "text-black"
                    : "text-text group-hover:text-black"
                }`}>
                {vendorData[0].name}
              </h2>
            </NavLink>
          </div>
        </>
      );
    }
  };

  return (
    <>
      <div className="min-h-full w-80 shadow-lg fixed left-0 px-10 py-12 bg-white">
        <div className="flex justify-center mb-16">
          <Image
            src={Logo}
            alt="logo"
            width={"184px"}
            height={"39px"}
          />
        </div>
        <div className="flex flex-col overflow-ellipsis gap-2">
          <Link
            user={props.user}
            active={props.active}
            name={props.name}
            status={props.userStatus}
          />
        </div>
      </div>
    </>
  );
};

export default Sidebar;
