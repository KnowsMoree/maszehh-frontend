import {
  FacebookFilled,
  InstagramOutlined,
  MailOutlined,
  PhoneOutlined,
  TwitterOutlined,
} from "@ant-design/icons";
import Image from "next/image";
import Link from "next/link";
import Logo from "/public/logo/logowhite.png";
import Line from "/components/micro/Line";

const Footer = () => {
  return (
    <>
      <div className="w-full bg-footer py-[46px] px-[84px] flex flex-col gap-[30px]">
        <div className="flex max-h-fit w-full justify-center">
          <Image src={Logo} alt="image" />
        </div>
        <div className="flex flex-row justify-center items-center gap-32">
          <div className="flex flex-col">
            <h1 className="font-bold text-[24px] text-white mb-[37px]">
              Layanan
            </h1>
            <div className="flex flex-col gap-[20px]">
              <Link href={"/paket"}>
                <h5 className="text-base text-[14px] text-white cursor-pointer">
                  Cari tukang
                </h5>
              </Link>
              <Link href={"/paket"}>
                <h5 className="text-base text-[14px] text-white cursor-pointer">
                  Cari paket
                </h5>
              </Link>
            </div>
          </div>
          <div className="flex flex-col">
            <h1 className="font-bold text-[24px] text-white mb-[37px]">
              Panduan
            </h1>
            <div className="flex flex-col gap-[20px] ">
              <Link href={"/paket"}>
                <h5 className="text-base text-[14px] text-white cursor-pointer">
                  Tentang kami
                </h5>
              </Link>
              <Link href={"/paket"}>
                <h5 className="text-base text-[14px] text-white cursor-pointer">
                  Cara memesan
                </h5>
              </Link>
            </div>
          </div>
          <div className="flex flex-col">
            <h1 className="font-bold text-[24px] text-white mb-[37px]">
              Kontak
            </h1>
            <div className="flex flex-col gap-[20px] ">
              <div className="flex flex-row gap-3">
                <PhoneOutlined className="text-white text-[17px] items-center flex" />
                <h5 className="text-base text-[14px] text-white m-0">
                  +62 823 2323 2323
                </h5>
              </div>
              <div className="flex flex-row gap-3">
                <MailOutlined className="text-white text-[17px] items-center flex" />
                <h5 className="text-base text-[14px] text-white m-0">
                  maszehh@gmail.com
                </h5>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-row gap-5 w-full justify-center">
          <a href="#">
            <div className="border-2 border-white rounded-full p-2 cursor-pointer">
              <FacebookFilled className="text-white text-sm items-center flex" />
            </div>
          </a>
          <a href="#">
            <div className="border-white border-2 rounded-full p-2 cursor-pointer">
              <TwitterOutlined className="text-white text-sm items-center flex" />
            </div>
          </a>
          <a href="#">
            <div className="border-white border-2 rounded-full p-2 cursor-pointer">
              <InstagramOutlined className="text-white text-sm items-center flex" />
            </div>
          </a>
        </div>
        <div className="w-full flex-col flex">
          <Line />
          <div className="flex flex-row justify-center gap-3">
            <p className="text-white text-xs font-medium">
              &copy; 2022 maszehh
            </p>
            <p className="text-white text-xs font-medium">
              All rights reserved
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
