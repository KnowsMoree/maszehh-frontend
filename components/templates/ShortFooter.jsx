import Image from "next/image";
import Line from "/components/micro/Line";
import Logo from "/public/logo/logo.svg";

const ShortFooter = () => {
  return (
    <>
      <div className="w-full relative bg-bg bottom-0 pb-3">
        <hr className="h-[1px] text-text my-[22px]" />
        <div className="flex flex-col justify-center items-center gap-2">
          <Image alt="logo" src={Logo} />
          <h6 className="m-0 text-text text-xs font-medium">
            &copy; 2022 maszehh
          </h6>
        </div>
      </div>
    </>
  );
};

export default ShortFooter;
