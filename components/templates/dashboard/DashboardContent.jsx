import { useRouter } from "next/router";
import React, { useEffect } from "react";

const DashboardContent = ({
  children,
  ...props
}) => {
  return (
    <>
      <div className="w-full">
        <div className="relative mb-7">
          <h1 className="m-0 font-bold text-3xl">
            {props.title}
          </h1>
        </div>
        <div className="w-full">{children}</div>
      </div>
    </>
  );
};

export default DashboardContent;
