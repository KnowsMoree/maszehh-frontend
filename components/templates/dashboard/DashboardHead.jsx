import {
  BellFilled,
  CheckCircleTwoTone,
  CheckOutlined,
  CloseCircleTwoTone,
  DownOutlined,
  EyeInvisibleOutlined,
  EyeOutlined,
} from "@ant-design/icons";
import {
  Menu,
  Popover,
  Transition,
} from "@headlessui/react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Img from "/public/avatar/avatar.png";
import { Fragment } from "react";
import { notifficationRepository } from "../../../repository/notification";
import { Badge, Empty, message } from "antd";
import { mutate } from "swr";

const DashboardHead = (props) => {
  const [show, setShow] = useState(false);
  const router = useRouter();
  const [notif, setNotif] = useState([]);
  const [notifCount, setNotifCount] = useState(0);

  const { data: userNotif } =
    notifficationRepository.hooks.useNotificationUser(
      props?.id
    );

  useEffect(() => {
    if (
      userNotif !== undefined &&
      userNotif !== null
    ) {
      setNotif(userNotif?.data);
      setNotifCount(userNotif?.count);
    }
  }, [userNotif]);

  const handleClick = () => setShow(!show);

  const handleClickLogout = () => {
    localStorage.removeItem("token");

    router.push("/login");
  };

  const readNotif = async (id) => {
    console.log(id, "id notifikasi");
    try {
      await notifficationRepository.manipulateData.updateReadAll(
        id
      );
      await mutate(
        notifficationRepository.url.notificationUser(
          props?.id
        )
      );
    } catch (error) {
      console.log(error);
      message.error("terjadi kesalahan");
    }
  };

  const Settings = (props) => {
    if (props.user === "admin") {
      return (
        <>
          <div
            onClick={handleClickLogout}
            className={`w-fit absolute group mt-[75px] cursor-pointer p-3 flex gap-3 flex-col group hover:bg-main bg-white drop-shadow-lg opacity-100 z-50 rounded-lg transition-colors duration-500`}>
            <h3 className="cursor-pointer text-sm  text-text group-hover:text-white transition-colors m-0 ">
              Keluar
            </h3>
          </div>
        </>
      );
    } else if (props.user === "vendor") {
      return (
        <>
          <div
            className={`w-fit absolute mt-[75px] cursor-pointer p-3 flex gap-3 flex-col bg-white drop-shadow-lg opacity-100 z-50 rounded-lg transition-colors duration-500`}>
            <Link href={`/account/vendor`}>
              <h3 className="cursor-pointer text-sm  text-text hover:text-main transition-colors m-0 ">
                Profil
              </h3>
            </Link>
            <h3
              onClick={handleClickLogout}
              className="cursor-pointer text-sm  text-text hover:text-main transition-colors m-0 ">
              Keluar
            </h3>
          </div>
        </>
      );
    }
  };

  return (
    <>
      <div className="w-full flex pt-[39px] justify-end sticky top-0 z-50 bg-bg border-b-redText pb-7 border-b-2 gap-4">
        <div className="flex items-center">
          <Popover>
            {({ open }) => (
              <>
                <Popover.Button
                  className={`
                ${open ? "" : "text-opacity-90"}
                group inline-flex items-center rounded-full p-3 text-base font-medium ${
                  open
                    ? "hover:bg-main/40"
                    : "hover:bg-slate-600/10"
                } transition-all text-white hover:text-opacity-100 focus:outline-none`}>
                  <Badge
                    size="small"
                    count={notifCount}>
                    <BellFilled
                      className={`${
                        open
                          ? "text-main/70"
                          : "text-text"
                      } text-lg flex items-center transition-all ease-in-out`}
                    />
                  </Badge>
                </Popover.Button>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-200"
                  enterFrom="opacity-0 translate-y-1"
                  enterTo="opacity-100 translate-y-0"
                  leave="transition ease-in duration-150"
                  leaveFrom="opacity-100 translate-y-0"
                  leaveTo="opacity-0 translate-y-1">
                  <Popover.Panel className="absolute right-0 z-10 mt-3 w-screen max-w-sm -translate-x-[40%] transform px-4 sm:px-0 lg:max-w-xs">
                    <div className="overflow-hidden divide-y-[1px] divide-dashed divide-slate-600/50 rounded-lg shadow-lg ring-1 ring-black ring-opacity-5">
                      <div className="bg-white py-4 px-5 cursor-default flex justify-between">
                        <div>
                          <span className="text-base font-semibold text-gray-900">
                            Notifikasi
                          </span>
                          <span className="block text-sm text-gray-500">
                            Kamu mendapatkan{" "}
                            {notifCount} pesan
                            yang belum dibaca
                          </span>
                        </div>
                      </div>
                      <div
                        className={`relative grid gap-8 max-h-[450px] bg-white p-7 lg:grid-cols-1 ${
                          notifCount > 5
                            ? "overflow-y-scroll overscroll-y-auto"
                            : "overflow-hidden"
                        } `}>
                        {notif?.length == 0 ? (
                          <div className="w-full h-auto flex items-center justify-center">
                            <Empty
                              image={
                                Empty.PRESENTED_IMAGE_SIMPLE
                              }
                              description={
                                "tidak ada notifikasi terbaru"
                              }
                            />
                          </div>
                        ) : (
                          notif?.map((item) => (
                            <a
                              key={item?.id}
                              href={
                                item?.pathAction
                              }
                              onClick={() =>
                                readNotif(
                                  item?.id
                                )
                              }
                              className="-m-3 flex items-center rounded-lg p-2 transition duration-150 ease-in-out hover:bg-gray-50 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50">
                              {item?.title ==
                                "Pembayaran Ditolak" ||
                              item?.title ==
                                "Pesanan Ditahan" ||
                              item?.title ==
                                "Tahan Pesanan" ||
                              item?.title ==
                                "Akun Non-Aktif" ? (
                                <CloseCircleTwoTone
                                  twoToneColor="#FF3B3F"
                                  className="flex justify-center text-[24px]"
                                />
                              ) : (
                                <CheckCircleTwoTone
                                  className="flex justify-center text-[24px]"
                                  twoToneColor="#52c41a"
                                />
                              )}
                              <div className="ml-4">
                                <p className="text-sm font-bold text-gray-900">
                                  {item?.title}
                                </p>
                                <p className="text-sm text-gray-500">
                                  {item?.message}
                                </p>
                              </div>
                            </a>
                          ))
                        )}
                      </div>
                    </div>
                  </Popover.Panel>
                </Transition>
              </>
            )}
          </Popover>
        </div>

        <div
          onClick={handleClick}
          className="select-none w-auto cursor-pointer rounded-3xl pointer-events-auto group hover:bg-main transition-colors flex gap-4 items-center flex-row bg-white py-[10px] px-[17px] drop-shadow-md">
          {props.photo === null ? (
            <Image
              src={Img}
              alt="coba"
              width={50}
              height={50}
            />
          ) : (
            <img
              src={props.photo}
              alt="coba"
              width={50}
              height={50}
              className="rounded-full"
            />
          )}

          <div className="w-auto flex flex-col ">
            <h2 className="font-bold group-hover:text-white text-redText text-base m-0">
              {props.vendorName}
            </h2>
            <h3 className="font-semibold group-hover:text-white text-text text-sm m-0">
              {props.role}
            </h3>
          </div>
          <div className="flex items-center justify-center">
            <DownOutlined
              className={`text-redText group-hover:text-white transition-all duration-200 ${
                show ? "rotate-180" : "rotate-0"
              }`}
            />
          </div>
        </div>
        {show ? (
          <Settings
            user={props.user}
            id={props.id}
          />
        ) : null}
      </div>
    </>
  );
};

export default DashboardHead;
