import Sidebar from "/components/templates/Sidebar";
import DashboardHead from "/components/templates/dashboard/DashboardHead";
import DashboardContent from "/components/templates/dashboard/DashboardContent";
import {
  useEffect,
  useLayoutEffect,
  useState,
} from "react";
import { parseJwt } from "../../../helper/parseJwt";
import { userRepository } from "../../../repository/user";
import Router, { useRouter } from "next/router";

const DashboardLayout = ({
  children,
  ...props
}) => {
  const [userLogin, setUserLogin] = useState({});
  const [id, setId] = useState("");
  const [allowed, setAllowed] = useState(false);

  const router = useRouter();

  const { data: dataDetailUser } =
    userRepository.hooks.useUserDetail(id);

  // useLayoutEffect(() => {
  //   const token = localStorage.getItem("token");
  //   const decodeJwt = parseJwt(token);
  // }, []);

  useEffect(() => {
    const token = localStorage.getItem("token");
    const decodeJwt = parseJwt(token);
    setId(decodeJwt.id);
    setUserLogin(dataDetailUser?.data);

    if (
      decodeJwt.role !== "admin" &&
      decodeJwt.role !== "vendor"
    ) {
      router.push("/package");
    } else if (decodeJwt.role === props?.isUser) {
      setAllowed(true);
    } else {
      setAllowed(false);
      router.push(`/${decodeJwt.role}`);
    }
  }, [dataDetailUser]);

  if (allowed) {
    return (
      <>
        <Sidebar
          user={props.isUser}
          active={props.activeSidebar}
          name={props.nameActive}
          userStatus={userLogin?.status}
        />
        <div className="relative ml-80 bg-bg max-w-screen pl-[49px] min-h-screen pr-[84px]">
          <DashboardHead
            id={userLogin?.id}
            user={props.isUser}
            vendorName={userLogin?.username}
            role={userLogin?.role?.roleName}
            photo={userLogin?.photo}
          />
          <div className="relative py-6 w-full">
            <DashboardContent
              title={props.title}
              role={userLogin?.role?.roleName}>
              {children}
            </DashboardContent>
          </div>
        </div>
      </>
    );
  }
};

export default DashboardLayout;
