import useSWR from "swr";
import { http } from "../utils/http";

const url = {
  uploadPhoto: () => "/review/upload",
  createReview: () => `/review/create`,
  reviewUser: ({ page, limit }) => `/review/user?page=${page}&limit=${limit}`,
  reviewDetail: (id) => `/review/detail/${id}`,
};

const hooks = {
  reviewUser(filter) {
    return useSWR(url.reviewUser(filter), http.fetcher);
  },
  reviewDetail(id) {
    return useSWR(url.reviewDetail(id), http.fetcher);
  },
};

const manipulateData = {
  uploadPhotoReview(file) {
    const formData = new FormData();
    formData.append("photo", file);
    return http.post(url.uploadPhoto()).send(formData);
  },
  createReview(data) {
    return http.post(url.createReview()).send(data);
  },
};

export const reviewRepository = {
  url,
  hooks,
  manipulateData,
};
