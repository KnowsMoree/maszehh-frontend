import useSWR from "swr";
import { http } from "../utils/http";

const url = {
  province: () => "/region/province",
  city: (provinceId) =>
    `/region/city/${provinceId}`,
};

const hooks = {
  useProvince() {
    return useSWR(url.province(), http.fetcher);
  },
  useCity(provinceId) {
    return useSWR(
      url.city(provinceId),
      http.fetcher
    );
  },
};

export const regionRepository = {
  url,
  hooks,
};
