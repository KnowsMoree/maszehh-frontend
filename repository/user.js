import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  user: (search) => `/users?search=${search}&limit=${100}`,
  userDetail: (id) => `/users/detail/${id}`,
  uploadProfilePhoto: () => `/users/upload/profile`,
  userUpdate: (id) => `/users/update/${id}`,
  userDelete: (id) => `/users/delete/${id}`,
};

const hooks = {
  useUser(filter) {
    return useSWR(url.user(filter), http.fetcher);
  },
  useUserDetail(id) {
    return useSWR(url.userDetail(id), http.fetcher);
  },
};

const manipulateData = {
  updateUser(id, data) {
    return http.put(url.userUpdate(id)).send(data);
  },
  uploadUserPhoto(file) {
    const formData = new FormData();
    //PROFILE KEY FROM API
    formData.append("profile", file);
    return http.post(url.uploadProfilePhoto()).send(formData);
  },
  deleteUser(id) {
    return http.del(url.userDelete(id));
  },
};

export const userRepository = {
  url,
  manipulateData,
  hooks,
};
