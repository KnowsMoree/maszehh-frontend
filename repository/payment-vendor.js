import useSWR from "swr";
import { http } from "../utils/http";

const url = {
  getAll: (search) => `/vendor-payment/all?search=${search}`,
  getDetailPayment: (id) => `/vendor-payment/detail/${id}`,
  getDataCreate: () => `/vendor-payment/get-data`,
  vendorPaymentProof: () => `/vendor-payment/upload/proof`,
  vendorPaymentCreate: () => `/vendor-payment/create`,
  vendorPaymentConfirm: (id) => `/vendor-payment/confirm/${id}`,
};

const hooks = {
  useAllVendorPaymet(filter) {
    return useSWR(url.getAll(filter), http.fetcher);
  },
  useDetailVendorPayment(id) {
    return useSWR(url.getDetailPayment(id), http.fetcher);
  },
};

const manipulateData = {
  getDataCreatePaymentVendor(data) {
    return http.post(url.getDataCreate()).send(data);
  },
  uploadVendorPaymentProof(file) {
    const formData = new FormData();
    formData.append("proof", file);
    return http.post(url.vendorPaymentProof()).send(formData);
  },
  createVendorPayment(data) {
    return http.post(url.vendorPaymentCreate()).send(data);
  },
  vendorPaymentConfirm(id, status) {
    return http.put(url.vendorPaymentConfirm(id)).send(status);
  },
};

export const paymentVendorRepository = {
  url,
  hooks,
  manipulateData,
};
