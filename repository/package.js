import { search } from "superagent";
import useSWR from "swr";
import { http } from "../utils/http";

const url = {
  package: ({ current, limit, selectCategory, selectProvince, search }) =>
    `/package?page=${current}&limit=${limit}&category=${selectCategory}&province=${selectProvince}&search=${search}`,
  detailPackage: (id) => `/package/detail/${id}`,
  editPackage: (id) => `/package/edit/${id}`,
  createPackageUrl: () => `/package`,
  uploadPhoto: () => `/package/upload`,
  getPackVendor: ({ search, limit }) =>
    `/package?search=${search}&limit=${limit}`,
  getReviewPackage: ({ id, current, limit }) =>
    `/package/detail/review/${id}?page=${current}&limit=${limit}`,
  deletePackage: (id) => `/package/delete/${id}`,
};

const hooks = {
  usePackage(filter) {
    return useSWR(url.package(filter), http.fetcher);
  },
  usePackageVendor(filter) {
    return useSWR(url.getPackVendor(filter), http.fetcher);
  },
  usePackageDetail(id) {
    return useSWR(url.detailPackage(id), http.fetcher);
  },
  usePackageReview(id) {
    return useSWR(url.getReviewPackage(id), http.fetcher);
  },
};

const manipulateData = {
  createPackage(data) {
    return http.post(url.createPackageUrl()).send(data);
  },
  uploadPackagePhoto(file) {
    const formData = new FormData();
    //PROFILE KEY FROM API
    formData.append("pack", file);
    return http.post(url.uploadPhoto()).send(formData);
  },
  editPackage(id, data) {
    return http.put(url.editPackage(id)).send(data);
  },
  deletePackage(id) {
    return http.del(url.deletePackage(id));
  },
};

export const packageRepository = {
  url,
  hooks,
  manipulateData,
};
