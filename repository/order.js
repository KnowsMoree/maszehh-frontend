import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  orderAll: ({ search, status }) =>
    `/order?search=${search}&status=${status}&limit=${100}`,
  createOrder: () => `/order/create`,
  getOrderDetail: (id) => `/order/${id}`,
  approvalVendorOrder: (id) => `/order/approval-vendor/${id}`,
  approvalByUser: (id) => `/order/approval-user/${id}`,
  orderByUser: ({ page, limit, status }) =>
    `/order/user?page=${page}&limit=${limit}&status=${status}`,
  uploadDesign: () => `/order/upload/design`,
  getOrderReport: () => `/order/report`,
  getOrderReportVendor: () => `/order/report-vendor`,
};

const hooks = {
  useOrderbyUser(filter) {
    return useSWR(url.orderByUser(filter), http.fetcher);
  },
  useReportVendor() {
    return useSWR(url.getOrderReportVendor(), http.fetcher);
  },
  useOrderReport() {
    return useSWR(url.getOrderReport(), http.fetcher);
  },
  useOrderAll(filter) {
    return useSWR(url.orderAll(filter), http.fetcher);
  },
  useOrderDetail(id) {
    return useSWR(url.getOrderDetail(id), http.fetcher);
  },
};

const manipulateData = {
  createOrder(data) {
    return http.post(url.createOrder()).send(data);
  },
  uploadDesignOrder(file) {
    const formData = new FormData();
    formData.append("design", file);
    return http.post(url.uploadDesign()).send(formData);
  },
  approveVendorOrder(id, status) {
    return http.put(url.approvalVendorOrder(id)).send(status);
  },
  approveUserOrder(id, status) {
    return http.put(url.approvalByUser(id)).send(status);
  },
};

export const orderRepository = {
  url,
  manipulateData,
  hooks,
};
