import useSWR from "swr";
import { http } from "../utils/http";

const url = {
  getHandyman: () => `/dashboard/vendor/handyman`,
  getOrder: () => `/dashboard/vendor/order`,
  countAllUser: () => `/dashboard/count-all-user`,
  getTablePayment: (status) => `/payment-user?status=${status}`,
  getPendingVendor: (status) => `/vendor?status=${status}&limit=4`,
  getMostPopularPackage: () => `/dashboard/most-selling-pack`,
  getReviewVendor: () => `/dashboard/vendor-review`,
};

const hooks = {
  useHandyman() {
    return useSWR(url.getHandyman(), http.fetcher);
  },
  usePendingVendor(status) {
    return useSWR(url.getPendingVendor(status), http.fetcher);
  },
  useOrderPayment(status) {
    return useSWR(url.getTablePayment(status), http.fetcher);
  },
  useOrder() {
    return useSWR(url.getOrder(), http.fetcher);
  },
  useCount() {
    return useSWR(url.countAllUser(), http.fetcher);
  },
  useMostSellingPack() {
    return useSWR(url.getMostPopularPackage(), http.fetcher);
  },
  useVendorReview() {
    return useSWR(url.getReviewVendor(), http.fetcher);
  },
};

export const dashboardRepository = {
  url,
  hooks,
};
