import useSWR from "swr";
import { http } from "../utils/http";

const url = {
  roleUser: () => `/roles`,
  registerUser: () => `/auth/register`,
  registerVendor: () => `/auth/register-vendor`,
};

const hooks = {
  useRoles() {
    return useSWR(url.roleUser(), http.fetcher);
  },
};

const manipulateData = {
  createUser(data) {
    return http.post(url.registerUser()).send(data);
  },
  createVendor(data) {
    return http.post(url.registerVendor()).send(data);
  },
};

export const authRepository = {
  url,
  hooks,
  manipulateData,
};
