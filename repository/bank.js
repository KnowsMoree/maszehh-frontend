import useSWR from "swr";
import { http } from "../utils/http";

const url = {
  bank: () => `/bank/user`,
  allBank: (role) => `/bank?role=${role}`,
  detailBank: (id) => `/bank/detail/${id}`,
  createBank: () => `/bank/create`,
  updateBank: (id) => `/bank/update/${id}`,
  deleteBank: (id) => `/bank/delete/${id}`,
};

const hooks = {
  useBank() {
    return useSWR(url.bank(), http.fetcher);
  },
  useAllBank(filter) {
    return useSWR(url.allBank(filter), http.fetcher);
  },
  useDetailBank(id) {
    return useSWR(url.detailBank(id), http.fetcher);
  },
};

const manipulateData = {
  createBank(data) {
    return http.post(url.createBank()).send(data);
  },
  updateBank(id, data) {
    return http.put(url.updateBank(id)).send(data);
  },
  deleteBank(id) {
    return http.del(url.deleteBank(id));
  },
};

export const bankRepository = {
  url,
  hooks,
  manipulateData,
};
