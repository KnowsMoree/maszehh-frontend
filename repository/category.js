import useSWR from "swr";
import { http } from "../utils/http";

const url = {
  category: () => `/category`,
  categoryCreate: () => `/category/create`,
  categoryUpdate: (id) => `/category/update/${id}`,
  categoryDelete: (id) => `/category/delete/${id}`,
};

const hooks = {
  useCategory() {
    return useSWR(url.category(), http.fetcher);
  },
};

const manipulateData = {
  createCategory(data) {
    return http.post(url.categoryCreate()).send(data);
  },
  updateCategory(id, data) {
    return http.put(url.categoryUpdate(id)).send(data);
  },
  deleteCategory(id) {
    return http.del(url.categoryDelete(id));
  },
};

export const categoryRepository = {
  url,
  hooks,
  manipulateData,
};
