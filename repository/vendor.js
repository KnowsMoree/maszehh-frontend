import useSWR from "swr";
import { http } from "../utils/http";

const url = {
  vendorAll: () => `/vendor`,
  vendor: (search) => `/vendor?search=${search}`,
  vendorDetail: (id) => `/vendor/detail/${id}`,
  vendorEdit: (id) => `/vendor/edit/${id}`,
  vendorDelete: (id) => `/vendor/delete/${id}`,
  vendorProfile: () => `/vendor/profile`,
  uploadSiujk: () => `/vendor/upload/siujk`,
  uploadKtp: () => `/vendor/upload/ktp`,
};

const hooks = {
  useVendor(filter) {
    return useSWR(url.vendor(filter), http.fetcher);
  },
  useVendorProfile() {
    return useSWR(url.vendorProfile(), http.fetcher);
  },
  useVendorDetail(id) {
    return useSWR(url.vendorDetail(id), http.fetcher);
  },
};

const manipulateData = {
  updateUser(id, data) {
    return http.put(url.userUpdate(id)).send(data);
  },
  uploadSiujk(file) {
    const formData = new FormData();
    formData.append("siujk", file);
    return http.post(url.uploadSiujk()).send(formData);
  },
  uploadKtp(file) {
    const formData = new FormData();
    formData.append("ktp", file);
    return http.post(url.uploadKtp()).send(formData);
  },
  editVendor(id, data) {
    return http.put(url.vendorEdit(id)).send(data);
  },
  deleteVendor(id) {
    return http.del(url.vendorDelete(id));
  },
};

export const vendorRepository = {
  url,
  manipulateData,
  hooks,
};
