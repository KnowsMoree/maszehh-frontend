import useSWR from "swr";
import { http } from "../utils/http";

const url = {
  notificationUser: (idUser) => `/notification/user/${idUser}`,
  read: (id) => `/notification/read/${id}`,
};

const hooks = {
  useNotificationUser(idUser) {
    return useSWR(url.notificationUser(idUser), http.fetcher);
  },
};

const manipulateData = {
  updateReadAll(id) {
    return http.put(url.read(id)).send();
  },
};

export const notifficationRepository = {
  url,
  hooks,
  manipulateData,
};
