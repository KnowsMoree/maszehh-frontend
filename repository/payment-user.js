import useSWR from "swr";
import { http } from "../utils/http";

const url = {
  allPaymentUser: (search) => `/payment-user?search=${search}`,
  detailPaymentUser: (id) => `/payment-user/detail/${id}`,
  paymentUserOrdered: (orderId) => `/payment-user/order/${orderId}`,
  paymentByUser: ({ status1, status2, page, limit }) =>
    `/payment-user/user?status1=${status1}&status2=${status2}&page=${page}&limit=${limit}`,
  uploadPhotoProofPayment: () => `/payment-user/upload/proof`,
  proofPayment: (id) => `/payment-user/upload-proof/${id}`,
  confirmPayment: (paymentId) => `/payment-user/confirm-payment/${paymentId}`,
};

const hooks = {
  useAllPaymentUser(filter) {
    return useSWR(url.allPaymentUser(filter), http.fetcher);
  },
  usePaymentUserOrdered(filter) {
    return useSWR(url.paymentUserOrdered(filter), http.fetcher);
  },
  usePaymentByUser(filter) {
    return useSWR(url.paymentByUser(filter), http.fetcher);
  },
  useDetailPaymentUser(filter) {
    return useSWR(url.detailPaymentUser(filter), http.fetcher);
  },
};

const manipulateData = {
  uploadPhotoProof(file) {
    const formData = new FormData();
    formData.append("proof", file);
    return http.post(url.uploadPhotoProofPayment()).send(formData);
  },
  uploadProofPayment(filter, data) {
    return http.put(url.proofPayment(filter)).send(data);
  },
  confirmProofPayment(paymentId, status) {
    return http.put(url.confirmPayment(paymentId)).send(status);
  },
};

export const paymentUserRepository = {
  url,
  hooks,
  manipulateData,
};
