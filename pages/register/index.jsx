import Image from "next/image";
import Header from "/layouts/Header";
import RegImg from "/public/hero/register/register.png";
import RegForm from "/components/register/RegForm";
import Logo from "/public/logo/logoTypoBlack.svg";
import usePublicPage from "/helper/usePublicPage";

const Register = () => {
  usePublicPage();
  return (
    <>
      <div className="w-screen max-h-screen flex flex-row">
        <div className="w-full px-[84px] py-12">
          <Image src={Logo} alt="logo" />
          <RegForm />
          <p className="text-center text-text font-medium text-[11px]">
            &copy; 2022 Maszehh
          </p>
        </div>
        <div className="w-full flex justify-end">
          <Image
            src={RegImg}
            alt="hero register"
          />
        </div>
      </div>
    </>
  );
};

Register.getLayout = (page) => {
  return <Header title="register">{page}</Header>;
};

export default Register;
