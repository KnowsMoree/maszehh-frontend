import Image from "next/image";
import Img from "/public/hero/register/vendor/vendor1.png";
import Header from "/layouts/Header";
import Logo from "/public/logo/logoTypo.svg";
import VendorRegistration from "/components/register/VendorRegistration";

const RegisterVendor = () => {
  return (
    <>
      <div className="w-screen overflow-hidden max-h-screen flex flex-row">
        <div className="w-1/3 flex justify-start">
          <Image
            alt=""
            src={Img}
            layout={"fixed"}
          />
          <div className="absolute top-[51px] md:left-[84px] left-[42px]">
            <div className="relative w-auto h-auto inline-flex">
              <Image
                src={Logo}
                alt="logo maszehh"
              />
            </div>
          </div>
        </div>
        <div className="w-full pr-[84px] py-7 flex flex-col gap-y-5">
          <div className="w-full justify-center flex">
            <h1 className="m-0 text-2xl font-bold capitalize">
              Daftar Vendor
            </h1>
          </div>
          <VendorRegistration />
          <div className="w-full justify-center flex">
            <p className="text-center text-text font-medium text-[11px]">
              &copy; 2022 Maszehh
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

RegisterVendor.getLayout = (page) => {
  return (
    <Header title="Registrasi vendor">
      {page}
    </Header>
  );
};

export default RegisterVendor;
