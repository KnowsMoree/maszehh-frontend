import Info from "../components/home/Info";
import Quality from "../components/home/Quality";
import Hero from "../components/home/Hero";
import Vendor from "../components/home/Vendor";
import Navbar from "../components/templates/Navbar";
import Header from "../layouts/Header";
import VendorHero from "/components/home/VendorHero";
import Pack from "/components/home/Pack";
import Footer from "../components/templates/Footer";
import { useEffect, useState } from "react";
import usePublicPage from "../helper/usePublicPage";

const Home = () => {
  usePublicPage();
  const [isLoggedIn, setIsLoggedIn] =
    useState(false);

  // useEffect(() => {}, []);

  return (
    <>
      <Navbar
        user_login={isLoggedIn}
        active="Beranda"
      />
      <div className="relative flex flex-col top-[103px]">
        <div className="relative mx-[84px]">
          <Hero />
        </div>
        <div className="w-full relative bg-white py-[55px] px-[84px]">
          <div className="flex flex-col gap-[80px] min-h-[595px]">
            <Quality />
            <Info />
          </div>
        </div>
        <div className="w-full bg-transparent my-[75px] px-[84px] flex gap-24 flex-col">
          <VendorHero />
          <Vendor />
        </div>
        <div className="w-full py-10 px-[84px] mb-[75px] bg-white">
          <Pack />
        </div>
        <Footer />
      </div>
    </>
  );
};

Home.getLayout = (page) => {
  return <Header title="Home">{page}</Header>;
};

export default Home;
