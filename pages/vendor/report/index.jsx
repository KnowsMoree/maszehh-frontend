import Header from "/layouts/Header";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import ReportTable from "/components/vendor/table/ReportTable";

const Report = () => {
  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar="Laporan pesanan"
        nameActive="Laporan pesanan"
        title="Laporan pesanan"
        id=""
        user="vendor">
        <ReportTable />
      </DashboardLayout>
    </>
  );
};

Report.getLayout = (page) => {
  return (
    <Header title="pembayaran pesanan">
      {page}
    </Header>
  );
};

export default Report;
