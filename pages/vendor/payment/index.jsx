import Header from "/layouts/Header";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import PaymentOrderTable from "/components/vendor/table/PaymentOrderTable";

const PaymentOrder = () => {
  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar="Pembayaran"
        nameActive="Pembayaran"
        title="Pembayaran"
        id=""
        user="vendor"
      >
        <PaymentOrderTable />
      </DashboardLayout>
    </>
  );
};

PaymentOrder.getLayout = (page) => {
  return <Header title="pembayaran vendor">{page}</Header>;
};

export default PaymentOrder;
