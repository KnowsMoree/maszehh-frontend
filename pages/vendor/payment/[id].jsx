import Header from "/layouts/Header";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import DetailPaymentContent from "../../../components/vendor/payment/DetailPayment";
import { useRouter } from "next/router";

const DetailPayment = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar="Pembayaran"
        nameActive="Pembayaran"
        title="Detail Pembayaran"
        id=""
        user="vendor"
      >
        <DetailPaymentContent id={id} />
      </DashboardLayout>
    </>
  );
};

DetailPayment.getLayout = (page) => {
  return <Header title="detail pembayaran">{page}</Header>;
};

export default DetailPayment;
