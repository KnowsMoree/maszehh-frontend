import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import Header from "/layouts/Header";
import Card from "/components/admin/Card";
import Table from "/components/admin/table/Table";
import RightBot from "/components/admin/RightBot";
import {
  FileDoneOutlined,
  HourglassTwoTone,
  InboxOutlined,
  ReconciliationOutlined,
  ShopOutlined,
  TeamOutlined,
  UserAddOutlined,
  UserDeleteOutlined,
  UsergroupAddOutlined,
  UsergroupDeleteOutlined,
  UserOutlined,
  UserSwitchOutlined,
} from "@ant-design/icons";
import BestService from "/components/admin/BestService";
import Image from "next/image";
import { useEffect, useState } from "react";
import { parseJwt } from "/helper/parseJwt";
import { dashboardRepository } from "../../repository/dashboard";
import moment from "moment";
import "moment/locale/id";
import { Empty, Rate } from "antd";

const DashboardVendor = () => {
  const [user, setUser] = useState({});
  const [handyman, setHandyman] = useState();
  const [order, setOrder] = useState();
  const [loading, setLoading] = useState(true);
  const [sellingPackage, setSellingPackage] =
    useState([]);
  const [reviewByVendor, setReviewByVendor] =
    useState([]);

  const [status, setStatus] = useState(
    "first paid done"
  );
  const [search, setSearch] = useState("");

  const { data: dataHandyman } =
    dashboardRepository.hooks.useHandyman();
  const { data: dataOrder } =
    dashboardRepository.hooks.useOrder({
      search,
      status,
    });

  const { data: mostSellingPackage } =
    dashboardRepository.hooks.useMostSellingPack();

  const { data: reviewVendor } =
    dashboardRepository.hooks.useVendorReview();

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }
    const decodeJwt = parseJwt(token);

    setUser(decodeJwt);
  }, []);

  useEffect(() => {
    setTimeout(() => {
      if (
        [
          dataHandyman,
          dataOrder,
          mostSellingPackage,
          reviewVendor,
        ] !== null &&
        [
          dataHandyman,
          dataOrder,
          mostSellingPackage,
          reviewVendor,
        ] !== undefined
      ) {
        setHandyman(dataHandyman?.data);
        setOrder(dataOrder?.data);
        setSellingPackage(mostSellingPackage);
        setReviewByVendor(reviewVendor);
        setLoading(false);
      }
    }, 1000);
  }, [
    dataHandyman,
    dataOrder,
    mostSellingPackage,
    reviewVendor,
  ]);

  const card = [
    {
      icon: (
        <TeamOutlined className="text-[60px] text-main group-hover:text-white mb-[15px]" />
      ),
      title: "Jumlah total tukang",
      count: handyman?.handymanTotal,
    },
    {
      icon: (
        <UsergroupAddOutlined className="text-[60px] text-main group-hover:text-white mb-[15px]" />
      ),
      title: "Jumlah tukang tersedia",
      count: handyman?.handymanAvailable,
    },
    {
      icon: (
        <UsergroupDeleteOutlined className="text-[60px] text-main group-hover:text-white mb-[15px]" />
      ),
      title: "Jumlah tukang tidak tersedia",
      count: handyman?.handymanOnWork,
    },
  ];

  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar="Dashboard"
        nameActive="Dashboard"
        title="Dashboard"
        id=""
        user="vendor">
        {loading ? (
          <div className="w-full h-full flex-col gap-y-2 flex items-center justify-center px-[84px] pt-[100px] pb-[50px]">
            <HourglassTwoTone
              twoToneColor="#95A1AF"
              spin
              className="text-lg flex items-center"
            />
            <h2 className="text-sm m-0 text-text font-semibold">
              mohon tunggu...
            </h2>
          </div>
        ) : (
          <div className="w-full flex flex-wrap flex-col md:flex-nowrap md:flex-row gap-16">
            <div className="flex w-full flex-col gap-9">
              <div className="flex flex-row gap-20">
                {card?.map((v, i) => {
                  return (
                    <>
                      <div key={i}>
                        <div className="bg-white w-[250px] h-[230px]  transition-colors py-[30px] px-5 rounded-3xl">
                          {v.icon}
                          <h2 className="m-0 font-semibold text-lg text-text ">
                            {v.title}
                          </h2>
                          <h1 className="m-0 font-bold text-4xl text-main ">
                            {v.count}
                          </h1>
                        </div>
                      </div>
                    </>
                  );
                })}
              </div>
              <div className="w-full">
                <Table
                  title="Konfirmasi Pesanan"
                  userLogin={user?.role}
                  data={order}
                />
              </div>
            </div>
            <div className="w-full items-center flex flex-col gap-8">
              <BestService
                title={"Layanan Terlaris"}
                data={sellingPackage}
              />
              <RightBot title="Review Pengguna">
                {reviewByVendor?.length == 0 ? (
                  <div className="w-full h-auto flex items-center justify-center">
                    <Empty
                      image={
                        Empty.PRESENTED_IMAGE_SIMPLE
                      }
                    />
                  </div>
                ) : (
                  reviewByVendor?.map((v) => {
                    return (
                      <>
                        <div
                          key={v.id}
                          className="w-full flex flex-row gap-3">
                          <div className="rounded-full flex items-center">
                            <img
                              src={v?.user?.photo}
                              alt="ava"
                              width={40}
                              height={40}
                              className="rounded-full"
                            />
                          </div>
                          <div className="flex flex-col">
                            <div className="w-full flex text-sm">
                              <h4 className="after:content-['|'] after:mx-2 font-semibold m-0 capitalize">
                                {
                                  v?.user
                                    ?.fullname
                                }
                              </h4>
                              <h4 className="capitalize m-0 flex items-center text-xs">
                                {moment(
                                  v?.createdAt
                                ).format(
                                  "YYYY-MM-DD HH:mm"
                                )}
                              </h4>
                            </div>
                            <div className="w-full">
                              <p className="break-words font-medium text-sm m-0">
                                {v?.pack?.name}
                              </p>
                              <span>
                                <Rate
                                  style={{
                                    fontSize:
                                      "16px",
                                  }}
                                  value={v?.rate}
                                  disabled
                                />
                              </span>
                            </div>
                          </div>
                        </div>
                      </>
                    );
                  })
                )}
              </RightBot>
            </div>
          </div>
        )}
      </DashboardLayout>
    </>
  );
};

DashboardVendor.getLayout = (page) => {
  return (
    <Header title="vendor dashboard">
      {page}
    </Header>
  );
};

export default DashboardVendor;
