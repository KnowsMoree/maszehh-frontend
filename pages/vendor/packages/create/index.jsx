import Header from "/layouts/Header";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import CreatePackagesContent from "/components/vendor/packages/CreatePackage";

const CreatePackages = () => {
  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar="Paket Layanan"
        nameActive="Paket Layanan"
        title="Tambah Paket Layanan"
        id=""
        user="vendor">
        <div className="w-full flex justify-center">
          <CreatePackagesContent />
        </div>
      </DashboardLayout>
    </>
  );
};

CreatePackages.getLayout = (page) => {
  return (
    <Header title="Tambah paket">{page}</Header>
  );
};

export default CreatePackages;
