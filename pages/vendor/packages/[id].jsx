import Header from "/layouts/Header";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import DetailPackagesContent from "/components/vendor/packages/DetailPackages";
import { useRouter } from "next/router";

const DetailPackages = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar="Paket Layanan"
        nameActive="Paket Layanan"
        title="Detail Paket Layanan"
        id=""
        user="vendor"
      >
        <div className="w-full flex justify-center">
          <DetailPackagesContent param={id} />
        </div>
      </DashboardLayout>
    </>
  );
};

DetailPackages.getLayout = (page) => {
  return <Header title="paket vendor">{page}</Header>;
};

export default DetailPackages;
