import Header from "/layouts/Header";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import EditPackage from "../../../../components/vendor/packages/EditPackage";
import { useRouter } from "next/router";

const EditPack = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar="Paket Layanan"
        nameActive="Paket Layanan"
        title="Edit Paket Layanan"
        id=""
        user="vendor"
      >
        <div className="w-full flex justify-center">
          <EditPackage param={id} />
        </div>
      </DashboardLayout>
    </>
  );
};

EditPack.getLayout = (page) => {
  return <Header title="Edit paket">{page}</Header>;
};

export default EditPack;
