import Header from "/layouts/Header";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import PackagesTable from "/components/vendor/table/PackagesTable";

const Packages = () => {
  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar="Paket Layanan"
        nameActive="Paket Layanan"
        title="Paket Layanan"
        id=""
        user="vendor">
        <PackagesTable />
      </DashboardLayout>
    </>
  );
};

Packages.getLayout = (page) => {
  return (
    <Header title="paket vendor">{page}</Header>
  );
};

export default Packages;
