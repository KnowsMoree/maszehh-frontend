import Header from "/layouts/Header";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import OrderTable from "/components/vendor/table/OrderTable";

const OrderVendor = () => {
  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar="Pesanan"
        nameActive="Pesanan"
        title="Pesanan"
        id=""
        user="vendor">
        <OrderTable />
      </DashboardLayout>
    </>
  );
};

OrderVendor.getLayout = (page) => {
  return (
    <Header title="paket vendor">{page}</Header>
  );
};

export default OrderVendor;
