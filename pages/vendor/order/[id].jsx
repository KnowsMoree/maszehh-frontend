import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import Header from "/layouts/Header";
import DetailOrderContent from "/components/vendor/order/DetailOrder";
import { useRouter } from "next/router";

const DetailOrder = () => {
  const router = useRouter();

  const { id } = router.query;

  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar="Pesanan"
        nameActive="Pesanan"
        title="Detail Pesanan"
      >
        <div className="w-full ">
          <DetailOrderContent param={id} />
        </div>
      </DashboardLayout>
    </>
  );
};

DetailOrder.getLayout = (page) => {
  return <Header title="detail pesanan">{page}</Header>;
};

export default DetailOrder;
