import Header from "/layouts/Header";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import BankTable from "/components/vendor/table/BankTable";

const OrderVendor = () => {
  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar="Data Bank"
        nameActive="Data Bank"
        title="Data Bank"
        id=""
        user="vendor">
        <div className="w-full flex justify-center">
          <BankTable />
        </div>
      </DashboardLayout>
    </>
  );
};

OrderVendor.getLayout = (page) => {
  return (
    <Header title="paket vendor">{page}</Header>
  );
};

export default OrderVendor;
