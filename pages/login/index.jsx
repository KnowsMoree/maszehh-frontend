import Image from "next/image";
import Input from "/components/input/Input";
import Header from "/layouts/Header";
import Img from "/public/hero/login/login1.png";
import Logo from "/public/logo/logoTypo.svg";
import LogoBlack from "/public/logo/logoTypoBlack.svg";
import Button from "/components/input/Button";
import Link from "next/link";
import Line from "/components/micro/Line";
import { useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import usePublicPage from "/helper/usePublicPage";
import { parseJwt } from "../../helper/parseJwt";
import {
  EyeInvisibleOutlined,
  EyeOutlined,
} from "@ant-design/icons";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [passwordShown, setPasswordShown] =
    useState(false);
  const router = useRouter();

  const tooglePassword = (e) => {
    e.preventDefault();

    setPasswordShown(!passwordShown);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const data = {
        username,
        password,
      };

      const endpoint =
        "http://localhost:3222/auth/login";

      const res = await axios.post(
        endpoint,
        data
      );
      // console.log();
      localStorage.setItem(
        "token",
        res.data.data
      );

      const user = parseJwt(res.data.data);

      if (user.role == "admin") {
        router.push("/admin");
      } else if (user.role == "customer") {
        router.push("/package");
      } else if (user.role == "vendor") {
        router.push("/vendor");
      }
    } catch (e) {
      if (!username) {
        setError("username tidak boleh kosong");
      } else if (!password) {
        setError("password tidak boleh kosong");
      } else {
        setError(e.response.data.message[0]);
      }

      // alert(e.response.data);
    }
  };
  return (
    <>
      <div className="flex w-screen h-screen bg-bg ">
        <div className="w-full h-screen inline-flex absolute -z-9 lg:relative lg:h-fit">
          <div className="h-screen max-w-fit lg:inline-flex hidden">
            <Image src={Img} alt="gambar" />
          </div>
          <div className="absolute top-[51px] md:left-[84px] left-[42px]">
            <div className="relative cursor-pointer w-auto h-auto hidden lg:inline-flex">
              <Link href={"/"}>
                <Image
                  src={Logo}
                  alt="logo maszehh"
                />
              </Link>
            </div>
            <div className="relative w-auto h-auto inline-flex lg:hidden">
              <Image
                src={LogoBlack}
                alt="logo maszehh"
              />
            </div>
          </div>
        </div>
        <div className="w-full lg:w-3/4 h-screen z-50 bg-transparent flex justify-center items-center">
          <div className="flex justify-center max-w-[331px] h-auto flex-col">
            <form onSubmit={handleSubmit}>
              <div className="flex justify-center items-center flex-col mb-[24px]">
                <h2 className="font-bold text-[24px]">
                  Masuk Maszehh
                </h2>
                <p className="font-semibold text-[14px] text-text text-center">
                  Masukan username anda untuk
                  mengakses fitur
                </p>
                <p className="text-red-600 m-0">
                  {error}
                </p>
              </div>
              <Input
                type="text"
                name="username"
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
                placeholder="Masukan username"
              />
              <Input
                type={
                  passwordShown
                    ? "text"
                    : "password"
                }
                name="password"
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
                minLength={8}
                placeholder="Masukan password">
                <div
                  onClick={tooglePassword}
                  className="absolute right-4 top-5 text-gray-400 text-sm">
                  {passwordShown ? (
                    <EyeInvisibleOutlined className="text-base text-text flex items-center" />
                  ) : (
                    <EyeOutlined className="text-base text-text flex items-center" />
                  )}
                </div>
              </Input>
              <Button
                value="masuk"
                name="Masuk"
              />
            </form>
            <Line />
            <div className="w-full mb-3 bg-white/60 rounded-md ring-1 ring-gray-400 flex-col flex items-center py-2">
              <h1 className="m-0 text-sm font-medium">
                ingin menggunakan layanan?{" "}
              </h1>
              <div>
                <Link href={"/register"}>
                  <span className="text-main text-sm select-none font-medium cursor-pointer hover:text-secondary transition-colors">
                    Daftar
                  </span>
                </Link>
                <span className="text-sm font-medium">
                  {" "}
                  atau{" "}
                </span>
                <Link href={"/register/vendor"}>
                  <span className="text-main select-none text-sm font-medium cursor-pointer hover:text-secondary transition-colors">
                    Daftar Vendor
                  </span>
                </Link>
              </div>
            </div>
            <p className="text-center text-text font-medium text-[11px]">
              &copy; 2022 Maszehh
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

Login.getLayout = (page) => {
  return <Header title="Login">{page}</Header>;
};

export default Login;
