export default function handler(req, res) {
  const body = req.body;

  console.log("body: ", body);

  if (
    !body.username ||
    body.username == "undefined" ||
    body.password == "undefined" ||
    !body.password
  ) {
    return res.status(400).json({ data: "username dan password harus diisi" });
  }

  res.status(200).json({
    data: { username: `${body.username}`, password: `${body.password}` },
  });
}
