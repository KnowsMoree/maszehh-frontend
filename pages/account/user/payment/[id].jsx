import { useRouter } from "next/router";
import ProfileLayout from "/components/templates/ProfileLayout";
import UserPayment from "/components/profile/UserPayment";
import Header from "/layouts/Header";
import { Pagination, Tabs } from "antd";
import Transaction from "/components/profile/Transaction";
import { useState } from "react";

const { TabPane } = Tabs;

const WaitingPayment = () => {
  const [onClick, setOnClick] = useState(false);
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <ProfileLayout
        id={id}
        navbarActive="Pembayaran"
        active="menunggu pembayaran"
      >
        <div className="flex divide-y-2 flex-col">
          <Tabs
            defaultActiveKey="1"
            onChange={() => setOnClick(!onClick)}
            centered
            size={"small"}
          >
            <TabPane
              tab={
                <div
                  className={`py-2 px-5 group rounded-lg transition-colors select-none ${
                    onClick === false
                      ? "bg-main drop-shadow-lg"
                      : "bg-transparent hover:bg-main hover:drop-shadow-lg"
                  }`}
                >
                  <span
                    className={`m-0 font-semibold text-sm ${
                      onClick === false
                        ? "text-white"
                        : "group-hover:text-white text-text"
                    }`}
                  >
                    Menunggu Pembayaran
                  </span>
                </div>
              }
              key="1"
            >
              <div className="py-2 gap-5 w-full">
                <UserPayment />
              </div>
            </TabPane>
            <TabPane
              tab={
                <div
                  className={`py-2 px-5 group rounded-lg transition-colors select-none ${
                    onClick === true
                      ? "bg-main drop-shadow-lg"
                      : "bg-transparent hover:bg-main hover:drop-shadow-lg "
                  }`}
                >
                  <span
                    className={`m-0 font-semibold text-sm ${
                      onClick === true
                        ? "text-white"
                        : "group-hover:text-white text-text"
                    }`}
                  >
                    Daftar Pembayaran
                  </span>
                </div>
              }
              key="2"
            >
              <div className=" flex flex-col gap-5">
                <Transaction />
              </div>
            </TabPane>
          </Tabs>
        </div>
      </ProfileLayout>
    </>
  );
};

WaitingPayment.getLayout = (page) => {
  return <Header title="Menunggu pembayaran">{page}</Header>;
};

export default WaitingPayment;
