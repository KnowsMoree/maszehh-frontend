import { useRouter } from "next/router";
import ProfileLayout from "/components/templates/ProfileLayout";
import UserPayment from "/components/profile/UserPayment";
import Header from "/layouts/Header";
import Transaction from "/components/profile/Transaction";

const Order = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <ProfileLayout
        id={id}
        active="daftar transaksi">
        <div className="flex divide-y-2 flex-col">
          <div className="w-full">
            <h1 className="font-bold text-xl">
              Daftar Transaksi
            </h1>
          </div>
          <div className="py-2 flex flex-col gap-5">
            <Transaction />
          </div>
        </div>
      </ProfileLayout>
    </>
  );
};

Order.getLayout = (page) => {
  return (
    <Header title="daftar transaksi">
      {page}
    </Header>
  );
};

export default Order;
