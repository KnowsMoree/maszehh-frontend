import { useRouter } from "next/router";
import ProfileLayout from "/components/templates/ProfileLayout";
import WaitingReviewCard from "/components/profile/WaitingReview";
import MyReviewCard from "/components/profile/MyReview";
import Header from "/layouts/Header";
import { Tabs } from "antd";
import { useState } from "react";

const { TabPane } = Tabs;

const Review = () => {
  const router = useRouter();
  const [onClick, setOnClick] = useState(false);
  const { id } = router.query;

  return (
    <>
      <ProfileLayout
        id={id}
        active="ulasan"
        navbarActive="Ulasan">
        <div className="flex divide-y-2 flex-col">
          <div className="flex flex-col gap-5">
            <Tabs
              defaultActiveKey="1"
              onChange={() =>
                setOnClick(!onClick)
              }
              centered
              size={"small"}>
              <TabPane
                tab={
                  <div
                    className={`py-2 px-5 group rounded-lg transition-colors select-none ${
                      onClick === false
                        ? "bg-main drop-shadow-lg"
                        : "bg-transparent hover:bg-main hover:drop-shadow-lg"
                    }`}>
                    <span
                      className={`m-0 font-semibold text-sm ${
                        onClick === false
                          ? "text-white"
                          : "group-hover:text-white text-text"
                      }`}>
                      Menunggu ulasan
                    </span>
                  </div>
                }
                key="1">
                <div className="w-full flex flex-col gap-y-4">
                  <WaitingReviewCard param={id} />
                </div>
              </TabPane>
              <TabPane
                tab={
                  <div
                    className={`py-2 px-5 group rounded-lg transition-colors select-none ${
                      onClick === true
                        ? "bg-main drop-shadow-lg"
                        : "bg-transparent hover:bg-main hover:drop-shadow-lg "
                    }`}>
                    <span
                      className={`m-0 font-semibold text-sm ${
                        onClick === true
                          ? "text-white"
                          : "group-hover:text-white text-text"
                      }`}>
                      Ulasan saya
                    </span>
                  </div>
                }
                key="2">
                <div className="w-full flex flex-col gap-y-4">
                  <MyReviewCard param={id} />
                </div>
              </TabPane>
            </Tabs>
          </div>
        </div>
      </ProfileLayout>
    </>
  );
};

Review.getLayout = (page) => {
  return <Header title="ulasan">{page}</Header>;
};

export default Review;
