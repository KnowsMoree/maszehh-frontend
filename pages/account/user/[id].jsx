import { useRouter } from "next/router";
import ProfileLayout from "/components/templates/ProfileLayout";
import UserProfile from "/components/profile/UserProfile";
import Header from "/layouts/Header";

const Profile = () => {
  const router = useRouter();
  const { id } = router.query;
  return (
    <>
      <ProfileLayout
        id=""
        noNavigation={true}
        active="profil"
        center={true}>
        <div className="w-full">
          <UserProfile param={id} />
        </div>
      </ProfileLayout>
    </>
  );
};

Profile.getLayout = (page) => {
  return <Header title="profile">{page}</Header>;
};

export default Profile;
