import { useRouter } from "next/router";
import ProfileLayout from "/components/templates/ProfileLayout";
import OrderCard from "/components/profile/Order";
import Header from "/layouts/Header";

const HistoryOrder = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <ProfileLayout navbarActive="Pesanan" noNavigation={true}>
        <div className="flex divide-y-2 flex-col">
          <div className="w-full">
            <h1 className="font-bold text-xl capitalize"> daftar pesanan</h1>
          </div>
          <OrderCard param={id} />
        </div>
      </ProfileLayout>
    </>
  );
};

HistoryOrder.getLayout = (page) => {
  return <Header title="daftar pesanan">{page}</Header>;
};

export default HistoryOrder;
