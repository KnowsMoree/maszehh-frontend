import { useRouter } from "next/router";
import ProfileLayout from "/components/templates/ProfileLayout";
import EditProfileContent from "/components/profile/EditProfile";
import Header from "/layouts/Header";

const ProfileEdit = () => {
  const router = useRouter();
  const { id } = router.query;
  return (
    <>
      <ProfileLayout
        id=""
        center={true}
        active="profil">
        <div className="w-full flex items-center">
          <EditProfileContent param={id} />
        </div>
      </ProfileLayout>
    </>
  );
};

ProfileEdit.getLayout = (page) => {
  return <Header title="profile">{page}</Header>;
};

export default ProfileEdit;
