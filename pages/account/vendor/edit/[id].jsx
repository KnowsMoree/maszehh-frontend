import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import Header from "/layouts/Header";
import EditProfileVendor from "/components/vendor/EditProfileVendor";

const ProfileVendor = () => {
  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar=""
        nameActive=""
        title="Edit Profil"
        id=""
        user="vendor"
      >
        <EditProfileVendor />
      </DashboardLayout>
    </>
  );
};

ProfileVendor.getLayout = (page) => {
  return <Header title="edit profil">{page}</Header>;
};

export default ProfileVendor;
