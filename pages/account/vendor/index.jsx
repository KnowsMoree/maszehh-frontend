import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import Header from "/layouts/Header";
import Profile from "/components/vendor/Profile";
import { useRouter } from "next/router";

const ProfileVendor = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <DashboardLayout
        isUser="vendor"
        activeSidebar=""
        nameActive=""
        title="Profil vendor"
        id={id}
        user="vendor"
      >
        <Profile param={id} />
      </DashboardLayout>
    </>
  );
};

ProfileVendor.getLayout = (page) => {
  return <Header title="Profil">{page}</Header>;
};

export default ProfileVendor;
