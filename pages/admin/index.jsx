import Card from "/components/admin/Card";
import Header from "/layouts/Header";
import Table from "/components/admin/table/Table";
import RightBot from "/components/admin/RightBot";
import {
  HourglassTwoTone,
  ReconciliationOutlined,
  ShopOutlined,
  UserOutlined,
} from "@ant-design/icons";
import BestService from "/components/admin/BestService";
import Image from "next/image";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import { useEffect, useState } from "react";
import { parseJwt } from "../../helper/parseJwt";
import { dashboardRepository } from "../../repository/dashboard";
import { Empty } from "antd";
import Link from "next/link";

const requestList = [
  {
    img: "avatar.png",
    vendorName: "CV Bahagian Bersama",
    location: "Bekasi Selatan",
    desc: "Memiliki Pengalaman sejak 1990. CV Bahagia Bersama merupakan pelayanan konstruksi sadsadsa",
  },
  {
    img: "avatar.png",
    vendorName: "CV Bahagian Bersama",
    location: "Bekasi Selatan",
    desc: "Memiliki Pengalaman sejak 1990. CV Bahagia Bersama merupakan pelayanan konstruksi sadsadsa",
  },
  {
    img: "avatar.png",
    vendorName: "CV Bahagian Bersama",
    location: "Bekasi Selatan",
    desc: "Memiliki Pengalaman sejak 1990. CV Bahagia Bersama merupakan pelayanan konstruksi sadsadsa",
  },
  {
    img: "avatar.png",
    vendorName: "CV Bahagian Bersama",
    location: "Bekasi Selatan",
    desc: "Memiliki Pengalaman sejak 1990. CV Bahagia Bersama merupakan pelayanan konstruksi sadsadsa",
  },
];

const Dashboard = () => {
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);
  const [order, setOrder] = useState();
  const [sellingPackage, setSellingPackage] =
    useState([]);

  const [count, setCount] = useState();
  const [vendor, setVendor] = useState();

  const { data: countData } =
    dashboardRepository.hooks.useCount();

  const { data: orderPayment } =
    dashboardRepository.hooks.useOrderPayment(
      "waiting admin confirm"
    );

  const { data: pendingVendor } =
    dashboardRepository.hooks.usePendingVendor(
      "pending"
    );

  const { data: mostSellingPackage } =
    dashboardRepository.hooks.useMostSellingPack();

  useEffect(() => {
    if (
      [
        countData,
        orderPayment,
        pendingVendor,
        mostSellingPackage,
      ] !== null &&
      [
        countData,
        orderPayment,
        pendingVendor,
        mostSellingPackage,
      ] !== undefined
    ) {
      setCount(countData?.data);
      setOrder(orderPayment?.items);
      setSellingPackage(mostSellingPackage);
      setVendor(pendingVendor?.items);
      console.log(pendingVendor?.items);

      setTimeout(() => {
        setLoading(false);
      }, 1000);
    }
  }, [
    countData,
    orderPayment,
    pendingVendor,
    mostSellingPackage,
  ]);

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    const decodeJwt = parseJwt(token);

    setUser(decodeJwt);
  }, []);

  const card = [
    {
      icon: (
        <UserOutlined className="text-[60px] text-main group-hover:text-white mb-[15px]" />
      ),
      title: "Jumlah Pengguna",
      count: count?.countUser,
      href: "/admin/user",
    },
    {
      icon: (
        <ShopOutlined className="text-[60px] text-main group-hover:text-white mb-[15px]" />
      ),
      title: "Jumlah Vendor",
      count: count?.countVendor,
      href: "/admin/vendor",
    },
    {
      icon: (
        <ReconciliationOutlined className="text-[60px] text-main group-hover:text-white mb-[15px]" />
      ),
      title: "Jumlah Pesanan",
      count: count?.countOrder,
      href: "/admin/order",
    },
  ];

  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Dashboard"
        nameActive="Dashboard"
        title="Dashboard">
        {loading ? (
          <div className="w-full h-full flex-col gap-y-2 flex items-center justify-center px-[84px] pt-[100px] pb-[50px]">
            <HourglassTwoTone
              twoToneColor="#95A1AF"
              spin
              className="text-lg flex items-center"
            />
            <h2 className="text-sm m-0 text-text font-semibold">
              mohon tunggu...
            </h2>
          </div>
        ) : (
          <div className="w-full flex flex-wrap flex-col md:flex-nowrap md:flex-row gap-16">
            <div className="flex flex-col gap-9">
              <div className="flex flex-row gap-20">
                {card.map((v, i) => {
                  return (
                    <>
                      <div key={i}>
                        <Card
                          href={v.href}
                          icon={v.icon}
                          title={v.title}
                          count={v.count}
                        />
                      </div>
                    </>
                  );
                })}
              </div>
              <div className="w-full">
                <Table
                  title="Pembayaran Pesanan"
                  userLogin={user?.role}
                  dataAdmin={order}
                />
              </div>
            </div>
            <div className="w-full items-center flex flex-col gap-8">
              <BestService
                title="Layanan Telaris"
                data={sellingPackage}
              />
              <RightBot title="Permintaan Vendor">
                {vendor?.length == 0 ? (
                  <div className="w-full h-auto flex items-center justify-center">
                    <Empty
                      image={
                        Empty.PRESENTED_IMAGE_SIMPLE
                      }
                    />
                  </div>
                ) : (
                  vendor?.map((v, i) => {
                    return (
                      <>
                        <div
                          key={i}
                          className="w-full flex flex-row gap-3">
                          <div className="rounded-full flex items-center">
                            <img
                              src={v?.user?.photo}
                              height={50}
                              width={50}
                              alt=""
                              className="rounded-full"
                            />
                          </div>
                          <div className="flex flex-col">
                            <div className="w-full flex flex-col text-base">
                              <Link
                                href={`/admin/vendor/${v.id}`}>
                                <h4 className="truncate after:mx-2 font-semibold m-0 text-blue-500 hover:text-secondary duration-300 text-base cursor-pointer">
                                  {v?.name}
                                </h4>
                              </Link>
                              <h4 className="uppercase text-sm text-ellipsis m-0">
                                {v?.city?.name}
                              </h4>
                            </div>
                          </div>
                        </div>
                      </>
                    );
                  })
                )}
              </RightBot>
            </div>
          </div>
        )}
      </DashboardLayout>
    </>
  );
};

Dashboard.getLayout = (page) => {
  return (
    <Header title="dashboard admin">
      {page}
    </Header>
  );
};

export default Dashboard;
