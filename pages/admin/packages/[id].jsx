import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import Header from "/layouts/Header";
import DetailPackage from "/components/admin/package/DetailPackage";
import { useRouter } from "next/router";

const Packages = () => {
  const router = useRouter();

  const { id } = router.query;
  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Paket Layanan"
        nameActive="Paket Layanan"
        title="Detail Paket Layanan">
        <div className="w-full flex justify-center">
          <DetailPackage param={id} />
        </div>
      </DashboardLayout>
    </>
  );
};

Packages.getLayout = (page) => {
  return (
    <Header title="detail paket layanan">
      {page}
    </Header>
  );
};

export default Packages;
