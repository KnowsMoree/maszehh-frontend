import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import Header from "/layouts/Header";
import PackagesTable from "/components/admin/table/PackagesTable";

const Packages = () => {
  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Paket Layanan"
        nameActive="Paket Layanan"
        title="Paket Layanan">
        <PackagesTable />
      </DashboardLayout>
    </>
  );
};

Packages.getLayout = (page) => {
  return (
    <Header title="Kumpulan paket">{page}</Header>
  );
};

export default Packages;
