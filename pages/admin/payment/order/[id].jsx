import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import Header from "/layouts/Header";
import DetailPaymentOrder from "/components/admin/payment/DetailPaymentOrder";
import { useRouter } from "next/router";

const PaymentOrder = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Pembayaran Pesanan"
        nameActive="Pembayaran Pesanan"
        title="Detail Pembayaran Pesanan">
        <div className="w-full justify-center flex">
          <DetailPaymentOrder id={id} />
        </div>
      </DashboardLayout>
    </>
  );
};

PaymentOrder.getLayout = (page) => {
  return (
    <Header title="Detail Pembayaran pesanan">
      {page}
    </Header>
  );
};

export default PaymentOrder;
