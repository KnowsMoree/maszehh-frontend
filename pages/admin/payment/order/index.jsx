import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import Header from "/layouts/Header";
import PaymentOrderTable from "/components/admin/table/PaymentOrderTable";

const PaymentOrder = () => {
  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Pembayaran Pesanan"
        nameActive="Pembayaran Pesanan"
        title="Pembayaran Pesanan">
        <PaymentOrderTable />
      </DashboardLayout>
    </>
  );
};

PaymentOrder.getLayout = (page) => {
  return (
    <Header title="Pembayaran pesanan">
      {page}
    </Header>
  );
};

export default PaymentOrder;
