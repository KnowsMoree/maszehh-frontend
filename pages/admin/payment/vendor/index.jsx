import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import PaymentVendorTable from "/components/admin/table/PaymentVendorTable";
import Header from "/layouts/Header";

const PaymentVendor = () => {
  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Pembayaran Vendor"
        nameActive="Pembayaran Vendor"
        title="Pembayaran Vendor">
        <PaymentVendorTable />
      </DashboardLayout>
    </>
  );
};

PaymentVendor.getLayout = (page) => {
  return (
    <Header title="Pembayaran Vendor">
      {page}
    </Header>
  );
};

export default PaymentVendor;
