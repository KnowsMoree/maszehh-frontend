import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import CreatePaymentVendor from "/components/admin/payment/CreatePaymentVendor";
import Header from "/layouts/Header";

const PaymentVendorCreate = () => {
  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Pembayaran Vendor"
        nameActive="Pembayaran Vendor"
        title="Tambah Pembayaran Vendor">
        <div className="w-full">
          <CreatePaymentVendor />
        </div>
      </DashboardLayout>
    </>
  );
};

PaymentVendorCreate.getLayout = (page) => {
  return (
    <Header title="Tambah Pembayaran Vendor">
      {page}
    </Header>
  );
};

export default PaymentVendorCreate;
