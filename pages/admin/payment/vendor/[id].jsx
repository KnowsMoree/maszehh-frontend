import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import Header from "/layouts/Header";
import DetailPaymentVendor from "/components/admin/payment/DetailPaymentVendor";
import { useRouter } from "next/router";

const PaymentVendor = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Pembayaran Vendor"
        nameActive="Pembayaran Vendor"
        title="Detail Pembayaran Vendor">
        <div className="w-full">
          <DetailPaymentVendor param={id} />
        </div>
      </DashboardLayout>
    </>
  );
};

PaymentVendor.getLayout = (page) => {
  return (
    <Header title="detail pembayaran pesanan">
      {page}
    </Header>
  );
};

export default PaymentVendor;
