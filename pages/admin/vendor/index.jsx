import VendorTable from "/components/admin/table/VendorTable";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import Header from "/layouts/Header";

const Vendor = () => {
  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Vendor"
        nameActive="Vendor"
        title="Vendor">
        {/* disini table nya */}
        <VendorTable />
      </DashboardLayout>
    </>
  );
};

Vendor.getLayout = (page) => {
  return (
    <Header title="List Vendor">{page}</Header>
  );
};

export default Vendor;
