import Header from "/layouts/Header";
import { useRouter } from "next/router";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import DetailUser from "/components/admin/user/DetailUser";
import DetailVendor from "/components/admin/vendor/DetailVendor";

const UserDetail = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Vendor"
        nameActive="Vendor"
        title="Detail Vendor">
        <div className="w-full flex justify-center">
          <DetailVendor param={id} />
        </div>
      </DashboardLayout>
    </>
  );
};

UserDetail.getLayout = (page) => {
  return (
    <Header title={`profile`}>{page}</Header>
  );
};

export default UserDetail;
