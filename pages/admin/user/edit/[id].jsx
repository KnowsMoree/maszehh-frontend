import Header from "/layouts/Header";
import { useRouter } from "next/router";
import { userRepository } from "/repository/user";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import { useEffect, useState } from "react";
import EditUser from "/components/admin/user/EditUser";

const UserEdit = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Pengguna"
        nameActive="Pengguna"
        title="Edit Pengguna">
        <div className="w-full flex justify-center">
          <EditUser param={id} />
        </div>
      </DashboardLayout>
    </>
  );
};

UserEdit.getLayout = (page) => {
  return (
    <Header title={`edit profile`}>{page}</Header>
  );
};

export default UserEdit;
