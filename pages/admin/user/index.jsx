import Header from "/layouts/Header";
import UserTable from "/components/admin/table/UserTable";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";

const User = () => {
  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Pengguna"
        nameActive="Pengguna"
        title="Pengguna">
        <UserTable />
      </DashboardLayout>
    </>
  );
};

User.getLayout = (page) => {
  return (
    <Header title="List User">{page}</Header>
  );
};

export default User;
