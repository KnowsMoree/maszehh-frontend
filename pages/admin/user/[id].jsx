import Header from "/layouts/Header";
import { useRouter } from "next/router";
import { userRepository } from "/repository/user";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import { useEffect, useState } from "react";
import DetailUser from "/components/admin/user/DetailUser";

const UserDetail = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Pengguna"
        nameActive="Pengguna"
        title="Detail Pengguna">
        <div className="w-full flex justify-center">
          <DetailUser param={id} />
        </div>
      </DashboardLayout>
    </>
  );
};

UserDetail.getLayout = (page) => {
  return (
    <Header title={`profile`}>{page}</Header>
  );
};

export default UserDetail;
