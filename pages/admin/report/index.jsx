import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import ReportTable from "/components/admin/table/ReportTable";
import Header from "/layouts/Header";

const Report = () => {
  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Laporan pesanan"
        nameActive="Laporan pesanan"
        title="Laporan pesanan">
        <ReportTable />
      </DashboardLayout>
    </>
  );
};

Report.getLayout = (page) => {
  return (
    <Header title="laporan pesanan">
      {page}
    </Header>
  );
};

export default Report;
