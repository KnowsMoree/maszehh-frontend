import Header from "/layouts/Header";
import BankForm from "/components/admin/table/Bank";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";

const Bank = () => {
  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Data Bank"
        nameActive="Data Bank"
        title="Data Bank">
        {/* disini table nya */}
        <div className="flex justify-center">
          <BankForm />
        </div>
      </DashboardLayout>
    </>
  );
};

Bank.getLayout = (page) => {
  return (
    <Header title="Bank admin">{page}</Header>
  );
};

export default Bank;
