import Header from "/layouts/Header";
import CategoryTable from "/components/admin/table/CategoryTable";
import DashboardLayout from "/components/templates/dashboard/DashboardLayout";

const Category = () => {
  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Kategori"
        nameActive="Kategori"
        title="Kategori">
        {/* disini table nya */}
        <div className="flex justify-center">
          <CategoryTable />
        </div>
      </DashboardLayout>
    </>
  );
};

Category.getLayout = (page) => {
  return <Header title="kategori">{page}</Header>;
};

export default Category;
