import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import Header from "/layouts/Header";
import OrderTable from "/components/admin/table/OrderTable";

const Order = () => {
  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Pesanan"
        nameActive="Pesanan"
        title="Pesanan">
        <OrderTable />
      </DashboardLayout>
    </>
  );
};

Order.getLayout = (page) => {
  return <Header title="Pesanan">{page}</Header>;
};

export default Order;
