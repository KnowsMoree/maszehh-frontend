import DashboardLayout from "/components/templates/dashboard/DashboardLayout";
import Header from "/layouts/Header";
import DetailOrder from "/components/admin/order/DetailOrder";
import { useRouter } from "next/router";

const PaymentOrder = () => {
  const router = useRouter();

  const { id } = router.query;

  return (
    <>
      <DashboardLayout
        isUser="admin"
        activeSidebar="Pesanan"
        nameActive="Pesanan"
        title="Detail Pesanan">
        <div className="w-full ">
          <DetailOrder param={id} />
        </div>
      </DashboardLayout>
    </>
  );
};

PaymentOrder.getLayout = (page) => {
  return (
    <Header title="detail pesanan">{page}</Header>
  );
};

export default PaymentOrder;
