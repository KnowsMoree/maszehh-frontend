import Header from "/layouts/Header";
import Navbar from "/components/templates/Navbar";
import PackHero from "/components/package/PackHero";
import Footer from "/components/templates/Footer";
import { parseJwt } from "/helper/parseJwt";
import { useState, useEffect } from "react";
import { userRepository } from "../../repository/user";

const Package = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [user, setUser] = useState({});

  const [id, setId] = useState("");

  const { data: dataDetailUser } = userRepository.hooks.useUserDetail(id);

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    const decodeJwt = parseJwt(token);
    if (decodeJwt) {
      setId(decodeJwt.id);
      setUser(dataDetailUser?.data);
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }, [dataDetailUser]);

  return (
    <>
      <Navbar user_login={isLoggedIn} user={user} active="Paket" />
      <div className="flex flex-col top-[100px]">
        <PackHero />
      </div>
      <div>
        <Footer />
      </div>
    </>
  );
};

Package.getLayout = (page) => {
  return <Header title={"package"}>{page}</Header>;
};

export default Package;
