import Header from "/layouts/Header";
import Navbar from "/components/templates/Navbar";
import ReceiptPackageContent from "/components/package/ReceiptPackageContent";
import ShortFooter from "/components/templates/ShortFooter";
import { parseJwt } from "/helper/parseJwt";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";

const ReceiptPackage = () => {
  const router = useRouter();
  const { id } = router.query;

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [user, setUser] = useState({});

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    const decodeJwt = parseJwt(token);
    if (decodeJwt) {
      setUser(decodeJwt);
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }, []);

  return (
    <>
      <Navbar user_login={isLoggedIn} user={user} active="Paket" />
      <div className="w-full flex flex-col justify-center relative top-[108px]">
        <div className="flex px-[84px] flex-col pt-4 gap-y-4 h-[calc(100vh-132px-108px)] items-center">
          <ReceiptPackageContent id={id} />
        </div>
        <ShortFooter />
      </div>
    </>
  );
};

ReceiptPackage.getLayout = (page) => {
  return <Header title={"Pesanan anda"}>{page}</Header>;
};

export default ReceiptPackage;
