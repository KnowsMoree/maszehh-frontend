import Image from "next/image";
import ShortFooter from "/components/templates/ShortFooter";
import Header from "/layouts/Header";
import Img from "/public/image/pack.png";
import IsInput from "/components/input/Input";
import { useRouter } from "next/router";
import {
  DatePicker,
  Form,
  Spin,
  Input,
  Select,
  Button,
  message,
  Upload,
  Row,
  Col,
  InputNumber,
} from "antd";
import { addDays } from "../../../helper/addDays";
import { packageRepository } from "/repository/package";
import { useEffect, useLayoutEffect, useState } from "react";
import {
  CheckOutlined,
  CloseOutlined,
  LoadingOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import { bankRepository } from "../../../repository/bank";
import { useForm } from "antd/lib/form/Form";
import { parseJwt } from "/helper/parseJwt";
import { orderRepository } from "../../../repository/order";
import moment from "moment";
import { appConfig } from "../../../config/app";
import Navbar from "/components/templates/Navbar";
import { userRepository } from "../../../repository/user";
import Link from "next/link";

moment().format();

const Checkout = () => {
  const [checkout, setCheckout] = useState(null);
  const [loading, setLoading] = useState(true);
  const [priceTotal, setPriceTotal] = useState();
  const [grandTotal, setGrandTotal] = useState(null);
  const [paymentType, setPaymentType] = useState("tipe pembayaran");
  const [bank, setBank] = useState([]);
  const [role, setRole] = useState("admin");

  const [fileList, setFileList] = useState();
  const [rangeDate, setRangeDate] = useState(0);
  const { Option } = Select;
  const { TextArea } = Input;
  const { RangePicker } = DatePicker;
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const [form] = useForm();
  const router = useRouter();
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [user, setUser] = useState({});
  const [id, setId] = useState("");

  const { data: dataDetailUser } = userRepository.hooks.useUserDetail(id);

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    const decodeJwt = parseJwt(token);

    if (decodeJwt) {
      setId(decodeJwt.id);
      setUser(dataDetailUser?.data);

      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }, [dataDetailUser]);

  const { packId, qty, note } = router.query;

  const { data: dataCheckoutPack } =
    packageRepository.hooks.usePackageDetail(packId);

  const { data: dataBankAdmin } = bankRepository.hooks.useAllBank(role);

  useLayoutEffect(() => {
    if (dataCheckoutPack !== null && dataCheckoutPack !== undefined) {
      setRole("admin");
      setCheckout(dataCheckoutPack.data);
      setBank(dataBankAdmin?.data);
      setPriceTotal(dataCheckoutPack.data.price * qty);
      setLoading(false);
      setFileList([
        {
          url: appConfig.apiUrl + "/order/photo/design/arsitek.jpg",
          name: "order",
        },
      ]);
    }
  }, [dataCheckoutPack, qty, dataBankAdmin]);

  const loadingIcon = (
    <LoadingOutlined
      style={{
        fontSize: 45,
      }}
      spin
    />
  );

  const formatRupiah = (money) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
    }).format(money);
  };

  const handleFinish = async (values) => {
    try {
      if (startDate < moment().format("YYYY-MM-DD")) {
        message.error("Masukan tanggal dengan benar");
      } else {
        let orderDetail = [];
        const packDetail = {
          pack: packId,
          startDate: startDate,
          endDate: endDate,
          handymanQty: qty,
          notes: note,
          design: fileList[0].url,
        };

        //push order detail
        orderDetail.push(packDetail);

        const data = {
          paymentType: form.getFieldValue("paymentType"),
          locationService: form.getFieldValue("locationService"),
          serviceArea: form.getFieldValue("serviceArea"),
          adminAccount: form.getFieldValue("adminAccount"),
          orderDetail: orderDetail,
        };
        const result = await orderRepository.manipulateData.createOrder(data);
        const orderId = result.body.data.id;
        setTimeout(() => {
          router.push(`/package/checkout/receipt/${orderId}`);
          message.success("Transaksi berhasil dibuat");
        }, 1000);
      }
    } catch (error) {
      message.error(error.response.body.message[0]);
    }
  };

  const countDays = (startDate, endDate) => {
    const start = moment(startDate, "YYYY-MM-DD");
    const end = moment(endDate, "YYYY-MM-DD");
    const date = moment.duration(end.diff(start)).asDays();
    return date + 1;
  };

  const handleChangeDate = (_, dateString) => {
    setRangeDate(countDays(dateString[0].toString(), dateString[1].toString()));
    setStartDate(dateString[0].toString());
    setEndDate(dateString[1].toString());
  };

  useEffect(() => {
    setGrandTotal(priceTotal * rangeDate);
  }, [priceTotal, rangeDate]);

  const handleImage = async (args) => {
    const file = args.file;
    try {
      const processUpload =
        await orderRepository.manipulateData.uploadDesignOrder(file);
      setFileList([
        {
          url:
            appConfig.apiUrl +
            "/order/photo/design/" +
            processUpload.body.design,
          name: "design",
        },
      ]);
      message.success("upload berhasil");
    } catch (e) {
      console.log(e);
      message.error("upload gagal");
    }
  };

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div
        style={{
          marginTop: 8,
        }}
      >
        Upload
      </div>
    </div>
  );

  return (
    <>
      <Navbar user_login={isLoggedIn} user={user} active="Paket" />
      <div className="w-full flex flex-col justify-center relative top-[108px]">
        {loading ? (
          <div className="w-full h-[calc(100vh-115px)] flex items-center justify-center">
            <Spin indicator={loadingIcon} />
          </div>
        ) : (
          <>
            <div className="flex px-[84px] flex-col pt-9 gap-y-4 h-[calc(100vh-132px-108px)] items-center">
              <div className="w-full flex justify-center">
                <h1 className="text-2xl font-bold m-0">
                  Checkout <span className="truncate">{checkout.name}</span>
                </h1>
              </div>

              <Form
                form={form}
                size="large"
                className="w-full justify-center"
                layout="vertical"
                onFinish={handleFinish}
              >
                <Row
                  gutter={24}
                  className="m-0"
                  style={{
                    margin: "0px",
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                    gap: "38px",
                  }}
                >
                  <Col
                    span={10}
                    style={{
                      margin: "0px",
                      display: "flex",
                      flexDirection: "column",
                      gap: "20px",
                      padding: "20px 18px",
                      background: "white",
                      borderRadius: "10px",
                      filter:
                        "drop-shadow(0 20px 13px rgb(0 0 0 / 0.03)) drop-shadow(0 8px 5px rgb(0 0 0 / 0.08))",
                    }}
                  >
                    <Row
                      span={24}
                      gutter={24}
                      style={{
                        margin: "0px",
                        width: "100%",
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                      }}
                    >
                      <Col
                        span={11}
                        style={{
                          padding: "0px",
                          width: "100%",
                          display: "flex",
                          flexDirection: "column",
                          gap: "20px 0px",
                        }}
                      >
                        <Row
                          span={24}
                          style={{
                            width: "100%",
                          }}
                        >
                          <Col span={24}>
                            <Form.Item
                              name={"serviceArea"}
                              label={
                                <label className="m-0 text-base font-semibold capitalize">
                                  luas rumah {"(meter persegi)"}
                                </label>
                              }
                              style={{
                                margin: "0px",
                              }}
                              rules={[
                                {
                                  required: true,
                                  message: "masukan luas rumah",
                                },
                              ]}
                            >
                              <InputNumber
                                min={0}
                                defaultValue={0}
                                placeholder="*luas satuan meter"
                                className="w-full font-medium text-base"
                              />
                            </Form.Item>
                          </Col>
                        </Row>
                        <Row
                          span={24}
                          style={{
                            width: "100%",
                          }}
                        >
                          <Col span={24}>
                            <Form.Item
                              name={"locationService"}
                              label={
                                <label className="m-0 text-base font-semibold capitalize">
                                  alamat layanan
                                </label>
                              }
                              style={{
                                margin: "0px",
                              }}
                              rules={[
                                {
                                  required: true,
                                  message: "masukan alamat layanan",
                                },
                              ]}
                            >
                              <TextArea
                                type="text"
                                rows={3}
                                placeholder="masukan alamat"
                                className="w-full font-medium resize-none text-base"
                                // className="w-full text-base hover:drop-shadow-md outline-none border-none rounded-lg bg-bg py-3 px-2"
                              />
                            </Form.Item>
                          </Col>
                        </Row>
                        <Row
                          span={24}
                          style={{
                            width: "100%",
                          }}
                        >
                          <Col span={24}>
                            <Form.Item
                              name={"photo"}
                              label={
                                <label className="m-0 text-base font-semibold capitalize">
                                  Foto Perancangan
                                </label>
                              }
                            >
                              <div className="w-full rounded-lg h-full py-8 bg-bg">
                                <Upload
                                  name="photo"
                                  listType="picture-card"
                                  className="avatar-uploader flex justify-center"
                                  onChange={(args) => handleImage(args)}
                                  fileList={fileList}
                                  beforeUpload={() => false}
                                  maxCount={1}
                                >
                                  {uploadButton}
                                </Upload>
                              </div>
                            </Form.Item>
                          </Col>
                        </Row>
                      </Col>
                      <Col
                        span={11}
                        style={{
                          padding: "0px",
                          width: "100%",
                          display: "flex",
                          flexDirection: "column",
                          gap: "20px 0px",
                        }}
                      >
                        <Row
                          span={24}
                          style={{
                            width: "100%",
                          }}
                        >
                          <Col span={24}>
                            <Form.Item
                              name={"date"}
                              label={
                                <label className="m-0 text-base font-semibold capitalize">
                                  tanggal Pengerjaan
                                </label>
                              }
                              style={{
                                margin: "0px",
                              }}
                              rules={[
                                {
                                  required: true,
                                  message: "masukan tanggal pengerjaan",
                                },
                              ]}
                            >
                              <RangePicker
                                onChange={handleChangeDate}
                                placeholder={[
                                  "tanggal mulai",
                                  "tanggal selesai",
                                ]}
                                className="w-full focus:outline-none focus:ring-0 focus:border-none peer-focus:ring-main/50 flex items-center text-text bg-transparent"
                              />
                            </Form.Item>
                          </Col>
                        </Row>
                        <Row
                          span={24}
                          style={{
                            width: "100%",
                          }}
                        >
                          <Col span={24}>
                            <Form.Item
                              name={"paymentType"}
                              style={{
                                margin: "0px",
                              }}
                              label={
                                <label className="m-0 text-base font-semibold capitalize">
                                  tipe pembayaran
                                </label>
                              }
                              rules={[
                                {
                                  required: true,
                                  message: "pilih tipe pembayaran",
                                },
                              ]}
                            >
                              {rangeDate <= 3 ? (
                                <Select
                                  placeholder="tipe pembayaran"
                                  className="w-full text-base shadow-sm bg-bg font-medium"
                                  onChange={(e) => setPaymentType(e)}
                                >
                                  <Option
                                    value="fullpayment"
                                    className="capitalize"
                                  >
                                    pembayaran penuh
                                  </Option>
                                </Select>
                              ) : rangeDate >= 3 && rangeDate <= 7 ? (
                                <Select
                                  placeholder="tipe pembayaran"
                                  className="w-full text-base shadow-sm bg-bg font-medium"
                                  onChange={(e) => setPaymentType(e)}
                                >
                                  <Option
                                    value="fullpayment"
                                    className="capitalize"
                                  >
                                    pembayaran penuh
                                  </Option>
                                  <Option
                                    value="dp_lastpayment"
                                    className="capitalize"
                                  >
                                    dp + pembayaran akhir
                                  </Option>
                                </Select>
                              ) : (
                                <Select
                                  placeholder="tipe pembayaran"
                                  className="w-full text-base shadow-sm bg-bg font-medium"
                                  onChange={(e) => setPaymentType(e)}
                                >
                                  <Option
                                    value="fullpayment"
                                    className="capitalize"
                                  >
                                    pembayaran penuh
                                  </Option>
                                  <Option
                                    value="dp_lastpayment"
                                    className="capitalize"
                                  >
                                    dp + pembayaran akhir
                                  </Option>
                                  <Option
                                    value="dp_payment1_payment2"
                                    className="capitalize"
                                  >
                                    dp + dua kali pembayaran
                                  </Option>
                                </Select>
                              )}
                            </Form.Item>
                          </Col>
                        </Row>
                        <Row
                          span={24}
                          style={{
                            width: "100%",
                          }}
                        >
                          <Col span={24}>
                            <Form.Item
                              name={"adminAccount"}
                              style={{
                                margin: "0px",
                              }}
                              label={
                                <label className="m-0 text-base font-semibold capitalize">
                                  Pembayaran melalui
                                </label>
                              }
                              rules={[
                                {
                                  required: true,
                                  message:
                                    "pilih bank untuk melakukan pembayaran",
                                },
                              ]}
                            >
                              <Select
                                id="bank"
                                placeholder="pilih bank"
                                className="w-full text-base rounded-md shadow-sm bg-bg font-medium"
                              >
                                {bank?.map((v) => {
                                  return (
                                    <>
                                      <Option
                                        key={v.bank_id}
                                        className="border-0 text-sm font-medium"
                                        value={v.bank_id}
                                      >
                                        {v.bank_bank_name}
                                      </Option>
                                    </>
                                  );
                                })}
                              </Select>
                            </Form.Item>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                    <Row
                      style={{
                        margin: "0px",
                        width: "100%",
                      }}
                    >
                      <Col
                        span={24}
                        style={{
                          padding: "0px",
                          width: "100%",
                        }}
                      >
                        <h1 className="m-0 text-base font-semibold text-text">
                          Catatan: <span>{note}</span>
                        </h1>
                      </Col>
                    </Row>
                  </Col>
                  <Col
                    span={4}
                    className="gutter-row break-words"
                    style={{
                      padding: "20px 15px",
                      background: "white",
                      display: "flex",
                      flexDirection: "column",
                      gap: "10px 0",
                      borderRadius: "10px",
                      height: "fit-content",
                      filter:
                        "drop-shadow(0 20px 13px rgb(0 0 0 / 0.03)) drop-shadow(0 8px 5px rgb(0 0 0 / 0.08))",
                    }}
                  >
                    <Row>
                      <Col
                        style={{
                          marginBottom: "17px",
                        }}
                        span={24}
                      >
                        <h1 className="m-0 text-lg font-bold capitalize ">
                          Ringkasan penjualan
                        </h1>
                      </Col>
                      <Col
                        span={24}
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          gap: "5px",
                        }}
                      >
                        <Row>
                          <Col
                            span={24}
                            style={{
                              display: "flex",
                              flexDirection: "row",
                            }}
                          >
                            <Col span={12}>
                              <h1 className="m-0 text-base font-normal text-text">
                                Jumlah Tukang
                              </h1>
                            </Col>
                            <Col
                              span={12}
                              style={{
                                justifyContent: "end",
                                display: "flex",
                              }}
                            >
                              <h1 className="m-0 text-base font-bold text-black">
                                {qty}
                              </h1>
                            </Col>
                          </Col>
                        </Row>
                        <Row>
                          <Col
                            span={24}
                            style={{
                              display: "flex",
                              flexDirection: "row",
                            }}
                          >
                            <Col span={12}>
                              <h1 className="m-0 text-base font-normal text-text">
                                Tipe pembayaran
                              </h1>
                            </Col>
                            <Col
                              span={12}
                              style={{
                                justifyContent: "end",
                                display: "flex",
                              }}
                            >
                              <h1 className="m-0 text-right text-base font-bold break-words text-black">
                                {paymentType === "dp_payment1_payment2"
                                  ? "dp + 2 pembayaran"
                                  : paymentType === "dp_lastpayment"
                                  ? "dp + 1 pembayaran"
                                  : "pembayaran penuh"}
                              </h1>
                            </Col>
                          </Col>
                        </Row>
                        <Row>
                          <Col
                            span={24}
                            style={{
                              display: "flex",
                              flexDirection: "row",
                            }}
                          >
                            <Col span={12}>
                              <h1 className="m-0 text-base font-normal text-text">
                                harga 1 tukang
                              </h1>
                            </Col>
                            <Col
                              span={12}
                              style={{
                                justifyContent: "end",
                                display: "flex",
                              }}
                            >
                              <h1 className="m-0 text-base font-bold text-black">
                                {formatRupiah(checkout.price)}
                              </h1>
                            </Col>
                          </Col>
                        </Row>
                        <Row>
                          <Col
                            span={24}
                            style={{
                              display: rangeDate ? "flex" : "none",
                              flexDirection: "row",
                            }}
                          >
                            <Col span={12}>
                              <h1 className="m-0 text-base font-normal text-text">
                                Jumlah hari
                              </h1>
                            </Col>
                            <Col
                              span={12}
                              style={{
                                justifyContent: "end",
                                display: "flex",
                              }}
                            >
                              <h1 className="m-0 capitalize text-base font-bold text-black">
                                {`${rangeDate} hari`}
                              </h1>
                            </Col>
                          </Col>
                        </Row>
                        <Row>
                          <Col span={24}>
                            <hr className="my-3" />
                          </Col>
                        </Row>
                      </Col>
                      <Col>
                        <Col
                          span={24}
                          style={{
                            display: "flex",
                            width: "100%",
                          }}
                        >
                          <Col
                            span={24}
                            style={{
                              width: "100%",
                              display: "flex",
                              flexDirection: "column",
                            }}
                          >
                            <h1 className="m-0 text-base font-normal text-text">
                              total tagihan
                            </h1>
                            <h1 className="m-0 text-lg font-bold text-black">
                              {grandTotal
                                ? formatRupiah(grandTotal)
                                : formatRupiah(priceTotal)}
                            </h1>
                          </Col>
                        </Col>
                      </Col>
                    </Row>
                    <Button
                      type="primary"
                      htmlType="submit"
                      icon={<CheckOutlined />}
                      size="large"
                      className="text-base w-full justify-center font-semibold cursor-pointer group hover:drop-shadow-md hover:bg-secondary transition-colors bg-blue-500 py-[11px] px-[21px] rounded-lg border-none flex items-center"
                    >
                      Pesan
                    </Button>
                  </Col>
                </Row>
              </Form>
            </div>
          </>
        )}
        <ShortFooter />
      </div>
    </>
  );
};

Checkout.getLayout = (page) => {
  return <Header title="checkout">{page}</Header>;
};

export default Checkout;
