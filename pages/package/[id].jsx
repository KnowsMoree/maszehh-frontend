import Footer from "/components/templates/Footer";
import DetailPackChild from "/components/package/DetailPack";
import Navbar from "/components/templates/Navbar";
import Header from "/layouts/Header";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { parseJwt } from "/helper/parseJwt";

const DetailPack = () => {
  const router = useRouter();
  const { id } = router.query;
  const [isLoggedIn, setIsLoggedIn] =
    useState(false);
  const [user, setUser] = useState({});

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    const decodeJwt = parseJwt(token);
    if (decodeJwt) {
      setUser(decodeJwt);
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }, []);

  return (
    <>
      <Navbar
        user_login={isLoggedIn}
        user={user}
        active="Paket"
      />
      <div className=" flex-col relative top-[103px] mb-9">
        <div className="flex justify-center">
          <DetailPackChild param={id} />
        </div>
        <Footer />
      </div>
    </>
  );
};

DetailPack.getLayout = (page) => {
  return (
    <Header title="detail paket">{page}</Header>
  );
};

export default DetailPack;
