import "../styles/globals.css";
import "antd/dist/antd.css";
import "tailwindcss/tailwind.css";
import "../styles/index.css";
import { StoreProvider } from "../components/StoreProvider";
import { useEffect } from "react";
import { parseJwt } from "../helper/parseJwt";
import { TokenUtil } from "../utils/token";
import { getCentrifugeInstance } from "../services/centrifuge";
import { Button, notification } from "antd";
import {
  CheckCircleOutlined,
  CloseOutlined,
} from "@ant-design/icons";
import { Router, useRouter } from "next/router";
import Link from "next/link";

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    let token;
    let userId;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    if (token) {
      const decodeJwt = parseJwt(token);
      userId = decodeJwt.id;
    }

    if (token && userId) {
      init(userId);
    }
  });

  const init = (userId) => {
    const instance = getCentrifugeInstance();
    console.log(
      `notification_${userId}`,
      "id user for notif"
    );

    instance.subscribe(
      `notification_${userId}`,
      function (ctx) {
        console.log(ctx);
        console.log(
          "response send notif client",
          ctx?.data?.type == "Disabled Status"
        );

        if (
          ctx?.data?.type == "Disabled Status" ||
          ctx?.data?.type === "Blocked Account"
        ) {
          TokenUtil.clearAccessToken();
          TokenUtil.clearRefreshToken();
          router.push("/login");
        }

        openNotification("topRight", ctx);
      }
    );
  };

  const openNotification = (placement, data) => {
    console.log(data, "ada");
    const key = "buka";

    if (
      data?.data?.type == "Pembayaran Ditolak" ||
      data?.data?.type == "Pesanan Ditahan" ||
      data?.data?.type == "Tahan Pesanan" ||
      data?.data?.type ==
        "Pembayaran Vendor Ditolak"
    ) {
      notification.info({
        message: data?.data?.type,
        description: data?.data?.message,
        placement,
        maxCount: 1,
        icon: (
          <CloseOutlined className="text-red-500" />
        ),
        duration: 5,
        key,
      });
    } else {
      notification.info({
        message: data?.data?.type,
        description: data?.data?.message,
        placement,
        maxCount: 1,
        duration: 5,
        icon: (
          <CheckCircleOutlined className="text-green-500" />
        ),
        key,
      });
    }
  };
  // Use the layout defined at the page level, if available
  const getLayout =
    Component.getLayout || ((page) => page);

  return getLayout(
    <StoreProvider {...pageProps}>
      <Component {...pageProps} />
    </StoreProvider>
  );
}

export default MyApp;
