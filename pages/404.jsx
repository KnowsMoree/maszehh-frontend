import { Button, Result } from "antd";
import Link from "next/link";
import Header from "../layouts/Header";

const Custom404 = () => {
  return (
    <>
      <section className="flex items-center h-screen p-16 bg-bg text-justBlack">
        <div className="container flex flex-col items-center justify-center px-5 mx-auto my-8">
          <div className="max-w-md text-center">
            <h2 className="mb-8 font-extrabold text-9xl text-justBlack">
              <span className="sr-only">
                Error
              </span>
              404
            </h2>
            <p className="text-2xl font-semibold md:text-3xl">
              maaf, tidak dapat mengakses halaman
              ini.
            </p>
            <p className="mt-4 mb-8 text-text">
              anda dapat kembali ke halaman
              beranda
            </p>
            <Link href={"/"}>
              <h2 className="cursor-pointer px-8 py-3 font-semibold rounded bg-main text-white">
                Kembali ke beranda
              </h2>
            </Link>
          </div>
        </div>
      </section>
    </>
  );
};

Custom404.getLayout = (page) => {
  return <Header title={"salah"}>{page}</Header>;
};

export default Custom404;
