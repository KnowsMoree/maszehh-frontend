const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: "media",
  theme: {
    fontFamily: {
      main: ["Lato", "sans-serif"],
    },
    extend: {
      fontFamily: {
        sans: [
          "Lato",
          ...defaultTheme.fontFamily.sans,
        ],
      },
      colors: {
        main: "#FF3B3F",
        secondary: "#FFBC25",
        bg: "#F9FAFC",
        redText: "#AD2A2D",
        justBlack: "#000",
        text: "#95A1AF",
        filled: "#FFD706",
        unfilled: "#dcdcdc",
        footer: "#361516",
      },
      backgroundImage: {
        login: "url('/public/login.png')",
      },
    },
  },
  plugins: [],
  important: "#tailwind-selector",
};
